set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2018.04.04'
,p_release=>'18.1.0.00.45'
,p_default_workspace_id=>23268441850672677
,p_default_application_id=>100
,p_default_owner=>'AXICCCDEV'
);
end;
/
 
prompt APPLICATION 100 - Coding Conventions Checker


prompt --application/delete_application
begin
wwv_flow_api.remove_flow(wwv_flow.g_flow_id);
end;
/
prompt --application/create_application
begin
wwv_flow_api.create_flow(
 p_id=>wwv_flow.g_flow_id
,p_display_id=>nvl(wwv_flow_application_install.get_application_id,100)
,p_owner=>nvl(wwv_flow_application_install.get_schema,'AXICCCDEV')
,p_name=>nvl(wwv_flow_application_install.get_application_name,'Coding Conventions Checker')
,p_alias=>nvl(wwv_flow_application_install.get_application_alias,'100')
,p_page_view_logging=>'YES'
,p_page_protection_enabled_y_n=>'Y'
,p_checksum_salt=>'5B89A01D259DA377351935233D2564B7C6E829EBB3435217808095F91A79FE9B'
,p_bookmark_checksum_function=>'SH512'
,p_compatibility_mode=>'5.1'
,p_flow_language=>'af'
,p_flow_language_derived_from=>'SESSION'
,p_allow_feedback_yn=>'Y'
,p_date_format=>'DD-MON-YYYY'
,p_date_time_format=>'DD-MON-YYYY'
,p_timestamp_format=>'DD-MON-YYYY'
,p_timestamp_tz_format=>'DD-MON-YYYY'
,p_direction_right_to_left=>'N'
,p_flow_image_prefix => nvl(wwv_flow_application_install.get_image_prefix,'')
,p_documentation_banner=>'Application created from create application wizard 2021.05.03.'
,p_authentication=>'PLUGIN'
,p_authentication_id=>wwv_flow_api.id(28910012309135218)
,p_application_tab_set=>1
,p_logo_image=>'TEXT:Coding Conventions Checker'
,p_app_builder_icon_name=>'app-icon.svg'
,p_public_user=>'APEX_PUBLIC_USER'
,p_proxy_server=>nvl(wwv_flow_application_install.get_proxy,'')
,p_no_proxy_domains=>nvl(wwv_flow_application_install.get_no_proxy_domains,'')
,p_flow_version=>'Release 1.0'
,p_flow_status=>'AVAILABLE_W_EDIT_LINK'
,p_exact_substitutions_only=>'Y'
,p_browser_cache=>'N'
,p_browser_frame=>'D'
,p_rejoin_existing_sessions=>'N'
,p_csv_encoding=>'Y'
,p_auto_time_zone=>'N'
,p_substitution_string_01=>'APP_NAME'
,p_substitution_value_01=>'Coding Conventions Checker'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210810145736'
,p_file_prefix => nvl(wwv_flow_application_install.get_static_app_file_prefix,'')
,p_files_version=>3
,p_ui_type_name => null
);
end;
/
prompt --application/shared_components/navigation/lists/desktop_navigation_menu
begin
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(28910865468135213)
,p_name=>'Desktop Navigation Menu'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(29041044846135029)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'Home'
,p_list_item_link_target=>'f?p=&APP_ID.:1:&APP_SESSION.::&DEBUG.:'
,p_list_item_icon=>'fa-home'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(29073829068826705)
,p_list_item_display_sequence=>30
,p_list_item_link_text=>'Manage Apex Versions'
,p_list_item_link_target=>'f?p=&APP_ID.:210:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'fa-apex'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'210,211'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(34023202441587092)
,p_list_item_display_sequence=>50
,p_list_item_link_text=>'Manage Rules'
,p_list_item_link_target=>'f?p=&APP_ID.:200:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'fa-check'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'200,201'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(42489430525122786)
,p_list_item_display_sequence=>55
,p_list_item_link_text=>'Manage profiles'
,p_list_item_link_target=>'f?p=&APP_ID.:204:&SESSION.::&DEBUG.::::'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'204,205'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(29090647822816867)
,p_list_item_display_sequence=>60
,p_list_item_link_text=>'Manage Projects'
,p_list_item_link_target=>'f?p=&APP_ID.:214:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'fa-folders'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'214,215'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(34671044905337180)
,p_list_item_display_sequence=>70
,p_list_item_link_text=>'Results'
,p_list_item_link_target=>'f?p=&APP_ID.:300:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'fa-tasks'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'300'
);
end;
/
prompt --application/shared_components/navigation/lists/desktop_navigation_bar
begin
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(29030637967135107)
,p_name=>'Desktop Navigation Bar'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(39873311672913714)
,p_list_item_display_sequence=>5
,p_list_item_link_text=>'&APP_PROJECT_NAME. - Switch project'
,p_list_item_link_target=>'f?p=&APP_ID.:220:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'fa-refresh'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(29042693211135020)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'&APP_USER.'
,p_list_item_link_target=>'#'
,p_list_item_icon=>'fa-user'
,p_list_text_02=>'has-username'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(29043177762135019)
,p_list_item_display_sequence=>20
,p_list_item_link_text=>'---'
,p_list_item_link_target=>'separator'
,p_parent_list_item_id=>wwv_flow_api.id(29042693211135020)
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(29043507695135019)
,p_list_item_display_sequence=>30
,p_list_item_link_text=>'Sign Out'
,p_list_item_link_target=>'&LOGOUT_URL.'
,p_list_item_icon=>'fa-sign-out'
,p_parent_list_item_id=>wwv_flow_api.id(29042693211135020)
,p_list_item_current_type=>'TARGET_PAGE'
);
end;
/
prompt --application/shared_components/files/app_icon_svg
begin
wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
wwv_flow_api.g_varchar2_table(1) := '3C73766720786D6C6E733D22687474703A2F2F7777772E77332E6F72672F323030302F7376672220786D6C6E733A786C696E6B3D22687474703A2F2F7777772E77332E6F72672F313939392F786C696E6B222076696577426F783D223020302036342036';
wwv_flow_api.g_varchar2_table(2) := '34223E3C646566733E3C7374796C653E2E636C732D317B66696C6C3A75726C282372616469616C2D6772616469656E74293B7D2E636C732D327B6F7061636974793A302E313B7D2E636C732D332C2E636C732D347B66696C6C3A236666663B7D2E636C73';
wwv_flow_api.g_varchar2_table(3) := '2D337B6F7061636974793A302E363B7D3C2F7374796C653E3C72616469616C4772616469656E742069643D2272616469616C2D6772616469656E74222063783D223332222063793D222E30352220723D22363422206772616469656E74556E6974733D22';
wwv_flow_api.g_varchar2_table(4) := '7573657253706163654F6E557365223E3C73746F70206F66667365743D2230222073746F702D636F6C6F723D2223666666222073746F702D6F7061636974793D22302E3135222F3E3C73746F70206F66667365743D222E35222073746F702D636F6C6F72';
wwv_flow_api.g_varchar2_table(5) := '3D2223666666222073746F702D6F7061636974793D22302E31222F3E3C73746F70206F66667365743D2231222073746F702D636F6C6F723D2223666666222073746F702D6F7061636974793D2230222F3E3C2F72616469616C4772616469656E743E3C73';
wwv_flow_api.g_varchar2_table(6) := '796D626F6C2069643D22616D6269656E742D6C69676874696E67222076696577426F783D22302030203634203634223E3C7061746820636C6173733D22636C732D312220643D224D302030683634763634682D36347A222F3E3C2F73796D626F6C3E3C2F';
wwv_flow_api.g_varchar2_table(7) := '646566733E3C7469746C653E6261722D6C696E652D63686172743C2F7469746C653E3C726563742077696474683D22363422206865696768743D223634222066696C6C3D2223333039464442222F3E3C672069643D2269636F6E73223E3C706174682063';
wwv_flow_api.g_varchar2_table(8) := '6C6173733D22636C732D322220643D224D313920343668357631682D357A4D323620343668357631682D357A4D333320343668357631682D357A4D343020343668357631682D357A222F3E3C7061746820636C6173733D22636C732D332220643D224D31';
wwv_flow_api.g_varchar2_table(9) := '3920333868357638682D357A4D32362033326835763134682D357A4D33332033326835763134682D357A4D34302032376835763139682D357A222F3E3C6720636C6173733D22636C732D32223E3C636972636C652063783D2234322E35222063793D2232';
wwv_flow_api.g_varchar2_table(10) := '302E352220723D22312E35222F3E3C636972636C652063783D2233352E35222063793D2232352E352220723D22312E35222F3E3C636972636C652063783D2232382E35222063793D2232352E352220723D22312E35222F3E3C636972636C652063783D22';
wwv_flow_api.g_varchar2_table(11) := '32312E35222063793D2233312E352220723D22312E35222F3E3C7061746820643D224D32312E3832352033312E3837396C2D2E36352D2E37353820372E31342D362E31323168372E3032356C362E3836392D342E3930372E3538322E3831342D372E3133';
wwv_flow_api.g_varchar2_table(12) := '3120352E303933682D362E3937356C2D362E383620352E3837397A222F3E3C2F673E3C636972636C6520636C6173733D22636C732D34222063783D2234322E35222063793D2231392E352220723D22312E35222F3E3C636972636C6520636C6173733D22';
wwv_flow_api.g_varchar2_table(13) := '636C732D34222063783D2233352E35222063793D2232342E352220723D22312E35222F3E3C636972636C6520636C6173733D22636C732D34222063783D2232382E35222063793D2232342E352220723D22312E35222F3E3C636972636C6520636C617373';
wwv_flow_api.g_varchar2_table(14) := '3D22636C732D34222063783D2232312E35222063793D2233302E352220723D22312E35222F3E3C7061746820636C6173733D22636C732D342220643D224D32312E3832352033302E3837396C2D2E36352D2E37353820372E31342D362E31323168372E30';
wwv_flow_api.g_varchar2_table(15) := '32356C362E3836392D342E3930372E3538322E3831342D372E31333120352E303933682D362E3937356C2D362E383620352E3837397A222F3E3C2F673E3C7573652077696474683D22363422206865696768743D2236342220786C696E6B3A687265663D';
wwv_flow_api.g_varchar2_table(16) := '2223616D6269656E742D6C69676874696E67222069643D226C69676874696E67222F3E3C2F7376673E';
wwv_flow_api.create_app_static_file(
 p_id=>wwv_flow_api.id(29032015151135077)
,p_file_name=>'app-icon.svg'
,p_mime_type=>'image/svg+xml'
,p_file_charset=>'utf-8'
,p_file_content => wwv_flow_api.varchar2_to_blob(wwv_flow_api.g_varchar2_table)
);
end;
/
prompt --application/shared_components/files/app_icon_css
begin
wwv_flow_api.g_varchar2_table := wwv_flow_api.empty_varchar2_table;
wwv_flow_api.g_varchar2_table(1) := '2E6170702D69636F6E207B0A202020206261636B67726F756E642D696D6167653A2075726C286170702D69636F6E2E737667293B0A202020206261636B67726F756E642D7265706561743A206E6F2D7265706561743B0A202020206261636B67726F756E';
wwv_flow_api.g_varchar2_table(2) := '642D73697A653A20636F7665723B0A202020206261636B67726F756E642D706F736974696F6E3A203530253B0A202020206261636B67726F756E642D636F6C6F723A20233330394644423B0A7D';
wwv_flow_api.create_app_static_file(
 p_id=>wwv_flow_api.id(29032382125135075)
,p_file_name=>'app-icon.css'
,p_mime_type=>'text/css'
,p_file_charset=>'utf-8'
,p_file_content => wwv_flow_api.varchar2_to_blob(wwv_flow_api.g_varchar2_table)
);
end;
/
prompt --application/plugin_settings
begin
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(28907926427135222)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_RICH_TEXT_EDITOR'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(28908298599135221)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_IR'
,p_attribute_01=>'IG'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(28908532405135221)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_DISPLAY_SELECTOR'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(28908883148135221)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_YES_NO'
,p_attribute_01=>'Y'
,p_attribute_03=>'N'
,p_attribute_05=>'SWITCH'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(28909144604135221)
,p_plugin_type=>'ITEM TYPE'
,p_plugin=>'NATIVE_COLOR_PICKER'
,p_attribute_01=>'modern'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(28909477438135221)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_IG'
);
wwv_flow_api.create_plugin_setting(
 p_id=>wwv_flow_api.id(28909725370135221)
,p_plugin_type=>'REGION TYPE'
,p_plugin=>'NATIVE_CSS_CALENDAR'
);
end;
/
prompt --application/shared_components/security/authorizations/administration_rights
begin
wwv_flow_api.create_security_scheme(
 p_id=>wwv_flow_api.id(29033676742135075)
,p_name=>'Administration Rights'
,p_scheme_type=>'NATIVE_FUNCTION_BODY'
,p_attribute_01=>'return true;'
,p_error_message=>'Insufficient privileges, user is not an Administrator'
,p_caching=>'BY_USER_BY_PAGE_VIEW'
);
end;
/
prompt --application/shared_components/navigation/navigation_bar
begin
null;
end;
/
prompt --application/shared_components/logic/application_processes
begin
wwv_flow_api.create_flow_process(
 p_id=>wwv_flow_api.id(40062561943021734)
,p_process_sequence=>1
,p_process_point=>'ON_NEW_INSTANCE'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'After login'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'-- If the session is started as an APEX developer session, use the "fake" language',
'if wwv_flow.g_edit_cookie_session_id is not null then',
'    apex_util.set_session_lang(p_lang => ''af'');',
'else',
'    apex_util.set_session_lang(p_lang => ''en'');',
'end if;'))
);
end;
/
prompt --application/shared_components/logic/application_items
begin
wwv_flow_api.create_flow_item(
 p_id=>wwv_flow_api.id(39874015689898120)
,p_name=>'APP_PROJECT'
,p_protection_level=>'I'
);
wwv_flow_api.create_flow_item(
 p_id=>wwv_flow_api.id(40214219859638878)
,p_name=>'APP_PROJECT_NAME'
,p_protection_level=>'I'
);
end;
/
prompt --application/shared_components/logic/application_computations
begin
wwv_flow_api.create_flow_computation(
 p_id=>wwv_flow_api.id(40214425537634817)
,p_computation_sequence=>10
,p_computation_item=>'APP_PROJECT_NAME'
,p_computation_point=>'BEFORE_HEADER'
,p_computation_type=>'QUERY'
,p_computation_processed=>'REPLACE_EXISTING'
,p_computation=>'select dedescr from drproje where deproid = :APP_PROJECT'
);
end;
/
prompt --application/shared_components/logic/application_settings
begin
null;
end;
/
prompt --application/shared_components/navigation/tabs/standard
begin
null;
end;
/
prompt --application/shared_components/navigation/tabs/parent
begin
null;
end;
/
prompt --application/shared_components/user_interface/lovs/apexversions
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(29140382425652695)
,p_lov_name=>'APEXVERSIONS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select     deapeid, deapexv',
'from       drapexv',
'order by   deapexv'))
);
end;
/
prompt --application/shared_components/user_interface/lovs/gradaties
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(29140166001654578)
,p_lov_name=>'GRADATIES'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select     degraid, dedescr',
'from       drgrada',
'order by   dedescr'))
);
end;
/
prompt --application/shared_components/user_interface/lovs/link_types
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(29140859789635664)
,p_lov_name=>'LINK_TYPES'
,p_lov_query=>'.'||wwv_flow_api.id(29140859789635664)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(29141109022635663)
,p_lov_disp_sequence=>1
,p_lov_disp_value=>'Application'
,p_lov_return_value=>'APPLICATION'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(29141593847635663)
,p_lov_disp_sequence=>2
,p_lov_disp_value=>'Page'
,p_lov_return_value=>'PAGE'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(29141909724635663)
,p_lov_disp_sequence=>3
,p_lov_disp_value=>'Region'
,p_lov_return_value=>'REGION'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(29142388155635663)
,p_lov_disp_sequence=>4
,p_lov_disp_value=>'Page Item'
,p_lov_return_value=>'PAGE_ITEM'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(29142719240635663)
,p_lov_disp_sequence=>5
,p_lov_disp_value=>'Application Item'
,p_lov_return_value=>'APP_ITEM'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(29143113925635663)
,p_lov_disp_sequence=>6
,p_lov_disp_value=>'Button'
,p_lov_return_value=>'BUTTON'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(29143569016635663)
,p_lov_disp_sequence=>7
,p_lov_disp_value=>'List'
,p_lov_return_value=>'LIST'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(29143966865635663)
,p_lov_disp_sequence=>8
,p_lov_disp_value=>'List Entry'
,p_lov_return_value=>'LISTENTRY'
);
end;
/
prompt --application/shared_components/user_interface/lovs/login_remember_username
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(29036286403135048)
,p_lov_name=>'LOGIN_REMEMBER_USERNAME'
,p_lov_query=>'.'||wwv_flow_api.id(29036286403135048)||'.'
);
wwv_flow_api.create_static_lov_data(
 p_id=>wwv_flow_api.id(29036669966135047)
,p_lov_disp_sequence=>10
,p_lov_disp_value=>'Remember username'
,p_lov_return_value=>'Y'
);
end;
/
prompt --application/shared_components/user_interface/lovs/projects
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(29140520783650477)
,p_lov_name=>'PROJECTS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select     deproid, dedescr',
'from       drproje',
'order by   deproid'))
);
end;
/
prompt --application/shared_components/user_interface/lovs/types
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(29140744167648633)
,p_lov_name=>'TYPES'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select     detypid, dedescr',
'from       drrtype',
'order by   dedescr'))
);
end;
/
prompt --application/pages/page_groups
begin
wwv_flow_api.create_page_group(
 p_id=>wwv_flow_api.id(29034270961135069)
,p_group_name=>'Administration'
);
end;
/
prompt --application/shared_components/navigation/breadcrumbs/breadcrumb
begin
wwv_flow_api.create_menu(
 p_id=>wwv_flow_api.id(28910393807135217)
,p_name=>'Breadcrumb'
);
wwv_flow_api.create_menu_option(
 p_id=>wwv_flow_api.id(28910541499135217)
,p_short_name=>'Home'
,p_link=>'f?p=&APP_ID.:1:&APP_SESSION.::&DEBUG.'
,p_page_id=>1
);
end;
/
prompt --application/shared_components/user_interface/templates/page/master_detail
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(28911168797135199)
,p_theme_id=>42
,p_name=>'Marquee'
,p_internal_name=>'MASTER_DETAIL'
,p_is_popup=>false
,p_javascript_file_urls=>'#IMAGE_PREFIX#libraries/apex/#MIN_DIRECTORY#widget.stickyTableHeader#MIN#.js?v=#APEX_VERSION#'
,p_javascript_code_onload=>'apex.theme42.initializePage.masterDetail();'
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!DOCTYPE html>',
'<html class="no-js #RTL_CLASS# page-&APP_PAGE_ID. app-&APP_ALIAS." lang="&BROWSER_LANGUAGE." #TEXT_DIRECTION#>',
'<head>',
'  <meta http-equiv="x-ua-compatible" content="IE=edge" />',
'  <meta charset="utf-8">',
'  <title>#TITLE#</title>',
'  #APEX_CSS#',
'  #THEME_CSS#',
'  #TEMPLATE_CSS#',
'  #THEME_STYLE_CSS#',
'  #APPLICATION_CSS#',
'  #PAGE_CSS#',
'  #FAVICONS#',
'  #HEAD#',
'  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>',
'</head>',
'<body class="t-PageBody t-PageBody--masterDetail t-PageBody--hideLeft no-anim #PAGE_CSS_CLASSES#" #TEXT_DIRECTION# #ONLOAD# id="t_PageBody">',
'#FORM_OPEN#',
'<header class="t-Header" id="t_Header">',
'  #REGION_POSITION_07#',
'  <div class="t-Header-branding">',
'    <div class="t-Header-controls">',
'      <button class="t-Button t-Button--icon t-Button--header t-Button--headerTree" title="#EXPAND_COLLAPSE_NAV_LABEL#" id="t_Button_navControl" type="button"><span class="t-Icon fa fa-bars" aria-hidden="true"></span></button>',
'    </div>',
'    <div class="t-Header-logo">',
'      <a href="#HOME_LINK#" class="t-Header-logo-link">#LOGO#</a>',
'    </div>',
'    <div class="t-Header-navBar">',
'      #NAVIGATION_BAR#',
'    </div>',
'  </div>',
'  <div class="t-Header-nav">',
'    #TOP_GLOBAL_NAVIGATION_LIST#',
'    #REGION_POSITION_06#',
'  </div>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body">',
'#SIDE_GLOBAL_NAVIGATION_LIST#',
'  <div class="t-Body-main">',
'    <div class="t-Body-title" id="t_Body_title">',
'      #REGION_POSITION_01#',
'    </div>',
'    <div class="t-Body-content" id="t_Body_content">',
'      #SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'      <div class="t-Body-info" id="t_Body_info">',
'        #REGION_POSITION_02#',
'      </div>',
'      <div class="t-Body-contentInner">',
'        #BODY#',
'      </div>',
'      <footer class="t-Footer">',
'        <div class="t-Footer-body">',
'          <div class="t-Footer-content">#REGION_POSITION_05#</div>',
'          <div class="t-Footer-apex">',
'            <div class="t-Footer-version">#APP_VERSION#</div>  ',
'            <div class="t-Footer-customize">#CUSTOMIZE#</div>',
'            #BUILT_WITH_LOVE_USING_APEX#',
'          </div>',
'        </div>',
'        <div class="t-Footer-top">',
'          <a href="#top" class="t-Footer-topButton" id="t_Footer_topButton"><span class="a-Icon icon-up-chevron"></span></a>',
'        </div>',
'      </footer>',
'    </div>',
'  </div>',
'  <div class="t-Body-actions" id="t_Body_actions">',
'    <button class="t-Button t-Button--icon t-Button--header t-Button--headerRight" title="#EXPAND_COLLAPSE_SIDE_COL_LABEL#" id="t_Button_rightControlButton" type="button"><span class="t-Icon fa fa-bars" aria-hidden="true"></span></button>',
'    <div class="t-Body-actionsContent">',
'    #REGION_POSITION_03#',
'    </div>',
'  </div>',
'</div>',
'<div class="t-Body-inlineDialogs">',
'  #REGION_POSITION_04#',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#APEX_JAVASCRIPT#',
'#GENERATED_CSS#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#  ',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Success" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-header">',
'          <h2 class="t-Alert-title">#SUCCESS_MESSAGE#</h2>',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Success'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--warning t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Notification" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-body">',
'          #MESSAGE#',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Notification'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_navigation_bar=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<ul class="t-NavigationBar" data-mode="classic">',
'  <li class="t-NavigationBar-item">',
'    <span class="t-Button t-Button--icon t-Button--noUI t-Button--header t-Button--navBar t-Button--headerUser">',
'        <span class="t-Icon a-Icon icon-user"></span>',
'        <span class="t-Button-label">&APP_USER.</span>',
'    </span>',
'  </li>#BAR_BODY#',
'</ul>'))
,p_navbar_entry=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-NavigationBar-item">',
'  <a class="t-Button t-Button--icon t-Button--header t-Button--navBar" href="#LINK#">',
'      <span class="t-Icon #IMAGE#"></span>',
'      <span class="t-Button-label">#TEXT#</span>',
'  </a>',
'</li>'))
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_03'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>17
,p_error_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Alert t-Alert--danger t-Alert--wizard t-Alert--defaultIcons">',
'  <div class="t-Alert-wrap">',
'    <div class="t-Alert-icon">',
'      <span class="t-Icon"></span>',
'    </div>',
'    <div class="t-Alert-content">',
'      <div class="t-Alert-body">',
'        <h3>#MESSAGE#</h3>',
'        <p>#ADDITIONAL_INFO#</p>',
'        <div class="t-Alert-inset">#TECHNICAL_INFO#</div>',
'      </div>',
'    </div>',
'    <div class="t-Alert-buttons">',
'      <button onclick="#BACK_LINK#" class="t-Button t-Button--hot w50p t-Button--large" type="button">#OK#</button>',
'    </div>',
'  </div>',
'</div>'))
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>true
,p_grid_has_column_span=>true
,p_grid_always_emit=>true
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_default_label_col_span=>2
,p_grid_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="container">',
'#ROWS#',
'</div>'))
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="col col-#COLUMN_SPAN_NUMBER# #CSS_CLASSES#" #ATTRIBUTES#>',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_dialog_js_init_code=>'apex.navigation.dialog(#PAGE_URL#,{title:#TITLE#,height:#DIALOG_HEIGHT#,width:#DIALOG_WIDTH#,maxWidth:#DIALOG_MAX_WIDTH#,modal:#IS_MODAL#,dialog:#DIALOG#,#DIALOG_ATTRIBUTES#},#DIALOG_CSS_CLASSES#,#TRIGGERING_ELEMENT#);'
,p_dialog_js_close_code=>'apex.navigation.dialog.close(#IS_MODAL#,#TARGET#);'
,p_dialog_js_cancel_code=>'apex.navigation.dialog.cancel(#IS_MODAL#);'
,p_dialog_browser_frame=>'MODAL'
,p_reference_id=>1996914646461572319
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28911495413135188)
,p_page_template_id=>wwv_flow_api.id(28911168797135199)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>8
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28911720594135188)
,p_page_template_id=>wwv_flow_api.id(28911168797135199)
,p_name=>'Breadcrumb Bar'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28912062520135188)
,p_page_template_id=>wwv_flow_api.id(28911168797135199)
,p_name=>'Master Detail'
,p_placeholder=>'REGION_POSITION_02'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>8
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28912346274135188)
,p_page_template_id=>wwv_flow_api.id(28911168797135199)
,p_name=>'Right Side Column'
,p_placeholder=>'REGION_POSITION_03'
,p_has_grid_support=>false
,p_glv_new_row=>false
,p_max_fixed_grid_columns=>4
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28912652976135188)
,p_page_template_id=>wwv_flow_api.id(28911168797135199)
,p_name=>'Inline Dialogs'
,p_placeholder=>'REGION_POSITION_04'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28912951852135188)
,p_page_template_id=>wwv_flow_api.id(28911168797135199)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>8
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28913233186135188)
,p_page_template_id=>wwv_flow_api.id(28911168797135199)
,p_name=>'Page Navigation'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28913512415135188)
,p_page_template_id=>wwv_flow_api.id(28911168797135199)
,p_name=>'Page Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28913800029135188)
,p_page_template_id=>wwv_flow_api.id(28911168797135199)
,p_name=>'Before Navigation Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/page/modal_dialog
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(28914246562135185)
,p_theme_id=>42
,p_name=>'Modal Dialog'
,p_internal_name=>'MODAL_DIALOG'
,p_is_popup=>true
,p_javascript_code_onload=>'apex.theme42.initializePage.modalDialog();'
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!DOCTYPE html>',
'<html class="no-js #RTL_CLASS# page-&APP_PAGE_ID. app-&APP_ALIAS." lang="&BROWSER_LANGUAGE." #TEXT_DIRECTION#>',
'<head>',
'  <meta http-equiv="x-ua-compatible" content="IE=edge" />',
'  <meta charset="utf-8">',
'  <title>#TITLE#</title>',
'  #APEX_CSS#',
'  #THEME_CSS#',
'  #TEMPLATE_CSS#',
'  #THEME_STYLE_CSS#',
'  #APPLICATION_CSS#',
'  #PAGE_CSS#',
'  #FAVICONS#',
'  #HEAD#',
'  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>',
'</head>',
'<body class="t-Dialog-page t-Dialog-page--standard #DIALOG_CSS_CLASSES# #PAGE_CSS_CLASSES#" #TEXT_DIRECTION# #ONLOAD#>',
'#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Dialog" role="dialog" aria-label="#TITLE#">',
'  <div class="t-Dialog-header">#REGION_POSITION_01#</div>',
'  <div class="t-Dialog-bodyWrapperOut">',
'      <div class="t-Dialog-bodyWrapperIn"><div class="t-Dialog-body">',
'      #SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'      #BODY#',
'      </div></div>',
'  </div>',
'  <div class="t-Dialog-footer">#REGION_POSITION_03#</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#APEX_JAVASCRIPT#',
'#GENERATED_CSS#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#  ',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Success" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-header">',
'          <h2 class="t-Alert-title">#SUCCESS_MESSAGE#</h2>',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Success'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--warning t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Notification" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-body">',
'          #MESSAGE#',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Notification'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>3
,p_error_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Alert t-Alert--danger t-Alert--wizard t-Alert--defaultIcons">',
'  <div class="t-Alert-wrap">',
'    <div class="t-Alert-icon">',
'      <span class="t-Icon"></span>',
'    </div>',
'    <div class="t-Alert-content">',
'      <div class="t-Alert-body">',
'        <h3>#MESSAGE#</h3>',
'        <p>#ADDITIONAL_INFO#</p>',
'        <div class="t-Alert-inset">#TECHNICAL_INFO#</div>',
'      </div>',
'    </div>',
'    <div class="t-Alert-buttons">',
'      <button onclick="#BACK_LINK#" class="t-Button t-Button--hot w50p t-Button--large" type="button">#OK#</button>',
'    </div>',
'  </div>',
'</div>'))
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>true
,p_grid_has_column_span=>true
,p_grid_always_emit=>true
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_default_label_col_span=>2
,p_grid_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="container">',
'#ROWS#',
'</div>'))
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="col col-#COLUMN_SPAN_NUMBER# #CSS_CLASSES#" #ATTRIBUTES#>',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_dialog_js_init_code=>'apex.navigation.dialog(#PAGE_URL#,{title:#TITLE#,height:#DIALOG_HEIGHT#,width:#DIALOG_WIDTH#,maxWidth:#DIALOG_MAX_WIDTH#,modal:#IS_MODAL#,dialog:#DIALOG#,#DIALOG_ATTRIBUTES#},''t-Dialog-page--standard ''+#DIALOG_CSS_CLASSES#,#TRIGGERING_ELEMENT#);'
,p_dialog_js_close_code=>'apex.navigation.dialog.close(#IS_MODAL#,#TARGET#);'
,p_dialog_js_cancel_code=>'apex.navigation.dialog.cancel(#IS_MODAL#);'
,p_dialog_height=>'auto'
,p_dialog_width=>'720'
,p_dialog_max_width=>'960'
,p_dialog_browser_frame=>'MODAL'
,p_reference_id=>2098960803539086924
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28914587526135185)
,p_page_template_id=>wwv_flow_api.id(28914246562135185)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28914862777135185)
,p_page_template_id=>wwv_flow_api.id(28914246562135185)
,p_name=>'Dialog Header'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28915132913135185)
,p_page_template_id=>wwv_flow_api.id(28914246562135185)
,p_name=>'Dialog Footer'
,p_placeholder=>'REGION_POSITION_03'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/page/login
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(28915702668135184)
,p_theme_id=>42
,p_name=>'Login'
,p_internal_name=>'LOGIN'
,p_is_popup=>false
,p_javascript_code_onload=>'apex.theme42.initializePage.appLogin();'
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!DOCTYPE html>',
'<html class="no-js #RTL_CLASS# page-&APP_PAGE_ID. app-&APP_ALIAS." lang="&BROWSER_LANGUAGE." #TEXT_DIRECTION#>',
'<head>',
'  <meta http-equiv="x-ua-compatible" content="IE=edge" />',
'  <meta charset="utf-8">',
'  <title>#TITLE#</title>',
'  #APEX_CSS#',
'  #THEME_CSS#',
'  #TEMPLATE_CSS#',
'  #THEME_STYLE_CSS#',
'  #APPLICATION_CSS#',
'  #PAGE_CSS#',
'  #FAVICONS#',
'  #HEAD#',
'  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>',
'</head>',
'<body class="t-PageBody--login no-anim #PAGE_CSS_CLASSES#" #TEXT_DIRECTION# #ONLOAD#>',
'#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body">',
'  #REGION_POSITION_01#',
'  #SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'  <div class="t-Body-wrap">',
'    <div class="t-Body-col t-Body-col--main">',
'      <div class="t-Login-container">',
'      #BODY#',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#APEX_JAVASCRIPT#',
'#GENERATED_CSS#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#  ',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Success" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-header">',
'          <h2 class="t-Alert-title">#SUCCESS_MESSAGE#</h2>',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Success'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--warning t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Notification" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-body">',
'          #MESSAGE#',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Notification'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>6
,p_error_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Alert t-Alert--danger t-Alert--wizard t-Alert--defaultIcons">',
'  <div class="t-Alert-wrap">',
'    <div class="t-Alert-icon">',
'      <span class="t-Icon"></span>',
'    </div>',
'    <div class="t-Alert-content">',
'      <div class="t-Alert-body">',
'        <h3>#MESSAGE#</h3>',
'        <p>#ADDITIONAL_INFO#</p>',
'        <div class="t-Alert-inset">#TECHNICAL_INFO#</div>',
'      </div>',
'    </div>',
'    <div class="t-Alert-buttons">',
'      <button onclick="#BACK_LINK#" class="t-Button t-Button--hot w50p t-Button--large" type="button">#OK#</button>',
'    </div>',
'  </div>',
'</div>'))
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>true
,p_grid_has_column_span=>true
,p_grid_always_emit=>true
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_default_label_col_span=>2
,p_grid_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="container">',
'#ROWS#',
'</div>'))
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="col col-#COLUMN_SPAN_NUMBER# #CSS_CLASSES#" #ATTRIBUTES#>',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_dialog_js_init_code=>'apex.navigation.dialog(#PAGE_URL#,{title:#TITLE#,height:#DIALOG_HEIGHT#,width:#DIALOG_WIDTH#,maxWidth:#DIALOG_MAX_WIDTH#,modal:#IS_MODAL#,dialog:#DIALOG#,#DIALOG_ATTRIBUTES#},#DIALOG_CSS_CLASSES#,#TRIGGERING_ELEMENT#);'
,p_dialog_js_close_code=>'apex.navigation.dialog.close(#IS_MODAL#,#TARGET#);'
,p_dialog_js_cancel_code=>'apex.navigation.dialog.cancel(#IS_MODAL#);'
,p_dialog_browser_frame=>'MODAL'
,p_reference_id=>2099711150063350616
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28916052408135184)
,p_page_template_id=>wwv_flow_api.id(28915702668135184)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28916351890135184)
,p_page_template_id=>wwv_flow_api.id(28915702668135184)
,p_name=>'Body Header'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/page/wizard_modal_dialog
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(28916532187135184)
,p_theme_id=>42
,p_name=>'Wizard Modal Dialog'
,p_internal_name=>'WIZARD_MODAL_DIALOG'
,p_is_popup=>true
,p_javascript_code_onload=>'apex.theme42.initializePage.wizardModal();'
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!DOCTYPE html>',
'<html class="no-js #RTL_CLASS# page-&APP_PAGE_ID. app-&APP_ALIAS." lang="&BROWSER_LANGUAGE." #TEXT_DIRECTION#>',
'<head>',
'  <meta http-equiv="x-ua-compatible" content="IE=edge" />',
'  <meta charset="utf-8">',
'  <title>#TITLE#</title>',
'  #APEX_CSS#',
'  #THEME_CSS#',
'  #TEMPLATE_CSS#',
'  #THEME_STYLE_CSS#',
'  #APPLICATION_CSS#',
'  #PAGE_CSS#',
'  #FAVICONS#',
'  #HEAD#',
'  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>',
'</head>',
'<body class="t-Dialog-page t-Dialog-page--wizard #DIALOG_CSS_CLASSES# #PAGE_CSS_CLASSES#" #TEXT_DIRECTION# #ONLOAD#>',
'#FORM_OPEN#'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Dialog" role="dialog" aria-label="#TITLE#">',
'  <div class="t-Dialog-header">#REGION_POSITION_01#</div>',
'  <div class="t-Dialog-bodyWrapperOut">',
'      <div class="t-Dialog-bodyWrapperIn"><div class="t-Dialog-body">',
'      #SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'      #BODY#',
'      </div></div>',
'  </div>',
'  <div class="t-Dialog-footer">#REGION_POSITION_03#</div>',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#APEX_JAVASCRIPT#',
'#GENERATED_CSS#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#  ',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Success" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-header">',
'          <h2 class="t-Alert-title">#SUCCESS_MESSAGE#</h2>',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Success'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--warning t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Notification" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-body">',
'          #MESSAGE#',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Notification'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_theme_class_id=>3
,p_error_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Alert t-Alert--danger t-Alert--wizard t-Alert--defaultIcons">',
'  <div class="t-Alert-wrap">',
'    <div class="t-Alert-icon">',
'      <span class="t-Icon"></span>',
'    </div>',
'    <div class="t-Alert-content">',
'      <div class="t-Alert-body">',
'        <h3>#MESSAGE#</h3>',
'        <p>#ADDITIONAL_INFO#</p>',
'        <div class="t-Alert-inset">#TECHNICAL_INFO#</div>',
'      </div>',
'    </div>',
'    <div class="t-Alert-buttons">',
'      <button onclick="#BACK_LINK#" class="t-Button t-Button--hot w50p t-Button--large" type="button">#OK#</button>',
'    </div>',
'  </div>',
'</div>'))
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>true
,p_grid_has_column_span=>true
,p_grid_always_emit=>true
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_default_label_col_span=>2
,p_grid_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="container">',
'#ROWS#',
'</div>'))
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="col col-#COLUMN_SPAN_NUMBER# #CSS_CLASSES#" #ATTRIBUTES#>',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_dialog_js_init_code=>'apex.navigation.dialog(#PAGE_URL#,{title:#TITLE#,height:#DIALOG_HEIGHT#,width:#DIALOG_WIDTH#,maxWidth:#DIALOG_MAX_WIDTH#,modal:#IS_MODAL#,dialog:#DIALOG#,#DIALOG_ATTRIBUTES#},''t-Dialog-page--wizard ''+#DIALOG_CSS_CLASSES#,#TRIGGERING_ELEMENT#);'
,p_dialog_js_close_code=>'apex.navigation.dialog.close(#IS_MODAL#,#TARGET#);'
,p_dialog_js_cancel_code=>'apex.navigation.dialog.cancel(#IS_MODAL#);'
,p_dialog_height=>'auto'
,p_dialog_width=>'720'
,p_dialog_max_width=>'960'
,p_dialog_browser_frame=>'MODAL'
,p_reference_id=>2120348229686426515
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28916808575135184)
,p_page_template_id=>wwv_flow_api.id(28916532187135184)
,p_name=>'Wizard Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28917103842135184)
,p_page_template_id=>wwv_flow_api.id(28916532187135184)
,p_name=>'Wizard Progress Bar'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28917471424135184)
,p_page_template_id=>wwv_flow_api.id(28916532187135184)
,p_name=>'Wizard Buttons'
,p_placeholder=>'REGION_POSITION_03'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/page/left_side_column
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(28918014831135184)
,p_theme_id=>42
,p_name=>'Left Side Column'
,p_internal_name=>'LEFT_SIDE_COLUMN'
,p_is_popup=>false
,p_javascript_code_onload=>'apex.theme42.initializePage.leftSideCol();'
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!DOCTYPE html>',
'<html class="no-js #RTL_CLASS# page-&APP_PAGE_ID. app-&APP_ALIAS." lang="&BROWSER_LANGUAGE." #TEXT_DIRECTION#>',
'<head>',
'  <meta http-equiv="x-ua-compatible" content="IE=edge" />',
'  <meta charset="utf-8">',
'  <title>#TITLE#</title>',
'  #APEX_CSS#',
'  #THEME_CSS#',
'  #TEMPLATE_CSS#',
'  #THEME_STYLE_CSS#',
'  #APPLICATION_CSS#',
'  #PAGE_CSS#',
'  #FAVICONS#',
'  #HEAD#',
'  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>',
'</head>',
'<body class="t-PageBody t-PageBody--showLeft t-PageBody--hideActions no-anim #PAGE_CSS_CLASSES#" #TEXT_DIRECTION# #ONLOAD# id="t_PageBody">',
'#FORM_OPEN#',
'<header class="t-Header" id="t_Header">',
'  #REGION_POSITION_07#',
'  <div class="t-Header-branding">',
'    <div class="t-Header-controls">',
'      <button class="t-Button t-Button--icon t-Button--header t-Button--headerTree" title="#EXPAND_COLLAPSE_NAV_LABEL#" id="t_Button_navControl" type="button"><span class="t-Icon fa fa-bars" aria-hidden="true"></span></button>',
'    </div>',
'    <div class="t-Header-logo">',
'      <a href="#HOME_LINK#" class="t-Header-logo-link">#LOGO#</a>',
'    </div>',
'    <div class="t-Header-navBar">',
'      #NAVIGATION_BAR#',
'    </div>',
'  </div>',
'  <div class="t-Header-nav">',
'    #TOP_GLOBAL_NAVIGATION_LIST#',
'    #REGION_POSITION_06#',
'  </div>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body">',
'#SIDE_GLOBAL_NAVIGATION_LIST#',
'  <div class="t-Body-main">',
'    <div class="t-Body-title" id="t_Body_title">',
'      #REGION_POSITION_01#',
'    </div>',
'    <div class="t-Body-side" id="t_Body_side">',
'      #REGION_POSITION_02#',
'    </div>',
'    <div class="t-Body-content" id="t_Body_content">',
'      #SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'      <div class="t-Body-contentInner">',
'        #BODY#',
'      </div>',
'      <footer class="t-Footer">',
'        <div class="t-Footer-body">',
'          <div class="t-Footer-content">#REGION_POSITION_05#</div>',
'          <div class="t-Footer-apex">',
'            <div class="t-Footer-version">#APP_VERSION#</div>  ',
'            <div class="t-Footer-customize">#CUSTOMIZE#</div>',
'            #BUILT_WITH_LOVE_USING_APEX#',
'          </div>',
'        </div>',
'        <div class="t-Footer-top">',
'          <a href="#top" class="t-Footer-topButton" id="t_Footer_topButton"><span class="a-Icon icon-up-chevron"></span></a>',
'        </div>',
'      </footer>',
'    </div>',
'  </div>',
'</div>',
'<div class="t-Body-inlineDialogs">',
'  #REGION_POSITION_04#',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#APEX_JAVASCRIPT#',
'#GENERATED_CSS#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#  ',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Success" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-header">',
'          <h2 class="t-Alert-title">#SUCCESS_MESSAGE#</h2>',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Success'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--warning t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Notification" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-body">',
'          #MESSAGE#',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Notification'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_navigation_bar=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<ul class="t-NavigationBar" data-mode="classic">',
'  <li class="t-NavigationBar-item">',
'    <span class="t-Button t-Button--icon t-Button--noUI t-Button--header t-Button--navBar t-Button--headerUser">',
'        <span class="t-Icon a-Icon icon-user"></span>',
'        <span class="t-Button-label">&APP_USER.</span>',
'    </span>',
'  </li>#BAR_BODY#',
'</ul>'))
,p_navbar_entry=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-NavigationBar-item">',
'  <a class="t-Button t-Button--icon t-Button--header t-Button--navBar" href="#LINK#">',
'      <span class="t-Icon #IMAGE#"></span>',
'      <span class="t-Button-label">#TEXT#</span>',
'  </a>',
'</li>'))
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>17
,p_error_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Alert t-Alert--danger t-Alert--wizard t-Alert--defaultIcons">',
'  <div class="t-Alert-wrap">',
'    <div class="t-Alert-icon">',
'      <span class="t-Icon"></span>',
'    </div>',
'    <div class="t-Alert-content">',
'      <div class="t-Alert-body">',
'        <h3>#MESSAGE#</h3>',
'        <p>#ADDITIONAL_INFO#</p>',
'        <div class="t-Alert-inset">#TECHNICAL_INFO#</div>',
'      </div>',
'    </div>',
'    <div class="t-Alert-buttons">',
'      <button onclick="#BACK_LINK#" class="t-Button t-Button--hot w50p t-Button--large" type="button">#OK#</button>',
'    </div>',
'  </div>',
'</div>'))
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>true
,p_grid_has_column_span=>true
,p_grid_always_emit=>true
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_default_label_col_span=>2
,p_grid_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="container">',
'#ROWS#',
'</div>'))
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="col col-#COLUMN_SPAN_NUMBER# #CSS_CLASSES#" #ATTRIBUTES#>',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_dialog_js_init_code=>'apex.navigation.dialog(#PAGE_URL#,{title:#TITLE#,height:#DIALOG_HEIGHT#,width:#DIALOG_WIDTH#,maxWidth:#DIALOG_MAX_WIDTH#,modal:#IS_MODAL#,dialog:#DIALOG#,#DIALOG_ATTRIBUTES#},#DIALOG_CSS_CLASSES#,#TRIGGERING_ELEMENT#);'
,p_dialog_js_close_code=>'apex.navigation.dialog.close(#IS_MODAL#,#TARGET#);'
,p_dialog_js_cancel_code=>'apex.navigation.dialog.cancel(#IS_MODAL#);'
,p_dialog_browser_frame=>'MODAL'
,p_reference_id=>2525196570560608698
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28918396216135184)
,p_page_template_id=>wwv_flow_api.id(28918014831135184)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>false
,p_max_fixed_grid_columns=>8
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28918671445135184)
,p_page_template_id=>wwv_flow_api.id(28918014831135184)
,p_name=>'Breadcrumb Bar'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28918904661135184)
,p_page_template_id=>wwv_flow_api.id(28918014831135184)
,p_name=>'Left Column'
,p_placeholder=>'REGION_POSITION_02'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>4
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28919260679135184)
,p_page_template_id=>wwv_flow_api.id(28918014831135184)
,p_name=>'Inline Dialogs'
,p_placeholder=>'REGION_POSITION_04'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28919559952135183)
,p_page_template_id=>wwv_flow_api.id(28918014831135184)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>8
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28919847316135183)
,p_page_template_id=>wwv_flow_api.id(28918014831135184)
,p_name=>'Page Navigation'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28920156959135183)
,p_page_template_id=>wwv_flow_api.id(28918014831135184)
,p_name=>'Page Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28920432673135183)
,p_page_template_id=>wwv_flow_api.id(28918014831135184)
,p_name=>'Before Navigation Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/page/right_side_column
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(28920846815135183)
,p_theme_id=>42
,p_name=>'Right Side Column'
,p_internal_name=>'RIGHT_SIDE_COLUMN'
,p_is_popup=>false
,p_javascript_code_onload=>'apex.theme42.initializePage.rightSideCol();'
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!DOCTYPE html>',
'<html class="no-js #RTL_CLASS# page-&APP_PAGE_ID. app-&APP_ALIAS." lang="&BROWSER_LANGUAGE." #TEXT_DIRECTION#>',
'<head>',
'  <meta http-equiv="x-ua-compatible" content="IE=edge" />',
'  <meta charset="utf-8"> ',
'  <title>#TITLE#</title>',
'  #APEX_CSS#',
'  #THEME_CSS#',
'  #TEMPLATE_CSS#',
'  #THEME_STYLE_CSS#',
'  #APPLICATION_CSS#',
'  #PAGE_CSS#',
'  #FAVICONS#',
'  #HEAD#',
'  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>',
'</head>',
'<body class="t-PageBody t-PageBody--hideLeft no-anim #PAGE_CSS_CLASSES#" #TEXT_DIRECTION# #ONLOAD# id="t_PageBody">',
'#FORM_OPEN#',
'<header class="t-Header" id="t_Header">',
'  #REGION_POSITION_07#',
'  <div class="t-Header-branding">',
'    <div class="t-Header-controls">',
'      <button class="t-Button t-Button--icon t-Button--header t-Button--headerTree" title="#EXPAND_COLLAPSE_NAV_LABEL#" id="t_Button_navControl" type="button"><span class="t-Icon fa fa-bars" aria-hidden="true"></span></button>',
'    </div>',
'    <div class="t-Header-logo">',
'      <a href="#HOME_LINK#" class="t-Header-logo-link">#LOGO#</a>',
'    </div>',
'    <div class="t-Header-navBar">',
'      #NAVIGATION_BAR#',
'    </div>',
'  </div>',
'  <div class="t-Header-nav">',
'    #TOP_GLOBAL_NAVIGATION_LIST#',
'    #REGION_POSITION_06#',
'  </div>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body">',
'#SIDE_GLOBAL_NAVIGATION_LIST#',
'  <div class="t-Body-main">',
'    <div class="t-Body-title" id="t_Body_title">',
'      #REGION_POSITION_01#',
'    </div>',
'    <div class="t-Body-content" id="t_Body_content">',
'      #SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'      <div class="t-Body-contentInner">',
'        #BODY#',
'      </div>',
'      <footer class="t-Footer">',
'        <div class="t-Footer-body">',
'          <div class="t-Footer-content">#REGION_POSITION_05#</div>',
'          <div class="t-Footer-apex">',
'            <div class="t-Footer-version">#APP_VERSION#</div>  ',
'            <div class="t-Footer-customize">#CUSTOMIZE#</div>',
'            #BUILT_WITH_LOVE_USING_APEX#',
'          </div>',
'        </div>',
'        <div class="t-Footer-top">',
'          <a href="#top" class="t-Footer-topButton" id="t_Footer_topButton"><span class="a-Icon icon-up-chevron"></span></a>',
'        </div>',
'      </footer>',
'    </div>',
'  </div>',
'  <div class="t-Body-actions" id="t_Body_actions">',
'    <button class="t-Button t-Button--icon t-Button--header t-Button--headerRight" title="#EXPAND_COLLAPSE_SIDE_COL_LABEL#" id="t_Button_rightControlButton" type="button"><span class="t-Icon fa fa-bars" aria-hidden="true"></span></button>',
'    <div class="t-Body-actionsContent">',
'    #REGION_POSITION_03#',
'    </div>',
'  </div>',
'</div>',
'<div class="t-Body-inlineDialogs">',
'  #REGION_POSITION_04#',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#APEX_JAVASCRIPT#',
'#GENERATED_CSS#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#  ',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Success" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-header">',
'          <h2 class="t-Alert-title">#SUCCESS_MESSAGE#</h2>',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Success'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--warning t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Notification" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-body">',
'          #MESSAGE#',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Notification'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_navigation_bar=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<ul class="t-NavigationBar" data-mode="classic">',
'  <li class="t-NavigationBar-item">',
'    <span class="t-Button t-Button--icon t-Button--noUI t-Button--header t-Button--navBar t-Button--headerUser">',
'        <span class="t-Icon a-Icon icon-user"></span>',
'        <span class="t-Button-label">&APP_USER.</span>',
'    </span>',
'  </li>#BAR_BODY#',
'</ul>'))
,p_navbar_entry=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-NavigationBar-item">',
'  <a class="t-Button t-Button--icon t-Button--header t-Button--navBar" href="#LINK#">',
'      <span class="t-Icon #IMAGE#"></span>',
'      <span class="t-Button-label">#TEXT#</span>',
'  </a>',
'</li>'))
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_03'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>17
,p_error_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Alert t-Alert--danger t-Alert--wizard t-Alert--defaultIcons">',
'  <div class="t-Alert-wrap">',
'    <div class="t-Alert-icon">',
'      <span class="t-Icon"></span>',
'    </div>',
'    <div class="t-Alert-content">',
'      <div class="t-Alert-body">',
'        <h3>#MESSAGE#</h3>',
'        <p>#ADDITIONAL_INFO#</p>',
'        <div class="t-Alert-inset">#TECHNICAL_INFO#</div>',
'      </div>',
'    </div>',
'    <div class="t-Alert-buttons">',
'      <button onclick="#BACK_LINK#" class="t-Button t-Button--hot w50p t-Button--large" type="button">#OK#</button>',
'    </div>',
'  </div>',
'</div>'))
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>true
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_default_label_col_span=>2
,p_grid_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="container">',
'#ROWS#',
'</div>'))
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="col col-#COLUMN_SPAN_NUMBER# #CSS_CLASSES#" #ATTRIBUTES#>',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_dialog_js_init_code=>'apex.navigation.dialog(#PAGE_URL#,{title:#TITLE#,height:#DIALOG_HEIGHT#,width:#DIALOG_WIDTH#,maxWidth:#DIALOG_MAX_WIDTH#,modal:#IS_MODAL#,dialog:#DIALOG#,#DIALOG_ATTRIBUTES#},#DIALOG_CSS_CLASSES#,#TRIGGERING_ELEMENT#);'
,p_dialog_js_close_code=>'apex.navigation.dialog.close(#IS_MODAL#,#TARGET#);'
,p_dialog_js_cancel_code=>'apex.navigation.dialog.cancel(#IS_MODAL#);'
,p_dialog_browser_frame=>'MODAL'
,p_reference_id=>2525200116240651575
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28921180245135183)
,p_page_template_id=>wwv_flow_api.id(28920846815135183)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>8
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28921459906135183)
,p_page_template_id=>wwv_flow_api.id(28920846815135183)
,p_name=>'Breadcrumb Bar'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28921729190135183)
,p_page_template_id=>wwv_flow_api.id(28920846815135183)
,p_name=>'Right Column'
,p_placeholder=>'REGION_POSITION_03'
,p_has_grid_support=>false
,p_glv_new_row=>false
,p_max_fixed_grid_columns=>4
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28922091711135183)
,p_page_template_id=>wwv_flow_api.id(28920846815135183)
,p_name=>'Inline Dialogs'
,p_placeholder=>'REGION_POSITION_04'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28922326039135183)
,p_page_template_id=>wwv_flow_api.id(28920846815135183)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>8
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28922609991135183)
,p_page_template_id=>wwv_flow_api.id(28920846815135183)
,p_name=>'Page Navigation'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28922914930135183)
,p_page_template_id=>wwv_flow_api.id(28920846815135183)
,p_name=>'Page Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28923248231135182)
,p_page_template_id=>wwv_flow_api.id(28920846815135183)
,p_name=>'Before Navigation Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/page/left_and_right_side_columns
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(28923688046135182)
,p_theme_id=>42
,p_name=>'Left and Right Side Columns'
,p_internal_name=>'LEFT_AND_RIGHT_SIDE_COLUMNS'
,p_is_popup=>false
,p_javascript_code_onload=>'apex.theme42.initializePage.bothSideCols();'
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!DOCTYPE html>',
'<html class="no-js #RTL_CLASS# page-&APP_PAGE_ID. app-&APP_ALIAS." lang="&BROWSER_LANGUAGE." #TEXT_DIRECTION#>',
'<head>',
'  <meta http-equiv="x-ua-compatible" content="IE=edge" />',
'  <meta charset="utf-8">  ',
'  <title>#TITLE#</title>',
'  #APEX_CSS#',
'  #THEME_CSS#',
'  #TEMPLATE_CSS#',
'  #THEME_STYLE_CSS#',
'  #APPLICATION_CSS#',
'  #PAGE_CSS#',
'  #FAVICONS#',
'  #HEAD#',
'  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>',
'</head>',
'<body class="t-PageBody t-PageBody--showLeft no-anim #PAGE_CSS_CLASSES#" #TEXT_DIRECTION# #ONLOAD# id="t_PageBody">',
'#FORM_OPEN#',
'<header class="t-Header" id="t_Header">',
'  #REGION_POSITION_07#',
'  <div class="t-Header-branding">',
'    <div class="t-Header-controls">',
'      <button class="t-Button t-Button--icon t-Button--header t-Button--headerTree" title="#EXPAND_COLLAPSE_NAV_LABEL#" id="t_Button_navControl" type="button"><span class="t-Icon fa fa-bars" aria-hidden="true"></span></button>',
'    </div>',
'    <div class="t-Header-logo">',
'      <a href="#HOME_LINK#" class="t-Header-logo-link">#LOGO#</a>',
'    </div>',
'    <div class="t-Header-navBar">',
'      #NAVIGATION_BAR#',
'    </div>',
'  </div>',
'  <div class="t-Header-nav">',
'    #TOP_GLOBAL_NAVIGATION_LIST#',
'    #REGION_POSITION_06#',
'  </div>',
'</header>'))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body">',
'#SIDE_GLOBAL_NAVIGATION_LIST#',
'  <div class="t-Body-main">',
'    <div class="t-Body-title" id="t_Body_title">',
'      #REGION_POSITION_01#',
'    </div>',
'    <div class="t-Body-side" id="t_Body_side">',
'      #REGION_POSITION_02#',
'    </div>',
'    <div class="t-Body-content" id="t_Body_content">',
'      #SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'      <div class="t-Body-contentInner">',
'        #BODY#',
'      </div>',
'      <footer class="t-Footer">',
'        <div class="t-Footer-body">',
'          <div class="t-Footer-content">#REGION_POSITION_05#</div>',
'          <div class="t-Footer-apex">',
'            <div class="t-Footer-version">#APP_VERSION#</div>  ',
'            <div class="t-Footer-customize">#CUSTOMIZE#</div>',
'            #BUILT_WITH_LOVE_USING_APEX#',
'          </div>',
'        </div>',
'        <div class="t-Footer-top">',
'          <a href="#top" class="t-Footer-topButton" id="t_Footer_topButton"><span class="a-Icon icon-up-chevron"></span></a>',
'        </div>',
'      </footer>',
'    </div>',
'  </div>',
'  <div class="t-Body-actions" id="t_Body_actions">',
'    <button class="t-Button t-Button--icon t-Button--header t-Button--headerRight" title="#EXPAND_COLLAPSE_SIDE_COL_LABEL#" id="t_Button_rightControlButton" type="button"><span class="t-Icon fa fa-bars" aria-hidden="true"></span></button>',
'    <div class="t-Body-actionsContent">',
'    #REGION_POSITION_03#',
'    </div>',
'  </div>',
'</div>',
'<div class="t-Body-inlineDialogs">',
'  #REGION_POSITION_04#',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#APEX_JAVASCRIPT#',
'#GENERATED_CSS#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#  ',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>'))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Success" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-header">',
'          <h2 class="t-Alert-title">#SUCCESS_MESSAGE#</h2>',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Success'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--warning t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Notification" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-body">',
'          #MESSAGE#',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" onclick="apex.jQuery(''#t_Alert_Notification'').remove();" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_navigation_bar=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<ul class="t-NavigationBar" data-mode="classic">',
'  <li class="t-NavigationBar-item">',
'    <span class="t-Button t-Button--icon t-Button--noUI t-Button--header t-Button--navBar t-Button--headerUser">',
'        <span class="t-Icon a-Icon icon-user"></span>',
'        <span class="t-Button-label">&APP_USER.</span>',
'    </span>',
'  </li>#BAR_BODY#',
'</ul>'))
,p_navbar_entry=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-NavigationBar-item">',
'  <a class="t-Button t-Button--icon t-Button--header t-Button--navBar" href="#LINK#">',
'      <span class="t-Icon #IMAGE#"></span>',
'      <span class="t-Button-label">#TEXT#</span>',
'  </a>',
'</li>'))
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_sidebar_def_reg_pos=>'REGION_POSITION_03'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>17
,p_error_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Alert t-Alert--danger t-Alert--wizard t-Alert--defaultIcons">',
'  <div class="t-Alert-wrap">',
'    <div class="t-Alert-icon">',
'      <span class="t-Icon"></span>',
'    </div>',
'    <div class="t-Alert-content">',
'      <div class="t-Alert-body">',
'        <h3>#MESSAGE#</h3>',
'        <p>#ADDITIONAL_INFO#</p>',
'        <div class="t-Alert-inset">#TECHNICAL_INFO#</div>',
'      </div>',
'    </div>',
'    <div class="t-Alert-buttons">',
'      <button onclick="#BACK_LINK#" class="t-Button t-Button--hot w50p t-Button--large" type="button">#OK#</button>',
'    </div>',
'  </div>',
'</div>'))
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>true
,p_grid_has_column_span=>true
,p_grid_always_emit=>false
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_default_label_col_span=>2
,p_grid_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="container">',
'#ROWS#',
'</div>'))
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="col col-#COLUMN_SPAN_NUMBER# #CSS_CLASSES#" #ATTRIBUTES#>',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_dialog_js_init_code=>'apex.navigation.dialog(#PAGE_URL#,{title:#TITLE#,height:#DIALOG_HEIGHT#,width:#DIALOG_WIDTH#,maxWidth:#DIALOG_MAX_WIDTH#,modal:#IS_MODAL#,dialog:#DIALOG#,#DIALOG_ATTRIBUTES#},#DIALOG_CSS_CLASSES#,#TRIGGERING_ELEMENT#);'
,p_dialog_js_close_code=>'apex.navigation.dialog.close(#IS_MODAL#,#TARGET#);'
,p_dialog_js_cancel_code=>'apex.navigation.dialog.cancel(#IS_MODAL#);'
,p_dialog_browser_frame=>'MODAL'
,p_reference_id=>2525203692562657055
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28923993434135181)
,p_page_template_id=>wwv_flow_api.id(28923688046135182)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>false
,p_max_fixed_grid_columns=>6
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28924249224135181)
,p_page_template_id=>wwv_flow_api.id(28923688046135182)
,p_name=>'Breadcrumb Bar'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28924589320135181)
,p_page_template_id=>wwv_flow_api.id(28923688046135182)
,p_name=>'Left Column'
,p_placeholder=>'REGION_POSITION_02'
,p_has_grid_support=>false
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>3
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28924815171135181)
,p_page_template_id=>wwv_flow_api.id(28923688046135182)
,p_name=>'Right Column'
,p_placeholder=>'REGION_POSITION_03'
,p_has_grid_support=>false
,p_glv_new_row=>false
,p_max_fixed_grid_columns=>3
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28925125837135181)
,p_page_template_id=>wwv_flow_api.id(28923688046135182)
,p_name=>'Inline Dialogs'
,p_placeholder=>'REGION_POSITION_04'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28925453069135181)
,p_page_template_id=>wwv_flow_api.id(28923688046135182)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>6
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28925737379135181)
,p_page_template_id=>wwv_flow_api.id(28923688046135182)
,p_name=>'Page Navigation'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28926090193135181)
,p_page_template_id=>wwv_flow_api.id(28923688046135182)
,p_name=>'Page Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28926373054135181)
,p_page_template_id=>wwv_flow_api.id(28923688046135182)
,p_name=>'Before Navigation Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/page/minimal_no_navigation
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(28926764051135181)
,p_theme_id=>42
,p_name=>'Minimal (No Navigation)'
,p_internal_name=>'MINIMAL_NO_NAVIGATION'
,p_is_popup=>false
,p_javascript_code_onload=>'apex.theme42.initializePage.noSideCol();'
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!DOCTYPE html>',
'<html class="no-js #RTL_CLASS# page-&APP_PAGE_ID. app-&APP_ALIAS." lang="&BROWSER_LANGUAGE." #TEXT_DIRECTION#>',
'<head>',
'  <meta http-equiv="x-ua-compatible" content="IE=edge" />',
'  <meta charset="utf-8">',
'  <title>#TITLE#</title>',
'  #APEX_CSS#',
'  #THEME_CSS#',
'  #TEMPLATE_CSS#',
'  #THEME_STYLE_CSS#',
'  #APPLICATION_CSS#',
'  #PAGE_CSS#  ',
'  #FAVICONS#',
'  #HEAD#',
'  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>',
'</head>',
'<body class="t-PageBody t-PageBody--hideLeft t-PageBody--hideActions no-anim #PAGE_CSS_CLASSES# t-PageBody--noNav" #TEXT_DIRECTION# #ONLOAD# id="t_PageBody">',
'#FORM_OPEN#',
'<header class="t-Header" id="t_Header">',
'  #REGION_POSITION_07#',
'  <div class="t-Header-branding">',
'    <div class="t-Header-controls">',
'      <button class="t-Button t-Button--icon t-Button--header t-Button--headerTree" title="#EXPAND_COLLAPSE_NAV_LABEL#" id="t_Button_navControl" type="button"><span class="t-Icon fa fa-bars" aria-hidden="true"></span></button>',
'    </div>',
'    <div class="t-Header-logo">',
'      <a href="#HOME_LINK#" class="t-Header-logo-link">#LOGO#</a>',
'    </div>',
'    <div class="t-Header-navBar">',
'      #NAVIGATION_BAR#',
'    </div>',
'  </div>',
'</header>',
'    '))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body">',
'  <div class="t-Body-main">',
'      <div class="t-Body-title" id="t_Body_title">',
'        #REGION_POSITION_01#',
'      </div>',
'      <div class="t-Body-content" id="t_Body_content">',
'        #SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'        <div class="t-Body-contentInner">',
'          #BODY#',
'        </div>',
'        <footer class="t-Footer">',
'          <div class="t-Footer-body">',
'            <div class="t-Footer-content">#REGION_POSITION_05#</div>',
'            <div class="t-Footer-apex">',
'              <div class="t-Footer-version">#APP_VERSION#</div>  ',
'              <div class="t-Footer-customize">#CUSTOMIZE#</div>',
'              #BUILT_WITH_LOVE_USING_APEX#',
'            </div>',
'          </div>',
'          <div class="t-Footer-top">',
'            <a href="#top" class="t-Footer-topButton" id="t_Footer_topButton"><span class="a-Icon icon-up-chevron"></span></a>',
'          </div>',
'        </footer>',
'      </div>',
'  </div>',
'</div>',
'<div class="t-Body-inlineDialogs">',
'  #REGION_POSITION_04#',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#APEX_JAVASCRIPT#',
'#GENERATED_CSS#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#  ',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>',
''))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Success" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-header">',
'          <h2 class="t-Alert-title">#SUCCESS_MESSAGE#</h2>',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--warning t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Notification" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-body">',
'          #MESSAGE#',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_navigation_bar=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<ul class="t-NavigationBar t-NavigationBar--classic" data-mode="classic">',
'  <li class="t-NavigationBar-item">',
'    <span class="t-Button t-Button--icon t-Button--noUI t-Button--header t-Button--navBar t-Button--headerUser">',
'        <span class="t-Icon a-Icon icon-user"></span>',
'        <span class="t-Button-label">&APP_USER.</span>',
'    </span>',
'  </li>#BAR_BODY#',
'</ul>'))
,p_navbar_entry=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-NavigationBar-item">',
'  <a class="t-Button t-Button--icon t-Button--header" href="#LINK#">',
'      <span class="t-Icon #IMAGE#"></span>',
'      <span class="t-Button-label">#TEXT#</span>',
'  </a>',
'</li>'))
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>4
,p_error_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Alert t-Alert--danger t-Alert--wizard t-Alert--defaultIcons">',
'  <div class="t-Alert-wrap">',
'    <div class="t-Alert-icon">',
'      <span class="t-Icon"></span>',
'    </div>',
'    <div class="t-Alert-content">',
'      <div class="t-Alert-body">',
'        <h3>#MESSAGE#</h3>',
'        <p>#ADDITIONAL_INFO#</p>',
'        <div class="t-Alert-inset">#TECHNICAL_INFO#</div>',
'      </div>',
'    </div>',
'    <div class="t-Alert-buttons">',
'      <button onclick="#BACK_LINK#" class="t-Button t-Button--hot w50p t-Button--large" type="button">#OK#</button>',
'    </div>',
'  </div>',
'</div>'))
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>true
,p_grid_has_column_span=>true
,p_grid_always_emit=>true
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_default_label_col_span=>2
,p_grid_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="container">',
'#ROWS#',
'</div>'))
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="col col-#COLUMN_SPAN_NUMBER# #CSS_CLASSES#" #ATTRIBUTES#>',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_dialog_js_init_code=>'apex.navigation.dialog(#PAGE_URL#,{title:#TITLE#,height:#DIALOG_HEIGHT#,width:#DIALOG_WIDTH#,maxWidth:#DIALOG_MAX_WIDTH#,modal:#IS_MODAL#,dialog:#DIALOG#,#DIALOG_ATTRIBUTES#},#DIALOG_CSS_CLASSES#,#TRIGGERING_ELEMENT#);'
,p_dialog_js_close_code=>'apex.navigation.dialog.close(#IS_MODAL#,#TARGET#);'
,p_dialog_js_cancel_code=>'apex.navigation.dialog.cancel(#IS_MODAL#);'
,p_dialog_browser_frame=>'MODAL'
,p_reference_id=>2977628563533209425
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28927061171135181)
,p_page_template_id=>wwv_flow_api.id(28926764051135181)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28927377008135181)
,p_page_template_id=>wwv_flow_api.id(28926764051135181)
,p_name=>'Breadcrumb Bar'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28927638573135180)
,p_page_template_id=>wwv_flow_api.id(28926764051135181)
,p_name=>'Inline Dialogs'
,p_placeholder=>'REGION_POSITION_04'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28927949860135180)
,p_page_template_id=>wwv_flow_api.id(28926764051135181)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28928289951135180)
,p_page_template_id=>wwv_flow_api.id(28926764051135181)
,p_name=>'Page Navigation'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28928543409135180)
,p_page_template_id=>wwv_flow_api.id(28926764051135181)
,p_name=>'Page Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28928836078135180)
,p_page_template_id=>wwv_flow_api.id(28926764051135181)
,p_name=>'Before Navigation Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/page/standard
begin
wwv_flow_api.create_template(
 p_id=>wwv_flow_api.id(28929261131135180)
,p_theme_id=>42
,p_name=>'Standard'
,p_internal_name=>'STANDARD'
,p_is_popup=>false
,p_javascript_code_onload=>'apex.theme42.initializePage.noSideCol();'
,p_header_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!DOCTYPE html>',
'<html class="no-js #RTL_CLASS# page-&APP_PAGE_ID. app-&APP_ALIAS." lang="&BROWSER_LANGUAGE." #TEXT_DIRECTION#>',
'<head>',
'  <meta http-equiv="x-ua-compatible" content="IE=edge" />',
'  <meta charset="utf-8">',
'  <title>#TITLE#</title>',
'  #APEX_CSS#',
'  #THEME_CSS#',
'  #TEMPLATE_CSS#',
'  #THEME_STYLE_CSS#',
'  #APPLICATION_CSS#',
'  #PAGE_CSS#  ',
'  #FAVICONS#',
'  #HEAD#',
'  <meta name="viewport" content="width=device-width, initial-scale=1.0"/>',
'</head>',
'<body class="t-PageBody t-PageBody--hideLeft t-PageBody--hideActions no-anim #PAGE_CSS_CLASSES#" #TEXT_DIRECTION# #ONLOAD# id="t_PageBody">',
'#FORM_OPEN#',
'<header class="t-Header" id="t_Header">',
'  #REGION_POSITION_07#',
'  <div class="t-Header-branding">',
'    <div class="t-Header-controls">',
'      <button class="t-Button t-Button--icon t-Button--header t-Button--headerTree" title="#EXPAND_COLLAPSE_NAV_LABEL#" id="t_Button_navControl" type="button"><span class="t-Icon fa fa-bars" aria-hidden="true"></span></button>',
'    </div>',
'    <div class="t-Header-logo">',
'      <a href="#HOME_LINK#" class="t-Header-logo-link">#LOGO#</a>',
'    </div>',
'    <div class="t-Header-navBar">',
'      #NAVIGATION_BAR#',
'    </div>',
'  </div>',
'  <div class="t-Header-nav">',
'    #TOP_GLOBAL_NAVIGATION_LIST#',
'    #REGION_POSITION_06#',
'  </div>',
'</header>',
'    '))
,p_box=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body">',
'  #SIDE_GLOBAL_NAVIGATION_LIST#',
'  <div class="t-Body-main">',
'      <div class="t-Body-title" id="t_Body_title">',
'        #REGION_POSITION_01#',
'      </div>',
'      <div class="t-Body-content" id="t_Body_content">',
'        #SUCCESS_MESSAGE##NOTIFICATION_MESSAGE##GLOBAL_NOTIFICATION#',
'        <div class="t-Body-contentInner">',
'          #BODY#',
'        </div>',
'        <footer class="t-Footer">',
'          <div class="t-Footer-body">',
'            <div class="t-Footer-content">#REGION_POSITION_05#</div>',
'            <div class="t-Footer-apex">',
'              <div class="t-Footer-version">#APP_VERSION#</div>  ',
'              <div class="t-Footer-customize">#CUSTOMIZE#</div>',
'              #BUILT_WITH_LOVE_USING_APEX#',
'            </div>',
'          </div>',
'          <div class="t-Footer-top">',
'            <a href="#top" class="t-Footer-topButton" id="t_Footer_topButton"><span class="a-Icon icon-up-chevron"></span></a>',
'          </div>',
'        </footer>',
'      </div>',
'  </div>',
'</div>',
'<div class="t-Body-inlineDialogs">',
'  #REGION_POSITION_04#',
'</div>'))
,p_footer_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#FORM_CLOSE#',
'#DEVELOPER_TOOLBAR#',
'#APEX_JAVASCRIPT#',
'#GENERATED_CSS#',
'#THEME_JAVASCRIPT#',
'#TEMPLATE_JAVASCRIPT#',
'#APPLICATION_JAVASCRIPT#',
'#PAGE_JAVASCRIPT#  ',
'#GENERATED_JAVASCRIPT#',
'</body>',
'</html>',
''))
,p_success_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--success t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Success" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-header">',
'          <h2 class="t-Alert-title">#SUCCESS_MESSAGE#</h2>',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_notification_message=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-alert">',
'  <div class="t-Alert t-Alert--defaultIcons t-Alert--warning t-Alert--horizontal t-Alert--page t-Alert--colorBG" id="t_Alert_Notification" role="alert">',
'    <div class="t-Alert-wrap">',
'      <div class="t-Alert-icon">',
'        <span class="t-Icon"></span>',
'      </div>',
'      <div class="t-Alert-content">',
'        <div class="t-Alert-body">',
'          #MESSAGE#',
'        </div>',
'      </div>',
'      <div class="t-Alert-buttons">',
'        <button class="t-Button t-Button--noUI t-Button--icon t-Button--closeAlert" type="button" title="#CLOSE_NOTIFICATION#"><span class="t-Icon icon-close"></span></button>',
'      </div>',
'    </div>',
'  </div>',
'</div>'))
,p_navigation_bar=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<ul class="t-NavigationBar t-NavigationBar--classic" data-mode="classic">',
'  <li class="t-NavigationBar-item">',
'    <span class="t-Button t-Button--icon t-Button--noUI t-Button--header t-Button--navBar t-Button--headerUser">',
'        <span class="t-Icon a-Icon icon-user"></span>',
'        <span class="t-Button-label">&APP_USER.</span>',
'    </span>',
'  </li>#BAR_BODY#',
'</ul>'))
,p_navbar_entry=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-NavigationBar-item">',
'  <a class="t-Button t-Button--icon t-Button--header" href="#LINK#">',
'      <span class="t-Icon #IMAGE#"></span>',
'      <span class="t-Button-label">#TEXT#</span>',
'  </a>',
'</li>'))
,p_region_table_cattributes=>' summary="" cellpadding="0" border="0" cellspacing="0" width="100%"'
,p_breadcrumb_def_reg_pos=>'REGION_POSITION_01'
,p_theme_class_id=>1
,p_error_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Alert t-Alert--danger t-Alert--wizard t-Alert--defaultIcons">',
'  <div class="t-Alert-wrap">',
'    <div class="t-Alert-icon">',
'      <span class="t-Icon"></span>',
'    </div>',
'    <div class="t-Alert-content">',
'      <div class="t-Alert-body">',
'        <h3>#MESSAGE#</h3>',
'        <p>#ADDITIONAL_INFO#</p>',
'        <div class="t-Alert-inset">#TECHNICAL_INFO#</div>',
'      </div>',
'    </div>',
'    <div class="t-Alert-buttons">',
'      <button onclick="#BACK_LINK#" class="t-Button t-Button--hot w50p t-Button--large" type="button">#OK#</button>',
'    </div>',
'  </div>',
'</div>'))
,p_grid_type=>'FIXED'
,p_grid_max_columns=>12
,p_grid_always_use_max_columns=>true
,p_grid_has_column_span=>true
,p_grid_always_emit=>true
,p_grid_emit_empty_leading_cols=>true
,p_grid_emit_empty_trail_cols=>false
,p_grid_default_label_col_span=>2
,p_grid_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="container">',
'#ROWS#',
'</div>'))
,p_grid_row_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="row">',
'#COLUMNS#',
'</div>'))
,p_grid_column_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="col col-#COLUMN_SPAN_NUMBER# #CSS_CLASSES#" #ATTRIBUTES#>',
'#CONTENT#',
'</div>'))
,p_grid_first_column_attributes=>'alpha'
,p_grid_last_column_attributes=>'omega'
,p_dialog_js_init_code=>'apex.navigation.dialog(#PAGE_URL#,{title:#TITLE#,height:#DIALOG_HEIGHT#,width:#DIALOG_WIDTH#,maxWidth:#DIALOG_MAX_WIDTH#,modal:#IS_MODAL#,dialog:#DIALOG#,#DIALOG_ATTRIBUTES#},#DIALOG_CSS_CLASSES#,#TRIGGERING_ELEMENT#);'
,p_dialog_js_close_code=>'apex.navigation.dialog.close(#IS_MODAL#,#TARGET#);'
,p_dialog_js_cancel_code=>'apex.navigation.dialog.cancel(#IS_MODAL#);'
,p_dialog_browser_frame=>'MODAL'
,p_reference_id=>4070909157481059304
,p_translate_this_template=>'N'
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28929591133135179)
,p_page_template_id=>wwv_flow_api.id(28929261131135180)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28929802844135179)
,p_page_template_id=>wwv_flow_api.id(28929261131135180)
,p_name=>'Breadcrumb Bar'
,p_placeholder=>'REGION_POSITION_01'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28930103429135179)
,p_page_template_id=>wwv_flow_api.id(28929261131135180)
,p_name=>'Inline Dialogs'
,p_placeholder=>'REGION_POSITION_04'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28930470808135179)
,p_page_template_id=>wwv_flow_api.id(28929261131135180)
,p_name=>'Footer'
,p_placeholder=>'REGION_POSITION_05'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28930789141135179)
,p_page_template_id=>wwv_flow_api.id(28929261131135180)
,p_name=>'Page Navigation'
,p_placeholder=>'REGION_POSITION_06'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28931031864135179)
,p_page_template_id=>wwv_flow_api.id(28929261131135180)
,p_name=>'Page Header'
,p_placeholder=>'REGION_POSITION_07'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_page_tmpl_display_point(
 p_id=>wwv_flow_api.id(28931368894135179)
,p_page_template_id=>wwv_flow_api.id(28929261131135180)
,p_name=>'Before Navigation Bar'
,p_placeholder=>'REGION_POSITION_08'
,p_has_grid_support=>false
,p_glv_new_row=>false
);
end;
/
prompt --application/shared_components/user_interface/templates/button/icon
begin
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(29008925477135126)
,p_template_name=>'Icon'
,p_internal_name=>'ICON'
,p_template=>'<button class="t-Button t-Button--noLabel t-Button--icon #BUTTON_CSS_CLASSES#" #BUTTON_ATTRIBUTES# onclick="#JAVASCRIPT#" type="button" id="#BUTTON_ID#" title="#LABEL#" aria-label="#LABEL#"><span class="t-Icon #ICON_CSS_CLASSES#" aria-hidden="true"><'
||'/span></button>'
,p_hot_template=>'<button class="t-Button t-Button--noLabel t-Button--icon #BUTTON_CSS_CLASSES# t-Button--hot" #BUTTON_ATTRIBUTES# onclick="#JAVASCRIPT#" type="button" id="#BUTTON_ID#" title="#LABEL#" aria-label="#LABEL#"><span class="t-Icon #ICON_CSS_CLASSES#" aria-h'
||'idden="true"></span></button>'
,p_reference_id=>2347660919680321258
,p_translate_this_template=>'N'
,p_theme_class_id=>5
,p_theme_id=>42
);
end;
/
prompt --application/shared_components/user_interface/templates/button/text
begin
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(29009020462135125)
,p_template_name=>'Text'
,p_internal_name=>'TEXT'
,p_template=>'<button onclick="#JAVASCRIPT#" class="t-Button #BUTTON_CSS_CLASSES#" type="button" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#"><span class="t-Button-label">#LABEL#</span></button>'
,p_hot_template=>'<button onclick="#JAVASCRIPT#" class="t-Button t-Button--hot #BUTTON_CSS_CLASSES#" type="button" #BUTTON_ATTRIBUTES# id="#BUTTON_ID#"><span class="t-Button-label">#LABEL#</span></button>'
,p_reference_id=>4070916158035059322
,p_translate_this_template=>'N'
,p_theme_class_id=>1
,p_theme_id=>42
);
end;
/
prompt --application/shared_components/user_interface/templates/button/text_with_icon
begin
wwv_flow_api.create_button_templates(
 p_id=>wwv_flow_api.id(29009114813135125)
,p_template_name=>'Text with Icon'
,p_internal_name=>'TEXT_WITH_ICON'
,p_template=>'<button class="t-Button t-Button--icon #BUTTON_CSS_CLASSES#" #BUTTON_ATTRIBUTES# onclick="#JAVASCRIPT#" type="button" id="#BUTTON_ID#"><span class="t-Icon t-Icon--left #ICON_CSS_CLASSES#" aria-hidden="true"></span><span class="t-Button-label">#LABEL#'
||'</span><span class="t-Icon t-Icon--right #ICON_CSS_CLASSES#" aria-hidden="true"></span></button>'
,p_hot_template=>'<button class="t-Button t-Button--icon #BUTTON_CSS_CLASSES# t-Button--hot" #BUTTON_ATTRIBUTES# onclick="#JAVASCRIPT#" type="button" id="#BUTTON_ID#"><span class="t-Icon t-Icon--left #ICON_CSS_CLASSES#" aria-hidden="true"></span><span class="t-Button-'
||'label">#LABEL#</span><span class="t-Icon t-Icon--right #ICON_CSS_CLASSES#" aria-hidden="true"></span></button>'
,p_reference_id=>2081382742158699622
,p_translate_this_template=>'N'
,p_theme_class_id=>4
,p_preset_template_options=>'t-Button--iconRight'
,p_theme_id=>42
);
end;
/
prompt --application/shared_components/user_interface/templates/region/alert
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28931739757135175)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Alert #REGION_CSS_CLASSES#" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="t-Alert-wrap">',
'    <div class="t-Alert-icon">',
'      <span class="t-Icon #ICON_CSS_CLASSES#"></span>',
'    </div>',
'    <div class="t-Alert-content">',
'      <div class="t-Alert-header">',
'        <h2 class="t-Alert-title" id="#REGION_STATIC_ID#_heading">#TITLE#</h2>',
'      </div>',
'      <div class="t-Alert-body">#BODY#</div>',
'    </div>',
'    <div class="t-Alert-buttons">#PREVIOUS##CLOSE##CREATE##NEXT#</div>',
'  </div>',
'</div>'))
,p_page_plug_template_name=>'Alert'
,p_internal_name=>'ALERT'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>42
,p_theme_class_id=>21
,p_preset_template_options=>'t-Alert--horizontal:t-Alert--defaultIcons:t-Alert--warning'
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>2039236646100190748
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28932099982135173)
,p_plug_template_id=>wwv_flow_api.id(28931739757135175)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
end;
/
prompt --application/shared_components/user_interface/templates/region/blank_with_attributes
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28935454342135171)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES# class="#REGION_CSS_CLASSES#"> ',
'#PREVIOUS##BODY##SUB_REGIONS##NEXT#',
'</div>'))
,p_page_plug_template_name=>'Blank with Attributes'
,p_internal_name=>'BLANK_WITH_ATTRIBUTES'
,p_theme_id=>42
,p_theme_class_id=>7
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>4499993862448380551
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/region/blank_with_attributes_no_grid
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28935663051135170)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES# class="#REGION_CSS_CLASSES#"> ',
'#PREVIOUS##BODY##SUB_REGIONS##NEXT#',
'</div>'))
,p_page_plug_template_name=>'Blank with Attributes (No Grid)'
,p_internal_name=>'BLANK_WITH_ATTRIBUTES_NO_GRID'
,p_theme_id=>42
,p_theme_class_id=>7
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>3369790999010910123
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28935940448135170)
,p_plug_template_id=>wwv_flow_api.id(28935663051135170)
,p_name=>'Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28936234400135170)
,p_plug_template_id=>wwv_flow_api.id(28935663051135170)
,p_name=>'Sub Regions'
,p_placeholder=>'SUB_REGIONS'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/region/buttons_container
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28936438753135170)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-ButtonRegion t-Form--floatLeft #REGION_CSS_CLASSES#" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="t-ButtonRegion-wrap">',
'    <div class="t-ButtonRegion-col t-ButtonRegion-col--left"><div class="t-ButtonRegion-buttons">#PREVIOUS##CLOSE##DELETE#</div></div>',
'    <div class="t-ButtonRegion-col t-ButtonRegion-col--content">',
'      <h2 class="t-ButtonRegion-title" id="#REGION_STATIC_ID#_heading">#TITLE#</h2>',
'      #BODY#',
'      <div class="t-ButtonRegion-buttons">#CHANGE#</div>',
'    </div>',
'    <div class="t-ButtonRegion-col t-ButtonRegion-col--right"><div class="t-ButtonRegion-buttons">#EDIT##CREATE##NEXT#</div></div>',
'  </div>',
'</div>'))
,p_page_plug_template_name=>'Buttons Container'
,p_internal_name=>'BUTTONS_CONTAINER'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>42
,p_theme_class_id=>17
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>2124982336649579661
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28936779958135170)
,p_plug_template_id=>wwv_flow_api.id(28936438753135170)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28937096499135170)
,p_plug_template_id=>wwv_flow_api.id(28936438753135170)
,p_name=>'Sub Regions'
,p_placeholder=>'SUB_REGIONS'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
end;
/
prompt --application/shared_components/user_interface/templates/region/carousel_container
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28938655441135170)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Region t-Region--carousel #REGION_CSS_CLASSES#" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
' <div class="t-Region-header">',
'  <div class="t-Region-headerItems t-Region-headerItems--title">',
'    <span class="t-Region-headerIcon"><span class="t-Icon #ICON_CSS_CLASSES#" aria-hidden="true"></span></span>',
'    <h2 class="t-Region-title" id="#REGION_STATIC_ID#_heading">#TITLE#</h2>',
'  </div>',
'  <div class="t-Region-headerItems t-Region-headerItems--buttons">#COPY##EDIT#<span class="js-maximizeButtonContainer"></span></div>',
' </div>',
' <div class="t-Region-bodyWrap">',
'   <div class="t-Region-buttons t-Region-buttons--top">',
'    <div class="t-Region-buttons-left">#PREVIOUS#</div>',
'    <div class="t-Region-buttons-right">#NEXT#</div>',
'   </div>',
'   <div class="t-Region-body">',
'     #BODY#',
'   <div class="t-Region-carouselRegions">',
'     #SUB_REGIONS#',
'   </div>',
'   </div>',
'   <div class="t-Region-buttons t-Region-buttons--bottom">',
'    <div class="t-Region-buttons-left">#CLOSE##HELP#</div>',
'    <div class="t-Region-buttons-right">#DELETE##CHANGE##CREATE#</div>',
'   </div>',
' </div>',
'</div>'))
,p_sub_plug_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div data-label="#SUB_REGION_TITLE#" id="SR_#SUB_REGION_ID#">',
'  #SUB_REGION#',
'</div>'))
,p_page_plug_template_name=>'Carousel Container'
,p_internal_name=>'CAROUSEL_CONTAINER'
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#libraries/apex/#MIN_DIRECTORY#widget.apexTabs#MIN#.js?v=#APEX_VERSION#',
'#IMAGE_PREFIX#plugins/com.oracle.apex.carousel/1.1/com.oracle.apex.carousel#MIN#.js?v=#APEX_VERSION#'))
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>42
,p_theme_class_id=>5
,p_default_template_options=>'t-Region--showCarouselControls'
,p_preset_template_options=>'t-Region--hiddenOverflow'
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>2865840475322558786
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28938959649135169)
,p_plug_template_id=>wwv_flow_api.id(28938655441135170)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28939244068135169)
,p_plug_template_id=>wwv_flow_api.id(28938655441135170)
,p_name=>'Slides'
,p_placeholder=>'SUB_REGIONS'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
end;
/
prompt --application/shared_components/user_interface/templates/region/collapsible
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28945805107135167)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Region t-Region--hideShow #REGION_CSS_CLASSES#" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
' <div class="t-Region-header">',
'  <div class="t-Region-headerItems  t-Region-headerItems--controls">',
'    <button class="t-Button t-Button--icon t-Button--hideShow" type="button"></button>',
'  </div>',
'  <div class="t-Region-headerItems t-Region-headerItems--title">',
'    <h2 class="t-Region-title">#TITLE#</h2>',
'  </div>',
'  <div class="t-Region-headerItems t-Region-headerItems--buttons">#EDIT#</div>',
' </div>',
' <div class="t-Region-bodyWrap">',
'   <div class="t-Region-buttons t-Region-buttons--top">',
'    <div class="t-Region-buttons-left">#CLOSE#</div>',
'    <div class="t-Region-buttons-right">#CREATE#</div>',
'   </div>',
'   <div class="t-Region-body">',
'     #COPY#',
'     #BODY#',
'     #SUB_REGIONS#',
'     #CHANGE#',
'   </div>',
'   <div class="t-Region-buttons t-Region-buttons--bottom">',
'    <div class="t-Region-buttons-left">#PREVIOUS#</div>',
'    <div class="t-Region-buttons-right">#NEXT#</div>',
'   </div>',
' </div>',
'</div>'))
,p_page_plug_template_name=>'Collapsible'
,p_internal_name=>'COLLAPSIBLE'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>42
,p_theme_class_id=>1
,p_preset_template_options=>'is-expanded:t-Region--scrollBody'
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>2662888092628347716
,p_translate_this_template=>'N'
,p_template_comment=>'Red Theme'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28946170811135167)
,p_plug_template_id=>wwv_flow_api.id(28945805107135167)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28946442939135167)
,p_plug_template_id=>wwv_flow_api.id(28945805107135167)
,p_name=>'Sub Regions'
,p_placeholder=>'SUB_REGIONS'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
end;
/
prompt --application/shared_components/user_interface/templates/region/content_block
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28950410857135163)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-ContentBlock #REGION_CSS_CLASSES#" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="t-ContentBlock-header"><h1 class="t-ContentBlock-title">#TITLE#</h1></div>',
'  <div class="t-ContentBlock-body">#BODY#</div>',
'  <div class="t-ContentBlock-buttons">#PREVIOUS##NEXT#</div>',
'</div>'))
,p_page_plug_template_name=>'Content Block'
,p_internal_name=>'CONTENT_BLOCK'
,p_theme_id=>42
,p_theme_class_id=>21
,p_preset_template_options=>'t-ContentBlock--h1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>2320668864738842174
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/region/hero
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28952270918135162)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-HeroRegion #REGION_CSS_CLASSES#" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="t-HeroRegion-wrap">',
'    <div class="t-HeroRegion-col t-HeroRegion-col--left"><span class="t-HeroRegion-icon t-Icon #ICON_CSS_CLASSES#"></span></div>',
'    <div class="t-HeroRegion-col t-HeroRegion-col--content">',
'      <h1 class="t-HeroRegion-title">#TITLE#</h1>',
'      #BODY#',
'    </div>',
'    <div class="t-HeroRegion-col t-HeroRegion-col--right"><div class="t-HeroRegion-form">#SUB_REGIONS#</div><div class="t-HeroRegion-buttons">#NEXT#</div></div>',
'  </div>',
'</div>'))
,p_page_plug_template_name=>'Hero'
,p_internal_name=>'HERO'
,p_theme_id=>42
,p_theme_class_id=>22
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>2672571031438297268
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28952510898135162)
,p_plug_template_id=>wwv_flow_api.id(28952270918135162)
,p_name=>'Region Body'
,p_placeholder=>'#BODY#'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/region/inline_dialog
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28953760072135162)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="#REGION_STATIC_ID#_parent">',
'<div id="#REGION_STATIC_ID#"  class="t-DialogRegion #REGION_CSS_CLASSES# js-regionDialog" #REGION_ATTRIBUTES# style="display:none" title="#TITLE#">',
'  <div class="t-DialogRegion-wrap">',
'    <div class="t-DialogRegion-bodyWrapperOut"><div class="t-DialogRegion-bodyWrapperIn"><div class="t-DialogRegion-body">#BODY#</div></div></div>',
'    <div class="t-DialogRegion-buttons">',
'       <div class="t-ButtonRegion t-ButtonRegion--dialogRegion">',
'         <div class="t-ButtonRegion-wrap">',
'           <div class="t-ButtonRegion-col t-ButtonRegion-col--left"><div class="t-ButtonRegion-buttons">#PREVIOUS##DELETE##CLOSE#</div></div>',
'           <div class="t-ButtonRegion-col t-ButtonRegion-col--right"><div class="t-ButtonRegion-buttons">#EDIT##CREATE##NEXT#</div></div>',
'         </div>',
'       </div>',
'    </div>',
'  </div>',
'</div>',
'</div>'))
,p_page_plug_template_name=>'Inline Dialog'
,p_internal_name=>'INLINE_DIALOG'
,p_theme_id=>42
,p_theme_class_id=>24
,p_default_template_options=>'js-modal:js-draggable:js-resizable'
,p_preset_template_options=>'js-dialog-size600x400'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>2671226943886536762
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28954002803135161)
,p_plug_template_id=>wwv_flow_api.id(28953760072135162)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/region/interactive_report
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28955876667135161)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES# class="t-IRR-region #REGION_CSS_CLASSES#">',
'  <h2 class="u-VisuallyHidden" id="#REGION_STATIC_ID#_heading">#TITLE#</h2>',
'#PREVIOUS##BODY##SUB_REGIONS##NEXT#',
'</div>'))
,p_page_plug_template_name=>'Interactive Report'
,p_internal_name=>'INTERACTIVE_REPORT'
,p_theme_id=>42
,p_theme_class_id=>9
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>2099079838218790610
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/region/login
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28956456576135161)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Login-region t-Form--stretchInputs t-Form--labelsAbove #REGION_CSS_CLASSES#" id="#REGION_ID#" #REGION_ATTRIBUTES#>',
'  <div class="t-Login-header">',
'    <span class="t-Login-logo #ICON_CSS_CLASSES#"></span>',
'    <h1 class="t-Login-title" id="#REGION_STATIC_ID#_heading">#TITLE#</h1>',
'  </div>',
'  <div class="t-Login-body">',
'    #BODY#',
'  </div>',
'  <div class="t-Login-buttons">',
'    #NEXT#',
'  </div>',
'  <div class="t-Login-links">',
'    #EDIT##CREATE#',
'  </div>',
'  #SUB_REGIONS#',
'</div>'))
,p_page_plug_template_name=>'Login'
,p_internal_name=>'LOGIN'
,p_theme_id=>42
,p_theme_class_id=>23
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>2672711194551076376
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28956768576135161)
,p_plug_template_id=>wwv_flow_api.id(28956456576135161)
,p_name=>'Content Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/region/standard
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28956997224135161)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Region #REGION_CSS_CLASSES#" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
' <div class="t-Region-header">',
'  <div class="t-Region-headerItems t-Region-headerItems--title">',
'    <span class="t-Region-headerIcon"><span class="t-Icon #ICON_CSS_CLASSES#" aria-hidden="true"></span></span>',
'    <h2 class="t-Region-title" id="#REGION_STATIC_ID#_heading">#TITLE#</h2>',
'  </div>',
'  <div class="t-Region-headerItems t-Region-headerItems--buttons">#COPY##EDIT#<span class="js-maximizeButtonContainer"></span></div>',
' </div>',
' <div class="t-Region-bodyWrap">',
'   <div class="t-Region-buttons t-Region-buttons--top">',
'    <div class="t-Region-buttons-left">#PREVIOUS#</div>',
'    <div class="t-Region-buttons-right">#NEXT#</div>',
'   </div>',
'   <div class="t-Region-body">',
'     #BODY#',
'     #SUB_REGIONS#',
'   </div>',
'   <div class="t-Region-buttons t-Region-buttons--bottom">',
'    <div class="t-Region-buttons-left">#CLOSE##HELP#</div>',
'    <div class="t-Region-buttons-right">#DELETE##CHANGE##CREATE#</div>',
'   </div>',
' </div>',
'</div>',
''))
,p_page_plug_template_name=>'Standard'
,p_internal_name=>'STANDARD'
,p_plug_table_bgcolor=>'#ffffff'
,p_theme_id=>42
,p_theme_class_id=>8
,p_preset_template_options=>'t-Region--scrollBody'
,p_plug_heading_bgcolor=>'#ffffff'
,p_plug_font_size=>'-1'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>4070912133526059312
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28957282147135160)
,p_plug_template_id=>wwv_flow_api.id(28956997224135161)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28957500651135160)
,p_plug_template_id=>wwv_flow_api.id(28956997224135161)
,p_name=>'Sub Regions'
,p_placeholder=>'SUB_REGIONS'
,p_has_grid_support=>true
,p_glv_new_row=>true
,p_max_fixed_grid_columns=>12
);
end;
/
prompt --application/shared_components/user_interface/templates/region/tabs_container
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28963700310135152)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-TabsRegion #REGION_CSS_CLASSES# apex-tabs-region" #REGION_ATTRIBUTES# id="#REGION_STATIC_ID#">',
'  #BODY#',
'  <div class="t-TabsRegion-items">',
'    #SUB_REGIONS#',
'  </div>',
'</div>'))
,p_sub_plug_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div data-label="#SUB_REGION_TITLE#" id="SR_#SUB_REGION_ID#">',
'  #SUB_REGION#',
'</div>'))
,p_page_plug_template_name=>'Tabs Container'
,p_internal_name=>'TABS_CONTAINER'
,p_javascript_file_urls=>'#IMAGE_PREFIX#libraries/apex/#MIN_DIRECTORY#widget.apexTabs#MIN#.js?v=#APEX_VERSION#'
,p_theme_id=>42
,p_theme_class_id=>5
,p_preset_template_options=>'t-TabsRegion-mod--simple'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>3221725015618492759
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28964010556135152)
,p_plug_template_id=>wwv_flow_api.id(28963700310135152)
,p_name=>'Region Body'
,p_placeholder=>'BODY'
,p_has_grid_support=>true
,p_glv_new_row=>true
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28964365441135152)
,p_plug_template_id=>wwv_flow_api.id(28963700310135152)
,p_name=>'Tabs'
,p_placeholder=>'SUB_REGIONS'
,p_has_grid_support=>false
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/region/title_bar
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28966305468135152)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES# class="t-BreadcrumbRegion #REGION_CSS_CLASSES#"> ',
'  <div class="t-BreadcrumbRegion-body">',
'    <div class="t-BreadcrumbRegion-breadcrumb">',
'      #BODY#',
'    </div>',
'    <div class="t-BreadcrumbRegion-title">',
'      <h1 class="t-BreadcrumbRegion-titleText">#TITLE#</h1>',
'    </div>',
'  </div>',
'  <div class="t-BreadcrumbRegion-buttons">#PREVIOUS##CLOSE##DELETE##HELP##CHANGE##EDIT##COPY##CREATE##NEXT#</div>',
'</div>'))
,p_page_plug_template_name=>'Title Bar'
,p_internal_name=>'TITLE_BAR'
,p_theme_id=>42
,p_theme_class_id=>6
,p_default_template_options=>'t-BreadcrumbRegion--showBreadcrumb'
,p_preset_template_options=>'t-BreadcrumbRegion--useBreadcrumbTitle'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>2530016523834132090
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/region/wizard_container
begin
wwv_flow_api.create_plug_template(
 p_id=>wwv_flow_api.id(28967300119135151)
,p_layout=>'TABLE'
,p_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Wizard #REGION_CSS_CLASSES#" id="#REGION_STATIC_ID#" #REGION_ATTRIBUTES#>',
'  <div class="t-Wizard-header">',
'    <h1 class="t-Wizard-title">#TITLE#</h1>',
'    <div class="u-Table t-Wizard-controls">',
'      <div class="u-Table-fit t-Wizard-buttons">#PREVIOUS##CLOSE#</div>',
'      <div class="u-Table-fill t-Wizard-steps">',
'        #BODY#',
'      </div>',
'      <div class="u-Table-fit t-Wizard-buttons">#NEXT#</div>',
'    </div>',
'  </div>',
'  <div class="t-Wizard-body">',
'    #SUB_REGIONS#',
'  </div>',
'</div>'))
,p_page_plug_template_name=>'Wizard Container'
,p_internal_name=>'WIZARD_CONTAINER'
,p_theme_id=>42
,p_theme_class_id=>8
,p_preset_template_options=>'t-Wizard--hideStepsXSmall'
,p_default_label_alignment=>'RIGHT'
,p_default_field_alignment=>'LEFT'
,p_reference_id=>2117602213152591491
,p_translate_this_template=>'N'
);
wwv_flow_api.create_plug_tmpl_display_point(
 p_id=>wwv_flow_api.id(28967633470135151)
,p_plug_template_id=>wwv_flow_api.id(28967300119135151)
,p_name=>'Wizard Sub Regions'
,p_placeholder=>'SUB_REGIONS'
,p_has_grid_support=>true
,p_glv_new_row=>true
);
end;
/
prompt --application/shared_components/user_interface/templates/list/top_navigation_tabs
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(28988009183135141)
,p_list_template_current=>'<li class="t-NavTabs-item #A03# is-active" id="#A01#"><a href="#LINK#" class="t-NavTabs-link #A04# " title="#TEXT_ESC_SC#"><span class="t-Icon #ICON_CSS_CLASSES#" aria-hidden="true"></span><span class="t-NavTabs-label">#TEXT_ESC_SC#</span><span class'
||'="t-NavTabs-badge #A05#">#A02#</span></a></li>'
,p_list_template_noncurrent=>'<li class="t-NavTabs-item #A03#" id="#A01#"><a href="#LINK#" class="t-NavTabs-link #A04# " title="#TEXT_ESC_SC#"><span class="t-Icon #ICON_CSS_CLASSES#" aria-hidden="true"></span><span class="t-NavTabs-label">#TEXT_ESC_SC#</span><span class="t-NavTab'
||'s-badge #A05#">#A02#</span></a></li>'
,p_list_template_name=>'Top Navigation Tabs'
,p_internal_name=>'TOP_NAVIGATION_TABS'
,p_theme_id=>42
,p_theme_class_id=>7
,p_preset_template_options=>'t-NavTabs--inlineLabels-lg:t-NavTabs--displayLabels-sm'
,p_list_template_before_rows=>'<ul class="t-NavTabs #COMPONENT_CSS_CLASSES#" id="#PARENT_STATIC_ID#_navtabs">'
,p_list_template_after_rows=>'</ul>'
,p_a01_label=>'List Item ID'
,p_a02_label=>'Badge Value'
,p_a03_label=>'List Item Class'
,p_a04_label=>'Link Class'
,p_a05_label=>'Badge Class'
,p_reference_id=>1453011561172885578
);
end;
/
prompt --application/shared_components/user_interface/templates/list/wizard_progress
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(28989626226135138)
,p_list_template_current=>'<li class="t-WizardSteps-step is-active" id="#LIST_ITEM_ID#"><div class="t-WizardSteps-wrap"><span class="t-WizardSteps-marker"></span><span class="t-WizardSteps-label">#TEXT# <span class="t-WizardSteps-labelState"></span></span></div></li>'
,p_list_template_noncurrent=>'<li class="t-WizardSteps-step" id="#LIST_ITEM_ID#"><div class="t-WizardSteps-wrap"><span class="t-WizardSteps-marker"><span class="t-Icon a-Icon icon-check"></span></span><span class="t-WizardSteps-label">#TEXT# <span class="t-WizardSteps-labelState"'
||'></span></span></div></li>'
,p_list_template_name=>'Wizard Progress'
,p_internal_name=>'WIZARD_PROGRESS'
,p_javascript_code_onload=>'apex.theme.initWizardProgressBar();'
,p_theme_id=>42
,p_theme_class_id=>17
,p_preset_template_options=>'t-WizardSteps--displayLabels'
,p_list_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<h2 class="u-VisuallyHidden">#CURRENT_PROGRESS#</h2>',
'<ul class="t-WizardSteps #COMPONENT_CSS_CLASSES#" id="#LIST_ID#">'))
,p_list_template_after_rows=>'</ul>'
,p_reference_id=>2008702338707394488
);
end;
/
prompt --application/shared_components/user_interface/templates/list/menu_bar
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(28990847195135138)
,p_list_template_current=>'<li data-current="true" data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_list_template_noncurrent=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_list_template_name=>'Menu Bar'
,p_internal_name=>'MENU_BAR'
,p_javascript_code_onload=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var e = apex.jQuery("##PARENT_STATIC_ID#_menubar", apex.gPageContext$);',
'if (e.hasClass("js-addActions")) {',
'  apex.actions.addFromMarkup( e );',
'}',
'e.menu({',
'  behaveLikeTabs: e.hasClass("js-tabLike"),',
'  menubarShowSubMenuIcon: e.hasClass("js-showSubMenuIcons") || null,',
'  iconType: ''fa'',',
'  slide: e.hasClass("js-slide"),',
'  menubar: true,',
'  menubarOverflow: true',
'});'))
,p_theme_id=>42
,p_theme_class_id=>20
,p_default_template_options=>'js-showSubMenuIcons'
,p_list_template_before_rows=>'<div class="t-MenuBar #COMPONENT_CSS_CLASSES#" id="#PARENT_STATIC_ID#_menubar"><ul style="display:none">'
,p_list_template_after_rows=>'</ul></div>'
,p_before_sub_list=>'<ul>'
,p_after_sub_list=>'</ul></li>'
,p_sub_list_item_current=>'<li data-current="true" data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_sub_list_item_noncurrent=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_item_templ_curr_w_child=>'<li data-current="true" data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_item_templ_noncurr_w_child=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_sub_templ_curr_w_child=>'<li data-current="true" data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_sub_templ_noncurr_w_child=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_reference_id=>2008709236185638887
);
end;
/
prompt --application/shared_components/user_interface/templates/list/badge_list
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(28991834801135137)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-BadgeList-item #A02#">',
'  <a class="t-BadgeList-wrap u-color #A04#" href="#LINK#" #A03#>',
'  <span class="t-BadgeList-label">#TEXT#</span>',
'  <span class="t-BadgeList-value">#A01#</span>',
'  </a>',
'</li>'))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-BadgeList-item #A02#">',
'  <a class="t-BadgeList-wrap u-color #A04#" href="#LINK#" #A03#>',
'  <span class="t-BadgeList-label">#TEXT#</span>',
'  <span class="t-BadgeList-value">#A01#</span>',
'  </a>',
'</li>'))
,p_list_template_name=>'Badge List'
,p_internal_name=>'BADGE_LIST'
,p_theme_id=>42
,p_theme_class_id=>3
,p_preset_template_options=>'t-BadgeList--large:t-BadgeList--cols t-BadgeList--3cols:t-BadgeList--circular'
,p_list_template_before_rows=>'<ul class="t-BadgeList #COMPONENT_CSS_CLASSES#">'
,p_list_template_after_rows=>'</ul>'
,p_a01_label=>'Value'
,p_a02_label=>'List item CSS Classes'
,p_a03_label=>'Link Attributes'
,p_a04_label=>'Link Classes'
,p_reference_id=>2062482847268086664
,p_list_template_comment=>wwv_flow_string.join(wwv_flow_t_varchar2(
'A01: Large Number',
'A02: List Item Classes',
'A03: Link Attributes'))
);
end;
/
prompt --application/shared_components/user_interface/templates/list/media_list
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(28995830029135136)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-MediaList-item is-active #A04#">',
'    <a href="#LINK#" class="t-MediaList-itemWrap #A05#" #A03#>',
'        <div class="t-MediaList-iconWrap">',
'            <span class="t-MediaList-icon u-color #A06#"><span class="t-Icon #ICON_CSS_CLASSES#" #IMAGE_ATTR#></span></span>',
'        </div>',
'        <div class="t-MediaList-body">',
'            <h3 class="t-MediaList-title">#TEXT#</h3>',
'            <p class="t-MediaList-desc">#A01#</p>',
'        </div>',
'        <div class="t-MediaList-badgeWrap">',
'            <span class="t-MediaList-badge">#A02#</span>',
'        </div>',
'    </a>',
'</li>'))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-MediaList-item  #A04#">',
'    <a href="#LINK#" class="t-MediaList-itemWrap #A05#" #A03#>',
'        <div class="t-MediaList-iconWrap">',
'            <span class="t-MediaList-icon u-color #A06#"><span class="t-Icon #ICON_CSS_CLASSES#" #IMAGE_ATTR#></span></span>',
'        </div>',
'        <div class="t-MediaList-body">',
'            <h3 class="t-MediaList-title">#TEXT#</h3>',
'            <p class="t-MediaList-desc">#A01#</p>',
'        </div>',
'        <div class="t-MediaList-badgeWrap">',
'            <span class="t-MediaList-badge">#A02#</span>',
'        </div>',
'    </a>',
'</li>'))
,p_list_template_name=>'Media List'
,p_internal_name=>'MEDIA_LIST'
,p_theme_id=>42
,p_theme_class_id=>5
,p_default_template_options=>'t-MediaList--showIcons:t-MediaList--showDesc'
,p_list_template_before_rows=>'<ul class="t-MediaList #COMPONENT_CSS_CLASSES#">'
,p_list_template_after_rows=>'</ul>'
,p_a01_label=>'Description'
,p_a02_label=>'Badge Value'
,p_a03_label=>'Link Attributes'
,p_a04_label=>'List Item CSS Classes'
,p_a05_label=>'Link Class'
,p_a06_label=>'Icon Color Class'
,p_reference_id=>2066548068783481421
);
end;
/
prompt --application/shared_components/user_interface/templates/list/side_navigation_menu
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(28998212159135136)
,p_list_template_current=>'<li data-current="true" data-id="#A01#" data-disabled="#A02#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_list_template_noncurrent=>'<li data-id="#A01#" data-disabled="#A02#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_list_template_name=>'Side Navigation Menu'
,p_internal_name=>'SIDE_NAVIGATION_MENU'
,p_javascript_file_urls=>'#IMAGE_PREFIX#libraries/apex/#MIN_DIRECTORY#widget.treeView#MIN#.js?v=#APEX_VERSION#'
,p_javascript_code_onload=>'apex.jQuery(''body'').addClass(''t-PageBody--leftNav'');'
,p_theme_id=>42
,p_theme_class_id=>19
,p_list_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Body-nav" id="t_Body_nav" role="navigation" aria-label="&APP_TITLE!ATTR.">',
'<div class="t-TreeNav #COMPONENT_CSS_CLASSES#" id="t_TreeNav" data-id="#PARENT_STATIC_ID#_tree" aria-label="&APP_TITLE!ATTR."><ul style="display:none">'))
,p_list_template_after_rows=>'</ul></div></div>'
,p_before_sub_list=>'<ul>'
,p_after_sub_list=>'</ul></li>'
,p_sub_list_item_current=>'<li data-current="true" data-id="#A01#" data-disabled="#A02#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_sub_list_item_noncurrent=>'<li data-id="#A01#" data-disabled="#A02#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_item_templ_curr_w_child=>'<li data-current="true" data-id="#A01#" data-disabled="#A02#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_item_templ_noncurr_w_child=>'<li data-id="#A01#" data-disabled="#A02#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_sub_templ_curr_w_child=>'<li data-current="true" data-id="#A01#" data-disabled="#A02#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_sub_templ_noncurr_w_child=>'<li data-id="#A01#" data-disabled="#A02#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_a01_label=>'ID Attribute'
,p_a02_label=>'Disabled True/False'
,p_a04_label=>'Title'
,p_reference_id=>2466292414354694776
);
end;
/
prompt --application/shared_components/user_interface/templates/list/top_navigation_menu
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(28998625185135135)
,p_list_template_current=>'<li data-current="true" data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_list_template_noncurrent=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_list_template_name=>'Top Navigation Menu'
,p_internal_name=>'TOP_NAVIGATION_MENU'
,p_javascript_code_onload=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var e = apex.jQuery("##PARENT_STATIC_ID#_menubar", apex.gPageContext$);',
'if (e.hasClass("js-addActions")) {',
'  if ( apex.actions ) {',
'    apex.actions.addFromMarkup( e );',
'  } else {',
'    apex.debug.warn("Include actions.js to support menu shortcuts");',
'  }',
'}',
'e.menu({',
'  behaveLikeTabs: e.hasClass("js-tabLike"),',
'  menubarShowSubMenuIcon: e.hasClass("js-showSubMenuIcons") || null,',
'  slide: e.hasClass("js-slide"),',
'  menubar: true,',
'  menubarOverflow: true',
'});'))
,p_theme_id=>42
,p_theme_class_id=>20
,p_default_template_options=>'js-tabLike'
,p_list_template_before_rows=>'<div class="t-Header-nav-list #COMPONENT_CSS_CLASSES#" id="#PARENT_STATIC_ID#_menubar"><ul style="display:none">'
,p_list_template_after_rows=>'</ul></div>'
,p_before_sub_list=>'<ul>'
,p_after_sub_list=>'</ul></li>'
,p_sub_list_item_current=>'<li data-current="true" data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_sub_list_item_noncurrent=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_item_templ_curr_w_child=>'<li data-current="true" data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_item_templ_noncurr_w_child=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_sub_templ_curr_w_child=>'<li data-current="true" data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_sub_templ_noncurr_w_child=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_a01_label=>'ID Attribute'
,p_a02_label=>'Disabled True / False'
,p_a03_label=>'Hide'
,p_a04_label=>'Title Attribute'
,p_a05_label=>'Shortcut Key'
,p_reference_id=>2525307901300239072
);
end;
/
prompt --application/shared_components/user_interface/templates/list/navigation_bar
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(28999682801135131)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-NavigationBar-item is-active #A02#">',
'  <a class="t-Button t-Button--icon t-Button--header t-Button--navBar" href="#LINK#">',
'      <span class="t-Icon #ICON_CSS_CLASSES#"></span><span class="t-Button-label">#TEXT_ESC_SC#</span><span class="t-Button-badge">#A01#</span>',
'  </a>',
'</li>'))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-NavigationBar-item #A02#">',
'  <a class="t-Button t-Button--icon t-Button--header t-Button--navBar" href="#LINK#">',
'    <span class="t-Icon #ICON_CSS_CLASSES#"></span><span class="t-Button-label">#TEXT_ESC_SC#</span><span class="t-Button-badge">#A01#</span>',
'  </a>',
'</li>'))
,p_list_template_name=>'Navigation Bar'
,p_internal_name=>'NAVIGATION_BAR'
,p_theme_id=>42
,p_theme_class_id=>20
,p_list_template_before_rows=>'<ul class="t-NavigationBar #COMPONENT_CSS_CLASSES#" id="#LIST_ID#">'
,p_list_template_after_rows=>'</ul>'
,p_before_sub_list=>'<div class="t-NavigationBar-menu" style="display: none" id="menu_#PARENT_LIST_ITEM_ID#"><ul>'
,p_after_sub_list=>'</ul></div></li>'
,p_sub_list_item_current=>'<li data-current="true" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#">#TEXT_ESC_SC#</a></li>'
,p_sub_list_item_noncurrent=>'<li data-current="false" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#">#TEXT_ESC_SC#</a></li>'
,p_item_templ_curr_w_child=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-NavigationBar-item is-active #A02#">',
'  <button class="t-Button t-Button--icon t-Button t-Button--header t-Button--navBar js-menuButton" type="button" id="#LIST_ITEM_ID#" data-menu="menu_#LIST_ITEM_ID#">',
'      <span class="t-Icon #ICON_CSS_CLASSES#"></span><span class="t-Button-label">#TEXT_ESC_SC#</span><span class="t-Button-badge">#A01#</span><span class="a-Icon icon-down-arrow"></span>',
'  </button>'))
,p_item_templ_noncurr_w_child=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-NavigationBar-item #A02#">',
'  <button class="t-Button t-Button--icon t-Button t-Button--header t-Button--navBar js-menuButton" type="button" id="#LIST_ITEM_ID#" data-menu="menu_#LIST_ITEM_ID#">',
'      <span class="t-Icon #ICON_CSS_CLASSES#"></span><span class="t-Button-label">#TEXT_ESC_SC#</span><span class="t-Button-badge">#A01#</span><span class="a-Icon icon-down-arrow"></span>',
'  </button>'))
,p_sub_templ_curr_w_child=>'<li data-current="true" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#">#TEXT_ESC_SC#</a></li>'
,p_sub_templ_noncurr_w_child=>'<li data-current="false" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#">#TEXT_ESC_SC#</a></li>'
,p_a01_label=>'Badge Value'
,p_a02_label=>'List  Item CSS Classes'
,p_reference_id=>2846096252961119197
);
end;
/
prompt --application/shared_components/user_interface/templates/list/cards
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(28999807576135131)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-Cards-item is-active #A04#">',
'  <div class="t-Card">',
'    <a href="#LINK#" class="t-Card-wrap" #A05#>',
'      <div class="t-Card-icon u-color #A06#"><span class="t-Icon #ICON_CSS_CLASSES#"><span class="t-Card-initials" role="presentation">#A03#</span></span></div>',
'      <div class="t-Card-titleWrap"><h3 class="t-Card-title">#TEXT#</h3><h4 class="t-Card-subtitle">#A07#</h4></div>',
'      <div class="t-Card-body">',
'        <div class="t-Card-desc">#A01#</div>',
'        <div class="t-Card-info">#A02#</div>',
'      </div>',
'      <span class="t-Card-colorFill u-color #A06#"></span>',
'    </a>',
'  </div>',
'</li>'))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-Cards-item #A04#">',
'  <div class="t-Card">',
'    <a href="#LINK#" class="t-Card-wrap" #A05#>',
'      <div class="t-Card-icon u-color #A06#"><span class="t-Icon #ICON_CSS_CLASSES#"><span class="t-Card-initials" role="presentation">#A03#</span></span></div>',
'      <div class="t-Card-titleWrap"><h3 class="t-Card-title">#TEXT#</h3><h4 class="t-Card-subtitle">#A07#</h4></div>',
'      <div class="t-Card-body">',
'        <div class="t-Card-desc">#A01#</div>',
'        <div class="t-Card-info">#A02#</div>',
'      </div>',
'      <span class="t-Card-colorFill u-color #A06#"></span>',
'    </a>',
'  </div>',
'</li>'))
,p_list_template_name=>'Cards'
,p_internal_name=>'CARDS'
,p_theme_id=>42
,p_theme_class_id=>4
,p_preset_template_options=>'t-Cards--animColorFill:t-Cards--3cols:t-Cards--basic'
,p_list_template_before_rows=>'<ul class="t-Cards #COMPONENT_CSS_CLASSES#">'
,p_list_template_after_rows=>'</ul>'
,p_a01_label=>'Description'
,p_a02_label=>'Secondary Information'
,p_a03_label=>'Initials'
,p_a04_label=>'List Item CSS Classes'
,p_a05_label=>'Link Attributes'
,p_a06_label=>'Card Color Class'
,p_a07_label=>'Subtitle'
,p_reference_id=>2885322685880632508
);
end;
/
prompt --application/shared_components/user_interface/templates/list/tabs
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(29004876969135130)
,p_list_template_current=>'<li class="t-Tabs-item is-active"><a href="#LINK#" class="t-Tabs-link"><span class="t-Icon #ICON_CSS_CLASSES#"></span><span class="t-Tabs-label">#TEXT#</span></a></li>'
,p_list_template_noncurrent=>'<li class="t-Tabs-item"><a href="#LINK#" class="t-Tabs-link"><span class="t-Icon #ICON_CSS_CLASSES#"></span><span class="t-Tabs-label">#TEXT#</span></a></li>'
,p_list_template_name=>'Tabs'
,p_internal_name=>'TABS'
,p_javascript_file_urls=>'#IMAGE_PREFIX#libraries/apex/#MIN_DIRECTORY#widget.apexTabs#MIN#.js?v=#APEX_VERSION#'
,p_theme_id=>42
,p_theme_class_id=>7
,p_preset_template_options=>'t-Tabs--simple'
,p_list_template_before_rows=>'<ul class="t-Tabs #COMPONENT_CSS_CLASSES#">'
,p_list_template_after_rows=>'</ul>'
,p_reference_id=>3288206686691809997
);
end;
/
prompt --application/shared_components/user_interface/templates/list/menu_popup
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(29006432477135129)
,p_list_template_current=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>',
''))
,p_list_template_noncurrent=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>',
''))
,p_list_template_name=>'Menu Popup'
,p_internal_name=>'MENU_POPUP'
,p_javascript_code_onload=>wwv_flow_string.join(wwv_flow_t_varchar2(
'var e = apex.jQuery("##PARENT_STATIC_ID#_menu", apex.gPageContext$);',
'if (e.hasClass("js-addActions")) {',
'  apex.actions.addFromMarkup( e );',
'}',
'e.menu({ slide: e.hasClass("js-slide")});'))
,p_theme_id=>42
,p_theme_class_id=>20
,p_list_template_before_rows=>'<div id="#PARENT_STATIC_ID#_menu" class="#COMPONENT_CSS_CLASSES#" style="display:none;"><ul>'
,p_list_template_after_rows=>'</ul></div>'
,p_before_sub_list=>'<ul>'
,p_after_sub_list=>'</ul></li>'
,p_sub_list_item_current=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_sub_list_item_noncurrent=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a></li>'
,p_item_templ_curr_w_child=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_item_templ_noncurr_w_child=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_sub_templ_curr_w_child=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_sub_templ_noncurr_w_child=>'<li data-id="#A01#" data-disabled="#A02#" data-hide="#A03#" data-shortcut="#A05#" data-icon="#ICON_CSS_CLASSES#"><a href="#LINK#" title="#A04#">#TEXT_ESC_SC#</a>'
,p_a01_label=>'Data ID'
,p_a02_label=>'Disabled (True/False)'
,p_a03_label=>'Hidden (True/False)'
,p_a04_label=>'Title Attribute'
,p_a05_label=>'Shortcut'
,p_reference_id=>3492264004432431646
);
end;
/
prompt --application/shared_components/user_interface/templates/list/links_list
begin
wwv_flow_api.create_list_template(
 p_id=>wwv_flow_api.id(29006620859135129)
,p_list_template_current=>'<li class="t-LinksList-item is-current #A03#"><a href="#LINK#" class="t-LinksList-link" #A02#><span class="t-LinksList-icon"><span class="t-Icon #ICON_CSS_CLASSES#"></span></span><span class="t-LinksList-label">#TEXT#</span><span class="t-LinksList-b'
||'adge">#A01#</span></a></li>'
,p_list_template_noncurrent=>'<li class="t-LinksList-item #A03#"><a href="#LINK#" class="t-LinksList-link" #A02#><span class="t-LinksList-icon"><span class="t-Icon #ICON_CSS_CLASSES#"></span></span><span class="t-LinksList-label">#TEXT#</span><span class="t-LinksList-badge">#A01#'
||'</span></a></li>'
,p_list_template_name=>'Links List'
,p_internal_name=>'LINKS_LIST'
,p_theme_id=>42
,p_theme_class_id=>18
,p_list_template_before_rows=>'<ul class="t-LinksList #COMPONENT_CSS_CLASSES#" id="#LIST_ID#">'
,p_list_template_after_rows=>'</ul>'
,p_before_sub_list=>'<ul class="t-LinksList-list">'
,p_after_sub_list=>'</ul>'
,p_sub_list_item_current=>'<li class="t-LinksList-item is-current #A03#"><a href="#LINK#" class="t-LinksList-link" #A02#><span class="t-LinksList-icon"><span class="t-Icon #ICON_CSS_CLASSES#"></span></span><span class="t-LinksList-label">#TEXT#</span><span class="t-LinksList-b'
||'adge">#A01#</span></a></li>'
,p_sub_list_item_noncurrent=>'<li class="t-LinksList-item#A03#"><a href="#LINK#" class="t-LinksList-link" #A02#><span class="t-LinksList-icon"><span class="t-Icon #ICON_CSS_CLASSES#"></span></span><span class="t-LinksList-label">#TEXT#</span><span class="t-LinksList-badge">#A01#<'
||'/span></a></li>'
,p_item_templ_curr_w_child=>'<li class="t-LinksList-item is-current is-expanded #A03#"><a href="#LINK#" class="t-LinksList-link" #A02#><span class="t-LinksList-icon"><span class="t-Icon #ICON_CSS_CLASSES#"></span></span><span class="t-LinksList-label">#TEXT#</span><span class="t'
||'-LinksList-badge">#A01#</span></a>#SUB_LISTS#</li>'
,p_item_templ_noncurr_w_child=>'<li class="t-LinksList-item #A03#"><a href="#LINK#" class="t-LinksList-link" #A02#><span class="t-LinksList-icon"><span class="t-Icon #ICON_CSS_CLASSES#"></span></span><span class="t-LinksList-label">#TEXT#</span><span class="t-LinksList-badge">#A01#'
||'</span></a></li>'
,p_a01_label=>'Badge Value'
,p_a02_label=>'Link Attributes'
,p_a03_label=>'List Item CSS Classes'
,p_reference_id=>4070914341144059318
);
end;
/
prompt --application/shared_components/user_interface/templates/report/timeline
begin
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(28968648521135151)
,p_row_template_name=>'Timeline'
,p_internal_name=>'TIMELINE'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-Timeline-item #EVENT_MODIFIERS#" #EVENT_ATTRIBUTES#>',
'  <div class="t-Timeline-wrap">',
'    <div class="t-Timeline-user">',
'      <div class="t-Timeline-avatar #USER_COLOR#">',
'        #USER_AVATAR#',
'      </div>',
'      <div class="t-Timeline-userinfo">',
'        <span class="t-Timeline-username">#USER_NAME#</span>',
'        <span class="t-Timeline-date">#EVENT_DATE#</span>',
'      </div>',
'    </div>',
'    <div class="t-Timeline-content">',
'      <div class="t-Timeline-typeWrap">',
'        <div class="t-Timeline-type #EVENT_STATUS#">',
'          <span class="t-Icon #EVENT_ICON#"></span>',
'          <span class="t-Timeline-typename">#EVENT_TYPE#</span>',
'        </div>',
'      </div>',
'      <div class="t-Timeline-body">',
'        <h3 class="t-Timeline-title">#EVENT_TITLE#</h3>',
'        <p class="t-Timeline-desc">#EVENT_DESC#</p>',
'      </div>',
'    </div>',
'  </div>',
'</li>'))
,p_row_template_condition1=>':EVENT_LINK is null'
,p_row_template2=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-Timeline-item #EVENT_MODIFIERS#" #EVENT_ATTRIBUTES#>',
'  <a href="#EVENT_LINK#" class="t-Timeline-wrap">',
'    <div class="t-Timeline-user">',
'      <div class="t-Timeline-avatar #USER_COLOR#">',
'        #USER_AVATAR#',
'      </div>',
'      <div class="t-Timeline-userinfo">',
'        <span class="t-Timeline-username">#USER_NAME#</span>',
'        <span class="t-Timeline-date">#EVENT_DATE#</span>',
'      </div>',
'    </div>',
'    <div class="t-Timeline-content">',
'      <div class="t-Timeline-typeWrap">',
'        <div class="t-Timeline-type #EVENT_STATUS#">',
'          <span class="t-Icon #EVENT_ICON#"></span>',
'          <span class="t-Timeline-typename">#EVENT_TYPE#</span>',
'        </div>',
'      </div>',
'      <div class="t-Timeline-body">',
'        <h3 class="t-Timeline-title">#EVENT_TITLE#</h3>',
'        <p class="t-Timeline-desc">#EVENT_DESC#</p>',
'      </div>',
'    </div>',
'  </a>',
'</li>'))
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<ul class="t-Timeline #COMPONENT_CSS_CLASSES#" #REPORT_ATTRIBUTES# id="#REGION_STATIC_ID#_timeline" data-region-id="#REGION_STATIC_ID#">',
''))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</ul>',
'<table class="t-Report-pagination" role="presentation">#PAGINATION#</table>'))
,p_row_template_type=>'NAMED_COLUMNS'
,p_row_template_display_cond1=>'NOT_CONDITIONAL'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'NOT_CONDITIONAL'
,p_pagination_template=>'<span class="t-Report-paginationText">#TEXT#</span>'
,p_next_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS#',
'</a>'))
,p_next_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT_SET#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS_SET#',
'</a>'))
,p_theme_id=>42
,p_theme_class_id=>7
,p_reference_id=>1513373588340069864
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/report/media_list
begin
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(28969210851135149)
,p_row_template_name=>'Media List'
,p_internal_name=>'MEDIA_LIST'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-MediaList-item #LIST_CLASS#">',
'    <a href="#LINK#" class="t-MediaList-itemWrap #LINK_CLASS#" #LINK_ATTR#>',
'        <div class="t-MediaList-iconWrap">',
'            <span class="t-MediaList-icon u-color #ICON_COLOR_CLASS#"><span class="t-Icon #ICON_CLASS#"></span></span>',
'        </div>',
'        <div class="t-MediaList-body">',
'            <h3 class="t-MediaList-title">#LIST_TITLE#</h3>',
'            <p class="t-MediaList-desc">#LIST_TEXT#</p>',
'        </div>',
'        <div class="t-MediaList-badgeWrap">',
'            <span class="t-MediaList-badge">#LIST_BADGE#</span>',
'        </div>',
'    </a>',
'</li>'))
,p_row_template_before_rows=>'<ul class="t-MediaList #COMPONENT_CSS_CLASSES#" #REPORT_ATTRIBUTES# id="#REGION_STATIC_ID#_report" data-region-id="#REGION_STATIC_ID#">'
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</ul>',
'<table class="t-Report-pagination" role="presentation">#PAGINATION#</table>'))
,p_row_template_type=>'NAMED_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_pagination_template=>'<span class="t-Report-paginationText">#TEXT#</span>'
,p_next_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS#',
'</a>'))
,p_next_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT_SET#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS_SET#',
'</a>'))
,p_theme_id=>42
,p_theme_class_id=>1
,p_default_template_options=>'t-MediaList--showDesc:t-MediaList--showIcons'
,p_preset_template_options=>'t-MediaList--stack'
,p_reference_id=>2092157460408299055
,p_translate_this_template=>'N'
,p_row_template_comment=>' (SELECT link_text, link_target, detail1, detail2, last_modified)'
);
end;
/
prompt --application/shared_components/user_interface/templates/report/value_attribute_pairs_row
begin
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(28972031346135149)
,p_row_template_name=>'Value Attribute Pairs - Row'
,p_internal_name=>'VALUE_ATTRIBUTE_PAIRS_ROW'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<dt class="t-AVPList-label">',
'  #1#',
'</dt>',
'<dd class="t-AVPList-value">',
'  #2#',
'</dd>'))
,p_row_template_before_rows=>'<dl class="t-AVPList #COMPONENT_CSS_CLASSES#" #REPORT_ATTRIBUTES# id="report_#REGION_STATIC_ID#" data-region-id="#REGION_STATIC_ID#">'
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</dl>',
'<table class="t-Report-pagination" role="presentation">#PAGINATION#</table>'))
,p_row_template_type=>'NAMED_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_pagination_template=>'<span class="t-Report-paginationText">#TEXT#</span>'
,p_next_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS#',
'</a>'))
,p_next_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT_SET#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS_SET#',
'</a>'))
,p_theme_id=>42
,p_theme_class_id=>7
,p_preset_template_options=>'t-AVPList--leftAligned'
,p_reference_id=>2099068321678681753
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/report/value_attribute_pairs_column
begin
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(28974074185135146)
,p_row_template_name=>'Value Attribute Pairs - Column'
,p_internal_name=>'VALUE_ATTRIBUTE_PAIRS_COLUMN'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<dt class="t-AVPList-label">',
'  #COLUMN_HEADER#',
'</dt>',
'<dd class="t-AVPList-value">',
'  #COLUMN_VALUE#',
'</dd>'))
,p_row_template_before_rows=>'<dl class="t-AVPList #COMPONENT_CSS_CLASSES#" #REPORT_ATTRIBUTES# data-region-id="#REGION_STATIC_ID#">'
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</dl>',
'<table class="t-Report-pagination" role="presentation">#PAGINATION#</table>'))
,p_row_template_type=>'GENERIC_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_pagination_template=>'<span class="t-Report-paginationText">#TEXT#</span>'
,p_next_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS#',
'</a>'))
,p_next_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT_SET#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS_SET#',
'</a>'))
,p_theme_id=>42
,p_theme_class_id=>6
,p_preset_template_options=>'t-AVPList--leftAligned'
,p_reference_id=>2099068636272681754
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/report/badge_list
begin
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(28975823088135145)
,p_row_template_name=>'Badge List'
,p_internal_name=>'BADGE_LIST'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-BadgeList-item">',
' <span class="t-BadgeList-wrap u-color">',
'  <span class="t-BadgeList-label">#COLUMN_HEADER#</span>',
'  <span class="t-BadgeList-value">#COLUMN_VALUE#</span>',
' </span>',
'</li>'))
,p_row_template_before_rows=>'<ul class="t-BadgeList #COMPONENT_CSS_CLASSES#" data-region-id="#REGION_STATIC_ID#">'
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</ul>',
'<table class="t-Report-pagination" role="presentation">#PAGINATION#</table>'))
,p_row_template_type=>'GENERIC_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_pagination_template=>'<span class="t-Report-paginationText">#TEXT#</span>'
,p_next_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS#',
'</a>'))
,p_next_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT_SET#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS_SET#',
'</a>'))
,p_theme_id=>42
,p_theme_class_id=>6
,p_preset_template_options=>'t-BadgeList--large:t-BadgeList--fixed:t-BadgeList--circular'
,p_reference_id=>2103197159775914759
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/report/standard
begin
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(28979442408135144)
,p_row_template_name=>'Standard'
,p_internal_name=>'STANDARD'
,p_row_template1=>'<td class="t-Report-cell" #ALIGNMENT# headers="#COLUMN_HEADER_NAME#">#COLUMN_VALUE#</td>'
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Report #COMPONENT_CSS_CLASSES#" id="report_#REGION_STATIC_ID#" #REPORT_ATTRIBUTES# data-region-id="#REGION_STATIC_ID#">',
'  <div class="t-Report-wrap">',
'    <table class="t-Report-pagination" role="presentation">#TOP_PAGINATION#</table>',
'    <div class="t-Report-tableWrap">',
'    <table class="t-Report-report" summary="#REGION_TITLE#">'))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'      </tbody>',
'    </table>',
'    </div>',
'    <div class="t-Report-links">#EXTERNAL_LINK##CSV_LINK#</div>',
'    <table class="t-Report-pagination t-Report-pagination--bottom" role="presentation">#PAGINATION#</table>',
'  </div>',
'</div>'))
,p_row_template_type=>'GENERIC_COLUMNS'
,p_before_column_heading=>'<thead>'
,p_column_heading_template=>'<th class="t-Report-colHead" #ALIGNMENT# id="#COLUMN_HEADER_NAME#" #COLUMN_WIDTH#>#COLUMN_HEADER#</th>'
,p_after_column_heading=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</thead>',
'<tbody>'))
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_pagination_template=>'<span class="t-Report-paginationText">#TEXT#</span>'
,p_next_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS#',
'</a>'))
,p_next_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT_SET#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS_SET#',
'</a>'))
,p_theme_id=>42
,p_theme_class_id=>4
,p_preset_template_options=>'t-Report--altRowsDefault:t-Report--rowHighlight'
,p_reference_id=>2537207537838287671
,p_translate_this_template=>'N'
);
begin
wwv_flow_api.create_row_template_patch(
 p_id=>wwv_flow_api.id(28979442408135144)
,p_row_template_before_first=>'<tr>'
,p_row_template_after_last=>'</tr>'
);
exception when others then null;
end;
end;
/
prompt --application/shared_components/user_interface/templates/report/comments
begin
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(28982093919135143)
,p_row_template_name=>'Comments'
,p_internal_name=>'COMMENTS'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-Comments-item #COMMENT_MODIFIERS#">',
'    <div class="t-Comments-icon">',
'        <div class="t-Comments-userIcon #ICON_MODIFIER#" aria-hidden="true">#USER_ICON#</div>',
'    </div>',
'    <div class="t-Comments-body">',
'        <div class="t-Comments-info">',
'            #USER_NAME# <span class="t-Comments-date">#COMMENT_DATE#</span> <span class="t-Comments-actions">#ACTIONS#</span>',
'        </div>',
'        <div class="t-Comments-comment">',
'            #COMMENT_TEXT##ATTRIBUTE_1##ATTRIBUTE_2##ATTRIBUTE_3##ATTRIBUTE_4#',
'        </div>',
'    </div>',
'</li>'))
,p_row_template_before_rows=>'<ul class="t-Comments #COMPONENT_CSS_CLASSES#" #REPORT_ATTRIBUTES# id="#REGION_STATIC_ID#_report" data-region-id="#REGION_STATIC_ID#">'
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</ul>',
'<table class="t-Report-pagination" role="presentation">#PAGINATION#</table>'))
,p_row_template_type=>'NAMED_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'NOT_CONDITIONAL'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_pagination_template=>'<span class="t-Report-paginationText">#TEXT#</span>'
,p_next_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS#',
'</a>'))
,p_next_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT_SET#<span class="a-Icon icon-right-arrow"></span>',
'</a>',
''))
,p_previous_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS_SET#',
'</a>'))
,p_theme_id=>42
,p_theme_class_id=>7
,p_preset_template_options=>'t-Comments--chat'
,p_reference_id=>2611722012730764232
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/report/alerts
begin
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(28982811014135142)
,p_row_template_name=>'Alerts'
,p_internal_name=>'ALERTS'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Alert t-Alert--horizontal t-Alert--colorBG t-Alert--defaultIcons t-Alert--#ALERT_TYPE#" role="alert">',
'  <div class="t-Alert-wrap">',
'    <div class="t-Alert-icon">',
'      <span class="t-Icon"></span>',
'    </div>',
'    <div class="t-Alert-content">',
'      <div class="t-Alert-header">',
'        <h2 class="t-Alert-title">#ALERT_TITLE#</h2>',
'      </div>',
'      <div class="t-Alert-body">',
'        #ALERT_DESC#',
'      </div>',
'    </div>',
'    <div class="t-Alert-buttons">',
'      #ALERT_ACTION#',
'    </div>',
'  </div>',
'</div>'))
,p_row_template_before_rows=>'<div class="t-Alerts #COMPONENT_CSS_CLASSES#" #REPORT_ATTRIBUTES# id="#REGION_STATIC_ID#_alerts" data-region-id="#REGION_STATIC_ID#">'
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</div>',
'<table class="t-Report-pagination" role="presentation">#PAGINATION#</table>'))
,p_row_template_type=>'NAMED_COLUMNS'
,p_row_template_display_cond1=>'0'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'0'
,p_pagination_template=>'<span class="t-Report-paginationText">#TEXT#</span>'
,p_next_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS#',
'</a>'))
,p_next_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT_SET#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS_SET#',
'</a>'))
,p_theme_id=>42
,p_theme_class_id=>14
,p_reference_id=>2881456138952347027
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/report/cards
begin
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(28983039117135142)
,p_row_template_name=>'Cards'
,p_internal_name=>'CARDS'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-Cards-item #CARD_MODIFIERS#">',
'  <div class="t-Card">',
'    <a href="#CARD_LINK#" class="t-Card-wrap">',
'      <div class="t-Card-icon u-color #CARD_COLOR#"><span class="t-Icon fa #CARD_ICON#"><span class="t-Card-initials" role="presentation">#CARD_INITIALS#</span></span></div>',
'      <div class="t-Card-titleWrap"><h3 class="t-Card-title">#CARD_TITLE#</h3><h4 class="t-Card-subtitle">#CARD_SUBTITLE#</h4></div>',
'      <div class="t-Card-body">',
'        <div class="t-Card-desc">#CARD_TEXT#</div>',
'        <div class="t-Card-info">#CARD_SUBTEXT#</div>',
'      </div>',
'      <span class="t-Card-colorFill u-color #CARD_COLOR#"></span>',
'    </a>',
'  </div>',
'</li>'))
,p_row_template_condition1=>':CARD_LINK is not null'
,p_row_template2=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<li class="t-Cards-item #CARD_MODIFIERS#">',
'  <div class="t-Card">',
'    <div class="t-Card-wrap">',
'      <div class="t-Card-icon u-color #CARD_COLOR#"><span class="t-Icon fa #CARD_ICON#"><span class="t-Card-initials" role="presentation">#CARD_INITIALS#</span></span></div>',
'      <div class="t-Card-titleWrap"><h3 class="t-Card-title">#CARD_TITLE#</h3><h4 class="t-Card-subtitle">#CARD_SUBTITLE#</h4></div>',
'      <div class="t-Card-body">',
'        <div class="t-Card-desc">#CARD_TEXT#</div>',
'        <div class="t-Card-info">#CARD_SUBTEXT#</div>',
'      </div>',
'      <span class="t-Card-colorFill u-color #CARD_COLOR#"></span>',
'    </div>',
'  </div>',
'</li>'))
,p_row_template_before_rows=>'<ul class="t-Cards #COMPONENT_CSS_CLASSES#" #REPORT_ATTRIBUTES# id="#REGION_STATIC_ID#_cards" data-region-id="#REGION_STATIC_ID#">'
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</ul>',
'<table class="t-Report-pagination" role="presentation">#PAGINATION#</table>'))
,p_row_template_type=>'NAMED_COLUMNS'
,p_row_template_display_cond1=>'NOT_CONDITIONAL'
,p_row_template_display_cond2=>'0'
,p_row_template_display_cond3=>'0'
,p_row_template_display_cond4=>'NOT_CONDITIONAL'
,p_pagination_template=>'<span class="t-Report-paginationText">#TEXT#</span>'
,p_next_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS#',
'</a>'))
,p_next_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT_SET#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS_SET#',
'</a>'))
,p_theme_id=>42
,p_theme_class_id=>7
,p_preset_template_options=>'t-Cards--animColorFill:t-Cards--3cols:t-Cards--basic'
,p_reference_id=>2973535649510699732
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/report/search_results
begin
wwv_flow_api.create_row_template(
 p_id=>wwv_flow_api.id(28987838107135141)
,p_row_template_name=>'Search Results'
,p_internal_name=>'SEARCH_RESULTS'
,p_row_template1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  <li class="t-SearchResults-item">',
'    <h3 class="t-SearchResults-title"><a href="#SEARCH_LINK#">#SEARCH_TITLE#</a></h3>',
'    <div class="t-SearchResults-info">',
'      <p class="t-SearchResults-desc">#SEARCH_DESC#</p>',
'      <span class="t-SearchResults-misc">#LABEL_01#: #VALUE_01#</span>',
'    </div>',
'  </li>'))
,p_row_template_condition1=>':LABEL_02 is null'
,p_row_template2=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  <li class="t-SearchResults-item">',
'    <h3 class="t-SearchResults-title"><a href="#SEARCH_LINK#">#SEARCH_TITLE#</a></h3>',
'    <div class="t-SearchResults-info">',
'      <p class="t-SearchResults-desc">#SEARCH_DESC#</p>',
'      <span class="t-SearchResults-misc">#LABEL_01#: #VALUE_01#</span>',
'      <span class="t-SearchResults-misc">#LABEL_02#: #VALUE_02#</span>',
'    </div>',
'  </li>'))
,p_row_template_condition2=>':LABEL_03 is null'
,p_row_template3=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  <li class="t-SearchResults-item">',
'    <h3 class="t-SearchResults-title"><a href="#SEARCH_LINK#">#SEARCH_TITLE#</a></h3>',
'    <div class="t-SearchResults-info">',
'      <p class="t-SearchResults-desc">#SEARCH_DESC#</p>',
'      <span class="t-SearchResults-misc">#LABEL_01#: #VALUE_01#</span>',
'      <span class="t-SearchResults-misc">#LABEL_02#: #VALUE_02#</span>',
'      <span class="t-SearchResults-misc">#LABEL_03#: #VALUE_03#</span>',
'    </div>',
'  </li>'))
,p_row_template_condition3=>':LABEL_04 is null'
,p_row_template4=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  <li class="t-SearchResults-item">',
'    <h3 class="t-SearchResults-title"><a href="#SEARCH_LINK#">#SEARCH_TITLE#</a></h3>',
'    <div class="t-SearchResults-info">',
'      <p class="t-SearchResults-desc">#SEARCH_DESC#</p>',
'      <span class="t-SearchResults-misc">#LABEL_01#: #VALUE_01#</span>',
'      <span class="t-SearchResults-misc">#LABEL_02#: #VALUE_02#</span>',
'      <span class="t-SearchResults-misc">#LABEL_03#: #VALUE_03#</span>',
'      <span class="t-SearchResults-misc">#LABEL_04#: #VALUE_04#</span>',
'    </div>',
'  </li>'))
,p_row_template_before_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-SearchResults #COMPONENT_CSS_CLASSES#" #REPORT_ATTRIBUTES# id="#REGION_STATIC_ID#_report" data-region-id="#REGION_STATIC_ID#">',
'<ul class="t-SearchResults-list">'))
,p_row_template_after_rows=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</ul>',
'<table class="t-Report-pagination" role="presentation">#PAGINATION#</table>',
'</div>'))
,p_row_template_type=>'NAMED_COLUMNS'
,p_row_template_display_cond1=>'NOT_CONDITIONAL'
,p_row_template_display_cond2=>'NOT_CONDITIONAL'
,p_row_template_display_cond3=>'NOT_CONDITIONAL'
,p_row_template_display_cond4=>'NOT_CONDITIONAL'
,p_pagination_template=>'<span class="t-Report-paginationText">#TEXT#</span>'
,p_next_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_page_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS#',
'</a>'))
,p_next_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--next">',
'  #PAGINATION_NEXT_SET#<span class="a-Icon icon-right-arrow"></span>',
'</a>'))
,p_previous_set_template=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<a href="#LINK#" class="t-Button t-Button--small t-Button--noUI t-Report-paginationLink t-Report-paginationLink--prev">',
'  <span class="a-Icon icon-left-arrow"></span>#PAGINATION_PREVIOUS_SET#',
'</a>'))
,p_theme_id=>42
,p_theme_class_id=>1
,p_reference_id=>4070913431524059316
,p_translate_this_template=>'N'
,p_row_template_comment=>' (SELECT link_text, link_target, detail1, detail2, last_modified)'
);
end;
/
prompt --application/shared_components/user_interface/templates/label/hidden
begin
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(29008249445135128)
,p_template_name=>'Hidden'
,p_internal_name=>'HIDDEN'
,p_template_body1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Form-labelContainer t-Form-labelContainer--hiddenLabel col col-#LABEL_COLUMN_SPAN_NUMBER#">',
'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="t-Form-label u-VisuallyHidden">'))
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</label>',
'</div>'))
,p_before_item=>'<div class="t-Form-fieldContainer t-Form-fieldContainer--hiddenLabel rel-col #ITEM_CSS_CLASSES#" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>'</div>'
,p_item_pre_text=>'<span class="t-Form-itemText t-Form-itemText--pre">#CURRENT_ITEM_PRE_TEXT#</span>'
,p_item_post_text=>'<span class="t-Form-itemText t-Form-itemText--post">#CURRENT_ITEM_POST_TEXT#</span>'
,p_before_element=>'<div class="t-Form-inputContainer col col-#ITEM_COLUMN_SPAN_NUMBER#"><div class="t-Form-itemWrapper">#ITEM_PRE_TEXT#'
,p_after_element=>'#ITEM_POST_TEXT##HELP_TEMPLATE#</div>#INLINE_HELP_TEMPLATE##ERROR_TEMPLATE#</div>'
,p_help_link=>'<button class="t-Form-helpButton js-itemHelp" data-itemhelp="#CURRENT_ITEM_ID#" title="#CURRENT_ITEM_HELP_LABEL#" aria-label="#CURRENT_ITEM_HELP_LABEL#" tabindex="-1" type="button"><span class="a-Icon icon-help" aria-hidden="true"></span></button>'
,p_inline_help_text=>'<span class="t-Form-inlineHelp">#CURRENT_ITEM_INLINE_HELP_TEXT#</span>'
,p_error_template=>'<span class="t-Form-error">#ERROR_MESSAGE#</span>'
,p_theme_id=>42
,p_theme_class_id=>13
,p_reference_id=>2039339104148359505
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/label/optional
begin
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(29008330984135127)
,p_template_name=>'Optional'
,p_internal_name=>'OPTIONAL'
,p_template_body1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Form-labelContainer col col-#LABEL_COLUMN_SPAN_NUMBER#">',
'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="t-Form-label">'))
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</label>',
'</div>',
''))
,p_before_item=>'<div class="t-Form-fieldContainer rel-col #ITEM_CSS_CLASSES#" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>'</div>'
,p_item_pre_text=>'<span class="t-Form-itemText t-Form-itemText--pre">#CURRENT_ITEM_PRE_TEXT#</span>'
,p_item_post_text=>'<span class="t-Form-itemText t-Form-itemText--post">#CURRENT_ITEM_POST_TEXT#</span>'
,p_before_element=>'<div class="t-Form-inputContainer col col-#ITEM_COLUMN_SPAN_NUMBER#"><div class="t-Form-itemWrapper">#ITEM_PRE_TEXT#'
,p_after_element=>'#ITEM_POST_TEXT##HELP_TEMPLATE#</div>#INLINE_HELP_TEMPLATE##ERROR_TEMPLATE#</div>'
,p_help_link=>'<button class="t-Form-helpButton js-itemHelp" data-itemhelp="#CURRENT_ITEM_ID#" title="#CURRENT_ITEM_HELP_LABEL#" aria-label="#CURRENT_ITEM_HELP_LABEL#" tabindex="-1" type="button"><span class="a-Icon icon-help" aria-hidden="true"></span></button>'
,p_inline_help_text=>'<span class="t-Form-inlineHelp">#CURRENT_ITEM_INLINE_HELP_TEXT#</span>'
,p_error_template=>'<span class="t-Form-error">#ERROR_MESSAGE#</span>'
,p_theme_id=>42
,p_theme_class_id=>3
,p_reference_id=>2317154212072806530
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/label/optional_above
begin
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(29008453741135127)
,p_template_name=>'Optional - Above'
,p_internal_name=>'OPTIONAL_ABOVE'
,p_template_body1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Form-labelContainer">',
'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="t-Form-label">'))
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</label>#HELP_TEMPLATE#',
'</div>'))
,p_before_item=>'<div class="t-Form-fieldContainer t-Form-fieldContainer--stacked #ITEM_CSS_CLASSES#" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>'</div>'
,p_item_pre_text=>'<span class="t-Form-itemText t-Form-itemText--pre">#CURRENT_ITEM_PRE_TEXT#</span>'
,p_item_post_text=>'<span class="t-Form-itemText t-Form-itemText--post">#CURRENT_ITEM_POST_TEXT#</span>'
,p_before_element=>'<div class="t-Form-inputContainer"><div class="t-Form-itemWrapper">#ITEM_PRE_TEXT#'
,p_after_element=>'#ITEM_POST_TEXT#</div>#INLINE_HELP_TEMPLATE##ERROR_TEMPLATE#</div>'
,p_help_link=>'<button class="t-Form-helpButton js-itemHelp" data-itemhelp="#CURRENT_ITEM_ID#" title="#CURRENT_ITEM_HELP_LABEL#" aria-label="#CURRENT_ITEM_HELP_LABEL#" tabindex="-1" type="button"><span class="a-Icon icon-help" aria-hidden="true"></span></button>'
,p_inline_help_text=>'<span class="t-Form-inlineHelp">#CURRENT_ITEM_INLINE_HELP_TEXT#</span>'
,p_error_template=>'<span class="t-Form-error">#ERROR_MESSAGE#</span>'
,p_theme_id=>42
,p_theme_class_id=>3
,p_reference_id=>3030114864004968404
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/label/optional_floating
begin
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(29008583468135127)
,p_template_name=>'Optional - Floating'
,p_internal_name=>'OPTIONAL_FLOATING'
,p_template_body1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Form-labelContainer">',
'<label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="t-Form-label">'))
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</label>',
'</div>'))
,p_before_item=>'<div class="t-Form-fieldContainer t-Form-fieldContainer--floatingLabel #ITEM_CSS_CLASSES#" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>'</div>'
,p_item_pre_text=>'<span class="t-Form-itemText t-Form-itemText--pre">#CURRENT_ITEM_PRE_TEXT#</span>'
,p_item_post_text=>'<span class="t-Form-itemText t-Form-itemText--post">#CURRENT_ITEM_POST_TEXT#</span>'
,p_before_element=>'<div class="t-Form-inputContainer"><div class="t-Form-itemWrapper">#ITEM_PRE_TEXT#'
,p_after_element=>'#ITEM_POST_TEXT##HELP_TEMPLATE#</div>#INLINE_HELP_TEMPLATE##ERROR_TEMPLATE#</div>'
,p_help_link=>'<button class="t-Form-helpButton js-itemHelp" data-itemhelp="#CURRENT_ITEM_ID#" title="#CURRENT_ITEM_HELP_LABEL#" aria-label="#CURRENT_ITEM_HELP_LABEL#" tabindex="-1" type="button"><span class="a-Icon icon-help" aria-hidden="true"></span></button>'
,p_inline_help_text=>'<span class="t-Form-inlineHelp">#CURRENT_ITEM_INLINE_HELP_TEXT#</span>'
,p_error_template=>'<span class="t-Form-error">#ERROR_MESSAGE#</span>'
,p_theme_id=>42
,p_theme_class_id=>3
,p_reference_id=>1607675164727151865
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/label/required
begin
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(29008675084135127)
,p_template_name=>'Required'
,p_internal_name=>'REQUIRED'
,p_template_body1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Form-labelContainer col col-#LABEL_COLUMN_SPAN_NUMBER#">',
'  <label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="t-Form-label">'))
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
' <span class="u-VisuallyHidden">(#VALUE_REQUIRED#)</span></label>',
'</div>'))
,p_before_item=>'<div class="t-Form-fieldContainer is-required rel-col #ITEM_CSS_CLASSES#" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>'</div>'
,p_item_pre_text=>'<span class="t-Form-itemText t-Form-itemText--pre">#CURRENT_ITEM_PRE_TEXT#</span>'
,p_item_post_text=>'<span class="t-Form-itemText t-Form-itemText--post">#CURRENT_ITEM_POST_TEXT#</span>'
,p_before_element=>'<div class="t-Form-inputContainer col col-#ITEM_COLUMN_SPAN_NUMBER#"><div class="t-Form-itemWrapper">#ITEM_PRE_TEXT#'
,p_after_element=>'#ITEM_POST_TEXT##HELP_TEMPLATE#</div>#INLINE_HELP_TEMPLATE##ERROR_TEMPLATE#</div>'
,p_help_link=>'<button class="t-Form-helpButton js-itemHelp" data-itemhelp="#CURRENT_ITEM_ID#" title="#CURRENT_ITEM_HELP_LABEL#" aria-label="#CURRENT_ITEM_HELP_LABEL#" tabindex="-1" type="button"><span class="a-Icon icon-help" aria-hidden="true"></span></button>'
,p_inline_help_text=>'<span class="t-Form-inlineHelp">#CURRENT_ITEM_INLINE_HELP_TEXT#</span>'
,p_error_template=>'<span class="t-Form-error">#ERROR_MESSAGE#</span>'
,p_theme_id=>42
,p_theme_class_id=>4
,p_reference_id=>2525313812251712801
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/label/required_above
begin
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(29008770292135127)
,p_template_name=>'Required - Above'
,p_internal_name=>'REQUIRED_ABOVE'
,p_template_body1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Form-labelContainer">',
'  <label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="t-Form-label">'))
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
' <span class="u-VisuallyHidden">(#VALUE_REQUIRED#)</span></label> #HELP_TEMPLATE#',
'</div>'))
,p_before_item=>'<div class="t-Form-fieldContainer t-Form-fieldContainer--stacked is-required #ITEM_CSS_CLASSES#" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>'</div>'
,p_item_pre_text=>'<span class="t-Form-itemText t-Form-itemText--pre">#CURRENT_ITEM_PRE_TEXT#</span>'
,p_item_post_text=>'<span class="t-Form-itemText t-Form-itemText--post">#CURRENT_ITEM_POST_TEXT#</span>'
,p_before_element=>'<div class="t-Form-inputContainer"><div class="t-Form-itemWrapper">#ITEM_PRE_TEXT#'
,p_after_element=>'#ITEM_POST_TEXT#</div>#INLINE_HELP_TEMPLATE##ERROR_TEMPLATE#</div>'
,p_help_link=>'<button class="t-Form-helpButton js-itemHelp" data-itemhelp="#CURRENT_ITEM_ID#" title="#CURRENT_ITEM_HELP_LABEL#" aria-label="#CURRENT_ITEM_HELP_LABEL#" tabindex="-1" type="button"><span class="a-Icon icon-help" aria-hidden="true"></span></button>'
,p_inline_help_text=>'<span class="t-Form-inlineHelp">#CURRENT_ITEM_INLINE_HELP_TEXT#</span>'
,p_error_template=>'<span class="t-Form-error">#ERROR_MESSAGE#</span>'
,p_theme_id=>42
,p_theme_class_id=>4
,p_reference_id=>3030115129444970113
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/label/required_floating
begin
wwv_flow_api.create_field_template(
 p_id=>wwv_flow_api.id(29008861129135127)
,p_template_name=>'Required - Floating'
,p_internal_name=>'REQUIRED_FLOATING'
,p_template_body1=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-Form-labelContainer">',
'  <label for="#CURRENT_ITEM_NAME#" id="#LABEL_ID#" class="t-Form-label">'))
,p_template_body2=>wwv_flow_string.join(wwv_flow_t_varchar2(
' <span class="u-VisuallyHidden">(#VALUE_REQUIRED#)</span></label>',
'</div>'))
,p_before_item=>'<div class="t-Form-fieldContainer t-Form-fieldContainer--floatingLabel is-required #ITEM_CSS_CLASSES#" id="#CURRENT_ITEM_CONTAINER_ID#">'
,p_after_item=>'</div>'
,p_item_pre_text=>'<span class="t-Form-itemText t-Form-itemText--pre">#CURRENT_ITEM_PRE_TEXT#</span>'
,p_item_post_text=>'<span class="t-Form-itemText t-Form-itemText--post">#CURRENT_ITEM_POST_TEXT#</span>'
,p_before_element=>'<div class="t-Form-inputContainer"><div class="t-Form-itemWrapper">#ITEM_PRE_TEXT#'
,p_after_element=>'#ITEM_POST_TEXT##HELP_TEMPLATE#</div>#INLINE_HELP_TEMPLATE##ERROR_TEMPLATE#</div>'
,p_help_link=>'<button class="t-Form-helpButton js-itemHelp" data-itemhelp="#CURRENT_ITEM_ID#" title="#CURRENT_ITEM_HELP_LABEL#" aria-label="#CURRENT_ITEM_HELP_LABEL#" tabindex="-1" type="button"><span class="a-Icon icon-help" aria-hidden="true"></span></button>'
,p_inline_help_text=>'<span class="t-Form-inlineHelp">#CURRENT_ITEM_INLINE_HELP_TEXT#</span>'
,p_error_template=>'<span class="t-Form-error">#ERROR_MESSAGE#</span>'
,p_theme_id=>42
,p_theme_class_id=>4
,p_reference_id=>1607675344320152883
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/breadcrumb/breadcrumb
begin
wwv_flow_api.create_menu_template(
 p_id=>wwv_flow_api.id(29009807487135125)
,p_name=>'Breadcrumb'
,p_internal_name=>'BREADCRUMB'
,p_before_first=>'<ul class="t-Breadcrumb #COMPONENT_CSS_CLASSES#">'
,p_current_page_option=>'<li class="t-Breadcrumb-item is-active"><h1 class="t-Breadcrumb-label">#NAME#</h1></li>'
,p_non_current_page_option=>'<li class="t-Breadcrumb-item"><a href="#LINK#" class="t-Breadcrumb-label">#NAME#</a></li>'
,p_after_last=>'</ul>'
,p_max_levels=>6
,p_start_with_node=>'PARENT_TO_LEAF'
,p_theme_id=>42
,p_theme_class_id=>1
,p_reference_id=>4070916542570059325
,p_translate_this_template=>'N'
);
end;
/
prompt --application/shared_components/user_interface/templates/popuplov
begin
wwv_flow_api.create_popup_lov_template(
 p_id=>wwv_flow_api.id(29010004473135122)
,p_page_name=>'winlov'
,p_page_title=>'Search Dialog'
,p_page_html_head=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<!DOCTYPE html>',
'<html lang="&BROWSER_LANGUAGE.">',
'<head>',
'<title>#TITLE#</title>',
'#APEX_CSS#',
'#THEME_CSS#',
'#THEME_STYLE_CSS#',
'#FAVICONS#',
'#APEX_JAVASCRIPT#',
'#THEME_JAVASCRIPT#',
'<meta name="viewport" content="width=device-width,initial-scale=1.0" />',
'</head>'))
,p_page_body_attr=>'onload="first_field()" class="t-Page t-Page--popupLOV"'
,p_before_field_text=>'<div class="t-PopupLOV-actions t-Form--large">'
,p_filter_width=>'20'
,p_filter_max_width=>'100'
,p_filter_text_attr=>'class="apex-item-text"'
,p_find_button_text=>'Search'
,p_find_button_attr=>'class="t-Button t-Button--hot t-Button--padLeft"'
,p_close_button_text=>'Close'
,p_close_button_attr=>'class="t-Button u-pullRight"'
,p_next_button_text=>'Next &gt;'
,p_next_button_attr=>'class="t-Button t-PopupLOV-button"'
,p_prev_button_text=>'&lt; Previous'
,p_prev_button_attr=>'class="t-Button t-PopupLOV-button"'
,p_after_field_text=>'</div>'
,p_scrollbars=>'1'
,p_resizable=>'1'
,p_width=>'380'
,p_result_row_x_of_y=>'<div class="t-PopupLOV-pagination">Row(s) #FIRST_ROW# - #LAST_ROW#</div>'
,p_result_rows_per_pg=>100
,p_before_result_set=>'<div class="t-PopupLOV-links">'
,p_theme_id=>42
,p_theme_class_id=>1
,p_reference_id=>2885398517835871876
,p_translate_this_template=>'N'
,p_after_result_set=>'</div>'
);
end;
/
prompt --application/shared_components/user_interface/templates/calendar/calendar
begin
wwv_flow_api.create_calendar_template(
 p_id=>wwv_flow_api.id(29009991750135124)
,p_cal_template_name=>'Calendar'
,p_internal_name=>'CALENDAR'
,p_day_of_week_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<th id="#DY#" scope="col" class="t-ClassicCalendar-dayColumn">',
'  <span class="visible-md visible-lg">#IDAY#</span>',
'  <span class="hidden-md hidden-lg">#IDY#</span>',
'</th>'))
,p_month_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-ClassicCalendar">',
'<h1 class="t-ClassicCalendar-title">#IMONTH# #YYYY#</h1>'))
,p_month_open_format=>'<table class="t-ClassicCalendar-calendar" cellpadding="0" cellspacing="0" border="0" summary="#IMONTH# #YYYY#">'
,p_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table>',
'</div>',
''))
,p_day_title_format=>'<span class="t-ClassicCalendar-date">#DD#</span>'
,p_day_open_format=>'<td class="t-ClassicCalendar-day" headers="#DY#">#TITLE_FORMAT#<div class="t-ClassicCalendar-dayEvents">#DATA#</div>'
,p_day_close_format=>'</td>'
,p_today_open_format=>'<td class="t-ClassicCalendar-day is-today" headers="#DY#">#TITLE_FORMAT#<div class="t-ClassicCalendar-dayEvents">#DATA#</div>'
,p_weekend_title_format=>'<span class="t-ClassicCalendar-date">#DD#</span>'
,p_weekend_open_format=>'<td class="t-ClassicCalendar-day is-weekend" headers="#DY#">#TITLE_FORMAT#<div class="t-ClassicCalendar-dayEvents">#DATA#</div>'
,p_weekend_close_format=>'</td>'
,p_nonday_title_format=>'<span class="t-ClassicCalendar-date">#DD#</span>'
,p_nonday_open_format=>'<td class="t-ClassicCalendar-day is-inactive" headers="#DY#">'
,p_nonday_close_format=>'</td>'
,p_week_open_format=>'<tr>'
,p_week_close_format=>'</tr> '
,p_daily_title_format=>'<table cellspacing="0" cellpadding="0" border="0" summary="" class="t1DayCalendarHolder"> <tr> <td class="t1MonthTitle">#IMONTH# #DD#, #YYYY#</td> </tr> <tr> <td>'
,p_daily_open_format=>'<tr>'
,p_daily_close_format=>'</tr>'
,p_weekly_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-ClassicCalendar t-ClassicCalendar--weekly">',
'<h1 class="t-ClassicCalendar-title">#WTITLE#</h1>'))
,p_weekly_day_of_week_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<th scope="col" class="t-ClassicCalendar-dayColumn" id="#DY#">',
'  <span class="visible-md visible-lg">#DD# #IDAY#</span>',
'  <span class="hidden-md hidden-lg">#DD# #IDY#</span>',
'</th>'))
,p_weekly_month_open_format=>'<table border="0" cellpadding="0" cellspacing="0" summary="#CALENDAR_TITLE# #START_DL# - #END_DL#" class="t-ClassicCalendar-calendar">'
,p_weekly_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table>',
'</div>'))
,p_weekly_day_open_format=>'<td class="t-ClassicCalendar-day" headers="#DY#"><div class="t-ClassicCalendar-dayEvents">'
,p_weekly_day_close_format=>'</div></td>'
,p_weekly_today_open_format=>'<td class="t-ClassicCalendar-day is-today" headers="#DY#"><div class="t-ClassicCalendar-dayEvents">'
,p_weekly_weekend_open_format=>'<td class="t-ClassicCalendar-day is-weekend" headers="#DY#"><div class="t-ClassicCalendar-dayEvents">'
,p_weekly_weekend_close_format=>'</div></td>'
,p_weekly_time_open_format=>'<th scope="row" class="t-ClassicCalendar-day t-ClassicCalendar-timeCol">'
,p_weekly_time_close_format=>'</th>'
,p_weekly_time_title_format=>'#TIME#'
,p_weekly_hour_open_format=>'<tr>'
,p_weekly_hour_close_format=>'</tr>'
,p_daily_day_of_week_format=>'<th scope="col" id="#DY#" class="t-ClassicCalendar-dayColumn">#IDAY#</th>'
,p_daily_month_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-ClassicCalendar t-ClassicCalendar--daily">',
'<h1 class="t-ClassicCalendar-title">#IMONTH# #DD#, #YYYY#</h1>'))
,p_daily_month_open_format=>'<table border="0" cellpadding="0" cellspacing="0" summary="#CALENDAR_TITLE# #START_DL#" class="t-ClassicCalendar-calendar">'
,p_daily_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table>',
'</div>'))
,p_daily_day_open_format=>'<td class="t-ClassicCalendar-day" headers="#DY#"><div class="t-ClassicCalendar-dayEvents">'
,p_daily_day_close_format=>'</div></td>'
,p_daily_today_open_format=>'<td class="t-ClassicCalendar-day is-today" headers="#DY#"><div class="t-ClassicCalendar-dayEvents">'
,p_daily_time_open_format=>'<th scope="row" class="t-ClassicCalendar-day t-ClassicCalendar-timeCol" id="#TIME#">'
,p_daily_time_close_format=>'</th>'
,p_daily_time_title_format=>'#TIME#'
,p_daily_hour_open_format=>'<tr>'
,p_daily_hour_close_format=>'</tr>'
,p_cust_month_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-ClassicCalendar">',
'<h1 class="t-ClassicCalendar-title">#IMONTH# #YYYY#</h1>'))
,p_cust_day_of_week_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<th id="#DY#" scope="col" class="t-ClassicCalendar-dayColumn">',
'  <span class="visible-md visible-lg">#IDAY#</span>',
'  <span class="hidden-md hidden-lg">#IDY#</span>',
'</th>'))
,p_cust_month_open_format=>'<table class="t-ClassicCalendar-calendar" cellpadding="0" cellspacing="0" border="0" summary="#IMONTH# #YYYY#">'
,p_cust_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table>',
'</div>'))
,p_cust_week_open_format=>'<tr>'
,p_cust_week_close_format=>'</tr> '
,p_cust_day_title_format=>'<span class="t-ClassicCalendar-date">#DD#</span>'
,p_cust_day_open_format=>'<td class="t-ClassicCalendar-day" headers="#DY#">'
,p_cust_day_close_format=>'</td>'
,p_cust_today_open_format=>'<td class="t-ClassicCalendar-day is-today" headers="#DY#">'
,p_cust_nonday_title_format=>'<span class="t-ClassicCalendar-date">#DD#</span>'
,p_cust_nonday_open_format=>'<td class="t-ClassicCalendar-day is-inactive" headers="#DY#">'
,p_cust_nonday_close_format=>'</td>'
,p_cust_weekend_title_format=>'<span class="t-ClassicCalendar-date">#DD#</span>'
,p_cust_weekend_open_format=>'<td class="t-ClassicCalendar-day is-weekend" headers="#DY#">'
,p_cust_weekend_close_format=>'</td>'
,p_cust_hour_open_format=>'<tr>'
,p_cust_hour_close_format=>'</tr>'
,p_cust_time_title_format=>'#TIME#'
,p_cust_time_open_format=>'<th scope="row" class="t-ClassicCalendar-day t-ClassicCalendar-timeCol">'
,p_cust_time_close_format=>'</th>'
,p_cust_wk_month_title_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-ClassicCalendar">',
'<h1 class="t-ClassicCalendar-title">#WTITLE#</h1>'))
,p_cust_wk_day_of_week_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<th scope="col" class="t-ClassicCalendar-dayColumn" id="#DY#">',
'  <span class="visible-md visible-lg">#DD# #IDAY#</span>',
'  <span class="hidden-md hidden-lg">#DD# #IDY#</span>',
'</th>'))
,p_cust_wk_month_open_format=>'<table border="0" cellpadding="0" cellspacing="0" summary="#CALENDAR_TITLE# #START_DL# - #END_DL#" class="t-ClassicCalendar-calendar">'
,p_cust_wk_month_close_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'</table>',
'</div>'))
,p_cust_wk_week_open_format=>'<tr>'
,p_cust_wk_week_close_format=>'</tr> '
,p_cust_wk_day_open_format=>'<td class="t-ClassicCalendar-day" headers="#DY#"><div class="t-ClassicCalendar-dayEvents">'
,p_cust_wk_day_close_format=>'</div></td>'
,p_cust_wk_today_open_format=>'<td class="t-ClassicCalendar-day is-today" headers="#DY#"><div class="t-ClassicCalendar-dayEvents">'
,p_cust_wk_weekend_open_format=>'<td class="t-ClassicCalendar-day" headers="#DY#">'
,p_cust_wk_weekend_close_format=>'</td>'
,p_agenda_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<div class="t-ClassicCalendar t-ClassicCalendar--list">',
'  <div class="t-ClassicCalendar-title">#IMONTH# #YYYY#</div>',
'  <ul class="t-ClassicCalendar-list">',
'    #DAYS#',
'  </ul>',
'</div>'))
,p_agenda_past_day_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  <li class="t-ClassicCalendar-listTitle is-past">',
'    <span class="t-ClassicCalendar-listDayTitle">#IDAY#</span><span class="t-ClassicCalendar-listDayDate">#IMONTH# #DD#</span>',
'  </li>'))
,p_agenda_today_day_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  <li class="t-ClassicCalendar-listTitle is-today">',
'    <span class="t-ClassicCalendar-listDayTitle">#IDAY#</span><span class="t-ClassicCalendar-listDayDate">#IMONTH# #DD#</span>',
'  </li>'))
,p_agenda_future_day_format=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  <li class="t-ClassicCalendar-listTitle is-future">',
'    <span class="t-ClassicCalendar-listDayTitle">#IDAY#</span><span class="t-ClassicCalendar-listDayDate">#IMONTH# #DD#</span>',
'  </li>'))
,p_agenda_past_entry_format=>'  <li class="t-ClassicCalendar-listEvent is-past">#DATA#</li>'
,p_agenda_today_entry_format=>'  <li class="t-ClassicCalendar-listEvent is-today">#DATA#</li>'
,p_agenda_future_entry_format=>'  <li class="t-ClassicCalendar-listEvent is-future">#DATA#</li>'
,p_month_data_format=>'#DAYS#'
,p_month_data_entry_format=>'<span class="t-ClassicCalendar-event">#DATA#</span>'
,p_theme_id=>42
,p_theme_class_id=>1
,p_reference_id=>4070916747979059326
);
end;
/
prompt --application/shared_components/user_interface/themes
begin
wwv_flow_api.create_theme(
 p_id=>wwv_flow_api.id(29011241088135118)
,p_theme_id=>42
,p_theme_name=>'Universal Theme'
,p_theme_internal_name=>'UNIVERSAL_THEME'
,p_ui_type_name=>'DESKTOP'
,p_navigation_type=>'L'
,p_nav_bar_type=>'LIST'
,p_reference_id=>4070917134413059350
,p_is_locked=>false
,p_default_page_template=>wwv_flow_api.id(28929261131135180)
,p_default_dialog_template=>wwv_flow_api.id(28914246562135185)
,p_error_template=>wwv_flow_api.id(28915702668135184)
,p_printer_friendly_template=>wwv_flow_api.id(28929261131135180)
,p_breadcrumb_display_point=>'REGION_POSITION_01'
,p_sidebar_display_point=>'REGION_POSITION_02'
,p_login_template=>wwv_flow_api.id(28915702668135184)
,p_default_button_template=>wwv_flow_api.id(29009020462135125)
,p_default_region_template=>wwv_flow_api.id(28956997224135161)
,p_default_chart_template=>wwv_flow_api.id(28956997224135161)
,p_default_form_template=>wwv_flow_api.id(28956997224135161)
,p_default_reportr_template=>wwv_flow_api.id(28956997224135161)
,p_default_tabform_template=>wwv_flow_api.id(28956997224135161)
,p_default_wizard_template=>wwv_flow_api.id(28956997224135161)
,p_default_menur_template=>wwv_flow_api.id(28966305468135152)
,p_default_listr_template=>wwv_flow_api.id(28956997224135161)
,p_default_irr_template=>wwv_flow_api.id(28955876667135161)
,p_default_report_template=>wwv_flow_api.id(28979442408135144)
,p_default_label_template=>wwv_flow_api.id(29008583468135127)
,p_default_menu_template=>wwv_flow_api.id(29009807487135125)
,p_default_calendar_template=>wwv_flow_api.id(29009991750135124)
,p_default_list_template=>wwv_flow_api.id(29006620859135129)
,p_default_nav_list_template=>wwv_flow_api.id(28998625185135135)
,p_default_top_nav_list_temp=>wwv_flow_api.id(28998625185135135)
,p_default_side_nav_list_temp=>wwv_flow_api.id(28998212159135136)
,p_default_nav_list_position=>'SIDE'
,p_default_dialogbtnr_template=>wwv_flow_api.id(28936438753135170)
,p_default_dialogr_template=>wwv_flow_api.id(28935454342135171)
,p_default_option_label=>wwv_flow_api.id(29008583468135127)
,p_default_required_label=>wwv_flow_api.id(29008861129135127)
,p_default_page_transition=>'NONE'
,p_default_popup_transition=>'NONE'
,p_default_navbar_list_template=>wwv_flow_api.id(28999682801135131)
,p_file_prefix => nvl(wwv_flow_application_install.get_static_theme_file_prefix(42),'#IMAGE_PREFIX#themes/theme_42/1.2/')
,p_files_version=>62
,p_icon_library=>'FONTAPEX'
,p_javascript_file_urls=>wwv_flow_string.join(wwv_flow_t_varchar2(
'#IMAGE_PREFIX#libraries/apex/#MIN_DIRECTORY#widget.stickyWidget#MIN#.js?v=#APEX_VERSION#',
'#THEME_IMAGES#js/theme42#MIN#.js?v=#APEX_VERSION#'))
,p_css_file_urls=>'#THEME_IMAGES#css/Core#MIN#.css?v=#APEX_VERSION#'
);
end;
/
prompt --application/shared_components/user_interface/theme_style
begin
wwv_flow_api.create_theme_style(
 p_id=>wwv_flow_api.id(29010277760135122)
,p_theme_id=>42
,p_name=>'Vista'
,p_css_file_urls=>'#THEME_IMAGES#css/Vista#MIN#.css?v=#APEX_VERSION#'
,p_is_current=>false
,p_is_public=>true
,p_is_accessible=>false
,p_theme_roller_read_only=>true
,p_reference_id=>4007676303523989775
);
wwv_flow_api.create_theme_style(
 p_id=>wwv_flow_api.id(29010429024135122)
,p_theme_id=>42
,p_name=>'Vita'
,p_is_current=>true
,p_is_public=>true
,p_is_accessible=>true
,p_theme_roller_input_file_urls=>'#THEME_IMAGES#less/theme/Vita.less'
,p_theme_roller_output_file_url=>'#THEME_IMAGES#css/Vita#MIN#.css?v=#APEX_VERSION#'
,p_theme_roller_read_only=>true
,p_reference_id=>2719875314571594493
);
wwv_flow_api.create_theme_style(
 p_id=>wwv_flow_api.id(29010640980135121)
,p_theme_id=>42
,p_name=>'Vita - Dark'
,p_is_current=>false
,p_is_public=>true
,p_is_accessible=>false
,p_theme_roller_input_file_urls=>'#THEME_IMAGES#less/theme/Vita-Dark.less'
,p_theme_roller_output_file_url=>'#THEME_IMAGES#css/Vita-Dark#MIN#.css?v=#APEX_VERSION#'
,p_theme_roller_read_only=>true
,p_reference_id=>3543348412015319650
);
wwv_flow_api.create_theme_style(
 p_id=>wwv_flow_api.id(29010881935135121)
,p_theme_id=>42
,p_name=>'Vita - Red'
,p_is_current=>false
,p_is_public=>true
,p_is_accessible=>false
,p_theme_roller_input_file_urls=>'#THEME_IMAGES#less/theme/Vita-Red.less'
,p_theme_roller_output_file_url=>'#THEME_IMAGES#css/Vita-Red#MIN#.css?v=#APEX_VERSION#'
,p_theme_roller_read_only=>true
,p_reference_id=>1938457712423918173
);
wwv_flow_api.create_theme_style(
 p_id=>wwv_flow_api.id(29011085637135121)
,p_theme_id=>42
,p_name=>'Vita - Slate'
,p_is_current=>false
,p_is_public=>true
,p_is_accessible=>false
,p_theme_roller_input_file_urls=>'#THEME_IMAGES#less/theme/Vita-Slate.less'
,p_theme_roller_config=>'{"customCSS":"","vars":{"@g_Accent-BG":"#505f6d","@g_Accent-OG":"#ececec","@g_Body-Title-BG":"#dee1e4","@l_Link-Base":"#337ac0","@g_Body-BG":"#f5f5f5"}}'
,p_theme_roller_output_file_url=>'#THEME_IMAGES#css/Vita-Slate#MIN#.css?v=#APEX_VERSION#'
,p_theme_roller_read_only=>true
,p_reference_id=>3291983347983194966
);
end;
/
prompt --application/shared_components/user_interface/theme_files
begin
null;
end;
/
prompt --application/shared_components/user_interface/theme_display_points
begin
null;
end;
/
prompt --application/shared_components/user_interface/template_opt_groups
begin
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28932536977135171)
,p_theme_id=>42
,p_name=>'ALERT_TYPE'
,p_display_name=>'Alert Type'
,p_display_sequence=>3
,p_template_types=>'REGION'
,p_help_text=>'Sets the type of alert which can be used to determine the icon, icon color, and the background color.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28932961501135171)
,p_theme_id=>42
,p_name=>'ALERT_TITLE'
,p_display_name=>'Alert Title'
,p_display_sequence=>40
,p_template_types=>'REGION'
,p_help_text=>'Determines how the title of the alert is displayed.'
,p_null_text=>'Visible - Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28933525149135171)
,p_theme_id=>42
,p_name=>'ALERT_ICONS'
,p_display_name=>'Alert Icons'
,p_display_sequence=>2
,p_template_types=>'REGION'
,p_help_text=>'Sets how icons are handled for the Alert Region.'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28933942113135171)
,p_theme_id=>42
,p_name=>'ALERT_DISPLAY'
,p_display_name=>'Alert Display'
,p_display_sequence=>1
,p_template_types=>'REGION'
,p_help_text=>'Sets the layout of the Alert Region.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28937352081135170)
,p_theme_id=>42
,p_name=>'STYLE'
,p_display_name=>'Style'
,p_display_sequence=>40
,p_template_types=>'REGION'
,p_help_text=>'Determines how the region is styled. Use the "Remove Borders" template option to remove the region''s borders and shadows.'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28937754447135170)
,p_theme_id=>42
,p_name=>'BODY_PADDING'
,p_display_name=>'Body Padding'
,p_display_sequence=>1
,p_template_types=>'REGION'
,p_help_text=>'Sets the Region Body padding for the region.'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28939579398135169)
,p_theme_id=>42
,p_name=>'TIMER'
,p_display_name=>'Timer'
,p_display_sequence=>2
,p_template_types=>'REGION'
,p_help_text=>'Sets the timer for when to automatically navigate to the next region within the Carousel Region.'
,p_null_text=>'No Timer'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28940333624135169)
,p_theme_id=>42
,p_name=>'BODY_HEIGHT'
,p_display_name=>'Body Height'
,p_display_sequence=>10
,p_template_types=>'REGION'
,p_help_text=>'Sets the Region Body height. You can also specify a custom height by modifying the Region''s CSS Classes and using the height helper classes "i-hXXX" where XXX is any increment of 10 from 100 to 800.'
,p_null_text=>'Auto - Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28941506456135168)
,p_theme_id=>42
,p_name=>'ACCENT'
,p_display_name=>'Accent'
,p_display_sequence=>30
,p_template_types=>'REGION'
,p_help_text=>'Set the Region''s accent. This accent corresponds to a Theme-Rollable color and sets the background of the Region''s Header.'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28942767777135168)
,p_theme_id=>42
,p_name=>'HEADER'
,p_display_name=>'Header'
,p_display_sequence=>20
,p_template_types=>'REGION'
,p_help_text=>'Determines the display of the Region Header which also contains the Region Title.'
,p_null_text=>'Visible - Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28943151335135168)
,p_theme_id=>42
,p_name=>'BODY_OVERFLOW'
,p_display_name=>'Body Overflow'
,p_display_sequence=>1
,p_template_types=>'REGION'
,p_help_text=>'Determines the scroll behavior when the region contents are larger than their container.'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28945101934135167)
,p_theme_id=>42
,p_name=>'ANIMATION'
,p_display_name=>'Animation'
,p_display_sequence=>10
,p_template_types=>'REGION'
,p_help_text=>'Sets the animation when navigating within the Carousel Region.'
,p_null_text=>'Fade'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28948541601135166)
,p_theme_id=>42
,p_name=>'DEFAULT_STATE'
,p_display_name=>'Default State'
,p_display_sequence=>1
,p_template_types=>'REGION'
,p_help_text=>'Sets the default state of the region.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28950914960135162)
,p_theme_id=>42
,p_name=>'REGION_TITLE'
,p_display_name=>'Region Title'
,p_display_sequence=>1
,p_template_types=>'REGION'
,p_help_text=>'Sets the source of the Title Bar region''s title.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28951723657135162)
,p_theme_id=>42
,p_name=>'BODY_STYLE'
,p_display_name=>'Body Style'
,p_display_sequence=>20
,p_template_types=>'REGION'
,p_help_text=>'Controls the display of the region''s body container.'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28952854722135162)
,p_theme_id=>42
,p_name=>'DISPLAY_ICON'
,p_display_name=>'Display Icon'
,p_display_sequence=>50
,p_template_types=>'REGION'
,p_help_text=>'Display the Hero Region icon.'
,p_null_text=>'Yes (Default)'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28954749725135161)
,p_theme_id=>42
,p_name=>'DIALOG_SIZE'
,p_display_name=>'Dialog Size'
,p_display_sequence=>1
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28964617727135152)
,p_theme_id=>42
,p_name=>'LAYOUT'
,p_display_name=>'Layout'
,p_display_sequence=>1
,p_template_types=>'REGION'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28965023310135152)
,p_theme_id=>42
,p_name=>'TAB_STYLE'
,p_display_name=>'Tab Style'
,p_display_sequence=>1
,p_template_types=>'REGION'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28965855241135152)
,p_theme_id=>42
,p_name=>'TABS_SIZE'
,p_display_name=>'Tabs Size'
,p_display_sequence=>1
,p_template_types=>'REGION'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28967944066135151)
,p_theme_id=>42
,p_name=>'HIDE_STEPS_FOR'
,p_display_name=>'Hide Steps For'
,p_display_sequence=>1
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28968997129135150)
,p_theme_id=>42
,p_name=>'STYLE'
,p_display_name=>'Style'
,p_display_sequence=>10
,p_template_types=>'REPORT'
,p_help_text=>'Determines the overall style for the component.'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28969514287135149)
,p_theme_id=>42
,p_name=>'LAYOUT'
,p_display_name=>'Layout'
,p_display_sequence=>30
,p_template_types=>'REPORT'
,p_help_text=>'Determines the layout of Cards in the report.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28970733811135149)
,p_theme_id=>42
,p_name=>'SIZE'
,p_display_name=>'Size'
,p_display_sequence=>35
,p_template_types=>'REPORT'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28972394054135149)
,p_theme_id=>42
,p_name=>'LABEL_WIDTH'
,p_display_name=>'Label Width'
,p_display_sequence=>10
,p_template_types=>'REPORT'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28976104794135145)
,p_theme_id=>42
,p_name=>'BADGE_SIZE'
,p_display_name=>'Badge Size'
,p_display_sequence=>10
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28979742750135144)
,p_theme_id=>42
,p_name=>'ALTERNATING_ROWS'
,p_display_name=>'Alternating Rows'
,p_display_sequence=>10
,p_template_types=>'REPORT'
,p_help_text=>'Shades alternate rows in the report with slightly different background colors.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28980310033135144)
,p_theme_id=>42
,p_name=>'ROW_HIGHLIGHTING'
,p_display_name=>'Row Highlighting'
,p_display_sequence=>20
,p_template_types=>'REPORT'
,p_help_text=>'Determines whether you want the row to be highlighted on hover.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28980754118135144)
,p_theme_id=>42
,p_name=>'REPORT_BORDER'
,p_display_name=>'Report Border'
,p_display_sequence=>30
,p_template_types=>'REPORT'
,p_help_text=>'Controls the display of the Report''s borders.'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28982301271135143)
,p_theme_id=>42
,p_name=>'COMMENTS_STYLE'
,p_display_name=>'Comments Style'
,p_display_sequence=>10
,p_template_types=>'REPORT'
,p_help_text=>'Determines the style in which comments are displayed.'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28983521501135142)
,p_theme_id=>42
,p_name=>'BODY_TEXT'
,p_display_name=>'Body Text'
,p_display_sequence=>40
,p_template_types=>'REPORT'
,p_help_text=>'Determines the height of the card body.'
,p_null_text=>'Auto'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28985304625135142)
,p_theme_id=>42
,p_name=>'ANIMATION'
,p_display_name=>'Animation'
,p_display_sequence=>70
,p_template_types=>'REPORT'
,p_help_text=>'Sets the hover and focus animation.'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28986156456135142)
,p_theme_id=>42
,p_name=>'ICONS'
,p_display_name=>'Icons'
,p_display_sequence=>20
,p_template_types=>'REPORT'
,p_help_text=>'Controls how to handle icons in the report.'
,p_null_text=>'No Icons'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28988353221135139)
,p_theme_id=>42
,p_name=>'MOBILE'
,p_display_name=>'Mobile'
,p_display_sequence=>100
,p_template_types=>'LIST'
,p_help_text=>'Determines the display for a mobile-sized screen'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28988932787135139)
,p_theme_id=>42
,p_name=>'DESKTOP'
,p_display_name=>'Desktop'
,p_display_sequence=>90
,p_template_types=>'LIST'
,p_help_text=>'Determines the display for a desktop-sized screen'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28989955641135138)
,p_theme_id=>42
,p_name=>'LABEL_DISPLAY'
,p_display_name=>'Label Display'
,p_display_sequence=>50
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28992182569135137)
,p_theme_id=>42
,p_name=>'LAYOUT'
,p_display_name=>'Layout'
,p_display_sequence=>30
,p_template_types=>'LIST'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28993375643135137)
,p_theme_id=>42
,p_name=>'STYLE'
,p_display_name=>'Style'
,p_display_sequence=>10
,p_template_types=>'LIST'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28994548485135136)
,p_theme_id=>42
,p_name=>'BADGE_SIZE'
,p_display_name=>'Badge Size'
,p_display_sequence=>70
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(28997194397135136)
,p_theme_id=>42
,p_name=>'SIZE'
,p_display_name=>'Size'
,p_display_sequence=>1
,p_template_types=>'LIST'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29000382966135131)
,p_theme_id=>42
,p_name=>'BODY_TEXT'
,p_display_name=>'Body Text'
,p_display_sequence=>40
,p_template_types=>'LIST'
,p_help_text=>'Determines the height of the card body.'
,p_null_text=>'Auto'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29002335931135130)
,p_theme_id=>42
,p_name=>'ANIMATION'
,p_display_name=>'Animation'
,p_display_sequence=>80
,p_template_types=>'LIST'
,p_help_text=>'Sets the hover and focus animation.'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29002922113135130)
,p_theme_id=>42
,p_name=>'ICONS'
,p_display_name=>'Icons'
,p_display_sequence=>20
,p_template_types=>'LIST'
,p_null_text=>'No Icons'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29007766236135128)
,p_theme_id=>42
,p_name=>'DISPLAY_ICONS'
,p_display_name=>'Display Icons'
,p_display_sequence=>30
,p_template_types=>'LIST'
,p_null_text=>'No Icons'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29009326980135125)
,p_theme_id=>42
,p_name=>'ICON_POSITION'
,p_display_name=>'Icon Position'
,p_display_sequence=>50
,p_template_types=>'BUTTON'
,p_help_text=>'Sets the position of the icon relative to the label.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29011444010135114)
,p_theme_id=>42
,p_name=>'BOTTOM_MARGIN'
,p_display_name=>'Bottom Margin'
,p_display_sequence=>220
,p_template_types=>'FIELD'
,p_help_text=>'Set the bottom margin for this field.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29011846526135114)
,p_theme_id=>42
,p_name=>'REGION_BOTTOM_MARGIN'
,p_display_name=>'Bottom Margin'
,p_display_sequence=>210
,p_template_types=>'REGION'
,p_help_text=>'Set the bottom margin for this region.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29013441899135114)
,p_theme_id=>42
,p_name=>'LEFT_MARGIN'
,p_display_name=>'Left Margin'
,p_display_sequence=>220
,p_template_types=>'FIELD'
,p_help_text=>'Set the left margin for this field.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29013898525135114)
,p_theme_id=>42
,p_name=>'REGION_LEFT_MARGIN'
,p_display_name=>'Left Margin'
,p_display_sequence=>220
,p_template_types=>'REGION'
,p_help_text=>'Set the left margin for this region.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29015484693135113)
,p_theme_id=>42
,p_name=>'RIGHT_MARGIN'
,p_display_name=>'Right Margin'
,p_display_sequence=>230
,p_template_types=>'FIELD'
,p_help_text=>'Set the right margin for this field.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29015834039135113)
,p_theme_id=>42
,p_name=>'REGION_RIGHT_MARGIN'
,p_display_name=>'Right Margin'
,p_display_sequence=>230
,p_template_types=>'REGION'
,p_help_text=>'Set the right margin for this region.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29017459993135113)
,p_theme_id=>42
,p_name=>'TOP_MARGIN'
,p_display_name=>'Top Margin'
,p_display_sequence=>200
,p_template_types=>'FIELD'
,p_help_text=>'Set the top margin for this field.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29017833520135113)
,p_theme_id=>42
,p_name=>'REGION_TOP_MARGIN'
,p_display_name=>'Top Margin'
,p_display_sequence=>200
,p_template_types=>'REGION'
,p_help_text=>'Set the top margin for this region.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29019437388135112)
,p_theme_id=>42
,p_name=>'TYPE'
,p_display_name=>'Type'
,p_display_sequence=>20
,p_template_types=>'BUTTON'
,p_null_text=>'Normal'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29019872084135112)
,p_theme_id=>42
,p_name=>'SPACING_BOTTOM'
,p_display_name=>'Spacing Bottom'
,p_display_sequence=>100
,p_template_types=>'BUTTON'
,p_help_text=>'Controls the spacing to the bottom of the button.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29020233236135112)
,p_theme_id=>42
,p_name=>'SPACING_LEFT'
,p_display_name=>'Spacing Left'
,p_display_sequence=>70
,p_template_types=>'BUTTON'
,p_help_text=>'Controls the spacing to the left of the button.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29020676705135112)
,p_theme_id=>42
,p_name=>'SPACING_RIGHT'
,p_display_name=>'Spacing Right'
,p_display_sequence=>80
,p_template_types=>'BUTTON'
,p_help_text=>'Controls the spacing to the right of the button.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29021071146135112)
,p_theme_id=>42
,p_name=>'SPACING_TOP'
,p_display_name=>'Spacing Top'
,p_display_sequence=>90
,p_template_types=>'BUTTON'
,p_help_text=>'Controls the spacing to the top of the button.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29021406045135112)
,p_theme_id=>42
,p_name=>'SIZE'
,p_display_name=>'Size'
,p_display_sequence=>10
,p_template_types=>'BUTTON'
,p_help_text=>'Sets the size of the button.'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29021842886135112)
,p_theme_id=>42
,p_name=>'STYLE'
,p_display_name=>'Style'
,p_display_sequence=>30
,p_template_types=>'BUTTON'
,p_help_text=>'Sets the style of the button. Use the "Simple" option for secondary actions or sets of buttons. Use the "Remove UI Decoration" option to make the button appear as text.'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29023248271135112)
,p_theme_id=>42
,p_name=>'BUTTON_SET'
,p_display_name=>'Button Set'
,p_display_sequence=>40
,p_template_types=>'BUTTON'
,p_help_text=>'Enables you to group many buttons together into a pill. You can use this option to specify where the button is within this set. Set the option to Default if this button is not part of a button set.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29024681779135111)
,p_theme_id=>42
,p_name=>'WIDTH'
,p_display_name=>'Width'
,p_display_sequence=>60
,p_template_types=>'BUTTON'
,p_help_text=>'Sets the width of the button.'
,p_null_text=>'Auto - Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29025662985135111)
,p_theme_id=>42
,p_name=>'LABEL_POSITION'
,p_display_name=>'Label Position'
,p_display_sequence=>140
,p_template_types=>'REGION'
,p_help_text=>'Sets the position of the label relative to the form item.'
,p_null_text=>'Inline - Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29026082898135111)
,p_theme_id=>42
,p_name=>'ITEM_SIZE'
,p_display_name=>'Item Size'
,p_display_sequence=>110
,p_template_types=>'REGION'
,p_help_text=>'Sets the size of the form items within this region.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29026460276135111)
,p_theme_id=>42
,p_name=>'LABEL_ALIGNMENT'
,p_display_name=>'Label Alignment'
,p_display_sequence=>130
,p_template_types=>'REGION'
,p_help_text=>'Set the label text alignment for items within this region.'
,p_null_text=>'Right'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29026815865135111)
,p_theme_id=>42
,p_name=>'ITEM_PADDING'
,p_display_name=>'Item Padding'
,p_display_sequence=>100
,p_template_types=>'REGION'
,p_help_text=>'Sets the padding around items within this region.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29027446507135110)
,p_theme_id=>42
,p_name=>'ITEM_WIDTH'
,p_display_name=>'Item Width'
,p_display_sequence=>120
,p_template_types=>'REGION'
,p_help_text=>'Sets the width of the form items within this region.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29028095758135110)
,p_theme_id=>42
,p_name=>'SIZE'
,p_display_name=>'Size'
,p_display_sequence=>10
,p_template_types=>'FIELD'
,p_null_text=>'Default'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29028448234135110)
,p_theme_id=>42
,p_name=>'ITEM_POST_TEXT'
,p_display_name=>'Item Post Text'
,p_display_sequence=>30
,p_template_types=>'FIELD'
,p_help_text=>'Adjust the display of the Item Post Text'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29028834662135110)
,p_theme_id=>42
,p_name=>'ITEM_PRE_TEXT'
,p_display_name=>'Item Pre Text'
,p_display_sequence=>20
,p_template_types=>'FIELD'
,p_help_text=>'Adjust the display of the Item Pre Text'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29029269839135110)
,p_theme_id=>42
,p_name=>'RADIO_GROUP_DISPLAY'
,p_display_name=>'Item Group Display'
,p_display_sequence=>300
,p_template_types=>'FIELD'
,p_help_text=>'Determines the display style for radio and check box items.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
wwv_flow_api.create_template_opt_group(
 p_id=>wwv_flow_api.id(29030073696135110)
,p_theme_id=>42
,p_name=>'PAGINATION_DISPLAY'
,p_display_name=>'Pagination Display'
,p_display_sequence=>10
,p_template_types=>'REPORT'
,p_help_text=>'Controls the display of pagination for this region.'
,p_null_text=>'Default'
,p_is_advanced=>'Y'
);
end;
/
prompt --application/shared_components/user_interface/template_options
begin
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28914172993135185)
,p_theme_id=>42
,p_name=>'STICKY_HEADER_ON_MOBILE'
,p_display_name=>'Sticky Header on Mobile'
,p_display_sequence=>100
,p_page_template_id=>wwv_flow_api.id(28911168797135199)
,p_css_classes=>'js-pageStickyMobileHeader'
,p_template_types=>'PAGE'
,p_help_text=>'This will position the contents of the Breadcrumb Bar region position so it sticks to the top of the screen for small screens.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28915486398135185)
,p_theme_id=>42
,p_name=>'REMOVE_BODY_PADDING'
,p_display_name=>'Remove Body Padding'
,p_display_sequence=>20
,p_page_template_id=>wwv_flow_api.id(28914246562135185)
,p_css_classes=>'t-Dialog--noPadding'
,p_template_types=>'PAGE'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28915646737135184)
,p_theme_id=>42
,p_name=>'STRETCH_TO_FIT_WINDOW'
,p_display_name=>'Stretch to Fit Window'
,p_display_sequence=>1
,p_page_template_id=>wwv_flow_api.id(28914246562135185)
,p_css_classes=>'ui-dialog--stretch'
,p_template_types=>'PAGE'
,p_help_text=>'Stretch the dialog to fit the browser window.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28917782125135184)
,p_theme_id=>42
,p_name=>'REMOVE_BODY_PADDING'
,p_display_name=>'Remove Body Padding'
,p_display_sequence=>20
,p_page_template_id=>wwv_flow_api.id(28916532187135184)
,p_css_classes=>'t-Dialog--noPadding'
,p_template_types=>'PAGE'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28917998417135184)
,p_theme_id=>42
,p_name=>'STRETCH_TO_FIT_WINDOW'
,p_display_name=>'Stretch to Fit Window'
,p_display_sequence=>10
,p_page_template_id=>wwv_flow_api.id(28916532187135184)
,p_css_classes=>'ui-dialog--stretch'
,p_template_types=>'PAGE'
,p_help_text=>'Stretch the dialog to fit the browser window.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28920777737135183)
,p_theme_id=>42
,p_name=>'STICKY_HEADER_ON_MOBILE'
,p_display_name=>'Sticky Header on Mobile'
,p_display_sequence=>100
,p_page_template_id=>wwv_flow_api.id(28918014831135184)
,p_css_classes=>'js-pageStickyMobileHeader'
,p_template_types=>'PAGE'
,p_help_text=>'This will position the contents of the Breadcrumb Bar region position so it sticks to the top of the screen for small screens.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28923585934135182)
,p_theme_id=>42
,p_name=>'STICKY_HEADER_ON_MOBILE'
,p_display_name=>'Sticky Header on Mobile'
,p_display_sequence=>100
,p_page_template_id=>wwv_flow_api.id(28920846815135183)
,p_css_classes=>'js-pageStickyMobileHeader'
,p_template_types=>'PAGE'
,p_help_text=>'This will position the contents of the Breadcrumb Bar region position so it sticks to the top of the screen for small screens.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28926657805135181)
,p_theme_id=>42
,p_name=>'STICKY_HEADER_ON_MOBILE'
,p_display_name=>'Sticky Header on Mobile'
,p_display_sequence=>100
,p_page_template_id=>wwv_flow_api.id(28923688046135182)
,p_css_classes=>'js-pageStickyMobileHeader'
,p_template_types=>'PAGE'
,p_help_text=>'This will position the contents of the Breadcrumb Bar region position so it sticks to the top of the screen for small screens.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28929124989135180)
,p_theme_id=>42
,p_name=>'STICKY_HEADER_ON_MOBILE'
,p_display_name=>'Sticky Header on Mobile'
,p_display_sequence=>100
,p_page_template_id=>wwv_flow_api.id(28926764051135181)
,p_css_classes=>'js-pageStickyMobileHeader'
,p_template_types=>'PAGE'
,p_help_text=>'This will position the contents of the Breadcrumb Bar region position so it sticks to the top of the screen for small screens.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28931633511135179)
,p_theme_id=>42
,p_name=>'STICKY_HEADER_ON_MOBILE'
,p_display_name=>'Sticky Header on Mobile'
,p_display_sequence=>100
,p_page_template_id=>wwv_flow_api.id(28929261131135180)
,p_css_classes=>'js-pageStickyMobileHeader'
,p_template_types=>'PAGE'
,p_help_text=>'This will position the contents of the Breadcrumb Bar region position so it sticks to the top of the screen for small screens.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28932370734135172)
,p_theme_id=>42
,p_name=>'COLOREDBACKGROUND'
,p_display_name=>'Highlight Background'
,p_display_sequence=>1
,p_region_template_id=>wwv_flow_api.id(28931739757135175)
,p_css_classes=>'t-Alert--colorBG'
,p_template_types=>'REGION'
,p_help_text=>'Set alert background color to that of the alert type (warning, success, etc.)'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28932729796135171)
,p_theme_id=>42
,p_name=>'DANGER'
,p_display_name=>'Danger'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28931739757135175)
,p_css_classes=>'t-Alert--danger'
,p_group_id=>wwv_flow_api.id(28932536977135171)
,p_template_types=>'REGION'
,p_help_text=>'Show an error or danger alert.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28933102907135171)
,p_theme_id=>42
,p_name=>'HIDDENHEADER'
,p_display_name=>'Hidden but Accessible'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28931739757135175)
,p_css_classes=>'t-Alert--accessibleHeading'
,p_group_id=>wwv_flow_api.id(28932961501135171)
,p_template_types=>'REGION'
,p_help_text=>'Visually hides the alert title, but assistive technologies can still read it.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28933328005135171)
,p_theme_id=>42
,p_name=>'HIDDENHEADERNOAT'
,p_display_name=>'Hidden'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28931739757135175)
,p_css_classes=>'t-Alert--removeHeading'
,p_group_id=>wwv_flow_api.id(28932961501135171)
,p_template_types=>'REGION'
,p_help_text=>'Hides the Alert Title from being displayed.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28933735631135171)
,p_theme_id=>42
,p_name=>'HIDE_ICONS'
,p_display_name=>'Hide Icons'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28931739757135175)
,p_css_classes=>'t-Alert--noIcon'
,p_group_id=>wwv_flow_api.id(28933525149135171)
,p_template_types=>'REGION'
,p_help_text=>'Hides alert icons'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28934192045135171)
,p_theme_id=>42
,p_name=>'HORIZONTAL'
,p_display_name=>'Horizontal'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28931739757135175)
,p_css_classes=>'t-Alert--horizontal'
,p_group_id=>wwv_flow_api.id(28933942113135171)
,p_template_types=>'REGION'
,p_help_text=>'Show horizontal alert with buttons to the right.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28934392361135171)
,p_theme_id=>42
,p_name=>'INFORMATION'
,p_display_name=>'Information'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28931739757135175)
,p_css_classes=>'t-Alert--info'
,p_group_id=>wwv_flow_api.id(28932536977135171)
,p_template_types=>'REGION'
,p_help_text=>'Show informational alert.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28934535433135171)
,p_theme_id=>42
,p_name=>'SHOW_CUSTOM_ICONS'
,p_display_name=>'Show Custom Icons'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28931739757135175)
,p_css_classes=>'t-Alert--customIcons'
,p_group_id=>wwv_flow_api.id(28933525149135171)
,p_template_types=>'REGION'
,p_help_text=>'Set custom icons by modifying the Alert Region''s Icon CSS Classes property.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28934763820135171)
,p_theme_id=>42
,p_name=>'SUCCESS'
,p_display_name=>'Success'
,p_display_sequence=>40
,p_region_template_id=>wwv_flow_api.id(28931739757135175)
,p_css_classes=>'t-Alert--success'
,p_group_id=>wwv_flow_api.id(28932536977135171)
,p_template_types=>'REGION'
,p_help_text=>'Show success alert.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28934928516135171)
,p_theme_id=>42
,p_name=>'USEDEFAULTICONS'
,p_display_name=>'Show Default Icons'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28931739757135175)
,p_css_classes=>'t-Alert--defaultIcons'
,p_group_id=>wwv_flow_api.id(28933525149135171)
,p_template_types=>'REGION'
,p_help_text=>'Uses default icons for alert types.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28935167300135171)
,p_theme_id=>42
,p_name=>'WARNING'
,p_display_name=>'Warning'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28931739757135175)
,p_css_classes=>'t-Alert--warning'
,p_group_id=>wwv_flow_api.id(28932536977135171)
,p_template_types=>'REGION'
,p_help_text=>'Show a warning alert.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28935365910135171)
,p_theme_id=>42
,p_name=>'WIZARD'
,p_display_name=>'Wizard'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28931739757135175)
,p_css_classes=>'t-Alert--wizard'
,p_group_id=>wwv_flow_api.id(28933942113135171)
,p_template_types=>'REGION'
,p_help_text=>'Show the alert in a wizard style region.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28937539143135170)
,p_theme_id=>42
,p_name=>'BORDERLESS'
,p_display_name=>'Borderless'
,p_display_sequence=>1
,p_region_template_id=>wwv_flow_api.id(28936438753135170)
,p_css_classes=>'t-ButtonRegion--noBorder'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28937966420135170)
,p_theme_id=>42
,p_name=>'NOPADDING'
,p_display_name=>'No Padding'
,p_display_sequence=>3
,p_region_template_id=>wwv_flow_api.id(28936438753135170)
,p_css_classes=>'t-ButtonRegion--noPadding'
,p_group_id=>wwv_flow_api.id(28937754447135170)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28938144492135170)
,p_theme_id=>42
,p_name=>'REMOVEUIDECORATION'
,p_display_name=>'Remove UI Decoration'
,p_display_sequence=>4
,p_region_template_id=>wwv_flow_api.id(28936438753135170)
,p_css_classes=>'t-ButtonRegion--noUI'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28938338651135170)
,p_theme_id=>42
,p_name=>'SLIMPADDING'
,p_display_name=>'Slim Padding'
,p_display_sequence=>5
,p_region_template_id=>wwv_flow_api.id(28936438753135170)
,p_css_classes=>'t-ButtonRegion--slimPadding'
,p_group_id=>wwv_flow_api.id(28937754447135170)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28938502622135170)
,p_theme_id=>42
,p_name=>'STICK_TO_BOTTOM'
,p_display_name=>'Stick to Bottom for Mobile'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28936438753135170)
,p_css_classes=>'t-ButtonRegion--stickToBottom'
,p_template_types=>'REGION'
,p_help_text=>'This will position the button container region to the bottom of the screen for small screens.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28939723634135169)
,p_theme_id=>42
,p_name=>'10_SECONDS'
,p_display_name=>'10 Seconds'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'js-cycle10s'
,p_group_id=>wwv_flow_api.id(28939579398135169)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28939932416135169)
,p_theme_id=>42
,p_name=>'15_SECONDS'
,p_display_name=>'15 Seconds'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'js-cycle15s'
,p_group_id=>wwv_flow_api.id(28939579398135169)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28940189759135169)
,p_theme_id=>42
,p_name=>'20_SECONDS'
,p_display_name=>'20 Seconds'
,p_display_sequence=>40
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'js-cycle20s'
,p_group_id=>wwv_flow_api.id(28939579398135169)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28940551092135169)
,p_theme_id=>42
,p_name=>'240PX'
,p_display_name=>'240px'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'i-h240'
,p_group_id=>wwv_flow_api.id(28940333624135169)
,p_template_types=>'REGION'
,p_help_text=>'Sets region body height to 240px.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28940765870135169)
,p_theme_id=>42
,p_name=>'320PX'
,p_display_name=>'320px'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'i-h320'
,p_group_id=>wwv_flow_api.id(28940333624135169)
,p_template_types=>'REGION'
,p_help_text=>'Sets region body height to 320px.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28940935788135169)
,p_theme_id=>42
,p_name=>'480PX'
,p_display_name=>'480px'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'i-h480'
,p_group_id=>wwv_flow_api.id(28940333624135169)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28941103157135169)
,p_theme_id=>42
,p_name=>'5_SECONDS'
,p_display_name=>'5 Seconds'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'js-cycle5s'
,p_group_id=>wwv_flow_api.id(28939579398135169)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28941340297135169)
,p_theme_id=>42
,p_name=>'640PX'
,p_display_name=>'640px'
,p_display_sequence=>40
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'i-h640'
,p_group_id=>wwv_flow_api.id(28940333624135169)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28941760185135168)
,p_theme_id=>42
,p_name=>'ACCENT_1'
,p_display_name=>'Accent 1'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--accent1'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28941981578135168)
,p_theme_id=>42
,p_name=>'ACCENT_2'
,p_display_name=>'Accent 2'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--accent2'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28942119270135168)
,p_theme_id=>42
,p_name=>'ACCENT_3'
,p_display_name=>'Accent 3'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--accent3'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28942335614135168)
,p_theme_id=>42
,p_name=>'ACCENT_4'
,p_display_name=>'Accent 4'
,p_display_sequence=>40
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--accent4'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28942523638135168)
,p_theme_id=>42
,p_name=>'ACCENT_5'
,p_display_name=>'Accent 5'
,p_display_sequence=>50
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--accent5'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28942921511135168)
,p_theme_id=>42
,p_name=>'HIDDENHEADERNOAT'
,p_display_name=>'Hidden'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--removeHeader'
,p_group_id=>wwv_flow_api.id(28942767777135168)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28943368500135168)
,p_theme_id=>42
,p_name=>'HIDEOVERFLOW'
,p_display_name=>'Hide'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--hiddenOverflow'
,p_group_id=>wwv_flow_api.id(28943151335135168)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28943524510135167)
,p_theme_id=>42
,p_name=>'HIDEREGIONHEADER'
,p_display_name=>'Hidden but accessible'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--hideHeader'
,p_group_id=>wwv_flow_api.id(28942767777135168)
,p_template_types=>'REGION'
,p_help_text=>'This option will hide the region header.  Note that the region title will still be audible for Screen Readers. Buttons placed in the region header will be hidden and inaccessible.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28943782221135167)
,p_theme_id=>42
,p_name=>'NOBODYPADDING'
,p_display_name=>'Remove Body Padding'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--noPadding'
,p_template_types=>'REGION'
,p_help_text=>'Removes padding from region body.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28943960223135167)
,p_theme_id=>42
,p_name=>'NOBORDER'
,p_display_name=>'Remove Borders'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--noBorder'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
,p_help_text=>'Removes borders from the region.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28944132036135167)
,p_theme_id=>42
,p_name=>'REMEMBER_CAROUSEL_SLIDE'
,p_display_name=>'Remember Carousel Slide'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'js-useLocalStorage'
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28944308703135167)
,p_theme_id=>42
,p_name=>'SCROLLBODY'
,p_display_name=>'Scroll'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--scrollBody'
,p_group_id=>wwv_flow_api.id(28943151335135168)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28944568637135167)
,p_theme_id=>42
,p_name=>'SHOW_MAXIMIZE_BUTTON'
,p_display_name=>'Show Maximize Button'
,p_display_sequence=>40
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'js-showMaximizeButton'
,p_template_types=>'REGION'
,p_help_text=>'Displays a button in the Region Header to maximize the region. Clicking this button will toggle the maximize state and stretch the region to fill the screen.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28944714908135167)
,p_theme_id=>42
,p_name=>'SHOW_NEXT_AND_PREVIOUS_BUTTONS'
,p_display_name=>'Show Next and Previous Buttons'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--showCarouselControls'
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28944927854135167)
,p_theme_id=>42
,p_name=>'SHOW_REGION_ICON'
,p_display_name=>'Show Region Icon'
,p_display_sequence=>50
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--showIcon'
,p_template_types=>'REGION'
,p_help_text=>'Displays the region icon in the region header beside the region title'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28945333257135167)
,p_theme_id=>42
,p_name=>'SLIDE'
,p_display_name=>'Slide'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--carouselSlide'
,p_group_id=>wwv_flow_api.id(28945101934135167)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28945550069135167)
,p_theme_id=>42
,p_name=>'SPIN'
,p_display_name=>'Spin'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--carouselSpin'
,p_group_id=>wwv_flow_api.id(28945101934135167)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28945705565135167)
,p_theme_id=>42
,p_name=>'STACKED'
,p_display_name=>'Stack Region'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28938655441135170)
,p_css_classes=>'t-Region--stacked'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
,p_help_text=>'Removes side borders and shadows, and can be useful for accordions and regions that need to be grouped together vertically.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28946770254135167)
,p_theme_id=>42
,p_name=>'240PX'
,p_display_name=>'240px'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'i-h240'
,p_group_id=>wwv_flow_api.id(28940333624135169)
,p_template_types=>'REGION'
,p_help_text=>'Sets region body height to 240px.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28946946609135167)
,p_theme_id=>42
,p_name=>'320PX'
,p_display_name=>'320px'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'i-h320'
,p_group_id=>wwv_flow_api.id(28940333624135169)
,p_template_types=>'REGION'
,p_help_text=>'Sets region body height to 320px.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28947154367135167)
,p_theme_id=>42
,p_name=>'480PX'
,p_display_name=>'480px'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'i-h480'
,p_group_id=>wwv_flow_api.id(28940333624135169)
,p_template_types=>'REGION'
,p_help_text=>'Sets body height to 480px.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28947341493135167)
,p_theme_id=>42
,p_name=>'640PX'
,p_display_name=>'640px'
,p_display_sequence=>40
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'i-h640'
,p_group_id=>wwv_flow_api.id(28940333624135169)
,p_template_types=>'REGION'
,p_help_text=>'Sets body height to 640px.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28947528356135167)
,p_theme_id=>42
,p_name=>'ACCENT_1'
,p_display_name=>'Accent 1'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'t-Region--accent1'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28947782577135166)
,p_theme_id=>42
,p_name=>'ACCENT_2'
,p_display_name=>'Accent 2'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'t-Region--accent2'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28947961051135166)
,p_theme_id=>42
,p_name=>'ACCENT_3'
,p_display_name=>'Accent 3'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'t-Region--accent3'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28948141774135166)
,p_theme_id=>42
,p_name=>'ACCENT_4'
,p_display_name=>'Accent 4'
,p_display_sequence=>40
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'t-Region--accent4'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28948331577135166)
,p_theme_id=>42
,p_name=>'ACCENT_5'
,p_display_name=>'Accent 5'
,p_display_sequence=>50
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'t-Region--accent5'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28948722175135166)
,p_theme_id=>42
,p_name=>'COLLAPSED'
,p_display_name=>'Collapsed'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'is-collapsed'
,p_group_id=>wwv_flow_api.id(28948541601135166)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28948959015135163)
,p_theme_id=>42
,p_name=>'EXPANDED'
,p_display_name=>'Expanded'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'is-expanded'
,p_group_id=>wwv_flow_api.id(28948541601135166)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28949132467135163)
,p_theme_id=>42
,p_name=>'HIDEOVERFLOW'
,p_display_name=>'Hide'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'t-Region--hiddenOverflow'
,p_group_id=>wwv_flow_api.id(28943151335135168)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28949309848135163)
,p_theme_id=>42
,p_name=>'NOBODYPADDING'
,p_display_name=>'Remove Body Padding'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'t-Region--noPadding'
,p_template_types=>'REGION'
,p_help_text=>'Removes padding from region body.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28949593957135163)
,p_theme_id=>42
,p_name=>'NOBORDER'
,p_display_name=>'Remove Borders'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'t-Region--noBorder'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
,p_help_text=>'Removes borders from the region.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28949731414135163)
,p_theme_id=>42
,p_name=>'REMEMBER_COLLAPSIBLE_STATE'
,p_display_name=>'Remember Collapsible State'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'js-useLocalStorage'
,p_template_types=>'REGION'
,p_help_text=>'This option saves the current state of the collapsible region for the duration of the session.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28949925038135163)
,p_theme_id=>42
,p_name=>'REMOVE_UI_DECORATION'
,p_display_name=>'Remove UI Decoration'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'t-Region--noUI'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
,p_help_text=>'Removes UI decoration (borders, backgrounds, shadows, etc) from the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28950104828135163)
,p_theme_id=>42
,p_name=>'SCROLLBODY'
,p_display_name=>'Scroll - Default'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'t-Region--scrollBody'
,p_group_id=>wwv_flow_api.id(28943151335135168)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28950365087135163)
,p_theme_id=>42
,p_name=>'STACKED'
,p_display_name=>'Stack Region'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28945805107135167)
,p_css_classes=>'t-Region--stacked'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
,p_help_text=>'Removes side borders and shadows, and can be useful for accordions and regions that need to be grouped together vertically.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28950740547135162)
,p_theme_id=>42
,p_name=>'ADD_BODY_PADDING'
,p_display_name=>'Add Body Padding'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28950410857135163)
,p_css_classes=>'t-ContentBlock--padded'
,p_template_types=>'REGION'
,p_help_text=>'Adds padding to the region''s body container.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28951169044135162)
,p_theme_id=>42
,p_name=>'CONTENT_TITLE_H1'
,p_display_name=>'Heading Level 1'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28950410857135163)
,p_css_classes=>'t-ContentBlock--h1'
,p_group_id=>wwv_flow_api.id(28950914960135162)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28951346200135162)
,p_theme_id=>42
,p_name=>'CONTENT_TITLE_H2'
,p_display_name=>'Heading Level 2'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28950410857135163)
,p_css_classes=>'t-ContentBlock--h2'
,p_group_id=>wwv_flow_api.id(28950914960135162)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28951592884135162)
,p_theme_id=>42
,p_name=>'CONTENT_TITLE_H3'
,p_display_name=>'Heading Level 3'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28950410857135163)
,p_css_classes=>'t-ContentBlock--h3'
,p_group_id=>wwv_flow_api.id(28950914960135162)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28951981725135162)
,p_theme_id=>42
,p_name=>'LIGHT_BACKGROUND'
,p_display_name=>'Light Background'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28950410857135163)
,p_css_classes=>'t-ContentBlock--lightBG'
,p_group_id=>wwv_flow_api.id(28951723657135162)
,p_template_types=>'REGION'
,p_help_text=>'Gives the region body a slightly lighter background.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28952168145135162)
,p_theme_id=>42
,p_name=>'SHADOW_BACKGROUND'
,p_display_name=>'Shadow Background'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28950410857135163)
,p_css_classes=>'t-ContentBlock--shadowBG'
,p_group_id=>wwv_flow_api.id(28951723657135162)
,p_template_types=>'REGION'
,p_help_text=>'Gives the region body a slightly darker background.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28953066099135162)
,p_theme_id=>42
,p_name=>'DISPLAY_ICON_NO'
,p_display_name=>'No'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28952270918135162)
,p_css_classes=>'t-HeroRegion--hideIcon'
,p_group_id=>wwv_flow_api.id(28952854722135162)
,p_template_types=>'REGION'
,p_help_text=>'Hide the Hero Region icon.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28953265727135162)
,p_theme_id=>42
,p_name=>'FEATURED'
,p_display_name=>'Featured'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28952270918135162)
,p_css_classes=>'t-HeroRegion--featured'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28953493401135162)
,p_theme_id=>42
,p_name=>'REMOVE_BODY_PADDING'
,p_display_name=>'Remove Body Padding'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28952270918135162)
,p_css_classes=>'t-HeroRegion--noPadding'
,p_template_types=>'REGION'
,p_help_text=>'Removes the padding around the hero region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28953686522135162)
,p_theme_id=>42
,p_name=>'STACKED_FEATURED'
,p_display_name=>'Stacked Featured'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28952270918135162)
,p_css_classes=>'t-HeroRegion--featured t-HeroRegion--centered'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28954333409135161)
,p_theme_id=>42
,p_name=>'AUTO_HEIGHT_INLINE_DIALOG'
,p_display_name=>'Auto Height'
,p_display_sequence=>1
,p_region_template_id=>wwv_flow_api.id(28953760072135162)
,p_css_classes=>'js-dialog-autoheight'
,p_template_types=>'REGION'
,p_help_text=>'This option will set the height of the dialog to fit its contents.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28954544503135161)
,p_theme_id=>42
,p_name=>'DRAGGABLE'
,p_display_name=>'Draggable'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28953760072135162)
,p_css_classes=>'js-draggable'
,p_template_types=>'REGION'
);
end;
/
begin
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28954954356135161)
,p_theme_id=>42
,p_name=>'LARGE_720X480'
,p_display_name=>'Large (720x480)'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28953760072135162)
,p_css_classes=>'js-dialog-size720x480'
,p_group_id=>wwv_flow_api.id(28954749725135161)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28955102869135161)
,p_theme_id=>42
,p_name=>'MEDIUM_600X400'
,p_display_name=>'Medium (600x400)'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28953760072135162)
,p_css_classes=>'js-dialog-size600x400'
,p_group_id=>wwv_flow_api.id(28954749725135161)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28955346831135161)
,p_theme_id=>42
,p_name=>'MODAL'
,p_display_name=>'Modal'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28953760072135162)
,p_css_classes=>'js-modal'
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28955500696135161)
,p_theme_id=>42
,p_name=>'RESIZABLE'
,p_display_name=>'Resizable'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28953760072135162)
,p_css_classes=>'js-resizable'
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28955719718135161)
,p_theme_id=>42
,p_name=>'SMALL_480X320'
,p_display_name=>'Small (480x320)'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28953760072135162)
,p_css_classes=>'js-dialog-size480x320'
,p_group_id=>wwv_flow_api.id(28954749725135161)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28956151318135161)
,p_theme_id=>42
,p_name=>'REMOVEBORDERS'
,p_display_name=>'Remove Borders'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28955876667135161)
,p_css_classes=>'t-IRR-region--noBorders'
,p_template_types=>'REGION'
,p_help_text=>'Removes borders around the Interactive Report'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28956389059135161)
,p_theme_id=>42
,p_name=>'SHOW_MAXIMIZE_BUTTON'
,p_display_name=>'Show Maximize Button'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28955876667135161)
,p_css_classes=>'js-showMaximizeButton'
,p_template_types=>'REGION'
,p_help_text=>'Displays a button in the Interactive Reports toolbar to maximize the report. Clicking this button will toggle the maximize state and stretch the report to fill the screen.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28957823066135160)
,p_theme_id=>42
,p_name=>'240PX'
,p_display_name=>'240px'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'i-h240'
,p_group_id=>wwv_flow_api.id(28940333624135169)
,p_template_types=>'REGION'
,p_help_text=>'Sets region body height to 240px.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28958027995135160)
,p_theme_id=>42
,p_name=>'320PX'
,p_display_name=>'320px'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'i-h320'
,p_group_id=>wwv_flow_api.id(28940333624135169)
,p_template_types=>'REGION'
,p_help_text=>'Sets region body height to 320px.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28958237566135160)
,p_theme_id=>42
,p_name=>'480PX'
,p_display_name=>'480px'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'i-h480'
,p_group_id=>wwv_flow_api.id(28940333624135169)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28958442116135160)
,p_theme_id=>42
,p_name=>'640PX'
,p_display_name=>'640px'
,p_display_sequence=>40
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'i-h640'
,p_group_id=>wwv_flow_api.id(28940333624135169)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28958665129135160)
,p_theme_id=>42
,p_name=>'ACCENT_1'
,p_display_name=>'Accent 1'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent1'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28958887914135160)
,p_theme_id=>42
,p_name=>'ACCENT_10'
,p_display_name=>'Accent 10'
,p_display_sequence=>100
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent10'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28959097926135160)
,p_theme_id=>42
,p_name=>'ACCENT_11'
,p_display_name=>'Accent 11'
,p_display_sequence=>110
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent11'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28959219492135160)
,p_theme_id=>42
,p_name=>'ACCENT_12'
,p_display_name=>'Accent 12'
,p_display_sequence=>120
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent12'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28959477725135160)
,p_theme_id=>42
,p_name=>'ACCENT_13'
,p_display_name=>'Accent 13'
,p_display_sequence=>130
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent13'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28959633952135160)
,p_theme_id=>42
,p_name=>'ACCENT_14'
,p_display_name=>'Accent 14'
,p_display_sequence=>140
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent14'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28959828333135160)
,p_theme_id=>42
,p_name=>'ACCENT_15'
,p_display_name=>'Accent 15'
,p_display_sequence=>150
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent15'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28960046912135160)
,p_theme_id=>42
,p_name=>'ACCENT_2'
,p_display_name=>'Accent 2'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent2'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28960223598135160)
,p_theme_id=>42
,p_name=>'ACCENT_3'
,p_display_name=>'Accent 3'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent3'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28960486888135160)
,p_theme_id=>42
,p_name=>'ACCENT_4'
,p_display_name=>'Accent 4'
,p_display_sequence=>40
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent4'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28960641967135160)
,p_theme_id=>42
,p_name=>'ACCENT_5'
,p_display_name=>'Accent 5'
,p_display_sequence=>50
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent5'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28960881186135160)
,p_theme_id=>42
,p_name=>'ACCENT_6'
,p_display_name=>'Accent 6'
,p_display_sequence=>60
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent6'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28961038901135160)
,p_theme_id=>42
,p_name=>'ACCENT_7'
,p_display_name=>'Accent 7'
,p_display_sequence=>70
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent7'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28961293869135159)
,p_theme_id=>42
,p_name=>'ACCENT_8'
,p_display_name=>'Accent 8'
,p_display_sequence=>80
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent8'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28961468009135159)
,p_theme_id=>42
,p_name=>'ACCENT_9'
,p_display_name=>'Accent 9'
,p_display_sequence=>90
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--accent9'
,p_group_id=>wwv_flow_api.id(28941506456135168)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28961641165135159)
,p_theme_id=>42
,p_name=>'HIDDENHEADERNOAT'
,p_display_name=>'Hidden'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--removeHeader'
,p_group_id=>wwv_flow_api.id(28942767777135168)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28961840101135159)
,p_theme_id=>42
,p_name=>'HIDEOVERFLOW'
,p_display_name=>'Hide'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--hiddenOverflow'
,p_group_id=>wwv_flow_api.id(28943151335135168)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28962085588135159)
,p_theme_id=>42
,p_name=>'HIDEREGIONHEADER'
,p_display_name=>'Hidden but accessible'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--hideHeader'
,p_group_id=>wwv_flow_api.id(28942767777135168)
,p_template_types=>'REGION'
,p_help_text=>'This option will hide the region header.  Note that the region title will still be audible for Screen Readers. Buttons placed in the region header will be hidden and inaccessible.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28962294317135159)
,p_theme_id=>42
,p_name=>'NOBODYPADDING'
,p_display_name=>'Remove Body Padding'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--noPadding'
,p_template_types=>'REGION'
,p_help_text=>'Removes padding from region body.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28962431148135159)
,p_theme_id=>42
,p_name=>'NOBORDER'
,p_display_name=>'Remove Borders'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--noBorder'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
,p_help_text=>'Removes borders from the region.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28962697513135153)
,p_theme_id=>42
,p_name=>'REMOVE_UI_DECORATION'
,p_display_name=>'Remove UI Decoration'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--noUI'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
,p_help_text=>'Removes UI decoration (borders, backgrounds, shadows, etc) from the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28962866144135152)
,p_theme_id=>42
,p_name=>'SCROLLBODY'
,p_display_name=>'Scroll - Default'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--scrollBody'
,p_group_id=>wwv_flow_api.id(28943151335135168)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28963034245135152)
,p_theme_id=>42
,p_name=>'SHOW_MAXIMIZE_BUTTON'
,p_display_name=>'Show Maximize Button'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'js-showMaximizeButton'
,p_template_types=>'REGION'
,p_help_text=>'Displays a button in the Region Header to maximize the region. Clicking this button will toggle the maximize state and stretch the region to fill the screen.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28963297112135152)
,p_theme_id=>42
,p_name=>'SHOW_REGION_ICON'
,p_display_name=>'Show Region Icon'
,p_display_sequence=>30
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--showIcon'
,p_template_types=>'REGION'
,p_help_text=>'Displays the region icon in the region header beside the region title'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28963495867135152)
,p_theme_id=>42
,p_name=>'STACKED'
,p_display_name=>'Stack Region'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--stacked'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
,p_help_text=>'Removes side borders and shadows, and can be useful for accordions and regions that need to be grouped together vertically.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28963624592135152)
,p_theme_id=>42
,p_name=>'TEXT_CONTENT'
,p_display_name=>'Text Content'
,p_display_sequence=>40
,p_region_template_id=>wwv_flow_api.id(28956997224135161)
,p_css_classes=>'t-Region--textContent'
,p_group_id=>wwv_flow_api.id(28937352081135170)
,p_template_types=>'REGION'
,p_help_text=>'Useful for displaying primarily text-based content, such as FAQs and more.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28964827533135152)
,p_theme_id=>42
,p_name=>'FILL_TAB_LABELS'
,p_display_name=>'Fill Tab Labels'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28963700310135152)
,p_css_classes=>'t-TabsRegion-mod--fillLabels'
,p_group_id=>wwv_flow_api.id(28964617727135152)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28965299595135152)
,p_theme_id=>42
,p_name=>'PILL'
,p_display_name=>'Pill'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28963700310135152)
,p_css_classes=>'t-TabsRegion-mod--pill'
,p_group_id=>wwv_flow_api.id(28965023310135152)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28965474048135152)
,p_theme_id=>42
,p_name=>'REMEMBER_ACTIVE_TAB'
,p_display_name=>'Remember Active Tab'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28963700310135152)
,p_css_classes=>'js-useLocalStorage'
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28965667692135152)
,p_theme_id=>42
,p_name=>'SIMPLE'
,p_display_name=>'Simple'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28963700310135152)
,p_css_classes=>'t-TabsRegion-mod--simple'
,p_group_id=>wwv_flow_api.id(28965023310135152)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28966048052135152)
,p_theme_id=>42
,p_name=>'TABSLARGE'
,p_display_name=>'Large'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28963700310135152)
,p_css_classes=>'t-TabsRegion-mod--large'
,p_group_id=>wwv_flow_api.id(28965855241135152)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28966233028135152)
,p_theme_id=>42
,p_name=>'TABS_SMALL'
,p_display_name=>'Small'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28963700310135152)
,p_css_classes=>'t-TabsRegion-mod--small'
,p_group_id=>wwv_flow_api.id(28965855241135152)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28966643144135152)
,p_theme_id=>42
,p_name=>'GET_TITLE_FROM_BREADCRUMB'
,p_display_name=>'Use Current Breadcrumb Entry'
,p_display_sequence=>1
,p_region_template_id=>wwv_flow_api.id(28966305468135152)
,p_css_classes=>'t-BreadcrumbRegion--useBreadcrumbTitle'
,p_group_id=>wwv_flow_api.id(28950914960135162)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28966857800135151)
,p_theme_id=>42
,p_name=>'HIDE_BREADCRUMB'
,p_display_name=>'Show Breadcrumbs'
,p_display_sequence=>1
,p_region_template_id=>wwv_flow_api.id(28966305468135152)
,p_css_classes=>'t-BreadcrumbRegion--showBreadcrumb'
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28967061601135151)
,p_theme_id=>42
,p_name=>'REGION_HEADER_VISIBLE'
,p_display_name=>'Use Region Title'
,p_display_sequence=>1
,p_region_template_id=>wwv_flow_api.id(28966305468135152)
,p_css_classes=>'t-BreadcrumbRegion--useRegionTitle'
,p_group_id=>wwv_flow_api.id(28950914960135162)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28967230263135151)
,p_theme_id=>42
,p_name=>'USE_COMPACT_STYLE'
,p_display_name=>'Use Compact Style'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28966305468135152)
,p_css_classes=>'t-BreadcrumbRegion--compactTitle'
,p_template_types=>'REGION'
,p_help_text=>'Uses a compact style for the breadcrumbs.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28968116187135151)
,p_theme_id=>42
,p_name=>'HIDESMALLSCREENS'
,p_display_name=>'Small Screens (Tablet)'
,p_display_sequence=>20
,p_region_template_id=>wwv_flow_api.id(28967300119135151)
,p_css_classes=>'t-Wizard--hideStepsSmall'
,p_group_id=>wwv_flow_api.id(28967944066135151)
,p_template_types=>'REGION'
,p_help_text=>'Hides the wizard progress steps for screens that are smaller than 768px wide.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28968370998135151)
,p_theme_id=>42
,p_name=>'HIDEXSMALLSCREENS'
,p_display_name=>'X Small Screens (Mobile)'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28967300119135151)
,p_css_classes=>'t-Wizard--hideStepsXSmall'
,p_group_id=>wwv_flow_api.id(28967944066135151)
,p_template_types=>'REGION'
,p_help_text=>'Hides the wizard progress steps for screens that are smaller than 768px wide.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28968544562135151)
,p_theme_id=>42
,p_name=>'SHOW_TITLE'
,p_display_name=>'Show Title'
,p_display_sequence=>10
,p_region_template_id=>wwv_flow_api.id(28967300119135151)
,p_css_classes=>'t-Wizard--showTitle'
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28969150650135150)
,p_theme_id=>42
,p_name=>'COMPACT'
,p_display_name=>'Compact'
,p_display_sequence=>1
,p_report_template_id=>wwv_flow_api.id(28968648521135151)
,p_css_classes=>'t-Timeline--compact'
,p_group_id=>wwv_flow_api.id(28968997129135150)
,p_template_types=>'REPORT'
,p_help_text=>'Displays a compact version of timeline with smaller text and fewer columns.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28969708862135149)
,p_theme_id=>42
,p_name=>'2_COLUMN_GRID'
,p_display_name=>'2 Column Grid'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28969210851135149)
,p_css_classes=>'t-MediaList--cols t-MediaList--2cols'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28969910530135149)
,p_theme_id=>42
,p_name=>'3_COLUMN_GRID'
,p_display_name=>'3 Column Grid'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28969210851135149)
,p_css_classes=>'t-MediaList--cols t-MediaList--3cols'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28970150753135149)
,p_theme_id=>42
,p_name=>'4_COLUMN_GRID'
,p_display_name=>'4 Column Grid'
,p_display_sequence=>30
,p_report_template_id=>wwv_flow_api.id(28969210851135149)
,p_css_classes=>'t-MediaList--cols t-MediaList--4cols'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28970309819135149)
,p_theme_id=>42
,p_name=>'5_COLUMN_GRID'
,p_display_name=>'5 Column Grid'
,p_display_sequence=>40
,p_report_template_id=>wwv_flow_api.id(28969210851135149)
,p_css_classes=>'t-MediaList--cols t-MediaList--5cols'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28970595681135149)
,p_theme_id=>42
,p_name=>'APPLY_THEME_COLORS'
,p_display_name=>'Apply Theme Colors'
,p_display_sequence=>40
,p_report_template_id=>wwv_flow_api.id(28969210851135149)
,p_css_classes=>'u-colors'
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28970950138135149)
,p_theme_id=>42
,p_name=>'LARGE'
,p_display_name=>'Large'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28969210851135149)
,p_css_classes=>'t-MediaList--large force-fa-lg'
,p_group_id=>wwv_flow_api.id(28970733811135149)
,p_template_types=>'REPORT'
,p_help_text=>'Increases the size of the text and icons in the list.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28971143521135149)
,p_theme_id=>42
,p_name=>'SHOW_BADGES'
,p_display_name=>'Show Badges'
,p_display_sequence=>30
,p_report_template_id=>wwv_flow_api.id(28969210851135149)
,p_css_classes=>'t-MediaList--showBadges'
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28971325233135149)
,p_theme_id=>42
,p_name=>'SHOW_DESCRIPTION'
,p_display_name=>'Show Description'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28969210851135149)
,p_css_classes=>'t-MediaList--showDesc'
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28971512835135149)
,p_theme_id=>42
,p_name=>'SHOW_ICONS'
,p_display_name=>'Show Icons'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28969210851135149)
,p_css_classes=>'t-MediaList--showIcons'
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28971729158135149)
,p_theme_id=>42
,p_name=>'SPAN_HORIZONTAL'
,p_display_name=>'Span Horizontal'
,p_display_sequence=>50
,p_report_template_id=>wwv_flow_api.id(28969210851135149)
,p_css_classes=>'t-MediaList--horizontal'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28971948013135149)
,p_theme_id=>42
,p_name=>'STACK'
,p_display_name=>'Stack'
,p_display_sequence=>5
,p_report_template_id=>wwv_flow_api.id(28969210851135149)
,p_css_classes=>'t-MediaList--stack'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28972544014135149)
,p_theme_id=>42
,p_name=>'FIXED_LARGE'
,p_display_name=>'Fixed - Large'
,p_display_sequence=>30
,p_report_template_id=>wwv_flow_api.id(28972031346135149)
,p_css_classes=>'t-AVPList--fixedLabelLarge'
,p_group_id=>wwv_flow_api.id(28972394054135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28972751967135148)
,p_theme_id=>42
,p_name=>'FIXED_MEDIUM'
,p_display_name=>'Fixed - Medium'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28972031346135149)
,p_css_classes=>'t-AVPList--fixedLabelMedium'
,p_group_id=>wwv_flow_api.id(28972394054135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28972978039135148)
,p_theme_id=>42
,p_name=>'FIXED_SMALL'
,p_display_name=>'Fixed - Small'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28972031346135149)
,p_css_classes=>'t-AVPList--fixedLabelSmall'
,p_group_id=>wwv_flow_api.id(28972394054135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28973180095135148)
,p_theme_id=>42
,p_name=>'LEFT_ALIGNED_DETAILS'
,p_display_name=>'Left Aligned Details'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28972031346135149)
,p_css_classes=>'t-AVPList--leftAligned'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28973358517135148)
,p_theme_id=>42
,p_name=>'RIGHT_ALIGNED_DETAILS'
,p_display_name=>'Right Aligned Details'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28972031346135149)
,p_css_classes=>'t-AVPList--rightAligned'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28973571467135148)
,p_theme_id=>42
,p_name=>'VARIABLE_LARGE'
,p_display_name=>'Variable - Large'
,p_display_sequence=>60
,p_report_template_id=>wwv_flow_api.id(28972031346135149)
,p_css_classes=>'t-AVPList--variableLabelLarge'
,p_group_id=>wwv_flow_api.id(28972394054135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28973711181135148)
,p_theme_id=>42
,p_name=>'VARIABLE_MEDIUM'
,p_display_name=>'Variable - Medium'
,p_display_sequence=>50
,p_report_template_id=>wwv_flow_api.id(28972031346135149)
,p_css_classes=>'t-AVPList--variableLabelMedium'
,p_group_id=>wwv_flow_api.id(28972394054135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28973915803135146)
,p_theme_id=>42
,p_name=>'VARIABLE_SMALL'
,p_display_name=>'Variable - Small'
,p_display_sequence=>40
,p_report_template_id=>wwv_flow_api.id(28972031346135149)
,p_css_classes=>'t-AVPList--variableLabelSmall'
,p_group_id=>wwv_flow_api.id(28972394054135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28974307757135145)
,p_theme_id=>42
,p_name=>'FIXED_LARGE'
,p_display_name=>'Fixed - Large'
,p_display_sequence=>30
,p_report_template_id=>wwv_flow_api.id(28974074185135146)
,p_css_classes=>'t-AVPList--fixedLabelLarge'
,p_group_id=>wwv_flow_api.id(28972394054135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28974551765135145)
,p_theme_id=>42
,p_name=>'FIXED_MEDIUM'
,p_display_name=>'Fixed - Medium'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28974074185135146)
,p_css_classes=>'t-AVPList--fixedLabelMedium'
,p_group_id=>wwv_flow_api.id(28972394054135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28974793589135145)
,p_theme_id=>42
,p_name=>'FIXED_SMALL'
,p_display_name=>'Fixed - Small'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28974074185135146)
,p_css_classes=>'t-AVPList--fixedLabelSmall'
,p_group_id=>wwv_flow_api.id(28972394054135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28974994242135145)
,p_theme_id=>42
,p_name=>'LEFT_ALIGNED_DETAILS'
,p_display_name=>'Left Aligned Details'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28974074185135146)
,p_css_classes=>'t-AVPList--leftAligned'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28975119013135145)
,p_theme_id=>42
,p_name=>'RIGHT_ALIGNED_DETAILS'
,p_display_name=>'Right Aligned Details'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28974074185135146)
,p_css_classes=>'t-AVPList--rightAligned'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28975324492135145)
,p_theme_id=>42
,p_name=>'VARIABLE_LARGE'
,p_display_name=>'Variable - Large'
,p_display_sequence=>60
,p_report_template_id=>wwv_flow_api.id(28974074185135146)
,p_css_classes=>'t-AVPList--variableLabelLarge'
,p_group_id=>wwv_flow_api.id(28972394054135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28975544640135145)
,p_theme_id=>42
,p_name=>'VARIABLE_MEDIUM'
,p_display_name=>'Variable - Medium'
,p_display_sequence=>50
,p_report_template_id=>wwv_flow_api.id(28974074185135146)
,p_css_classes=>'t-AVPList--variableLabelMedium'
,p_group_id=>wwv_flow_api.id(28972394054135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28975764641135145)
,p_theme_id=>42
,p_name=>'VARIABLE_SMALL'
,p_display_name=>'Variable - Small'
,p_display_sequence=>40
,p_report_template_id=>wwv_flow_api.id(28974074185135146)
,p_css_classes=>'t-AVPList--variableLabelSmall'
,p_group_id=>wwv_flow_api.id(28972394054135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28976346249135145)
,p_theme_id=>42
,p_name=>'128PX'
,p_display_name=>'128px'
,p_display_sequence=>50
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--xxlarge'
,p_group_id=>wwv_flow_api.id(28976104794135145)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28976521604135145)
,p_theme_id=>42
,p_name=>'2COLUMNGRID'
,p_display_name=>'2 Column Grid'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--cols'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
,p_help_text=>'Arrange badges in a two column grid'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28976714241135145)
,p_theme_id=>42
,p_name=>'32PX'
,p_display_name=>'32px'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--small'
,p_group_id=>wwv_flow_api.id(28976104794135145)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28976978965135145)
,p_theme_id=>42
,p_name=>'3COLUMNGRID'
,p_display_name=>'3 Column Grid'
,p_display_sequence=>30
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--cols t-BadgeList--3cols'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
,p_help_text=>'Arrange badges in a 3 column grid'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28977146120135145)
,p_theme_id=>42
,p_name=>'48PX'
,p_display_name=>'48px'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--medium'
,p_group_id=>wwv_flow_api.id(28976104794135145)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28977307981135145)
,p_theme_id=>42
,p_name=>'4COLUMNGRID'
,p_display_name=>'4 Column Grid'
,p_display_sequence=>40
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--cols t-BadgeList--4cols'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28977561373135145)
,p_theme_id=>42
,p_name=>'5COLUMNGRID'
,p_display_name=>'5 Column Grid'
,p_display_sequence=>50
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--cols t-BadgeList--5cols'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28977742367135145)
,p_theme_id=>42
,p_name=>'64PX'
,p_display_name=>'64px'
,p_display_sequence=>30
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--large'
,p_group_id=>wwv_flow_api.id(28976104794135145)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28977936506135145)
,p_theme_id=>42
,p_name=>'96PX'
,p_display_name=>'96px'
,p_display_sequence=>40
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--xlarge'
,p_group_id=>wwv_flow_api.id(28976104794135145)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
end;
/
begin
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28978108946135144)
,p_theme_id=>42
,p_name=>'APPLY_THEME_COLORS'
,p_display_name=>'Apply Theme Colors'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'u-colors'
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28978384274135144)
,p_theme_id=>42
,p_name=>'CIRCULAR'
,p_display_name=>'Circular'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--circular'
,p_group_id=>wwv_flow_api.id(28968997129135150)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28978524287135144)
,p_theme_id=>42
,p_name=>'FIXED'
,p_display_name=>'Span Horizontally'
,p_display_sequence=>60
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--fixed'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28978777667135144)
,p_theme_id=>42
,p_name=>'FLEXIBLEBOX'
,p_display_name=>'Flexible Box'
,p_display_sequence=>80
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--flex'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28978947848135144)
,p_theme_id=>42
,p_name=>'FLOATITEMS'
,p_display_name=>'Float Items'
,p_display_sequence=>70
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--float'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28979102099135144)
,p_theme_id=>42
,p_name=>'GRID'
,p_display_name=>'Grid'
,p_display_sequence=>30
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--dash'
,p_group_id=>wwv_flow_api.id(28968997129135150)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28979344504135144)
,p_theme_id=>42
,p_name=>'STACKED'
,p_display_name=>'Stacked'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28975823088135145)
,p_css_classes=>'t-BadgeList--stacked'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28979940239135144)
,p_theme_id=>42
,p_name=>'ALTROWCOLORSDISABLE'
,p_display_name=>'Disable'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28979442408135144)
,p_css_classes=>'t-Report--staticRowColors'
,p_group_id=>wwv_flow_api.id(28979742750135144)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28980120728135144)
,p_theme_id=>42
,p_name=>'ALTROWCOLORSENABLE'
,p_display_name=>'Enable'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28979442408135144)
,p_css_classes=>'t-Report--altRowsDefault'
,p_group_id=>wwv_flow_api.id(28979742750135144)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28980559980135144)
,p_theme_id=>42
,p_name=>'ENABLE'
,p_display_name=>'Enable'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28979442408135144)
,p_css_classes=>'t-Report--rowHighlight'
,p_group_id=>wwv_flow_api.id(28980310033135144)
,p_template_types=>'REPORT'
,p_help_text=>'Enable row highlighting on mouse over'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28980925070135144)
,p_theme_id=>42
,p_name=>'HORIZONTALBORDERS'
,p_display_name=>'Horizontal Only'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28979442408135144)
,p_css_classes=>'t-Report--horizontalBorders'
,p_group_id=>wwv_flow_api.id(28980754118135144)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28981194211135143)
,p_theme_id=>42
,p_name=>'REMOVEALLBORDERS'
,p_display_name=>'No Borders'
,p_display_sequence=>30
,p_report_template_id=>wwv_flow_api.id(28979442408135144)
,p_css_classes=>'t-Report--noBorders'
,p_group_id=>wwv_flow_api.id(28980754118135144)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28981379578135143)
,p_theme_id=>42
,p_name=>'REMOVEOUTERBORDERS'
,p_display_name=>'No Outer Borders'
,p_display_sequence=>40
,p_report_template_id=>wwv_flow_api.id(28979442408135144)
,p_css_classes=>'t-Report--inline'
,p_group_id=>wwv_flow_api.id(28980754118135144)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28981576393135143)
,p_theme_id=>42
,p_name=>'ROWHIGHLIGHTDISABLE'
,p_display_name=>'Disable'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28979442408135144)
,p_css_classes=>'t-Report--rowHighlightOff'
,p_group_id=>wwv_flow_api.id(28980310033135144)
,p_template_types=>'REPORT'
,p_help_text=>'Disable row highlighting on mouse over'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28981724723135143)
,p_theme_id=>42
,p_name=>'STRETCHREPORT'
,p_display_name=>'Stretch Report'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28979442408135144)
,p_css_classes=>'t-Report--stretch'
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28981959925135143)
,p_theme_id=>42
,p_name=>'VERTICALBORDERS'
,p_display_name=>'Vertical Only'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28979442408135144)
,p_css_classes=>'t-Report--verticalBorders'
,p_group_id=>wwv_flow_api.id(28980754118135144)
,p_template_types=>'REPORT'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28982547087135142)
,p_theme_id=>42
,p_name=>'BASIC'
,p_display_name=>'Basic'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28982093919135143)
,p_css_classes=>'t-Comments--basic'
,p_group_id=>wwv_flow_api.id(28982301271135143)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28982772113135142)
,p_theme_id=>42
,p_name=>'SPEECH_BUBBLES'
,p_display_name=>'Speech Bubbles'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28982093919135143)
,p_css_classes=>'t-Comments--chat'
,p_group_id=>wwv_flow_api.id(28982301271135143)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28983346926135142)
,p_theme_id=>42
,p_name=>'2_COLUMNS'
,p_display_name=>'2 Columns'
,p_display_sequence=>15
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--cols'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28983770208135142)
,p_theme_id=>42
,p_name=>'2_LINES'
,p_display_name=>'2 Lines'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--desc-2ln'
,p_group_id=>wwv_flow_api.id(28983521501135142)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28983942767135142)
,p_theme_id=>42
,p_name=>'3_COLUMNS'
,p_display_name=>'3 Columns'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--3cols'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28984124165135142)
,p_theme_id=>42
,p_name=>'3_LINES'
,p_display_name=>'3 Lines'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--desc-3ln'
,p_group_id=>wwv_flow_api.id(28983521501135142)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28984369172135142)
,p_theme_id=>42
,p_name=>'4_COLUMNS'
,p_display_name=>'4 Columns'
,p_display_sequence=>30
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--4cols'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28984529062135142)
,p_theme_id=>42
,p_name=>'4_LINES'
,p_display_name=>'4 Lines'
,p_display_sequence=>30
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--desc-4ln'
,p_group_id=>wwv_flow_api.id(28983521501135142)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28984732816135142)
,p_theme_id=>42
,p_name=>'5_COLUMNS'
,p_display_name=>'5 Columns'
,p_display_sequence=>50
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--5cols'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28984943983135142)
,p_theme_id=>42
,p_name=>'BASIC'
,p_display_name=>'Basic'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--basic'
,p_group_id=>wwv_flow_api.id(28968997129135150)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28985193142135142)
,p_theme_id=>42
,p_name=>'BLOCK'
,p_display_name=>'Block'
,p_display_sequence=>40
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--featured t-Cards--block force-fa-lg'
,p_group_id=>wwv_flow_api.id(28968997129135150)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28985512666135142)
,p_theme_id=>42
,p_name=>'CARDS_COLOR_FILL'
,p_display_name=>'Color Fill'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--animColorFill'
,p_group_id=>wwv_flow_api.id(28985304625135142)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28985716800135142)
,p_theme_id=>42
,p_name=>'CARD_RAISE_CARD'
,p_display_name=>'Raise Card'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--animRaiseCard'
,p_group_id=>wwv_flow_api.id(28985304625135142)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28985966509135142)
,p_theme_id=>42
,p_name=>'COMPACT'
,p_display_name=>'Compact'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--compact'
,p_group_id=>wwv_flow_api.id(28968997129135150)
,p_template_types=>'REPORT'
,p_help_text=>'Use this option when you want to show smaller cards.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28986340052135142)
,p_theme_id=>42
,p_name=>'DISPLAY_ICONS'
,p_display_name=>'Display Icons'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--displayIcons'
,p_group_id=>wwv_flow_api.id(28986156456135142)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28986599115135142)
,p_theme_id=>42
,p_name=>'DISPLAY_INITIALS'
,p_display_name=>'Display Initials'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--displayInitials'
,p_group_id=>wwv_flow_api.id(28986156456135142)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28986789719135142)
,p_theme_id=>42
,p_name=>'DISPLAY_SUBTITLE'
,p_display_name=>'Display Subtitle'
,p_display_sequence=>20
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--displaySubtitle'
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28986928401135141)
,p_theme_id=>42
,p_name=>'FEATURED'
,p_display_name=>'Featured'
,p_display_sequence=>30
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--featured force-fa-lg'
,p_group_id=>wwv_flow_api.id(28968997129135150)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28987182804135141)
,p_theme_id=>42
,p_name=>'FLOAT'
,p_display_name=>'Float'
,p_display_sequence=>60
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--float'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28987315250135141)
,p_theme_id=>42
,p_name=>'HIDDEN_BODY_TEXT'
,p_display_name=>'Hidden'
,p_display_sequence=>50
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--hideBody'
,p_group_id=>wwv_flow_api.id(28983521501135142)
,p_template_types=>'REPORT'
,p_help_text=>'This option hides the card body which contains description and subtext.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28987565637135141)
,p_theme_id=>42
,p_name=>'SPAN_HORIZONTALLY'
,p_display_name=>'Span Horizontally'
,p_display_sequence=>70
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'t-Cards--spanHorizontally'
,p_group_id=>wwv_flow_api.id(28969514287135149)
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28987720049135141)
,p_theme_id=>42
,p_name=>'USE_THEME_COLORS'
,p_display_name=>'Apply Theme Colors'
,p_display_sequence=>10
,p_report_template_id=>wwv_flow_api.id(28983039117135142)
,p_css_classes=>'u-colors'
,p_template_types=>'REPORT'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28988592591135139)
,p_theme_id=>42
,p_name=>'DISPLAY_LABELS_SM'
,p_display_name=>'Display labels'
,p_display_sequence=>40
,p_list_template_id=>wwv_flow_api.id(28988009183135141)
,p_css_classes=>'t-NavTabs--displayLabels-sm'
,p_group_id=>wwv_flow_api.id(28988353221135139)
,p_template_types=>'LIST'
,p_help_text=>'Displays the label for the list items below the icon'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28988781698135139)
,p_theme_id=>42
,p_name=>'HIDE_LABELS_SM'
,p_display_name=>'Do not display labels'
,p_display_sequence=>50
,p_list_template_id=>wwv_flow_api.id(28988009183135141)
,p_css_classes=>'t-NavTabs--hiddenLabels-sm'
,p_group_id=>wwv_flow_api.id(28988353221135139)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28989120074135139)
,p_theme_id=>42
,p_name=>'LABEL_ABOVE_LG'
,p_display_name=>'Display labels above'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28988009183135141)
,p_css_classes=>'t-NavTabs--stacked'
,p_group_id=>wwv_flow_api.id(28988932787135139)
,p_template_types=>'LIST'
,p_help_text=>'Display the label stacked above the icon and badge'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28989387710135139)
,p_theme_id=>42
,p_name=>'LABEL_INLINE_LG'
,p_display_name=>'Display labels inline'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28988009183135141)
,p_css_classes=>'t-NavTabs--inlineLabels-lg'
,p_group_id=>wwv_flow_api.id(28988932787135139)
,p_template_types=>'LIST'
,p_help_text=>'Display the label inline with the icon and badge'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28989561679135138)
,p_theme_id=>42
,p_name=>'NO_LABEL_LG'
,p_display_name=>'Do not display labels'
,p_display_sequence=>30
,p_list_template_id=>wwv_flow_api.id(28988009183135141)
,p_css_classes=>'t-NavTabs--hiddenLabels-lg'
,p_group_id=>wwv_flow_api.id(28988932787135139)
,p_template_types=>'LIST'
,p_help_text=>'Hides the label for the list item'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28990103096135138)
,p_theme_id=>42
,p_name=>'ALLSTEPS'
,p_display_name=>'All Steps'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28989626226135138)
,p_css_classes=>'t-WizardSteps--displayLabels'
,p_group_id=>wwv_flow_api.id(28989955641135138)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28990398842135138)
,p_theme_id=>42
,p_name=>'CURRENTSTEPONLY'
,p_display_name=>'Current Step Only'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28989626226135138)
,p_css_classes=>'t-WizardSteps--displayCurrentLabelOnly'
,p_group_id=>wwv_flow_api.id(28989955641135138)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28990597313135138)
,p_theme_id=>42
,p_name=>'HIDELABELS'
,p_display_name=>'Hide Labels'
,p_display_sequence=>30
,p_list_template_id=>wwv_flow_api.id(28989626226135138)
,p_css_classes=>'t-WizardSteps--hideLabels'
,p_group_id=>wwv_flow_api.id(28989955641135138)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28990701616135138)
,p_theme_id=>42
,p_name=>'VERTICAL_LIST'
,p_display_name=>'Vertical Orientation'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28989626226135138)
,p_css_classes=>'t-WizardSteps--vertical'
,p_template_types=>'LIST'
,p_help_text=>'Displays the wizard progress list in a vertical orientation and is suitable for displaying within a side column of a page.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28991104521135138)
,p_theme_id=>42
,p_name=>'ADD_ACTIONS'
,p_display_name=>'Add Actions'
,p_display_sequence=>40
,p_list_template_id=>wwv_flow_api.id(28990847195135138)
,p_css_classes=>'js-addActions'
,p_template_types=>'LIST'
,p_help_text=>'Use this option to add shortcuts for menu items. Note that actions.js must be included on your page to support this functionality.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28991305524135137)
,p_theme_id=>42
,p_name=>'BEHAVE_LIKE_TABS'
,p_display_name=>'Behave Like Tabs'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28990847195135138)
,p_css_classes=>'js-tabLike'
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28991504368135137)
,p_theme_id=>42
,p_name=>'ENABLE_SLIDE_ANIMATION'
,p_display_name=>'Enable Slide Animation'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28990847195135138)
,p_css_classes=>'js-slide'
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28991747194135137)
,p_theme_id=>42
,p_name=>'SHOW_SUB_MENU_ICONS'
,p_display_name=>'Show Sub Menu Icons'
,p_display_sequence=>30
,p_list_template_id=>wwv_flow_api.id(28990847195135138)
,p_css_classes=>'js-showSubMenuIcons'
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28992315142135137)
,p_theme_id=>42
,p_name=>'2COLUMNGRID'
,p_display_name=>'2 Column Grid'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--cols'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_help_text=>'Arrange badges in a two column grid'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28992583609135137)
,p_theme_id=>42
,p_name=>'3COLUMNGRID'
,p_display_name=>'3 Column Grid'
,p_display_sequence=>30
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--cols t-BadgeList--3cols'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_help_text=>'Arrange badges in a 3 column grid'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28992758708135137)
,p_theme_id=>42
,p_name=>'4COLUMNGRID'
,p_display_name=>'4 Column Grid'
,p_display_sequence=>40
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--cols t-BadgeList--4cols'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_help_text=>'Arrange badges in 4 column grid'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28992977667135137)
,p_theme_id=>42
,p_name=>'5COLUMNGRID'
,p_display_name=>'5 Column Grid'
,p_display_sequence=>50
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--cols t-BadgeList--5cols'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_help_text=>'Arrange badges in a 5 column grid'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28993131302135137)
,p_theme_id=>42
,p_name=>'APPLY_THEME_COLORS'
,p_display_name=>'Apply Theme Colors'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'u-colors'
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28993528709135137)
,p_theme_id=>42
,p_name=>'CIRCULAR'
,p_display_name=>'Circular'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--circular'
,p_group_id=>wwv_flow_api.id(28993375643135137)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28993740999135137)
,p_theme_id=>42
,p_name=>'FIXED'
,p_display_name=>'Span Horizontally'
,p_display_sequence=>60
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--fixed'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_help_text=>'Span badges horizontally'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28993904027135137)
,p_theme_id=>42
,p_name=>'FLEXIBLEBOX'
,p_display_name=>'Flexible Box'
,p_display_sequence=>80
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--flex'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_help_text=>'Use flexbox to arrange items'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28994137572135137)
,p_theme_id=>42
,p_name=>'FLOATITEMS'
,p_display_name=>'Float Items'
,p_display_sequence=>70
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--float'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_help_text=>'Float badges to left'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28994348361135136)
,p_theme_id=>42
,p_name=>'GRID'
,p_display_name=>'Grid'
,p_display_sequence=>30
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--dash'
,p_group_id=>wwv_flow_api.id(28993375643135137)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28994760521135136)
,p_theme_id=>42
,p_name=>'LARGE'
,p_display_name=>'64px'
,p_display_sequence=>30
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--large'
,p_group_id=>wwv_flow_api.id(28994548485135136)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28994919229135136)
,p_theme_id=>42
,p_name=>'MEDIUM'
,p_display_name=>'48px'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--medium'
,p_group_id=>wwv_flow_api.id(28994548485135136)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28995137662135136)
,p_theme_id=>42
,p_name=>'SMALL'
,p_display_name=>'32px'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--small'
,p_group_id=>wwv_flow_api.id(28994548485135136)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28995397355135136)
,p_theme_id=>42
,p_name=>'STACKED'
,p_display_name=>'Stacked'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--stacked'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_help_text=>'Stack badges on top of each other'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28995587240135136)
,p_theme_id=>42
,p_name=>'XLARGE'
,p_display_name=>'96px'
,p_display_sequence=>40
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--xlarge'
,p_group_id=>wwv_flow_api.id(28994548485135136)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28995717003135136)
,p_theme_id=>42
,p_name=>'XXLARGE'
,p_display_name=>'128px'
,p_display_sequence=>50
,p_list_template_id=>wwv_flow_api.id(28991834801135137)
,p_css_classes=>'t-BadgeList--xxlarge'
,p_group_id=>wwv_flow_api.id(28994548485135136)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28996158209135136)
,p_theme_id=>42
,p_name=>'2COLUMNGRID'
,p_display_name=>'2 Column Grid'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28995830029135136)
,p_css_classes=>'t-MediaList--cols t-MediaList--2cols'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28996348450135136)
,p_theme_id=>42
,p_name=>'3COLUMNGRID'
,p_display_name=>'3 Column Grid'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28995830029135136)
,p_css_classes=>'t-MediaList--cols t-MediaList--3cols'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28996563681135136)
,p_theme_id=>42
,p_name=>'4COLUMNGRID'
,p_display_name=>'4 Column Grid'
,p_display_sequence=>30
,p_list_template_id=>wwv_flow_api.id(28995830029135136)
,p_css_classes=>'t-MediaList--cols t-MediaList--4cols'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28996755985135136)
,p_theme_id=>42
,p_name=>'5COLUMNGRID'
,p_display_name=>'5 Column Grid'
,p_display_sequence=>40
,p_list_template_id=>wwv_flow_api.id(28995830029135136)
,p_css_classes=>'t-MediaList--cols t-MediaList--5cols'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28996902433135136)
,p_theme_id=>42
,p_name=>'APPLY_THEME_COLORS'
,p_display_name=>'Apply Theme Colors'
,p_display_sequence=>40
,p_list_template_id=>wwv_flow_api.id(28995830029135136)
,p_css_classes=>'u-colors'
,p_template_types=>'LIST'
,p_help_text=>'Applies colors from the Theme''s color palette to icons in the list.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28997337847135136)
,p_theme_id=>42
,p_name=>'LIST_SIZE_LARGE'
,p_display_name=>'Large'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28995830029135136)
,p_css_classes=>'t-MediaList--large force-fa-lg'
,p_group_id=>wwv_flow_api.id(28997194397135136)
,p_template_types=>'LIST'
,p_help_text=>'Increases the size of the text and icons in the list.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28997569169135136)
,p_theme_id=>42
,p_name=>'SHOW_BADGES'
,p_display_name=>'Show Badges'
,p_display_sequence=>30
,p_list_template_id=>wwv_flow_api.id(28995830029135136)
,p_css_classes=>'t-MediaList--showBadges'
,p_template_types=>'LIST'
,p_help_text=>'Show a badge (Attribute 2) to the right of the list item.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28997749026135136)
,p_theme_id=>42
,p_name=>'SHOW_DESCRIPTION'
,p_display_name=>'Show Description'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28995830029135136)
,p_css_classes=>'t-MediaList--showDesc'
,p_template_types=>'LIST'
,p_help_text=>'Shows the description (Attribute 1) for each list item.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28997967920135136)
,p_theme_id=>42
,p_name=>'SHOW_ICONS'
,p_display_name=>'Show Icons'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28995830029135136)
,p_css_classes=>'t-MediaList--showIcons'
,p_template_types=>'LIST'
,p_help_text=>'Display an icon next to the list item.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28998155796135136)
,p_theme_id=>42
,p_name=>'SPANHORIZONTAL'
,p_display_name=>'Span Horizontal'
,p_display_sequence=>50
,p_list_template_id=>wwv_flow_api.id(28995830029135136)
,p_css_classes=>'t-MediaList--horizontal'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_help_text=>'Show all list items in one horizontal row.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28998569767135135)
,p_theme_id=>42
,p_name=>'COLLAPSED_DEFAULT'
,p_display_name=>'Collapsed by Default'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28998212159135136)
,p_css_classes=>'js-defaultCollapsed'
,p_template_types=>'LIST'
,p_help_text=>'This option will load the side navigation menu in a collapsed state by default.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28998926967135135)
,p_theme_id=>42
,p_name=>'ADD_ACTIONS'
,p_display_name=>'Add Actions'
,p_display_sequence=>1
,p_list_template_id=>wwv_flow_api.id(28998625185135135)
,p_css_classes=>'js-addActions'
,p_template_types=>'LIST'
,p_help_text=>'Use this option to add shortcuts for menu items. Note that actions.js must be included on your page to support this functionality.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28999195916135135)
,p_theme_id=>42
,p_name=>'BEHAVE_LIKE_TABS'
,p_display_name=>'Behave Like Tabs'
,p_display_sequence=>1
,p_list_template_id=>wwv_flow_api.id(28998625185135135)
,p_css_classes=>'js-tabLike'
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28999370568135135)
,p_theme_id=>42
,p_name=>'ENABLE_SLIDE_ANIMATION'
,p_display_name=>'Enable Slide Animation'
,p_display_sequence=>1
,p_list_template_id=>wwv_flow_api.id(28998625185135135)
,p_css_classes=>'js-slide'
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(28999537608135135)
,p_theme_id=>42
,p_name=>'SHOW_SUB_MENU_ICONS'
,p_display_name=>'Show Sub Menu Icons'
,p_display_sequence=>1
,p_list_template_id=>wwv_flow_api.id(28998625185135135)
,p_css_classes=>'js-showSubMenuIcons'
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29000102450135131)
,p_theme_id=>42
,p_name=>'2_COLUMNS'
,p_display_name=>'2 Columns'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--cols'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29000566483135131)
,p_theme_id=>42
,p_name=>'2_LINES'
,p_display_name=>'2 Lines'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--desc-2ln'
,p_group_id=>wwv_flow_api.id(29000382966135131)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29000793151135131)
,p_theme_id=>42
,p_name=>'3_COLUMNS'
,p_display_name=>'3 Columns'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--3cols'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29000915896135130)
,p_theme_id=>42
,p_name=>'3_LINES'
,p_display_name=>'3 Lines'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--desc-3ln'
,p_group_id=>wwv_flow_api.id(29000382966135131)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29001116081135130)
,p_theme_id=>42
,p_name=>'4_COLUMNS'
,p_display_name=>'4 Columns'
,p_display_sequence=>30
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--4cols'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29001309896135130)
,p_theme_id=>42
,p_name=>'4_LINES'
,p_display_name=>'4 Lines'
,p_display_sequence=>30
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--desc-4ln'
,p_group_id=>wwv_flow_api.id(29000382966135131)
,p_template_types=>'LIST'
);
end;
/
begin
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29001587283135130)
,p_theme_id=>42
,p_name=>'5_COLUMNS'
,p_display_name=>'5 Columns'
,p_display_sequence=>50
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--5cols'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29001761878135130)
,p_theme_id=>42
,p_name=>'BASIC'
,p_display_name=>'Basic'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--basic'
,p_group_id=>wwv_flow_api.id(28993375643135137)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29001989393135130)
,p_theme_id=>42
,p_name=>'BLOCK'
,p_display_name=>'Block'
,p_display_sequence=>40
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--featured t-Cards--block force-fa-lg'
,p_group_id=>wwv_flow_api.id(28993375643135137)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29002127863135130)
,p_theme_id=>42
,p_name=>'CARDS_STACKED'
,p_display_name=>'Stacked'
,p_display_sequence=>5
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--stacked'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_help_text=>'Stacks the cards on top of each other.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29002502666135130)
,p_theme_id=>42
,p_name=>'COLOR_FILL'
,p_display_name=>'Color Fill'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--animColorFill'
,p_group_id=>wwv_flow_api.id(29002335931135130)
,p_template_types=>'LIST'
,p_help_text=>'Fills the card background with the color of the icon or default link style.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29002716553135130)
,p_theme_id=>42
,p_name=>'COMPACT'
,p_display_name=>'Compact'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--compact'
,p_group_id=>wwv_flow_api.id(28993375643135137)
,p_template_types=>'LIST'
,p_help_text=>'Use this option when you want to show smaller cards.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29003165670135130)
,p_theme_id=>42
,p_name=>'DISPLAY_ICONS'
,p_display_name=>'Display Icons'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--displayIcons'
,p_group_id=>wwv_flow_api.id(29002922113135130)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29003303039135130)
,p_theme_id=>42
,p_name=>'DISPLAY_INITIALS'
,p_display_name=>'Display Initials'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--displayInitials'
,p_group_id=>wwv_flow_api.id(29002922113135130)
,p_template_types=>'LIST'
,p_help_text=>'Initials come from List Attribute 3'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29003534148135130)
,p_theme_id=>42
,p_name=>'DISPLAY_SUBTITLE'
,p_display_name=>'Display Subtitle'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--displaySubtitle'
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29003770462135130)
,p_theme_id=>42
,p_name=>'FEATURED'
,p_display_name=>'Featured'
,p_display_sequence=>30
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--featured force-fa-lg'
,p_group_id=>wwv_flow_api.id(28993375643135137)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29003916269135130)
,p_theme_id=>42
,p_name=>'FLOAT'
,p_display_name=>'Float'
,p_display_sequence=>60
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--float'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29004154909135130)
,p_theme_id=>42
,p_name=>'HIDDEN_BODY_TEXT'
,p_display_name=>'Hidden'
,p_display_sequence=>50
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--hideBody'
,p_group_id=>wwv_flow_api.id(29000382966135131)
,p_template_types=>'LIST'
,p_help_text=>'This option hides the card body which contains description and subtext.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29004365338135130)
,p_theme_id=>42
,p_name=>'RAISE_CARD'
,p_display_name=>'Raise Card'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--animRaiseCard'
,p_group_id=>wwv_flow_api.id(29002335931135130)
,p_template_types=>'LIST'
,p_help_text=>'Raises the card so it pops up.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29004520884135130)
,p_theme_id=>42
,p_name=>'SPAN_HORIZONTALLY'
,p_display_name=>'Span Horizontally'
,p_display_sequence=>70
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'t-Cards--spanHorizontally'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29004727930135130)
,p_theme_id=>42
,p_name=>'USE_THEME_COLORS'
,p_display_name=>'Apply Theme Colors'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(28999807576135131)
,p_css_classes=>'u-colors'
,p_template_types=>'LIST'
,p_help_text=>'Applies the colors from the theme''s color palette to the icons or initials within cards.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29005160965135129)
,p_theme_id=>42
,p_name=>'ABOVE_LABEL'
,p_display_name=>'Above Label'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(29004876969135130)
,p_css_classes=>'t-Tabs--iconsAbove'
,p_group_id=>wwv_flow_api.id(29002922113135130)
,p_template_types=>'LIST'
,p_help_text=>'Places icons above tab label.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29005309882135129)
,p_theme_id=>42
,p_name=>'FILL_LABELS'
,p_display_name=>'Fill Labels'
,p_display_sequence=>1
,p_list_template_id=>wwv_flow_api.id(29004876969135130)
,p_css_classes=>'t-Tabs--fillLabels'
,p_group_id=>wwv_flow_api.id(28992182569135137)
,p_template_types=>'LIST'
,p_help_text=>'Stretch tabs to fill to the width of the tabs container.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29005599582135129)
,p_theme_id=>42
,p_name=>'INLINE_WITH_LABEL'
,p_display_name=>'Inline with Label'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(29004876969135130)
,p_css_classes=>'t-Tabs--inlineIcons'
,p_group_id=>wwv_flow_api.id(29002922113135130)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29005771198135129)
,p_theme_id=>42
,p_name=>'LARGE'
,p_display_name=>'Large'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(29004876969135130)
,p_css_classes=>'t-Tabs--large'
,p_group_id=>wwv_flow_api.id(28997194397135136)
,p_template_types=>'LIST'
,p_help_text=>'Increases font size and white space around tab items.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29005903243135129)
,p_theme_id=>42
,p_name=>'PILL'
,p_display_name=>'Pill'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(29004876969135130)
,p_css_classes=>'t-Tabs--pill'
,p_group_id=>wwv_flow_api.id(28993375643135137)
,p_template_types=>'LIST'
,p_help_text=>'Displays tabs in a pill container.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29006111638135129)
,p_theme_id=>42
,p_name=>'SIMPLE'
,p_display_name=>'Simple'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(29004876969135130)
,p_css_classes=>'t-Tabs--simple'
,p_group_id=>wwv_flow_api.id(28993375643135137)
,p_template_types=>'LIST'
,p_help_text=>'A very simplistic tab UI.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29006344162135129)
,p_theme_id=>42
,p_name=>'SMALL'
,p_display_name=>'Small'
,p_display_sequence=>5
,p_list_template_id=>wwv_flow_api.id(29004876969135130)
,p_css_classes=>'t-Tabs--small'
,p_group_id=>wwv_flow_api.id(28997194397135136)
,p_template_types=>'LIST'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29006982990135128)
,p_theme_id=>42
,p_name=>'ACTIONS'
,p_display_name=>'Actions'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(29006620859135129)
,p_css_classes=>'t-LinksList--actions'
,p_group_id=>wwv_flow_api.id(28993375643135137)
,p_template_types=>'LIST'
,p_help_text=>'Render as actions to be placed on the right side column.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29007113666135128)
,p_theme_id=>42
,p_name=>'DISABLETEXTWRAPPING'
,p_display_name=>'Disable Text Wrapping'
,p_display_sequence=>30
,p_list_template_id=>wwv_flow_api.id(29006620859135129)
,p_css_classes=>'t-LinksList--nowrap'
,p_template_types=>'LIST'
,p_help_text=>'Do not allow link text to wrap to new lines. Truncate with ellipsis.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29007311098135128)
,p_theme_id=>42
,p_name=>'SHOWBADGES'
,p_display_name=>'Show Badges'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(29006620859135129)
,p_css_classes=>'t-LinksList--showBadge'
,p_template_types=>'LIST'
,p_help_text=>'Show badge to right of link (requires Attribute 1 to be populated)'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29007544618135128)
,p_theme_id=>42
,p_name=>'SHOWGOTOARROW'
,p_display_name=>'Show Right Arrow'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(29006620859135129)
,p_css_classes=>'t-LinksList--showArrow'
,p_template_types=>'LIST'
,p_help_text=>'Show arrow to the right of link'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29007961352135128)
,p_theme_id=>42
,p_name=>'SHOWICONS'
,p_display_name=>'For All Items'
,p_display_sequence=>20
,p_list_template_id=>wwv_flow_api.id(29006620859135129)
,p_css_classes=>'t-LinksList--showIcons'
,p_group_id=>wwv_flow_api.id(29007766236135128)
,p_template_types=>'LIST'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29008122302135128)
,p_theme_id=>42
,p_name=>'SHOWTOPICONS'
,p_display_name=>'For Top Level Items Only'
,p_display_sequence=>10
,p_list_template_id=>wwv_flow_api.id(29006620859135129)
,p_css_classes=>'t-LinksList--showTopIcons'
,p_group_id=>wwv_flow_api.id(29007766236135128)
,p_template_types=>'LIST'
,p_help_text=>'This will show icons for top level items of the list only. It will not show icons for sub lists.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29009597314135125)
,p_theme_id=>42
,p_name=>'LEFTICON'
,p_display_name=>'Left'
,p_display_sequence=>10
,p_button_template_id=>wwv_flow_api.id(29009114813135125)
,p_css_classes=>'t-Button--iconLeft'
,p_group_id=>wwv_flow_api.id(29009326980135125)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29009768909135125)
,p_theme_id=>42
,p_name=>'RIGHTICON'
,p_display_name=>'Right'
,p_display_sequence=>20
,p_button_template_id=>wwv_flow_api.id(29009114813135125)
,p_css_classes=>'t-Button--iconRight'
,p_group_id=>wwv_flow_api.id(29009326980135125)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29011605399135114)
,p_theme_id=>42
,p_name=>'FBM_LARGE'
,p_display_name=>'Large'
,p_display_sequence=>40
,p_css_classes=>'margin-bottom-lg'
,p_group_id=>wwv_flow_api.id(29011444010135114)
,p_template_types=>'FIELD'
,p_help_text=>'Adds a large bottom margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29012034720135114)
,p_theme_id=>42
,p_name=>'RBM_LARGE'
,p_display_name=>'Large'
,p_display_sequence=>40
,p_css_classes=>'margin-bottom-lg'
,p_group_id=>wwv_flow_api.id(29011846526135114)
,p_template_types=>'REGION'
,p_help_text=>'Adds a large bottom margin to the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29012293183135114)
,p_theme_id=>42
,p_name=>'FBM_MEDIUM'
,p_display_name=>'Medium'
,p_display_sequence=>30
,p_css_classes=>'margin-bottom-md'
,p_group_id=>wwv_flow_api.id(29011444010135114)
,p_template_types=>'FIELD'
,p_help_text=>'Adds a medium bottom margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29012485391135114)
,p_theme_id=>42
,p_name=>'RBM_MEDIUM'
,p_display_name=>'Medium'
,p_display_sequence=>30
,p_css_classes=>'margin-bottom-md'
,p_group_id=>wwv_flow_api.id(29011846526135114)
,p_template_types=>'REGION'
,p_help_text=>'Adds a medium bottom margin to the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29012697975135114)
,p_theme_id=>42
,p_name=>'FBM_NONE'
,p_display_name=>'None'
,p_display_sequence=>10
,p_css_classes=>'margin-bottom-none'
,p_group_id=>wwv_flow_api.id(29011444010135114)
,p_template_types=>'FIELD'
,p_help_text=>'Removes the bottom margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29012815196135114)
,p_theme_id=>42
,p_name=>'RBM_NONE'
,p_display_name=>'None'
,p_display_sequence=>10
,p_css_classes=>'margin-bottom-none'
,p_group_id=>wwv_flow_api.id(29011846526135114)
,p_template_types=>'REGION'
,p_help_text=>'Removes the bottom margin for this region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29013048501135114)
,p_theme_id=>42
,p_name=>'FBM_SMALL'
,p_display_name=>'Small'
,p_display_sequence=>20
,p_css_classes=>'margin-bottom-sm'
,p_group_id=>wwv_flow_api.id(29011444010135114)
,p_template_types=>'FIELD'
,p_help_text=>'Adds a small bottom margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29013278976135114)
,p_theme_id=>42
,p_name=>'RBM_SMALL'
,p_display_name=>'Small'
,p_display_sequence=>20
,p_css_classes=>'margin-bottom-sm'
,p_group_id=>wwv_flow_api.id(29011846526135114)
,p_template_types=>'REGION'
,p_help_text=>'Adds a small bottom margin to the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29013641819135114)
,p_theme_id=>42
,p_name=>'FLM_LARGE'
,p_display_name=>'Large'
,p_display_sequence=>40
,p_css_classes=>'margin-left-lg'
,p_group_id=>wwv_flow_api.id(29013441899135114)
,p_template_types=>'FIELD'
,p_help_text=>'Adds a large left margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29014040118135114)
,p_theme_id=>42
,p_name=>'RLM_LARGE'
,p_display_name=>'Large'
,p_display_sequence=>40
,p_css_classes=>'margin-left-lg'
,p_group_id=>wwv_flow_api.id(29013898525135114)
,p_template_types=>'REGION'
,p_help_text=>'Adds a large right margin to the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29014250650135114)
,p_theme_id=>42
,p_name=>'FLM_MEDIUM'
,p_display_name=>'Medium'
,p_display_sequence=>30
,p_css_classes=>'margin-left-md'
,p_group_id=>wwv_flow_api.id(29013441899135114)
,p_template_types=>'FIELD'
,p_help_text=>'Adds a medium left margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29014449337135114)
,p_theme_id=>42
,p_name=>'RLM_MEDIUM'
,p_display_name=>'Medium'
,p_display_sequence=>30
,p_css_classes=>'margin-left-md'
,p_group_id=>wwv_flow_api.id(29013898525135114)
,p_template_types=>'REGION'
,p_help_text=>'Adds a medium right margin to the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29014692551135114)
,p_theme_id=>42
,p_name=>'FLM_NONE'
,p_display_name=>'None'
,p_display_sequence=>10
,p_css_classes=>'margin-left-none'
,p_group_id=>wwv_flow_api.id(29013441899135114)
,p_template_types=>'FIELD'
,p_help_text=>'Removes the left margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29014837942135113)
,p_theme_id=>42
,p_name=>'RLM_NONE'
,p_display_name=>'None'
,p_display_sequence=>10
,p_css_classes=>'margin-left-none'
,p_group_id=>wwv_flow_api.id(29013898525135114)
,p_template_types=>'REGION'
,p_help_text=>'Removes the left margin from the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29015080420135113)
,p_theme_id=>42
,p_name=>'FLM_SMALL'
,p_display_name=>'Small'
,p_display_sequence=>20
,p_css_classes=>'margin-left-sm'
,p_group_id=>wwv_flow_api.id(29013441899135114)
,p_template_types=>'FIELD'
,p_help_text=>'Adds a small left margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29015271484135113)
,p_theme_id=>42
,p_name=>'RLM_SMALL'
,p_display_name=>'Small'
,p_display_sequence=>20
,p_css_classes=>'margin-left-sm'
,p_group_id=>wwv_flow_api.id(29013898525135114)
,p_template_types=>'REGION'
,p_help_text=>'Adds a small left margin to the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29015662981135113)
,p_theme_id=>42
,p_name=>'FRM_LARGE'
,p_display_name=>'Large'
,p_display_sequence=>40
,p_css_classes=>'margin-right-lg'
,p_group_id=>wwv_flow_api.id(29015484693135113)
,p_template_types=>'FIELD'
,p_help_text=>'Adds a large right margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29016088596135113)
,p_theme_id=>42
,p_name=>'RRM_LARGE'
,p_display_name=>'Large'
,p_display_sequence=>40
,p_css_classes=>'margin-right-lg'
,p_group_id=>wwv_flow_api.id(29015834039135113)
,p_template_types=>'REGION'
,p_help_text=>'Adds a large right margin to the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29016216040135113)
,p_theme_id=>42
,p_name=>'FRM_MEDIUM'
,p_display_name=>'Medium'
,p_display_sequence=>30
,p_css_classes=>'margin-right-md'
,p_group_id=>wwv_flow_api.id(29015484693135113)
,p_template_types=>'FIELD'
,p_help_text=>'Adds a medium right margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29016441946135113)
,p_theme_id=>42
,p_name=>'RRM_MEDIUM'
,p_display_name=>'Medium'
,p_display_sequence=>30
,p_css_classes=>'margin-right-md'
,p_group_id=>wwv_flow_api.id(29015834039135113)
,p_template_types=>'REGION'
,p_help_text=>'Adds a medium right margin to the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29016652892135113)
,p_theme_id=>42
,p_name=>'FRM_NONE'
,p_display_name=>'None'
,p_display_sequence=>10
,p_css_classes=>'margin-right-none'
,p_group_id=>wwv_flow_api.id(29015484693135113)
,p_template_types=>'FIELD'
,p_help_text=>'Removes the right margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29016841108135113)
,p_theme_id=>42
,p_name=>'RRM_NONE'
,p_display_name=>'None'
,p_display_sequence=>10
,p_css_classes=>'margin-right-none'
,p_group_id=>wwv_flow_api.id(29015834039135113)
,p_template_types=>'REGION'
,p_help_text=>'Removes the right margin from the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29017011024135113)
,p_theme_id=>42
,p_name=>'FRM_SMALL'
,p_display_name=>'Small'
,p_display_sequence=>20
,p_css_classes=>'margin-right-sm'
,p_group_id=>wwv_flow_api.id(29015484693135113)
,p_template_types=>'FIELD'
,p_help_text=>'Adds a small right margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29017283209135113)
,p_theme_id=>42
,p_name=>'RRM_SMALL'
,p_display_name=>'Small'
,p_display_sequence=>20
,p_css_classes=>'margin-right-sm'
,p_group_id=>wwv_flow_api.id(29015834039135113)
,p_template_types=>'REGION'
,p_help_text=>'Adds a small right margin to the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29017639902135113)
,p_theme_id=>42
,p_name=>'FTM_LARGE'
,p_display_name=>'Large'
,p_display_sequence=>40
,p_css_classes=>'margin-top-lg'
,p_group_id=>wwv_flow_api.id(29017459993135113)
,p_template_types=>'FIELD'
,p_help_text=>'Adds a large top margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29018072754135113)
,p_theme_id=>42
,p_name=>'RTM_LARGE'
,p_display_name=>'Large'
,p_display_sequence=>40
,p_css_classes=>'margin-top-lg'
,p_group_id=>wwv_flow_api.id(29017833520135113)
,p_template_types=>'REGION'
,p_help_text=>'Adds a large top margin to the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29018292162135113)
,p_theme_id=>42
,p_name=>'FTM_MEDIUM'
,p_display_name=>'Medium'
,p_display_sequence=>30
,p_css_classes=>'margin-top-md'
,p_group_id=>wwv_flow_api.id(29017459993135113)
,p_template_types=>'FIELD'
,p_help_text=>'Adds a medium top margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29018444134135113)
,p_theme_id=>42
,p_name=>'RTM_MEDIUM'
,p_display_name=>'Medium'
,p_display_sequence=>30
,p_css_classes=>'margin-top-md'
,p_group_id=>wwv_flow_api.id(29017833520135113)
,p_template_types=>'REGION'
,p_help_text=>'Adds a medium top margin to the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29018641959135113)
,p_theme_id=>42
,p_name=>'FTM_NONE'
,p_display_name=>'None'
,p_display_sequence=>10
,p_css_classes=>'margin-top-none'
,p_group_id=>wwv_flow_api.id(29017459993135113)
,p_template_types=>'FIELD'
,p_help_text=>'Removes the top margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29018826779135113)
,p_theme_id=>42
,p_name=>'RTM_NONE'
,p_display_name=>'None'
,p_display_sequence=>10
,p_css_classes=>'margin-top-none'
,p_group_id=>wwv_flow_api.id(29017833520135113)
,p_template_types=>'REGION'
,p_help_text=>'Removes the top margin for this region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29019059443135113)
,p_theme_id=>42
,p_name=>'FTM_SMALL'
,p_display_name=>'Small'
,p_display_sequence=>20
,p_css_classes=>'margin-top-sm'
,p_group_id=>wwv_flow_api.id(29017459993135113)
,p_template_types=>'FIELD'
,p_help_text=>'Adds a small top margin for this field.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29019238067135113)
,p_theme_id=>42
,p_name=>'RTM_SMALL'
,p_display_name=>'Small'
,p_display_sequence=>20
,p_css_classes=>'margin-top-sm'
,p_group_id=>wwv_flow_api.id(29017833520135113)
,p_template_types=>'REGION'
,p_help_text=>'Adds a small top margin to the region.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29019600273135112)
,p_theme_id=>42
,p_name=>'DANGER'
,p_display_name=>'Danger'
,p_display_sequence=>30
,p_css_classes=>'t-Button--danger'
,p_group_id=>wwv_flow_api.id(29019437388135112)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29020068996135112)
,p_theme_id=>42
,p_name=>'LARGEBOTTOMMARGIN'
,p_display_name=>'Large'
,p_display_sequence=>20
,p_css_classes=>'t-Button--gapBottom'
,p_group_id=>wwv_flow_api.id(29019872084135112)
,p_template_types=>'BUTTON'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29020499136135112)
,p_theme_id=>42
,p_name=>'LARGELEFTMARGIN'
,p_display_name=>'Large'
,p_display_sequence=>20
,p_css_classes=>'t-Button--gapLeft'
,p_group_id=>wwv_flow_api.id(29020233236135112)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29020819475135112)
,p_theme_id=>42
,p_name=>'LARGERIGHTMARGIN'
,p_display_name=>'Large'
,p_display_sequence=>20
,p_css_classes=>'t-Button--gapRight'
,p_group_id=>wwv_flow_api.id(29020676705135112)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29021230990135112)
,p_theme_id=>42
,p_name=>'LARGETOPMARGIN'
,p_display_name=>'Large'
,p_display_sequence=>20
,p_css_classes=>'t-Button--gapTop'
,p_group_id=>wwv_flow_api.id(29021071146135112)
,p_template_types=>'BUTTON'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29021606110135112)
,p_theme_id=>42
,p_name=>'LARGE'
,p_display_name=>'Large'
,p_display_sequence=>30
,p_css_classes=>'t-Button--large'
,p_group_id=>wwv_flow_api.id(29021406045135112)
,p_template_types=>'BUTTON'
,p_help_text=>'A large button.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29022069604135112)
,p_theme_id=>42
,p_name=>'DISPLAY_AS_LINK'
,p_display_name=>'Display as Link'
,p_display_sequence=>30
,p_css_classes=>'t-Button--link'
,p_group_id=>wwv_flow_api.id(29021842886135112)
,p_template_types=>'BUTTON'
,p_help_text=>'This option makes the button appear as a text link.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29022298771135112)
,p_theme_id=>42
,p_name=>'NOUI'
,p_display_name=>'Remove UI Decoration'
,p_display_sequence=>20
,p_css_classes=>'t-Button--noUI'
,p_group_id=>wwv_flow_api.id(29021842886135112)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29022458514135112)
,p_theme_id=>42
,p_name=>'SMALLBOTTOMMARGIN'
,p_display_name=>'Small'
,p_display_sequence=>10
,p_css_classes=>'t-Button--padBottom'
,p_group_id=>wwv_flow_api.id(29019872084135112)
,p_template_types=>'BUTTON'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29022653543135112)
,p_theme_id=>42
,p_name=>'SMALLLEFTMARGIN'
,p_display_name=>'Small'
,p_display_sequence=>10
,p_css_classes=>'t-Button--padLeft'
,p_group_id=>wwv_flow_api.id(29020233236135112)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29022863202135112)
,p_theme_id=>42
,p_name=>'SMALLRIGHTMARGIN'
,p_display_name=>'Small'
,p_display_sequence=>10
,p_css_classes=>'t-Button--padRight'
,p_group_id=>wwv_flow_api.id(29020676705135112)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29023055872135112)
,p_theme_id=>42
,p_name=>'SMALLTOPMARGIN'
,p_display_name=>'Small'
,p_display_sequence=>10
,p_css_classes=>'t-Button--padTop'
,p_group_id=>wwv_flow_api.id(29021071146135112)
,p_template_types=>'BUTTON'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29023410236135112)
,p_theme_id=>42
,p_name=>'PILL'
,p_display_name=>'Inner Button'
,p_display_sequence=>20
,p_css_classes=>'t-Button--pill'
,p_group_id=>wwv_flow_api.id(29023248271135112)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29023605661135112)
,p_theme_id=>42
,p_name=>'PILLEND'
,p_display_name=>'Last Button'
,p_display_sequence=>30
,p_css_classes=>'t-Button--pillEnd'
,p_group_id=>wwv_flow_api.id(29023248271135112)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29023804151135112)
,p_theme_id=>42
,p_name=>'PILLSTART'
,p_display_name=>'First Button'
,p_display_sequence=>10
,p_css_classes=>'t-Button--pillStart'
,p_group_id=>wwv_flow_api.id(29023248271135112)
,p_template_types=>'BUTTON'
,p_help_text=>'Use this for the start of a pill button.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29024080704135111)
,p_theme_id=>42
,p_name=>'PRIMARY'
,p_display_name=>'Primary'
,p_display_sequence=>10
,p_css_classes=>'t-Button--primary'
,p_group_id=>wwv_flow_api.id(29019437388135112)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29024256996135111)
,p_theme_id=>42
,p_name=>'SIMPLE'
,p_display_name=>'Simple'
,p_display_sequence=>10
,p_css_classes=>'t-Button--simple'
,p_group_id=>wwv_flow_api.id(29021842886135112)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29024406354135111)
,p_theme_id=>42
,p_name=>'SMALL'
,p_display_name=>'Small'
,p_display_sequence=>20
,p_css_classes=>'t-Button--small'
,p_group_id=>wwv_flow_api.id(29021406045135112)
,p_template_types=>'BUTTON'
,p_help_text=>'A small button.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29024856454135111)
,p_theme_id=>42
,p_name=>'STRETCH'
,p_display_name=>'Stretch'
,p_display_sequence=>10
,p_css_classes=>'t-Button--stretch'
,p_group_id=>wwv_flow_api.id(29024681779135111)
,p_template_types=>'BUTTON'
,p_help_text=>'Stretches button to fill container'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29025028166135111)
,p_theme_id=>42
,p_name=>'SUCCESS'
,p_display_name=>'Success'
,p_display_sequence=>40
,p_css_classes=>'t-Button--success'
,p_group_id=>wwv_flow_api.id(29019437388135112)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29025225048135111)
,p_theme_id=>42
,p_name=>'TINY'
,p_display_name=>'Tiny'
,p_display_sequence=>10
,p_css_classes=>'t-Button--tiny'
,p_group_id=>wwv_flow_api.id(29021406045135112)
,p_template_types=>'BUTTON'
,p_help_text=>'A very small button.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29025410532135111)
,p_theme_id=>42
,p_name=>'WARNING'
,p_display_name=>'Warning'
,p_display_sequence=>20
,p_css_classes=>'t-Button--warning'
,p_group_id=>wwv_flow_api.id(29019437388135112)
,p_template_types=>'BUTTON'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29025830803135111)
,p_theme_id=>42
,p_name=>'SHOWFORMLABELSABOVE'
,p_display_name=>'Show Form Labels Above'
,p_display_sequence=>10
,p_css_classes=>'t-Form--labelsAbove'
,p_group_id=>wwv_flow_api.id(29025662985135111)
,p_template_types=>'REGION'
,p_help_text=>'Show form labels above input fields.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29026234153135111)
,p_theme_id=>42
,p_name=>'FORMSIZELARGE'
,p_display_name=>'Large'
,p_display_sequence=>10
,p_css_classes=>'t-Form--large'
,p_group_id=>wwv_flow_api.id(29026082898135111)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29026647792135111)
,p_theme_id=>42
,p_name=>'FORMLEFTLABELS'
,p_display_name=>'Left'
,p_display_sequence=>20
,p_css_classes=>'t-Form--leftLabels'
,p_group_id=>wwv_flow_api.id(29026460276135111)
,p_template_types=>'REGION'
,p_help_text=>'Align form labels to left.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29027090622135110)
,p_theme_id=>42
,p_name=>'FORMREMOVEPADDING'
,p_display_name=>'Remove Padding'
,p_display_sequence=>20
,p_css_classes=>'t-Form--noPadding'
,p_group_id=>wwv_flow_api.id(29026815865135111)
,p_template_types=>'REGION'
,p_help_text=>'Removes padding between items.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29027296034135110)
,p_theme_id=>42
,p_name=>'FORMSLIMPADDING'
,p_display_name=>'Slim Padding'
,p_display_sequence=>10
,p_css_classes=>'t-Form--slimPadding'
,p_group_id=>wwv_flow_api.id(29026815865135111)
,p_template_types=>'REGION'
,p_help_text=>'Reduces form item padding to 4px.'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29027632171135110)
,p_theme_id=>42
,p_name=>'STRETCH_FORM_FIELDS'
,p_display_name=>'Stretch Form Fields'
,p_display_sequence=>10
,p_css_classes=>'t-Form--stretchInputs'
,p_group_id=>wwv_flow_api.id(29027446507135110)
,p_template_types=>'REGION'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29027869174135110)
,p_theme_id=>42
,p_name=>'FORMSIZEXLARGE'
,p_display_name=>'X Large'
,p_display_sequence=>20
,p_css_classes=>'t-Form--xlarge'
,p_group_id=>wwv_flow_api.id(29026082898135111)
,p_template_types=>'REGION'
,p_is_advanced=>'N'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29028225157135110)
,p_theme_id=>42
,p_name=>'LARGE_FIELD'
,p_display_name=>'Large'
,p_display_sequence=>10
,p_css_classes=>'t-Form-fieldContainer--large'
,p_group_id=>wwv_flow_api.id(29028095758135110)
,p_template_types=>'FIELD'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29028696015135110)
,p_theme_id=>42
,p_name=>'POST_TEXT_BLOCK'
,p_display_name=>'Display as Block'
,p_display_sequence=>10
,p_css_classes=>'t-Form-fieldContainer--postTextBlock'
,p_group_id=>wwv_flow_api.id(29028448234135110)
,p_template_types=>'FIELD'
,p_help_text=>'Displays the Item Post Text in a block style immediately after the item.'
);
end;
/
begin
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29029034113135110)
,p_theme_id=>42
,p_name=>'PRE_TEXT_BLOCK'
,p_display_name=>'Display as Block'
,p_display_sequence=>10
,p_css_classes=>'t-Form-fieldContainer--preTextBlock'
,p_group_id=>wwv_flow_api.id(29028834662135110)
,p_template_types=>'FIELD'
,p_help_text=>'Displays the Item Pre Text in a block style immediately before the item.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29029423478135110)
,p_theme_id=>42
,p_name=>'DISPLAY_AS_PILL_BUTTON'
,p_display_name=>'Display as Pill Button'
,p_display_sequence=>10
,p_css_classes=>'t-Form-fieldContainer--radioButtonGroup'
,p_group_id=>wwv_flow_api.id(29029269839135110)
,p_template_types=>'FIELD'
,p_help_text=>'Displays the radio buttons to look like a button set / pill button.  Note that the the radio buttons must all be in the same row for this option to work.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29029608054135110)
,p_theme_id=>42
,p_name=>'STRETCH_FORM_ITEM'
,p_display_name=>'Stretch Form Item'
,p_display_sequence=>10
,p_css_classes=>'t-Form-fieldContainer--stretchInputs'
,p_template_types=>'FIELD'
,p_help_text=>'Stretches the form item to fill its container.'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29029867954135110)
,p_theme_id=>42
,p_name=>'X_LARGE_SIZE'
,p_display_name=>'X Large'
,p_display_sequence=>20
,p_css_classes=>'t-Form-fieldContainer--xlarge'
,p_group_id=>wwv_flow_api.id(29028095758135110)
,p_template_types=>'FIELD'
);
wwv_flow_api.create_template_option(
 p_id=>wwv_flow_api.id(29030260745135110)
,p_theme_id=>42
,p_name=>'HIDE_WHEN_ALL_ROWS_DISPLAYED'
,p_display_name=>'Hide when all rows displayed'
,p_display_sequence=>10
,p_css_classes=>'t-Report--hideNoPagination'
,p_group_id=>wwv_flow_api.id(29030073696135110)
,p_template_types=>'REPORT'
,p_help_text=>'This option will hide the pagination when all rows are displayed.'
);
end;
/
prompt --application/shared_components/logic/build_options
begin
wwv_flow_api.create_build_option(
 p_id=>wwv_flow_api.id(39885966879027873)
,p_build_option_name=>'Link to Builder'
,p_build_option_status=>'INCLUDE'
,p_on_upgrade_keep_status=>true
);
end;
/
prompt --application/shared_components/globalization/language
begin
wwv_flow_api.create_language_map(
 p_id=>wwv_flow_api.id(39933748460081199)
,p_translation_flow_id=>901305012
,p_translation_flow_language_cd=>'en'
,p_direction_right_to_left=>'N'
);
end;
/
prompt --application/shared_components/globalization/translations
begin
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39933803442081185)
,p_page_id=>0
,p_translated_flow_id=>901305012
,p_translate_to_id=>.901305012
,p_translate_from_id=>0
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Global Page - Desktop'
,p_translate_from_text=>'Global Page - Desktop'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39934055549081185)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>1.901305012
,p_translate_from_id=>1
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Home'
,p_translate_from_text=>'Home'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39934689863081184)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>200.901305012
,p_translate_from_id=>200
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Rules'
,p_translate_from_text=>'Manage Rules'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39934871677081184)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>201.901305012
,p_translate_from_id=>201
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Rule'
,p_translate_from_text=>'Manage Rule'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39935084788081184)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>202.901305012
,p_translate_from_id=>202
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Statements per rule'
,p_translate_from_text=>'Statements per rule'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39935242992081184)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>203.901305012
,p_translate_from_id=>203
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Form statement per rule'
,p_translate_from_text=>'Form statement per rule'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39935454328081184)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>210.901305012
,p_translate_from_id=>210
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Apex Versions'
,p_translate_from_text=>'Manage Apex Versions'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39935634404081184)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>211.901305012
,p_translate_from_id=>211
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Apex Version'
,p_translate_from_text=>'Manage Apex Version'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39935893915081184)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>214.901305012
,p_translate_from_id=>214
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Projects'
,p_translate_from_text=>'Manage Projects'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39936067376081184)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>215.901305012
,p_translate_from_id=>215
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Project'
,p_translate_from_text=>'Manage Project'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39936282712081184)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>216.901305012
,p_translate_from_id=>216
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage rules for projects'
,p_translate_from_text=>'Manage rules for projects'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39936425149081184)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>217.901305012
,p_translate_from_id=>217
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Add Statements'
,p_translate_from_text=>'Add Statements'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39936611546081184)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>218.901305012
,p_translate_from_id=>218
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Add Application(s)'
,p_translate_from_text=>'Add Applications'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39936859290081184)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>220.901305012
,p_translate_from_id=>220
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Choose Project'
,p_translate_from_text=>'Choose Project'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39937033485081184)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>300.901305012
,p_translate_from_id=>300
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Run Result'
,p_translate_from_text=>'Run Result'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39937211814081184)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>9999.901305012
,p_translate_from_id=>9999
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Login Page'
,p_translate_from_text=>'Login Page'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39937493381081182)
,p_page_id=>0
,p_translated_flow_id=>901305012
,p_translate_to_id=>.901305012
,p_translate_from_id=>0
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Global Page - Desktop'
,p_translate_from_text=>'Global Page - Desktop'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39937630103081182)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>1.901305012
,p_translate_from_id=>1
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Coding Conventions Checker'
,p_translate_from_text=>'Coding Conventions Checker'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39938209166081182)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>200.901305012
,p_translate_from_id=>200
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Rules'
,p_translate_from_text=>'Manage Rules'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39938483210081182)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>201.901305012
,p_translate_from_id=>201
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Rule'
,p_translate_from_text=>'Manage Rule'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39938695974081182)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>202.901305012
,p_translate_from_id=>202
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Statements per rule'
,p_translate_from_text=>'Statements per rule'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39938871171081182)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>203.901305012
,p_translate_from_id=>203
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Form statement per rule'
,p_translate_from_text=>'Form statement per rule'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39939078168081181)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>210.901305012
,p_translate_from_id=>210
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Apex Versions'
,p_translate_from_text=>'Manage Apex Versions'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39939244758081181)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>211.901305012
,p_translate_from_id=>211
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Apex Version'
,p_translate_from_text=>'Manage Apex Version'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39939482206081181)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>214.901305012
,p_translate_from_id=>214
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Projects'
,p_translate_from_text=>'Manage Projects'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39939694442081181)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>215.901305012
,p_translate_from_id=>215
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Project'
,p_translate_from_text=>'Manage Project'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39939864067081181)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>216.901305012
,p_translate_from_id=>216
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage rules for projects'
,p_translate_from_text=>'Manage rules for projects'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39940035332081181)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>217.901305012
,p_translate_from_id=>217
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Add Statements'
,p_translate_from_text=>'Add Statements'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39940272273081181)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>218.901305012
,p_translate_from_id=>218
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Add Application(s)'
,p_translate_from_text=>'Add Applications'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39940485272081181)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>220.901305012
,p_translate_from_id=>220
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Choose Project'
,p_translate_from_text=>'Choose Project'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39940674335081181)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>300.901305012
,p_translate_from_id=>300
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Run Result'
,p_translate_from_text=>'Results'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39940816754081181)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>9999.901305012
,p_translate_from_id=>9999
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Coding Conventions Checker - Sign In'
,p_translate_from_text=>'Coding Conventions Checker - Sign In'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39941086560081173)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>217.901305012
,p_translate_from_id=>217
,p_translate_column_id=>12
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<p>To track standards for application(s) check the application and click the <b>Add Applications</b> button.</p>'
,p_translate_from_text=>'<p>To track standards for application(s) check the application and click the <b>Add Applications</b> button.</p>'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39941297206081173)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>218.901305012
,p_translate_from_id=>218
,p_translate_column_id=>12
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<p>To track standards for application(s) check the application and click the <b>Add Applications</b> button.</p>'
,p_translate_from_text=>'<p>To track standards for application(s) check the application and click the <b>Add Applications</b> button.</p>'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39941426257081171)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34030975428587086.901305012)
,p_translate_from_id=>wwv_flow_api.id(34030975428587086)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Create'
,p_translate_from_text=>'Create'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39941637731081171)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407840573820944.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407840573820944)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Create St'
,p_translate_from_text=>'Create St'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39941855631081171)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34032622778587085.901305012)
,p_translate_from_id=>wwv_flow_api.id(34032622778587085)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Cancel'
,p_translate_from_text=>'Cancel'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39942031118081171)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34032515752587085.901305012)
,p_translate_from_id=>wwv_flow_api.id(34032515752587085)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Delete'
,p_translate_from_text=>'Delete'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39942272124081171)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34032401739587085.901305012)
,p_translate_from_id=>wwv_flow_api.id(34032401739587085)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Save'
,p_translate_from_text=>'Save'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39942461622081171)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34032359666587085.901305012)
,p_translate_from_id=>wwv_flow_api.id(34032359666587085)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Create'
,p_translate_from_text=>'Create'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39942651258081171)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34086544373083520.901305012)
,p_translate_from_id=>wwv_flow_api.id(34086544373083520)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Cancel'
,p_translate_from_text=>'Cancel'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39942802374081171)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34086467756083520.901305012)
,p_translate_from_id=>wwv_flow_api.id(34086467756083520)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Delete'
,p_translate_from_text=>'Delete'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39943017652081171)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34086300793083520.901305012)
,p_translate_from_id=>wwv_flow_api.id(34086300793083520)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Apply Changes'
,p_translate_from_text=>'Apply Changes'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39943261051081171)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34086247051083520.901305012)
,p_translate_from_id=>wwv_flow_api.id(34086247051083520)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Create'
,p_translate_from_text=>'Create'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39943432382081171)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29080348373826697.901305012)
,p_translate_from_id=>wwv_flow_api.id(29080348373826697)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Create'
,p_translate_from_text=>'Create'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39943653406081171)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29082037944826696.901305012)
,p_translate_from_id=>wwv_flow_api.id(29082037944826696)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Cancel'
,p_translate_from_text=>'Cancel'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39943862334081171)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29081977540826696.901305012)
,p_translate_from_id=>wwv_flow_api.id(29081977540826696)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Delete'
,p_translate_from_text=>'Delete'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39944049682081171)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29081836824826696.901305012)
,p_translate_from_id=>wwv_flow_api.id(29081836824826696)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Save'
,p_translate_from_text=>'Save'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39944220125081171)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29081792767826696.901305012)
,p_translate_from_id=>wwv_flow_api.id(29081792767826696)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Create'
,p_translate_from_text=>'Create'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39944441826081171)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29116252307788990.901305012)
,p_translate_from_id=>wwv_flow_api.id(29116252307788990)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Create'
,p_translate_from_text=>'Create'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39944652480081170)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39257060349502732.901305012)
,p_translate_from_id=>wwv_flow_api.id(39257060349502732)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Add Application(s)'
,p_translate_from_text=>'Add Application'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39945035787081170)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29117946813788990.901305012)
,p_translate_from_id=>wwv_flow_api.id(29117946813788990)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Cancel'
,p_translate_from_text=>'Cancel'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39945218662081170)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29117881820788990.901305012)
,p_translate_from_id=>wwv_flow_api.id(29117881820788990)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Delete'
,p_translate_from_text=>'Delete'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39945485224081170)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29117787587788990.901305012)
,p_translate_from_id=>wwv_flow_api.id(29117787587788990)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Save'
,p_translate_from_text=>'Save'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39945623322081170)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29117612058788990.901305012)
,p_translate_from_id=>wwv_flow_api.id(29117612058788990)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Create'
,p_translate_from_text=>'Create'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39945801478081170)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34356741411804110.901305012)
,p_translate_from_id=>wwv_flow_api.id(34356741411804110)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Reset'
,p_translate_from_text=>'Reset'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39946019461081170)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34357473452804110.901305012)
,p_translate_from_id=>wwv_flow_api.id(34357473452804110)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Add Statements'
,p_translate_from_text=>'Add Statements'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39946297531081170)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34264646013219018.901305012)
,p_translate_from_id=>wwv_flow_api.id(34264646013219018)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Cancel'
,p_translate_from_text=>'Cancel'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39946463939081170)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39369976710101291.901305012)
,p_translate_from_id=>wwv_flow_api.id(39369976710101291)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Reset'
,p_translate_from_text=>'Reset'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39946674951081170)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39370366429101290.901305012)
,p_translate_from_id=>wwv_flow_api.id(39370366429101290)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Add Application(s)'
,p_translate_from_text=>'Add Applications'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39946801984081170)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39370779918101290.901305012)
,p_translate_from_id=>wwv_flow_api.id(39370779918101290)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Cancel'
,p_translate_from_text=>'Cancel'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39947001081081170)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39875680165883406.901305012)
,p_translate_from_id=>wwv_flow_api.id(39875680165883406)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Cancel'
,p_translate_from_text=>'Cancel'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39947278645081170)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39774201117497709.901305012)
,p_translate_from_id=>wwv_flow_api.id(39774201117497709)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Confirm'
,p_translate_from_text=>'Confirm'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39947497964081170)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39428140124667319.901305012)
,p_translate_from_id=>wwv_flow_api.id(39428140124667319)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Start'
,p_translate_from_text=>'Start'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39947681625081170)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29037994104135040.901305012)
,p_translate_from_id=>wwv_flow_api.id(29037994104135040)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Sign In'
,p_translate_from_text=>'Sign In'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39947846235081168)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035574074135049.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035574074135049)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'username'
,p_translate_from_text=>'username'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39948080722081168)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035962092135048.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035962092135048)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'password'
,p_translate_from_text=>'password'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39948295702081168)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29037019371135044.901305012)
,p_translate_from_id=>wwv_flow_api.id(29037019371135044)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Remember username'
,p_translate_from_text=>'Remember username'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39948403081081168)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29084020572826695.901305012)
,p_translate_from_id=>wwv_flow_api.id(29084020572826695)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'APEX version'
,p_translate_from_text=>'Deapeid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39948665160081168)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29084401693826695.901305012)
,p_translate_from_id=>wwv_flow_api.id(29084401693826695)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Apex Version'
,p_translate_from_text=>'Deapexv'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39948885728081167)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29119906758788989.901305012)
,p_translate_from_id=>wwv_flow_api.id(29119906758788989)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Project'
,p_translate_from_text=>'Deproid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39949076501081167)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29120325710788989.901305012)
,p_translate_from_id=>wwv_flow_api.id(29120325710788989)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'APEX version'
,p_translate_from_text=>'Deapeid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39949250635081167)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29120722589788989.901305012)
,p_translate_from_id=>wwv_flow_api.id(29120722589788989)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Description'
,p_translate_from_text=>'Dedescr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39949497570081167)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316114592630902.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316114592630902)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Project'
,p_translate_from_text=>'Deproid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39949616081081167)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316461016630905.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316461016630905)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Description'
,p_translate_from_text=>'Dedescr'
);
end;
/
begin
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39949802300081167)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29404460001820910.901305012)
,p_translate_from_id=>wwv_flow_api.id(29404460001820910)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'APEX Number'
,p_translate_from_text=>'Deapenr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39950049020081167)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405127966820917.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405127966820917)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Name'
,p_translate_from_text=>'Dername'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39950261887081167)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405295312820918.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405295312820918)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Description'
,p_translate_from_text=>'Dedescr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39950475488081167)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405398360820919.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405398360820919)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Gradation'
,p_translate_from_text=>'Degraid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39950675063081167)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34034698486587083.901305012)
,p_translate_from_id=>wwv_flow_api.id(34034698486587083)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule'
,p_translate_from_text=>'Derulid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39950832749081167)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34089742850083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34089742850083517)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Link'
,p_translate_from_text=>'Derlink'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39951020395081167)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34090165831083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34090165831083517)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Binding Variable'
,p_translate_from_text=>'Deapbiv'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39951296866081167)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34090520576083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34090520576083517)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Statement'
,p_translate_from_text=>'Destate'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39951857912081166)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39430342639667341.901305012)
,p_translate_from_id=>wwv_flow_api.id(39430342639667341)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Help'
,p_translate_from_text=>'Help'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39952045976081166)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34034207929587083.901305012)
,p_translate_from_id=>wwv_flow_api.id(34034207929587083)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rowid'
,p_translate_from_text=>'Rowid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39952208886081166)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34088583739083518.901305012)
,p_translate_from_id=>wwv_flow_api.id(34088583739083518)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Code'
,p_translate_from_text=>'Derlcid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39952401537081166)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34088910359083518.901305012)
,p_translate_from_id=>wwv_flow_api.id(34088910359083518)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'APEX version'
,p_translate_from_text=>'Deapeid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39952640533081166)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39878020111883403.901305012)
,p_translate_from_id=>wwv_flow_api.id(39878020111883403)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Project'
,p_translate_from_text=>'Deproid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39952842151081164)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34265226271219024.901305012)
,p_translate_from_id=>wwv_flow_api.id(34265226271219024)
,p_translate_column_id=>16
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'#LABEL# must be used in the validation query.'
,p_translate_from_text=>'#LABEL# must be used in the validation query.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39953004053081164)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34358877650804109.901305012)
,p_translate_from_id=>wwv_flow_api.id(34358877650804109)
,p_translate_column_id=>16
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Please check an statement to add.'
,p_translate_from_text=>'Please check an statement to add.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39953266896081164)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39371275384101287.901305012)
,p_translate_from_id=>wwv_flow_api.id(39371275384101287)
,p_translate_column_id=>16
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Please check an application to add.'
,p_translate_from_text=>'Please check an application to add.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39953453602081161)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34036161691587082.901305012)
,p_translate_from_id=>wwv_flow_api.id(34036161691587082)
,p_translate_column_id=>18
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Action Processed.'
,p_translate_from_text=>'Action Processed.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39953639008081161)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34095156904083515.901305012)
,p_translate_from_id=>wwv_flow_api.id(34095156904083515)
,p_translate_column_id=>18
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Action Processed.'
,p_translate_from_text=>'Action Processed.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39953800470081161)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29089035435826694.901305012)
,p_translate_from_id=>wwv_flow_api.id(29089035435826694)
,p_translate_column_id=>18
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Action Processed.'
,p_translate_from_text=>'Action Processed.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39954090083081161)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29125611403788987.901305012)
,p_translate_from_id=>wwv_flow_api.id(29125611403788987)
,p_translate_column_id=>18
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Action Processed.'
,p_translate_from_text=>'Action Processed.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39954275698081161)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34359181734804109.901305012)
,p_translate_from_id=>wwv_flow_api.id(34359181734804109)
,p_translate_column_id=>18
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Statement(s) added'
,p_translate_from_text=>'Statement(s) added'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39954431489081161)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39371593834101287.901305012)
,p_translate_from_id=>wwv_flow_api.id(39371593834101287)
,p_translate_column_id=>18
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Application(s) added'
,p_translate_from_text=>'Application(s) added'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39954665697081158)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29042021766135027.901305012)
,p_translate_from_id=>wwv_flow_api.id(29042021766135027)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Coding Conventions Checker'
,p_translate_from_text=>'Coding Conventions Checker'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39955019260081157)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39255522007502717.901305012)
,p_translate_from_id=>wwv_flow_api.id(39255522007502717)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Projects'
,p_translate_from_text=>'History Bugs'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39955624774081157)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34023606834587092.901305012)
,p_translate_from_id=>wwv_flow_api.id(34023606834587092)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Manage Rules'
,p_translate_from_text=>'Manage Rules'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39955847356081157)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34031963879587085.901305012)
,p_translate_from_id=>wwv_flow_api.id(34031963879587085)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Manage Rule'
,p_translate_from_text=>'Manage Rule'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39956026607081157)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405515958820921.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405515958820921)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Statements per rule'
,p_translate_from_text=>'Statements per rule'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39956216808081157)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34070858865228366.901305012)
,p_translate_from_id=>wwv_flow_api.id(34070858865228366)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Statements per rule'
,p_translate_from_text=>'Statements per rule'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39956401819081157)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34085831334083520.901305012)
,p_translate_from_id=>wwv_flow_api.id(34085831334083520)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Form statement per rule'
,p_translate_from_text=>'Form statement per rule'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39956809613081157)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29074299873826705.901305012)
,p_translate_from_id=>wwv_flow_api.id(29074299873826705)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Manage Apex Versions'
,p_translate_from_text=>'Manage Apex Versions'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39957064626081157)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29081388327826696.901305012)
,p_translate_from_id=>wwv_flow_api.id(29081388327826696)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Manage Apex Version'
,p_translate_from_text=>'Manage Apex Version'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39957259751081157)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29109531489788995.901305012)
,p_translate_from_id=>wwv_flow_api.id(29109531489788995)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Manage Projects'
,p_translate_from_text=>'Manage Projects'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39957621220081157)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29117224403788990.901305012)
,p_translate_from_id=>wwv_flow_api.id(29117224403788990)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Manage Project'
,p_translate_from_text=>'Manage Project'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39957855977081157)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39256930946502731.901305012)
,p_translate_from_id=>wwv_flow_api.id(39256930946502731)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Applications per project'
,p_translate_from_text=>'Applications per project'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39958075490081157)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316067573630901.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316067573630901)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Manage rules for projects'
,p_translate_from_text=>'Manage rules for projects'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39958292924081155)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316521513630906.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316521513630906)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Labels'
,p_translate_from_text=>'Labels'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39958457234081155)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316997309630910.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316997309630910)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Buttons'
,p_translate_from_text=>'Buttons'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39958689753081155)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(1481924344399434654.901305012)
,p_translate_from_id=>wwv_flow_api.id(1481924344399434654)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Add Statements'
,p_translate_from_text=>'Add Statements'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39958863069081155)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(1521289983605535955.901305012)
,p_translate_from_id=>wwv_flow_api.id(1521289983605535955)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Add Application(s)'
,p_translate_from_text=>'Add Applications'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39959069464081155)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39874500802883406.901305012)
,p_translate_from_id=>wwv_flow_api.id(39874500802883406)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Choose Project'
,p_translate_from_text=>'Choose Project'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39959242182081155)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39875204544883406.901305012)
,p_translate_from_id=>wwv_flow_api.id(39875204544883406)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Buttons'
,p_translate_from_text=>'Buttons'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39959444016081155)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34671456281337179.901305012)
,p_translate_from_id=>wwv_flow_api.id(34671456281337179)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Run Result'
,p_translate_from_text=>'Results'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39959605872081155)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39427743103667315.901305012)
,p_translate_from_id=>wwv_flow_api.id(39427743103667315)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Project'
,p_translate_from_text=>'Project'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39959876974081155)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035184224135054.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035184224135054)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Coding Conventions Checker'
,p_translate_from_text=>'Coding Conventions Checker'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39960048800081155)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29039822450135034.901305012)
,p_translate_from_id=>wwv_flow_api.id(29039822450135034)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Language Selector'
,p_translate_from_text=>'Language Selector'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39960468587081152)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39255522007502717.901305012)
,p_translate_from_id=>wwv_flow_api.id(39255522007502717)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT *',
'FROM (',
'  SELECT a.deproid, a.detypid, c.dedescr,a.decredt',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
')',
'PIVOT (',
'  count(*) as count',
'  FOR detypid IN (1,2,3)',
' )*/',
' ',
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid, b.decredt',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 1',
'group by b.derunid, c.dedescr, b.decredt, b.detypid',
'order by b.derunid;',
''))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT *',
'FROM (',
'  SELECT a.deproid, a.detypid, c.dedescr,a.decredt',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
')',
'PIVOT (',
'  count(*) as count',
'  FOR detypid IN (1,2,3)',
' )*/',
' ',
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid, b.decredt',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 1',
'group by b.derunid, c.dedescr, b.decredt, b.detypid',
'order by b.derunid;',
''))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39960621446081152)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34023606834587092.901305012)
,p_translate_from_id=>wwv_flow_api.id(34023606834587092)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select A.DERULID,',
'       A.DERNAME,',
'       A.DEGRAID,',
'       B.DEDESCR,',
'       A.DEDESCR as DEGRADDE,',
'       a.detypid,',
'       r.dedescr as dertype_d,',
'       A.DECREDT,',
'       A.DECREUS,',
'       A.DEWYZDT,',
'       A.DEWYZUS',
'  from DRRULES A, DRGRADA B, drrtype r',
'where A.DEGRAID = B.DEGRAID',
'and    a.detypid = r.detypid',
'order by A.DERNAME'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select A.DERULID,',
'       A.DERNAME,',
'       A.DEGRAID,',
'       B.DEDESCR,',
'       A.DEDESCR as DEGRADDE,',
'       a.detypid,',
'       r.dedescr as dertype_d,',
'       A.DECREDT,',
'       A.DECREUS,',
'       A.DEWYZDT,',
'       A.DEWYZUS',
'  from DRRULES A, DRGRADA B, drrtype r',
'where A.DEGRAID = B.DEGRAID',
'and    a.detypid = r.detypid',
'order by A.DERNAME'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39960886904081152)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405515958820921.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405515958820921)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select A.DERLCID,',
'       A.DERULID,',
'       A.DEAPEID,',
'       C.DEAPEXV,',
'       A.DERLINK,',
'       A.DEAPBIV,',
'       A.DESTATE     ',
'from DRRULCE A , DRRULES B, DRAPEXV C',
'where A.DERULID = :P201_DERULID',
'and A.DEAPEID = C.DEAPEID',
'and B.DERULID = A.DERULID;'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select A.DERLCID,',
'       A.DERULID,',
'       A.DEAPEID,',
'       C.DEAPEXV,',
'       A.DERLINK,',
'       A.DEAPBIV,',
'       A.DESTATE     ',
'from DRRULCE A , DRRULES B, DRAPEXV C',
'where A.DERULID = :P201_DERULID',
'and A.DEAPEID = C.DEAPEID',
'and B.DERULID = A.DERULID;'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39961032697081152)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34070858865228366.901305012)
,p_translate_from_id=>wwv_flow_api.id(34070858865228366)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select DERLCID,',
'       DERULID,',
'       DEAPEID,',
'       DERLINK,',
'       DEAPBIV,',
'       DESTATE,',
'       DETYPID,',
'       DECREDT,',
'       DECREUS,',
'       DEWYZDT,',
'       DEWYZUS',
'  from DRRULCE'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select DERLCID,',
'       DERULID,',
'       DEAPEID,',
'       DERLINK,',
'       DEAPBIV,',
'       DESTATE,',
'       DETYPID,',
'       DECREDT,',
'       DECREUS,',
'       DEWYZDT,',
'       DEWYZUS',
'  from DRRULCE'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39961213016081152)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29074299873826705.901305012)
,p_translate_from_id=>wwv_flow_api.id(29074299873826705)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select DEAPEID,',
'       DEAPEXV,',
'       DEAPENR,',
'       DECREDT,',
'       DECREUS,',
'       DEWYZDT,',
'       DEWYZUS',
'  from DRAPEXV'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select DEAPEID,',
'       DEAPEXV,',
'       DEAPENR,',
'       DECREDT,',
'       DECREUS,',
'       DEWYZDT,',
'       DEWYZUS',
'  from DRAPEXV'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39961437054081152)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29109531489788995.901305012)
,p_translate_from_id=>wwv_flow_api.id(29109531489788995)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select P.DEPROID,',
'       A.DEAPEXV,',
'       P.DEDESCR,',
'       P.DECREDT,',
'       P.DECREUS,',
'       P.DEWYZDT,',
'       P.DEWYZUS,',
'       P.DEAPEID',
'--''<a style="color:#000000;" onclick="javascript:l_printen = 1;" href="''|| apex_util.prepare_url(p_url => ''f?p=&APP_ID.:216:&APP_SESSION.::::P216_DEPROID,P216_DEDESCR:''|| deproid || '','' || dedescr) ||''"><i class="fa fa-print fa-2x" aria-hidden="true"'
||'></i></a>'' as label3',
'  from DRPROJE P, DRAPEXV A',
'where P.DEAPEID = A.DEAPEID'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select P.DEPROID,',
'       A.DEAPEXV,',
'       P.DEDESCR,',
'       P.DECREDT,',
'       P.DECREUS,',
'       P.DEWYZDT,',
'       P.DEWYZUS,',
'       P.DEAPEID',
'--''<a style="color:#000000;" onclick="javascript:l_printen = 1;" href="''|| apex_util.prepare_url(p_url => ''f?p=&APP_ID.:216:&APP_SESSION.::::P216_DEPROID,P216_DEDESCR:''|| deproid || '','' || dedescr) ||''"><i class="fa fa-print fa-2x" aria-hidden="true"'
||'></i></a>'' as label3',
'  from DRPROJE P, DRAPEXV A',
'where P.DEAPEID = A.DEAPEID'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39961884743081152)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39256930946502731.901305012)
,p_translate_from_id=>wwv_flow_api.id(39256930946502731)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select A.APPLICATION_ID,',
'       A.APPLICATION_NAME,',
'       B.DEAPPID,',
'       B.DEPROID',
'from APEX_APPLICATIONS A, DRPRAPP B',
'where A.APPLICATION_ID = B.DEAPPID',
'and B.DEPROID = :P215_DEPROID;'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select A.APPLICATION_ID,',
'       A.APPLICATION_NAME,',
'       B.DEAPPID,',
'       B.DEPROID',
'from APEX_APPLICATIONS A, DRPRAPP B',
'where A.APPLICATION_ID = B.DEAPPID',
'and B.DEPROID = :P215_DEPROID;'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39962028510081152)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316521513630906.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316521513630906)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select    a.deproid,c.derulid, c.dername, c.dedescr as description, b.destate, b.derlcid,',
'          APEX_ITEM.CHECKBOX2( ',
'              p_idx => 2,',
'              p_value => decode(d.derulid, null, ''N'', ''J''), ',
'              p_attributes => case when decode(d.derulid, null, ''N'', ''J'') = ''J'' then ''CHECKED '' else '' '' end ||  ',
'              ''onChange="change_checkbox('''''' || c.derulid || '''''','''''' || decode(d.derulid, null, ''N'', ''J'') || '''''');"''                 ',
'          ) as DEOPLJN',
'from      drproje a, ',
'          drrulce b, ',
'          drrules c,',
'          drprrul d',
'where     a.deapeid = b.deapeid',
'and       b.derulid = c.derulid',
'and       c.derulid = d.derulid (+)',
'and       a.deproid (+) = :P216_DEPROID',
'order by  a.deproid',
';',
'',
'',
''))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select    a.deproid,c.derulid, c.dername, c.dedescr as description, b.destate, b.derlcid,',
'          APEX_ITEM.CHECKBOX2( ',
'              p_idx => 2,',
'              p_value => decode(d.derulid, null, ''N'', ''J''), ',
'              p_attributes => case when decode(d.derulid, null, ''N'', ''J'') = ''J'' then ''CHECKED '' else '' '' end ||  ',
'              ''onChange="change_checkbox('''''' || c.derulid || '''''','''''' || decode(d.derulid, null, ''N'', ''J'') || '''''');"''                 ',
'          ) as DEOPLJN',
'from      drproje a, ',
'          drrulce b, ',
'          drrules c,',
'          drprrul d',
'where     a.deapeid = b.deapeid',
'and       b.derulid = c.derulid',
'and       c.derulid = d.derulid (+)',
'and       a.deproid (+) = :P216_DEPROID',
'order by  a.deproid',
';',
'',
'',
''))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39962203550081152)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(1481924344399434654.901305012)
,p_translate_from_id=>wwv_flow_api.id(1481924344399434654)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select apex_item.checkbox(1,A.DERULID) cb,',
'       A.DERULID,',
'       A.DERNAME,',
'       A.DEDESCR,',
'       B.DERLCID,',
'       B.DESTATE,',
'       a.DETYPID,',
'       C.DEPROFL_ID,',
'       D.DEDESCR DETYPDE',
'from DRRULES A, DRRULCE B, DRPROFL C, DRRTYPE D',
'where (C.DEPROFL_ID, A.DERULID) not in (select DEPROFL_ID, DERULID from DRPRRUL)',
'and B.DERULID = A.DERULID',
'and A.DERULID = B.DERULID',
'and C.DEPROFL_ID = :P217_DEPROFL_ID',
'and D.DETYPID = a.DETYPID'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select apex_item.checkbox(1,A.DERULID) cb,',
'       A.DERULID,',
'       A.DERNAME,',
'       A.DEDESCR,',
'       B.DERLCID,',
'       B.DESTATE,',
'       a.DETYPID,',
'       C.DEPROFL_ID,',
'       D.DEDESCR DETYPDE',
'from DRRULES A, DRRULCE B, DRPROFL C, DRRTYPE D',
'where (C.DEPROFL_ID, A.DERULID) not in (select DEPROFL_ID, DERULID from DRPRRUL)',
'and B.DERULID = A.DERULID',
'and A.DERULID = B.DERULID',
'and C.DEPROFL_ID = :P217_DEPROFL_ID',
'and D.DETYPID = a.DETYPID'))
);
end;
/
begin
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39962410830081152)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(1521289983605535955.901305012)
,p_translate_from_id=>wwv_flow_api.id(1521289983605535955)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct apex_item.checkbox(1,A.APPLICATION_ID) cb,',
'       A.APPLICATION_ID,',
'       A.APPLICATION_NAME',
'      ',
'from APEX_APPLICATIONS A',
'where (A.APPLICATION_ID, :P215_DEPROID) not in (select DEAPPID, DEPROID from DRPRAPP)',
'',
'',
''))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct apex_item.checkbox(1,A.APPLICATION_ID) cb,',
'       A.APPLICATION_ID,',
'       A.APPLICATION_NAME',
'      ',
'from APEX_APPLICATIONS A',
'where (A.APPLICATION_ID, :P215_DEPROID) not in (select DEAPPID, DEPROID from DRPRAPP)',
'',
'',
''))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39962623147081152)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34671456281337179.901305012)
,p_translate_from_id=>wwv_flow_api.id(34671456281337179)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''<a href="javascript:openInBuilder('''''' ||pck_checks.build_link( derslid, deappid, derefid) ||',
'        '''''')" class="t-Button t-Button--stretch">Go to</a>'' as edit_link,',
'        /* <span class="fa fa-mail-forward"></span> */',
'       A.DERSLID,',
'       A.DERUNID,',
'       A.DEAPPID,',
'       A.DEPAGEN,',
'       A.DEREGIO,',
'       A.DEITEMN,',
'       A.DELABEL,',
'       A.DERULID,',
'       B.DERNAME,',
'       A.DEGRAID,',
'       C.DEDESCR  as DEGRADE,',
'       A.DETYPID,',
'       D.DEDESCR as DETYPDE,',
'       A.DESTAID,',
'       A.DEPROID,',
'       E.DEDESCR as DEPRODE,',
'       A.DEIDENT,',
'       A.DECREDT,',
'       A.DECREUS,',
'       A.DERLINK,',
'       A.DEREFID',
'  from DRRESUL A, DRRULES B, DRGRADA C, DRRTYPE D, DRPROJE E',
'  where A.DERULID = B.DERULID',
'  and   A.DEGRAID = C.DEGRAID',
'  and   A.DETYPID = D.DETYPID',
'  and   A.DEPROID = E.DEPROID ',
'  and   A.DEPROID = :APP_PROJECT;',
''))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''<a href="javascript:openInBuilder('''''' ||pck_checks.build_link( derslid, deappid, derefid) ||',
'        '''''')" class="t-Button t-Button--stretch">Go to</a>'' as edit_link,',
'        /* <span class="fa fa-mail-forward"></span> */',
'       A.DERSLID,',
'       A.DERUNID,',
'       A.DEAPPID,',
'       A.DEPAGEN,',
'       A.DEREGIO,',
'       A.DEITEMN,',
'       A.DELABEL,',
'       A.DERULID,',
'       B.DERNAME,',
'       A.DEGRAID,',
'       C.DEDESCR  as DEGRADE,',
'       A.DETYPID,',
'       D.DEDESCR as DETYPDE,',
'       A.DESTAID,',
'       A.DEPROID,',
'       E.DEDESCR as DEPRODE,',
'       A.DEIDENT,',
'       A.DECREDT,',
'       A.DECREUS,',
'       A.DERLINK,',
'       A.DEREFID',
'  from DRRESUL A, DRRULES B, DRGRADA C, DRRTYPE D, DRPROJE E',
'  where A.DERULID = B.DERULID',
'  and   A.DEGRAID = C.DEGRAID',
'  and   A.DETYPID = D.DETYPID',
'  and   A.DEPROID = E.DEPROID ',
'  and   A.DEPROID = :APP_PROJECT;',
''))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39962838419081152)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29039822450135034.901305012)
,p_translate_from_id=>wwv_flow_api.id(29039822450135034)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'apex_lang.emit_language_selector_list;'
,p_translate_from_text=>'apex_lang.emit_language_selector_list;'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39963057288081142)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34023202441587092.901305012)
,p_translate_from_id=>wwv_flow_api.id(34023202441587092)
,p_translate_column_id=>28
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Rules'
,p_translate_from_text=>'Manage Rules'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39963237037081142)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34671044905337180.901305012)
,p_translate_from_id=>wwv_flow_api.id(34671044905337180)
,p_translate_column_id=>28
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Run Result'
,p_translate_from_text=>'Results'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39963421632081142)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29073829068826705.901305012)
,p_translate_from_id=>wwv_flow_api.id(29073829068826705)
,p_translate_column_id=>28
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Apex Versions'
,p_translate_from_text=>'Manage Apex Versions'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39963628450081142)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29090647822816867.901305012)
,p_translate_from_id=>wwv_flow_api.id(29090647822816867)
,p_translate_column_id=>28
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage Projects'
,p_translate_from_text=>'Manage Projects'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39963858645081142)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29041044846135029.901305012)
,p_translate_from_id=>wwv_flow_api.id(29041044846135029)
,p_translate_column_id=>28
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Home'
,p_translate_from_text=>'Home'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39964044167081142)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39873311672913714.901305012)
,p_translate_from_id=>wwv_flow_api.id(39873311672913714)
,p_translate_column_id=>28
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Choose project'
,p_translate_from_text=>'&APP_PROJECT_NAME. - Switch project'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39964258333081142)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29043177762135019.901305012)
,p_translate_from_id=>wwv_flow_api.id(29043177762135019)
,p_translate_column_id=>28
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'---'
,p_translate_from_text=>'---'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39964459156081142)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29043507695135019.901305012)
,p_translate_from_id=>wwv_flow_api.id(29043507695135019)
,p_translate_column_id=>28
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Sign Out'
,p_translate_from_text=>'Sign Out'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39964675642081142)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29042693211135020.901305012)
,p_translate_from_id=>wwv_flow_api.id(29042693211135020)
,p_translate_column_id=>28
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'&APP_USER.'
,p_translate_from_text=>'&APP_USER.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39964813783081086)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29010004473135122.901305012)
,p_translate_from_id=>wwv_flow_api.id(29010004473135122)
,p_translate_column_id=>63
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Search Dialog'
,p_translate_from_text=>'Search Dialog'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39965028302081081)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29010004473135122.901305012)
,p_translate_from_id=>wwv_flow_api.id(29010004473135122)
,p_translate_column_id=>66
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<div class="t-PopupLOV-actions t-Form--large">'
,p_translate_from_text=>'<div class="t-PopupLOV-actions t-Form--large">'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39965274820081080)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29010004473135122.901305012)
,p_translate_from_id=>wwv_flow_api.id(29010004473135122)
,p_translate_column_id=>67
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'</div>'
,p_translate_from_text=>'</div>'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39965474393081075)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29010004473135122.901305012)
,p_translate_from_id=>wwv_flow_api.id(29010004473135122)
,p_translate_column_id=>70
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<div class="t-PopupLOV-links">'
,p_translate_from_text=>'<div class="t-PopupLOV-links">'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39965688505081073)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29010004473135122.901305012)
,p_translate_from_id=>wwv_flow_api.id(29010004473135122)
,p_translate_column_id=>71
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'</div>'
,p_translate_from_text=>'</div>'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39965888894081071)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29010004473135122.901305012)
,p_translate_from_id=>wwv_flow_api.id(29010004473135122)
,p_translate_column_id=>72
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Search'
,p_translate_from_text=>'Search'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39966007972081070)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29010004473135122.901305012)
,p_translate_from_id=>wwv_flow_api.id(29010004473135122)
,p_translate_column_id=>73
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Close'
,p_translate_from_text=>'Close'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39966246302081068)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29010004473135122.901305012)
,p_translate_from_id=>wwv_flow_api.id(29010004473135122)
,p_translate_column_id=>74
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Next &gt;'
,p_translate_from_text=>'Next &gt;'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39966419169081067)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29010004473135122.901305012)
,p_translate_from_id=>wwv_flow_api.id(29010004473135122)
,p_translate_column_id=>75
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'&lt; Previous'
,p_translate_from_text=>'&lt; Previous'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39966689128081061)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29034095074135069.901305012)
,p_translate_from_id=>wwv_flow_api.id(29034095074135069)
,p_translate_column_id=>79
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Would you like to perform this delete action?'
,p_translate_from_text=>'Would you like to perform this delete action?'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39966813877081059)
,p_translated_flow_id=>901305012
,p_translate_to_id=>100.901305012
,p_translate_from_id=>100
,p_translate_column_id=>80
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Coding Conventions Checker'
,p_translate_from_text=>'Coding Conventions Checker'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39967058850081030)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(28910541499135217.901305012)
,p_translate_from_id=>wwv_flow_api.id(28910541499135217)
,p_translate_column_id=>100
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Home'
,p_translate_from_text=>'Home'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39967180549081025)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29036669966135047.901305012)
,p_translate_from_id=>wwv_flow_api.id(29036669966135047)
,p_translate_column_id=>103
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Remember username'
,p_translate_from_text=>'Remember username'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39967370943081025)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29141109022635663.901305012)
,p_translate_from_id=>wwv_flow_api.id(29141109022635663)
,p_translate_column_id=>103
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Application'
,p_translate_from_text=>'Application'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39967588611081025)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29141593847635663.901305012)
,p_translate_from_id=>wwv_flow_api.id(29141593847635663)
,p_translate_column_id=>103
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Page'
,p_translate_from_text=>'Page'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39967772409081025)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29141909724635663.901305012)
,p_translate_from_id=>wwv_flow_api.id(29141909724635663)
,p_translate_column_id=>103
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Region'
,p_translate_from_text=>'Region'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39967978594081025)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29142388155635663.901305012)
,p_translate_from_id=>wwv_flow_api.id(29142388155635663)
,p_translate_column_id=>103
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Page Item'
,p_translate_from_text=>'Page Item'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39968156862081025)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29142719240635663.901305012)
,p_translate_from_id=>wwv_flow_api.id(29142719240635663)
,p_translate_column_id=>103
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Application Item'
,p_translate_from_text=>'Application Item'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39968366673081025)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29143113925635663.901305012)
,p_translate_from_id=>wwv_flow_api.id(29143113925635663)
,p_translate_column_id=>103
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Button'
,p_translate_from_text=>'Button'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39968587043081025)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29143569016635663.901305012)
,p_translate_from_id=>wwv_flow_api.id(29143569016635663)
,p_translate_column_id=>103
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'List'
,p_translate_from_text=>'List'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39968719594081025)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29143966865635663.901305012)
,p_translate_from_id=>wwv_flow_api.id(29143966865635663)
,p_translate_column_id=>103
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'List Entry'
,p_translate_from_text=>'List Entry'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39968994865081020)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29403949682820905.901305012)
,p_translate_from_id=>wwv_flow_api.id(29403949682820905)
,p_translate_column_id=>106
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Deopljn'
,p_translate_from_text=>'Deopljn'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39969120901081020)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29404067578820906.901305012)
,p_translate_from_id=>wwv_flow_api.id(29404067578820906)
,p_translate_column_id=>106
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Project'
,p_translate_from_text=>'Deproid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39969385606081020)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29404167518820907.901305012)
,p_translate_from_id=>wwv_flow_api.id(29404167518820907)
,p_translate_column_id=>106
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Statement'
,p_translate_from_text=>'Destate'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39969544989081020)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29404247410820908.901305012)
,p_translate_from_id=>wwv_flow_api.id(29404247410820908)
,p_translate_column_id=>106
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Code'
,p_translate_from_text=>'Derlcid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39969765803081020)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29317943177630920.901305012)
,p_translate_from_id=>wwv_flow_api.id(29317943177630920)
,p_translate_column_id=>106
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule'
,p_translate_from_text=>'Derulid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39969922749081020)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29318014362630921.901305012)
,p_translate_from_id=>wwv_flow_api.id(29318014362630921)
,p_translate_column_id=>106
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Name'
,p_translate_from_text=>'Dername'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39970156605081019)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29318279205630923.901305012)
,p_translate_from_id=>wwv_flow_api.id(29318279205630923)
,p_translate_column_id=>106
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Description'
,p_translate_from_text=>'Description'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39972981077081010)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29037019371135044.901305012)
,p_translate_from_id=>wwv_flow_api.id(29037019371135044)
,p_translate_column_id=>111
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<p>',
'If you select this checkbox, the application will save your username in a persistent browser cookie named "LOGIN_USERNAME_COOKIE".',
'When you go to the login page the next time,',
'the username field will be automatically populated with this value.',
'</p>',
'<p>',
'If you deselect this checkbox and your username is already saved in the cookie,',
'the application will overwrite it with an empty value.',
'You can also use your browser''s developer tools to completely remove the cookie.',
'</p>'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<p>',
'If you select this checkbox, the application will save your username in a persistent browser cookie named "LOGIN_USERNAME_COOKIE".',
'When you go to the login page the next time,',
'the username field will be automatically populated with this value.',
'</p>',
'<p>',
'If you deselect this checkbox and your username is already saved in the cookie,',
'the application will overwrite it with an empty value.',
'You can also use your browser''s developer tools to completely remove the cookie.',
'</p>'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39973119102081009)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29010004473135122.901305012)
,p_translate_from_id=>wwv_flow_api.id(29010004473135122)
,p_translate_column_id=>112
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<div class="t-PopupLOV-pagination">Row(s) #FIRST_ROW# - #LAST_ROW#</div>'
,p_translate_from_text=>'<div class="t-PopupLOV-pagination">Row(s) #FIRST_ROW# - #LAST_ROW#</div>'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39973306714080978)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(1481924543448434655.901305012)
,p_translate_from_id=>wwv_flow_api.id(1481924543448434655)
,p_translate_column_id=>146
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_translate_from_text=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39973586900080978)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(1521290182654535956.901305012)
,p_translate_from_id=>wwv_flow_api.id(1521290182654535956)
,p_translate_column_id=>146
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_translate_from_text=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39973793996080977)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(1481924543448434655.901305012)
,p_translate_from_id=>wwv_flow_api.id(1481924543448434655)
,p_translate_column_id=>147
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'No data found.'
,p_translate_from_text=>'No data found.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39973995930080976)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(1521290182654535956.901305012)
,p_translate_from_id=>wwv_flow_api.id(1521290182654535956)
,p_translate_column_id=>147
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'No data found.'
,p_translate_from_text=>'No data found.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39974104739080972)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34355197641804111.901305012)
,p_translate_from_id=>wwv_flow_api.id(34355197641804111)
,p_translate_column_id=>148
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<input type="checkbox" onclick="checkallf01();" id="checkall" />'
,p_translate_from_text=>'<input type="checkbox" onclick="checkallf01();" id="checkall" />'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39974332051080971)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34263912197219011.901305012)
,p_translate_from_id=>wwv_flow_api.id(34263912197219011)
,p_translate_column_id=>148
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule'
,p_translate_from_text=>'Derulid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39974588986080971)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34264074079219012.901305012)
,p_translate_from_id=>wwv_flow_api.id(34264074079219012)
,p_translate_column_id=>148
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Name'
,p_translate_from_text=>'Dername'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39974708480080971)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34264142002219013.901305012)
,p_translate_from_id=>wwv_flow_api.id(34264142002219013)
,p_translate_column_id=>148
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Description'
,p_translate_from_text=>'Dedescr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39974949407080971)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34264286026219014.901305012)
,p_translate_from_id=>wwv_flow_api.id(34264286026219014)
,p_translate_column_id=>148
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Code'
,p_translate_from_text=>'Derlcid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39975320934080971)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34264496117219016.901305012)
,p_translate_from_id=>wwv_flow_api.id(34264496117219016)
,p_translate_column_id=>148
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Statement'
,p_translate_from_text=>'Destate'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39975580315080971)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34264586240219017.901305012)
,p_translate_from_id=>wwv_flow_api.id(34264586240219017)
,p_translate_column_id=>148
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Type ID'
,p_translate_from_text=>'Detypid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39975793352080971)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39430297642667340.901305012)
,p_translate_from_id=>wwv_flow_api.id(39430297642667340)
,p_translate_column_id=>148
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Type'
,p_translate_from_text=>'Detypde'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39976181598080971)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39369168635101293.901305012)
,p_translate_from_id=>wwv_flow_api.id(39369168635101293)
,p_translate_column_id=>148
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<input type="checkbox" onclick="checkallf01();" id="checkall" />'
,p_translate_from_text=>'<input type="checkbox" onclick="checkallf01();" id="checkall" />'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39976311336080971)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39258453876502746.901305012)
,p_translate_from_id=>wwv_flow_api.id(39258453876502746)
,p_translate_column_id=>148
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Application Id'
,p_translate_from_text=>'Application Id'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39976543005080971)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39258549187502747.901305012)
,p_translate_from_id=>wwv_flow_api.id(39258549187502747)
,p_translate_column_id=>148
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Application Name'
,p_translate_from_text=>'Application Name'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39976761943080969)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34355197641804111.901305012)
,p_translate_from_id=>wwv_flow_api.id(34355197641804111)
,p_translate_column_id=>149
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<input type="checkbox" onclick="checkallf01();" id="checkall" />'
,p_translate_from_text=>'<input type="checkbox" onclick="checkallf01();" id="checkall" />'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39976977265080969)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34263912197219011.901305012)
,p_translate_from_id=>wwv_flow_api.id(34263912197219011)
,p_translate_column_id=>149
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule'
,p_translate_from_text=>'Derulid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39977154085080969)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34264074079219012.901305012)
,p_translate_from_id=>wwv_flow_api.id(34264074079219012)
,p_translate_column_id=>149
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Name'
,p_translate_from_text=>'Dername'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39977302765080969)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34264142002219013.901305012)
,p_translate_from_id=>wwv_flow_api.id(34264142002219013)
,p_translate_column_id=>149
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Description'
,p_translate_from_text=>'Dedescr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39977539051080969)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34264286026219014.901305012)
,p_translate_from_id=>wwv_flow_api.id(34264286026219014)
,p_translate_column_id=>149
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Code'
,p_translate_from_text=>'Derlcid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39977921574080969)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34264496117219016.901305012)
,p_translate_from_id=>wwv_flow_api.id(34264496117219016)
,p_translate_column_id=>149
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Statement'
,p_translate_from_text=>'Destate'
);
end;
/
begin
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39978126618080968)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34264586240219017.901305012)
,p_translate_from_id=>wwv_flow_api.id(34264586240219017)
,p_translate_column_id=>149
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Type ID'
,p_translate_from_text=>'Detypid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39978302018080968)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39430297642667340.901305012)
,p_translate_from_id=>wwv_flow_api.id(39430297642667340)
,p_translate_column_id=>149
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Type'
,p_translate_from_text=>'Detypde'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39978737001080968)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39369168635101293.901305012)
,p_translate_from_id=>wwv_flow_api.id(39369168635101293)
,p_translate_column_id=>149
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<input type="checkbox" onclick="checkallf01();" id="checkall" />'
,p_translate_from_text=>'<input type="checkbox" onclick="checkallf01();" id="checkall" />'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39978966060080968)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39258453876502746.901305012)
,p_translate_from_id=>wwv_flow_api.id(39258453876502746)
,p_translate_column_id=>149
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Application Id'
,p_translate_from_text=>'Application Id'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39979139735080968)
,p_page_id=>218
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39258549187502747.901305012)
,p_translate_from_id=>wwv_flow_api.id(39258549187502747)
,p_translate_column_id=>149
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Application Name'
,p_translate_from_text=>'Application Name'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39979367107080809)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29033676742135075.901305012)
,p_translate_from_id=>wwv_flow_api.id(29033676742135075)
,p_translate_column_id=>257
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Insufficient privileges, user is not an Administrator'
,p_translate_from_text=>'Insufficient privileges, user is not an Administrator'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39979408433080793)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035574074135049.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035574074135049)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39979651995080793)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035962092135048.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035962092135048)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39979877255080793)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29037019371135044.901305012)
,p_translate_from_id=>wwv_flow_api.id(29037019371135044)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'1'
,p_translate_from_text=>'1'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39980082287080793)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29065669273897308.901305012)
,p_translate_from_id=>wwv_flow_api.id(29065669273897308)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39980200334080793)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29065761787897309.901305012)
,p_translate_from_id=>wwv_flow_api.id(29065761787897309)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39980400228080792)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29065853011897310.901305012)
,p_translate_from_id=>wwv_flow_api.id(29065853011897310)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39980656231080792)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29065973436897311.901305012)
,p_translate_from_id=>wwv_flow_api.id(29065973436897311)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39980801167080792)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29084020572826695.901305012)
,p_translate_from_id=>wwv_flow_api.id(29084020572826695)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39981092040080792)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29084401693826695.901305012)
,p_translate_from_id=>wwv_flow_api.id(29084401693826695)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39981241254080792)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29119906758788989.901305012)
,p_translate_from_id=>wwv_flow_api.id(29119906758788989)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39981400184080792)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29120325710788989.901305012)
,p_translate_from_id=>wwv_flow_api.id(29120325710788989)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39981640618080792)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29120722589788989.901305012)
,p_translate_from_id=>wwv_flow_api.id(29120722589788989)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39981848626080792)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29121153741788988.901305012)
,p_translate_from_id=>wwv_flow_api.id(29121153741788988)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39982067211080792)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29121528476788988.901305012)
,p_translate_from_id=>wwv_flow_api.id(29121528476788988)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39982261175080792)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29121900790788988.901305012)
,p_translate_from_id=>wwv_flow_api.id(29121900790788988)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39982426393080792)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29122379309788988.901305012)
,p_translate_from_id=>wwv_flow_api.id(29122379309788988)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39982697520080792)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316114592630902.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316114592630902)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39982868632080792)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316461016630905.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316461016630905)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39983039967080792)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29317593687630916.901305012)
,p_translate_from_id=>wwv_flow_api.id(29317593687630916)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39983295680080792)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29404833304820914.901305012)
,p_translate_from_id=>wwv_flow_api.id(29404833304820914)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39983406931080791)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29404987872820915.901305012)
,p_translate_from_id=>wwv_flow_api.id(29404987872820915)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39983628761080791)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405031218820916.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405031218820916)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39983879625080791)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405127966820917.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405127966820917)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39984003316080791)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405295312820918.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405295312820918)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39984208877080791)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405398360820919.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405398360820919)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39984441464080791)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405427389820920.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405427389820920)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39984652988080791)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34034698486587083.901305012)
,p_translate_from_id=>wwv_flow_api.id(34034698486587083)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39984834668080791)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34089352387083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34089352387083517)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39985077632080791)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34089742850083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34089742850083517)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39985247252080791)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34090165831083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34090165831083517)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39985422880080791)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34090520576083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34090520576083517)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39985886229080791)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34091367931083516.901305012)
,p_translate_from_id=>wwv_flow_api.id(34091367931083516)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39986077230080791)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34091766789083516.901305012)
,p_translate_from_id=>wwv_flow_api.id(34091766789083516)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39986252533080791)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34092132661083516.901305012)
,p_translate_from_id=>wwv_flow_api.id(34092132661083516)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39986484637080790)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34092506541083516.901305012)
,p_translate_from_id=>wwv_flow_api.id(34092506541083516)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39986677266080790)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39428008993667318.901305012)
,p_translate_from_id=>wwv_flow_api.id(39428008993667318)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39986838182080790)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39430342639667341.901305012)
,p_translate_from_id=>wwv_flow_api.id(39430342639667341)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39987071862080790)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34034207929587083.901305012)
,p_translate_from_id=>wwv_flow_api.id(34034207929587083)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39987240940080790)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34088583739083518.901305012)
,p_translate_from_id=>wwv_flow_api.id(34088583739083518)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39987423087080790)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34088910359083518.901305012)
,p_translate_from_id=>wwv_flow_api.id(34088910359083518)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39987641119080790)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39878020111883403.901305012)
,p_translate_from_id=>wwv_flow_api.id(39878020111883403)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39987807183080788)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035574074135049.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035574074135049)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39988036191080788)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035962092135048.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035962092135048)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39988267102080788)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29084401693826695.901305012)
,p_translate_from_id=>wwv_flow_api.id(29084401693826695)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39988402198080788)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29120325710788989.901305012)
,p_translate_from_id=>wwv_flow_api.id(29120325710788989)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39988665751080788)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29120722589788989.901305012)
,p_translate_from_id=>wwv_flow_api.id(29120722589788989)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39988864965080788)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316114592630902.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316114592630902)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39989081569080787)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316461016630905.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316461016630905)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39989208138080787)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405127966820917.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405127966820917)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39989411029080787)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405295312820918.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405295312820918)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39989697007080787)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405398360820919.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405398360820919)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39989828966080787)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34089742850083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34089742850083517)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39990094789080787)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34090165831083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34090165831083517)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39990203339080787)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34090520576083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34090520576083517)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39990898803080787)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39430342639667341.901305012)
,p_translate_from_id=>wwv_flow_api.id(39430342639667341)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39991071295080787)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34088910359083518.901305012)
,p_translate_from_id=>wwv_flow_api.id(34088910359083518)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39991250412080787)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39878020111883403.901305012)
,p_translate_from_id=>wwv_flow_api.id(39878020111883403)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39991429943080785)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035574074135049.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035574074135049)
,p_translate_column_id=>270
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39991632450080785)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316114592630902.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316114592630902)
,p_translate_column_id=>270
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39991877919080785)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316461016630905.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316461016630905)
,p_translate_column_id=>270
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39992068142080785)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29404460001820910.901305012)
,p_translate_from_id=>wwv_flow_api.id(29404460001820910)
,p_translate_column_id=>270
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'left'
,p_translate_from_text=>'left'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39992215814080785)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34090520576083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34090520576083517)
,p_translate_column_id=>270
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39992422763080783)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035574074135049.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035574074135049)
,p_translate_column_id=>271
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39992670132080783)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29084401693826695.901305012)
,p_translate_from_id=>wwv_flow_api.id(29084401693826695)
,p_translate_column_id=>271
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39992857195080782)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29120722589788989.901305012)
,p_translate_from_id=>wwv_flow_api.id(29120722589788989)
,p_translate_column_id=>271
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39993044259080782)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316114592630902.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316114592630902)
,p_translate_column_id=>271
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39993286113080782)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316461016630905.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316461016630905)
,p_translate_column_id=>271
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39993428309080782)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405127966820917.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405127966820917)
,p_translate_column_id=>271
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
end;
/
begin
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39993672046080782)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405295312820918.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405295312820918)
,p_translate_column_id=>271
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39993872050080782)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34090165831083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34090165831083517)
,p_translate_column_id=>271
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39994033097080782)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34090520576083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34090520576083517)
,p_translate_column_id=>271
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39994218741080782)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39430342639667341.901305012)
,p_translate_from_id=>wwv_flow_api.id(39430342639667341)
,p_translate_column_id=>271
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39994402492080780)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035574074135049.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035574074135049)
,p_translate_column_id=>272
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39994681665080780)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29084401693826695.901305012)
,p_translate_from_id=>wwv_flow_api.id(29084401693826695)
,p_translate_column_id=>272
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39994844392080780)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29120722589788989.901305012)
,p_translate_from_id=>wwv_flow_api.id(29120722589788989)
,p_translate_column_id=>272
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39995072590080780)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316114592630902.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316114592630902)
,p_translate_column_id=>272
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39995240376080780)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316461016630905.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316461016630905)
,p_translate_column_id=>272
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39995489558080780)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405127966820917.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405127966820917)
,p_translate_column_id=>272
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39995677219080780)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29405295312820918.901305012)
,p_translate_from_id=>wwv_flow_api.id(29405295312820918)
,p_translate_column_id=>272
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39995802907080780)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34090165831083517.901305012)
,p_translate_from_id=>wwv_flow_api.id(34090165831083517)
,p_translate_column_id=>272
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39996009392080770)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34036584990587082.901305012)
,p_translate_from_id=>wwv_flow_api.id(34036584990587082)
,p_translate_column_id=>278
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'CLEAR_CACHE_CURRENT_PAGE'
,p_translate_from_text=>'CLEAR_CACHE_CURRENT_PAGE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39996261645080770)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34079715575228361.901305012)
,p_translate_from_id=>wwv_flow_api.id(34079715575228361)
,p_translate_column_id=>278
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'REGION_SOURCE'
,p_translate_from_text=>'REGION_SOURCE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39996479279080770)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34095532917083515.901305012)
,p_translate_from_id=>wwv_flow_api.id(34095532917083515)
,p_translate_column_id=>278
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'CLEAR_CACHE_CURRENT_PAGE'
,p_translate_from_text=>'CLEAR_CACHE_CURRENT_PAGE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39996665745080770)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29089460823826694.901305012)
,p_translate_from_id=>wwv_flow_api.id(29089460823826694)
,p_translate_column_id=>278
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'CLEAR_CACHE_CURRENT_PAGE'
,p_translate_from_text=>'CLEAR_CACHE_CURRENT_PAGE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39996896867080770)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29126020999788987.901305012)
,p_translate_from_id=>wwv_flow_api.id(29126020999788987)
,p_translate_column_id=>278
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'CLEAR_CACHE_CURRENT_PAGE'
,p_translate_from_text=>'CLEAR_CACHE_CURRENT_PAGE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39997042826080770)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426558939667303.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426558939667303)
,p_translate_column_id=>278
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TABLE'
,p_translate_from_text=>'TABLE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39997686497080770)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34267364179219045.901305012)
,p_translate_from_id=>wwv_flow_api.id(34267364179219045)
,p_translate_column_id=>278
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'REGION_SOURCE'
,p_translate_from_text=>'REGION_SOURCE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39997805961080769)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29039579020135035.901305012)
,p_translate_from_id=>wwv_flow_api.id(29039579020135035)
,p_translate_column_id=>278
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'CLEAR_CACHE_CURRENT_PAGE'
,p_translate_from_text=>'CLEAR_CACHE_CURRENT_PAGE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39998070819080767)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34035786939587082.901305012)
,p_translate_from_id=>wwv_flow_api.id(34035786939587082)
,p_translate_column_id=>279
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DRRULES'
,p_translate_from_text=>'DRRULES'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39998221718080767)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34036161691587082.901305012)
,p_translate_from_id=>wwv_flow_api.id(34036161691587082)
,p_translate_column_id=>279
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DRRULES'
,p_translate_from_text=>'DRRULES'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39998419381080767)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34093992542083515.901305012)
,p_translate_from_id=>wwv_flow_api.id(34093992542083515)
,p_translate_column_id=>279
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DRRULCE'
,p_translate_from_text=>'DRRULCE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39998625489080767)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34095156904083515.901305012)
,p_translate_from_id=>wwv_flow_api.id(34095156904083515)
,p_translate_column_id=>279
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DRRULCE'
,p_translate_from_text=>'DRRULCE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39998867192080767)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29088699042826694.901305012)
,p_translate_from_id=>wwv_flow_api.id(29088699042826694)
,p_translate_column_id=>279
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DRAPEXV'
,p_translate_from_text=>'DRAPEXV'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39999040554080767)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29089035435826694.901305012)
,p_translate_from_id=>wwv_flow_api.id(29089035435826694)
,p_translate_column_id=>279
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DRAPEXV'
,p_translate_from_text=>'DRAPEXV'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39999256848080767)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29125260617788987.901305012)
,p_translate_from_id=>wwv_flow_api.id(29125260617788987)
,p_translate_column_id=>279
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DRPROJE'
,p_translate_from_text=>'DRPROJE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39999447335080766)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29125611403788987.901305012)
,p_translate_from_id=>wwv_flow_api.id(29125611403788987)
,p_translate_column_id=>279
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DRPROJE'
,p_translate_from_text=>'DRPROJE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(39999819367080763)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34035786939587082.901305012)
,p_translate_from_id=>wwv_flow_api.id(34035786939587082)
,p_translate_column_id=>280
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P201_DERULID'
,p_translate_from_text=>'P201_DERULID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40000087648080763)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34036161691587082.901305012)
,p_translate_from_id=>wwv_flow_api.id(34036161691587082)
,p_translate_column_id=>280
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P201_DERULID'
,p_translate_from_text=>'P201_DERULID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40000289608080763)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34093992542083515.901305012)
,p_translate_from_id=>wwv_flow_api.id(34093992542083515)
,p_translate_column_id=>280
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P203_DERLCID'
,p_translate_from_text=>'P203_DERLCID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40000494779080763)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34095156904083515.901305012)
,p_translate_from_id=>wwv_flow_api.id(34095156904083515)
,p_translate_column_id=>280
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P203_DERLCID'
,p_translate_from_text=>'P203_DERLCID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40000652274080763)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29088699042826694.901305012)
,p_translate_from_id=>wwv_flow_api.id(29088699042826694)
,p_translate_column_id=>280
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P211_DEAPEID'
,p_translate_from_text=>'P211_DEAPEID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40000833008080763)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29089035435826694.901305012)
,p_translate_from_id=>wwv_flow_api.id(29089035435826694)
,p_translate_column_id=>280
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P211_DEAPEID'
,p_translate_from_text=>'P211_DEAPEID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40001005532080763)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29125260617788987.901305012)
,p_translate_from_id=>wwv_flow_api.id(29125260617788987)
,p_translate_column_id=>280
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P215_DEPROID'
,p_translate_from_text=>'P215_DEPROID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40001290842080763)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29125611403788987.901305012)
,p_translate_from_id=>wwv_flow_api.id(29125611403788987)
,p_translate_column_id=>280
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P215_DEPROID'
,p_translate_from_text=>'P215_DEPROID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40001471234080762)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426558939667303.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426558939667303)
,p_translate_column_id=>280
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DRPRAPP'
,p_translate_from_text=>'DRPRAPP'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40002063788080760)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34035786939587082.901305012)
,p_translate_from_id=>wwv_flow_api.id(34035786939587082)
,p_translate_column_id=>281
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DERULID'
,p_translate_from_text=>'DERULID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40002274198080760)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34036161691587082.901305012)
,p_translate_from_id=>wwv_flow_api.id(34036161691587082)
,p_translate_column_id=>281
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DERULID'
,p_translate_from_text=>'DERULID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40002463884080760)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34093992542083515.901305012)
,p_translate_from_id=>wwv_flow_api.id(34093992542083515)
,p_translate_column_id=>281
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DERLCID'
,p_translate_from_text=>'DERLCID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40002641981080760)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34095156904083515.901305012)
,p_translate_from_id=>wwv_flow_api.id(34095156904083515)
,p_translate_column_id=>281
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DERLCID'
,p_translate_from_text=>'DERLCID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40002839161080760)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29088699042826694.901305012)
,p_translate_from_id=>wwv_flow_api.id(29088699042826694)
,p_translate_column_id=>281
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DEAPEID'
,p_translate_from_text=>'DEAPEID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40003027746080759)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29089035435826694.901305012)
,p_translate_from_id=>wwv_flow_api.id(29089035435826694)
,p_translate_column_id=>281
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DEAPEID'
,p_translate_from_text=>'DEAPEID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40003267591080759)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29125260617788987.901305012)
,p_translate_from_id=>wwv_flow_api.id(29125260617788987)
,p_translate_column_id=>281
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DEPROID'
,p_translate_from_text=>'DEPROID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40003482587080759)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29125611403788987.901305012)
,p_translate_from_id=>wwv_flow_api.id(29125611403788987)
,p_translate_column_id=>281
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DEPROID'
,p_translate_from_text=>'DEPROID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40003823742080757)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34079715575228361.901305012)
,p_translate_from_id=>wwv_flow_api.id(34079715575228361)
,p_translate_column_id=>282
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40003992323080757)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426558939667303.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426558939667303)
,p_translate_column_id=>282
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40004353456080757)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34267364179219045.901305012)
,p_translate_from_id=>wwv_flow_api.id(34267364179219045)
,p_translate_column_id=>282
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40004593252080755)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34079715575228361.901305012)
,p_translate_from_id=>wwv_flow_api.id(34079715575228361)
,p_translate_column_id=>283
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40004760238080755)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426558939667303.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426558939667303)
,p_translate_column_id=>283
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40005139642080755)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34267364179219045.901305012)
,p_translate_from_id=>wwv_flow_api.id(34267364179219045)
,p_translate_column_id=>283
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40005382218080752)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34079715575228361.901305012)
,p_translate_from_id=>wwv_flow_api.id(34079715575228361)
,p_translate_column_id=>285
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40005516285080752)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426558939667303.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426558939667303)
,p_translate_column_id=>285
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40005914243080752)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34267364179219045.901305012)
,p_translate_from_id=>wwv_flow_api.id(34267364179219045)
,p_translate_column_id=>285
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40006182917080747)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426948340667307.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426948340667307)
,p_translate_column_id=>288
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40006305120080747)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39430900726667347.901305012)
,p_translate_from_id=>wwv_flow_api.id(39430900726667347)
,p_translate_column_id=>288
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>':P203_HELP := pck_help.help_message(:P203_DERLINK);'
,p_translate_from_text=>':P203_HELP := pck_help.help_message(:P203_DERLINK);'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40007177161080747)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39427415756667312.901305012)
,p_translate_from_id=>wwv_flow_api.id(39427415756667312)
,p_translate_column_id=>288
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40007377539080747)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29317706112630918.901305012)
,p_translate_from_id=>wwv_flow_api.id(29317706112630918)
,p_translate_column_id=>288
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'customAlert(''E'', document.getElementById(''P216_MESSAGE'').value, 0);',
''))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'customAlert(''E'', document.getElementById(''P216_MESSAGE'').value, 0);',
''))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40007761918080747)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39428322063667321.901305012)
,p_translate_from_id=>wwv_flow_api.id(39428322063667321)
,p_translate_column_id=>288
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    pck_checks.start_run(:APP_PROJECT,:APP_USER);',
'end;'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    pck_checks.start_run(:APP_PROJECT,:APP_USER);',
'end;'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40007953188080745)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39430900726667347.901305012)
,p_translate_from_id=>wwv_flow_api.id(39430900726667347)
,p_translate_column_id=>289
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P203_DERLINK'
,p_translate_from_text=>'P203_DERLINK'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40008146359080745)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39428322063667321.901305012)
,p_translate_from_id=>wwv_flow_api.id(39428322063667321)
,p_translate_column_id=>289
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P300_DEPROID'
,p_translate_from_text=>'P300_DEPROID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40008308774080743)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39430900726667347.901305012)
,p_translate_from_id=>wwv_flow_api.id(39430900726667347)
,p_translate_column_id=>290
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P203_HELP'
,p_translate_from_text=>'P203_HELP'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40008503583080742)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39430900726667347.901305012)
,p_translate_from_id=>wwv_flow_api.id(39430900726667347)
,p_translate_column_id=>291
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40008748557080732)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(28909144604135221.901305012)
,p_translate_from_id=>wwv_flow_api.id(28909144604135221)
,p_translate_column_id=>298
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'modern'
,p_translate_from_text=>'modern'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40008997034080731)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(28907926427135222.901305012)
,p_translate_from_id=>wwv_flow_api.id(28907926427135222)
,p_translate_column_id=>298
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40009175619080731)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(28908883148135221.901305012)
,p_translate_from_id=>wwv_flow_api.id(28908883148135221)
,p_translate_column_id=>298
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40009383297080731)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(28908532405135221.901305012)
,p_translate_from_id=>wwv_flow_api.id(28908532405135221)
,p_translate_column_id=>298
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40009537755080731)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(28908298599135221.901305012)
,p_translate_from_id=>wwv_flow_api.id(28908298599135221)
,p_translate_column_id=>298
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'IG'
,p_translate_from_text=>'IG'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40009792903080728)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(28908883148135221.901305012)
,p_translate_from_id=>wwv_flow_api.id(28908883148135221)
,p_translate_column_id=>300
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40009941974080725)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(28908883148135221.901305012)
,p_translate_from_id=>wwv_flow_api.id(28908883148135221)
,p_translate_column_id=>302
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'SWITCH'
,p_translate_from_text=>'SWITCH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40010127563080717)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29042021766135027.901305012)
,p_translate_from_id=>wwv_flow_api.id(29042021766135027)
,p_translate_column_id=>308
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40010307599080717)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34031963879587085.901305012)
,p_translate_from_id=>wwv_flow_api.id(34031963879587085)
,p_translate_column_id=>308
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
end;
/
begin
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40010585857080717)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34085831334083520.901305012)
,p_translate_from_id=>wwv_flow_api.id(34085831334083520)
,p_translate_column_id=>308
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40010762250080716)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29081388327826696.901305012)
,p_translate_from_id=>wwv_flow_api.id(29081388327826696)
,p_translate_column_id=>308
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40010904205080716)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29117224403788990.901305012)
,p_translate_from_id=>wwv_flow_api.id(29117224403788990)
,p_translate_column_id=>308
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40011106299080716)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316067573630901.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316067573630901)
,p_translate_column_id=>308
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40011358469080716)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316997309630910.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316997309630910)
,p_translate_column_id=>308
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40011519748080716)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39874500802883406.901305012)
,p_translate_from_id=>wwv_flow_api.id(39874500802883406)
,p_translate_column_id=>308
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40011752734080716)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39875204544883406.901305012)
,p_translate_from_id=>wwv_flow_api.id(39875204544883406)
,p_translate_column_id=>308
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40011932105080716)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39427743103667315.901305012)
,p_translate_from_id=>wwv_flow_api.id(39427743103667315)
,p_translate_column_id=>308
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40012186655080716)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035184224135054.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035184224135054)
,p_translate_column_id=>308
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40012364259080714)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29042021766135027.901305012)
,p_translate_from_id=>wwv_flow_api.id(29042021766135027)
,p_translate_column_id=>309
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'HTML'
,p_translate_from_text=>'HTML'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40012532363080714)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34031963879587085.901305012)
,p_translate_from_id=>wwv_flow_api.id(34031963879587085)
,p_translate_column_id=>309
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40012794707080714)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34085831334083520.901305012)
,p_translate_from_id=>wwv_flow_api.id(34085831334083520)
,p_translate_column_id=>309
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40013195130080714)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29081388327826696.901305012)
,p_translate_from_id=>wwv_flow_api.id(29081388327826696)
,p_translate_column_id=>309
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40013337780080714)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29117224403788990.901305012)
,p_translate_from_id=>wwv_flow_api.id(29117224403788990)
,p_translate_column_id=>309
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40013531407080714)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316067573630901.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316067573630901)
,p_translate_column_id=>309
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'HTML'
,p_translate_from_text=>'HTML'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40013731877080714)
,p_page_id=>216
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29316997309630910.901305012)
,p_translate_from_id=>wwv_flow_api.id(29316997309630910)
,p_translate_column_id=>309
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'HTML'
,p_translate_from_text=>'HTML'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40013956057080714)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39874500802883406.901305012)
,p_translate_from_id=>wwv_flow_api.id(39874500802883406)
,p_translate_column_id=>309
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40014198040080714)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39875204544883406.901305012)
,p_translate_from_id=>wwv_flow_api.id(39875204544883406)
,p_translate_column_id=>309
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40014316742080714)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39427743103667315.901305012)
,p_translate_from_id=>wwv_flow_api.id(39427743103667315)
,p_translate_column_id=>309
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'HTML'
,p_translate_from_text=>'HTML'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40014544893080713)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035184224135054.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035184224135054)
,p_translate_column_id=>309
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40014785086080712)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29042021766135027.901305012)
,p_translate_from_id=>wwv_flow_api.id(29042021766135027)
,p_translate_column_id=>310
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40014914832080712)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34031963879587085.901305012)
,p_translate_from_id=>wwv_flow_api.id(34031963879587085)
,p_translate_column_id=>310
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40015128112080712)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34085831334083520.901305012)
,p_translate_from_id=>wwv_flow_api.id(34085831334083520)
,p_translate_column_id=>310
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40015557066080711)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29081388327826696.901305012)
,p_translate_from_id=>wwv_flow_api.id(29081388327826696)
,p_translate_column_id=>310
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40015769658080711)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29117224403788990.901305012)
,p_translate_from_id=>wwv_flow_api.id(29117224403788990)
,p_translate_column_id=>310
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40015988952080711)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39874500802883406.901305012)
,p_translate_from_id=>wwv_flow_api.id(39874500802883406)
,p_translate_column_id=>310
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40016102284080711)
,p_page_id=>220
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39875204544883406.901305012)
,p_translate_from_id=>wwv_flow_api.id(39875204544883406)
,p_translate_column_id=>310
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40016365716080711)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035184224135054.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035184224135054)
,p_translate_column_id=>310
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40016503492080691)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34036161691587082.901305012)
,p_translate_from_id=>wwv_flow_api.id(34036161691587082)
,p_translate_column_id=>323
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'I:U:D'
,p_translate_from_text=>'I:U:D'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40016707273080691)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34095156904083515.901305012)
,p_translate_from_id=>wwv_flow_api.id(34095156904083515)
,p_translate_column_id=>323
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'I:U:D'
,p_translate_from_text=>'I:U:D'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40016907253080691)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29089035435826694.901305012)
,p_translate_from_id=>wwv_flow_api.id(29089035435826694)
,p_translate_column_id=>323
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'I:U:D'
,p_translate_from_text=>'I:U:D'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40017115190080690)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29125611403788987.901305012)
,p_translate_from_id=>wwv_flow_api.id(29125611403788987)
,p_translate_column_id=>323
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'I:U:D'
,p_translate_from_text=>'I:U:D'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40017308748080689)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34036161691587082.901305012)
,p_translate_from_id=>wwv_flow_api.id(34036161691587082)
,p_translate_column_id=>324
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40017562637080689)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34095156904083515.901305012)
,p_translate_from_id=>wwv_flow_api.id(34095156904083515)
,p_translate_column_id=>324
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40017745299080689)
,p_page_id=>211
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29089035435826694.901305012)
,p_translate_from_id=>wwv_flow_api.id(29089035435826694)
,p_translate_column_id=>324
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40017973889080689)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29125611403788987.901305012)
,p_translate_from_id=>wwv_flow_api.id(29125611403788987)
,p_translate_column_id=>324
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40018103616080660)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29033676742135075.901305012)
,p_translate_from_id=>wwv_flow_api.id(29033676742135075)
,p_translate_column_id=>343
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'return true;'
,p_translate_from_text=>'return true;'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40018369601080606)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29042693211135020.901305012)
,p_translate_from_id=>wwv_flow_api.id(29042693211135020)
,p_translate_column_id=>377
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'has-username'
,p_translate_from_text=>'has-username'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40018462594080579)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035574074135049.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035574074135049)
,p_translate_column_id=>396
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'username'
,p_translate_from_text=>'username'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40018641452080579)
,p_page_id=>9999
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29035962092135048.901305012)
,p_translate_from_id=>wwv_flow_api.id(29035962092135048)
,p_translate_column_id=>396
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'password'
,p_translate_from_text=>'password'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40018850747080577)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29030956062135106.901305012)
,p_translate_from_id=>wwv_flow_api.id(29030956062135106)
,p_translate_column_id=>397
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Desktop'
,p_translate_from_text=>'Desktop'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40019091328080548)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34028491778587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34028491778587087)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Creation date'
,p_translate_from_text=>'Decredt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40019117047080547)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407323792820939.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407323792820939)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule'
,p_translate_from_text=>'Derulid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40019319491080547)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34077317688228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34077317688228362)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Creation date'
,p_translate_from_text=>'Decredt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40019515035080547)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39258385761502745.901305012)
,p_translate_from_id=>wwv_flow_api.id(39258385761502745)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Application Name'
,p_translate_from_text=>'Application Name'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40019737253080547)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29065115613897303.901305012)
,p_translate_from_id=>wwv_flow_api.id(29065115613897303)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Apex Version'
,p_translate_from_text=>'Deapexv'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40019942701080547)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34680476620337166.901305012)
,p_translate_from_id=>wwv_flow_api.id(34680476620337166)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Identifier'
,p_translate_from_text=>'Deident'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40020156871080547)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34026695843587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34026695843587087)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Name'
,p_translate_from_text=>'Dername'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40020305739080547)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34074364822228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34074364822228363)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'APEX version'
,p_translate_from_text=>'Deapeid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40020528146080547)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29111927509788994.901305012)
,p_translate_from_id=>wwv_flow_api.id(29111927509788994)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Project'
,p_translate_from_text=>'Deproid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40020725375080547)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34681673440337165.901305012)
,p_translate_from_id=>wwv_flow_api.id(34681673440337165)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Created by'
,p_translate_from_text=>'Decreus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40020968650080547)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34074937132228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34074937132228363)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Link'
,p_translate_from_text=>'Derlink'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40021109421080547)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29079030861826697.901305012)
,p_translate_from_id=>wwv_flow_api.id(29079030861826697)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Mutation date'
,p_translate_from_text=>'Dewyzdt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40021394763080547)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34675042818337168.901305012)
,p_translate_from_id=>wwv_flow_api.id(34675042818337168)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Page Number'
,p_translate_from_text=>'Depagen'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40021586021080547)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34073752716228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34073752716228363)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule'
,p_translate_from_text=>'Derulid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40021788282080547)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29115553156788990.901305012)
,p_translate_from_id=>wwv_flow_api.id(29115553156788990)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Mutation user'
,p_translate_from_text=>'Dewyzus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40022146479080546)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34675684226337168.901305012)
,p_translate_from_id=>wwv_flow_api.id(34675684226337168)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Region'
,p_translate_from_text=>'Deregio'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40022393392080546)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407768322820943.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407768322820943)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Statement'
,p_translate_from_text=>'Destate'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40022523304080546)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34076177451228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34076177451228362)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Statement'
,p_translate_from_text=>'Destate'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40022710711080546)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34266794053219039.901305012)
,p_translate_from_id=>wwv_flow_api.id(34266794053219039)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Name'
,p_translate_from_text=>'Dername'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40022998623080546)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34029667960587086.901305012)
,p_translate_from_id=>wwv_flow_api.id(34029667960587086)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Mutation date'
,p_translate_from_text=>'Dewyzdt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40023131557080546)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34030273299587086.901305012)
,p_translate_from_id=>wwv_flow_api.id(34030273299587086)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Mutation user'
,p_translate_from_text=>'Dewyzus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40023339246080546)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407648377820942.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407648377820942)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Binding Variable'
,p_translate_from_text=>'Deapbiv'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40023587087080546)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34077942060228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34077942060228362)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Created by'
,p_translate_from_text=>'Decreus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40023775562080546)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29078403661826697.901305012)
,p_translate_from_id=>wwv_flow_api.id(29078403661826697)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Created by'
,p_translate_from_text=>'Decreus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40023946555080546)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29404392189820909.901305012)
,p_translate_from_id=>wwv_flow_api.id(29404392189820909)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'APEX Number'
,p_translate_from_text=>'Deapenr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40024169468080546)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34674488859337168.901305012)
,p_translate_from_id=>wwv_flow_api.id(34674488859337168)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Application ID'
,p_translate_from_text=>'Deappid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40024384911080546)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34266898508219040.901305012)
,p_translate_from_id=>wwv_flow_api.id(34266898508219040)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Gradation'
,p_translate_from_text=>'Degrade'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40024579372080546)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34075554087228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34075554087228363)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Binding Variable'
,p_translate_from_text=>'Deapbiv'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40024766015080546)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34078523314228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34078523314228362)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Mutation date'
,p_translate_from_text=>'Dewyzdt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40024909163080546)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29076655505826699.901305012)
,p_translate_from_id=>wwv_flow_api.id(29076655505826699)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'APEX version'
,p_translate_from_text=>'Deapeid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40025144251080546)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39258208834502744.901305012)
,p_translate_from_id=>wwv_flow_api.id(39258208834502744)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Application Id'
,p_translate_from_text=>'Application Id'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40025374354080545)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34679252176337166.901305012)
,p_translate_from_id=>wwv_flow_api.id(34679252176337166)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Status ID'
,p_translate_from_text=>'Destaid'
);
end;
/
begin
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40025585691080545)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426676930667304.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426676930667304)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Gradation'
,p_translate_from_text=>'Degradde'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40025765772080545)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407582566820941.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407582566820941)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Link'
,p_translate_from_text=>'Derlink'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40025986237080545)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34076766424228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34076766424228362)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Type ID'
,p_translate_from_text=>'Detypid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40026104341080545)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29077847583826698.901305012)
,p_translate_from_id=>wwv_flow_api.id(29077847583826698)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Creation date'
,p_translate_from_text=>'Decredt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40026341012080545)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29113109886788993.901305012)
,p_translate_from_id=>wwv_flow_api.id(29113109886788993)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Description'
,p_translate_from_text=>'Dedescr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40026538620080545)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29114908475788991.901305012)
,p_translate_from_id=>wwv_flow_api.id(29114908475788991)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Mutation date'
,p_translate_from_text=>'Dewyzdt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40026727028080545)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39427011308667308.901305012)
,p_translate_from_id=>wwv_flow_api.id(39427011308667308)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Apex Version'
,p_translate_from_text=>'Deapexv'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40026965906080545)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29077222802826698.901305012)
,p_translate_from_id=>wwv_flow_api.id(29077222802826698)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Apex Version'
,p_translate_from_text=>'Deapexv'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40027375343080545)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34676802301337168.901305012)
,p_translate_from_id=>wwv_flow_api.id(34676802301337168)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Label'
,p_translate_from_text=>'Delabel'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40027534755080545)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34029030188587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34029030188587087)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Created by'
,p_translate_from_text=>'Decreus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40027709758080545)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29113793293788993.901305012)
,p_translate_from_id=>wwv_flow_api.id(29113793293788993)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Creation date'
,p_translate_from_text=>'Decredt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40027900119080545)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29114346391788993.901305012)
,p_translate_from_id=>wwv_flow_api.id(29114346391788993)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Created by'
,p_translate_from_text=>'Decreus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40028371054080544)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34673864616337169.901305012)
,p_translate_from_id=>wwv_flow_api.id(34673864616337169)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Run ID'
,p_translate_from_text=>'Derunid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40028503087080544)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34676269277337168.901305012)
,p_translate_from_id=>wwv_flow_api.id(34676269277337168)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Item Name'
,p_translate_from_text=>'Deitemn'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40028762589080544)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34266967432219041.901305012)
,p_translate_from_id=>wwv_flow_api.id(34266967432219041)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Type'
,p_translate_from_text=>'Detypde'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40028950343080544)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29079662777826697.901305012)
,p_translate_from_id=>wwv_flow_api.id(29079662777826697)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Mutation user'
,p_translate_from_text=>'Dewyzus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40029585679080544)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34681081650337166.901305012)
,p_translate_from_id=>wwv_flow_api.id(34681081650337166)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Creation date'
,p_translate_from_text=>'Decredt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40029784306080544)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34267878218219050.901305012)
,p_translate_from_id=>wwv_flow_api.id(34267878218219050)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Project'
,p_translate_from_text=>'Deprode'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40030172837080544)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34026047261587088.901305012)
,p_translate_from_id=>wwv_flow_api.id(34026047261587088)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule'
,p_translate_from_text=>'Derulid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40030348020080544)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34027880698587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34027880698587087)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Description'
,p_translate_from_text=>'Dedescr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40030582983080544)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34079139100228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34079139100228362)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Mutation user'
,p_translate_from_text=>'Dewyzus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40030928591080544)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407261182820938.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407261182820938)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Code'
,p_translate_from_text=>'Derlcid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40031584707080541)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39258385761502745.901305012)
,p_translate_from_id=>wwv_flow_api.id(39258385761502745)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40031712518080541)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34679820139337166.901305012)
,p_translate_from_id=>wwv_flow_api.id(34679820139337166)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40031978640080541)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34026695843587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34026695843587087)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40032156771080541)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34073156204228365.901305012)
,p_translate_from_id=>wwv_flow_api.id(34073156204228365)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40032372607080541)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34074937132228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34074937132228363)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40032562673080541)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34072084856228366.901305012)
,p_translate_from_id=>wwv_flow_api.id(34072084856228366)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40032773648080541)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426444220667302.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426444220667302)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40032949835080541)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407768322820943.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407768322820943)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40033154009080541)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34673299311337169.901305012)
,p_translate_from_id=>wwv_flow_api.id(34673299311337169)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40033306592080541)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34027217883587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34027217883587087)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40033507612080541)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34076177451228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34076177451228362)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40033736233080541)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34077942060228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34077942060228362)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40033923872080540)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39428531797667323.901305012)
,p_translate_from_id=>wwv_flow_api.id(39428531797667323)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40034106579080540)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39428670191667324.901305012)
,p_translate_from_id=>wwv_flow_api.id(39428670191667324)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40034553283080539)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34075554087228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34075554087228363)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40034702876080539)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426676930667304.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426676930667304)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40035366586080539)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34678600301337166.901305012)
,p_translate_from_id=>wwv_flow_api.id(34678600301337166)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40035539560080539)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407444533820940.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407444533820940)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40036158167080539)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34027880698587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34027880698587087)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40036341575080539)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34079139100228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34079139100228362)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40036772118080539)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34678032462337167.901305012)
,p_translate_from_id=>wwv_flow_api.id(34678032462337167)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40036943905080539)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39427613999667314.901305012)
,p_translate_from_id=>wwv_flow_api.id(39427613999667314)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40037349685080537)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34026695843587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34026695843587087)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40037556250080537)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34027880698587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34027880698587087)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40037768276080537)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426676930667304.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426676930667304)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40037986152080536)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407768322820943.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407768322820943)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40038149656080536)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34072084856228366.901305012)
,p_translate_from_id=>wwv_flow_api.id(34072084856228366)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40038369565080536)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34074937132228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34074937132228363)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40038584393080536)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34075554087228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34075554087228363)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40038723709080536)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34076177451228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34076177451228362)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40038920663080536)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34077942060228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34077942060228362)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40039115093080536)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34079139100228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34079139100228362)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40040390348080536)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39258385761502745.901305012)
,p_translate_from_id=>wwv_flow_api.id(39258385761502745)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40040541712080536)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426444220667302.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426444220667302)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40040770187080536)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34674488859337168.901305012)
,p_translate_from_id=>wwv_flow_api.id(34674488859337168)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40040989709080535)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34675042818337168.901305012)
,p_translate_from_id=>wwv_flow_api.id(34675042818337168)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40041139917080535)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34675684226337168.901305012)
,p_translate_from_id=>wwv_flow_api.id(34675684226337168)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40041300161080535)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34676269277337168.901305012)
,p_translate_from_id=>wwv_flow_api.id(34676269277337168)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40041568873080535)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34676802301337168.901305012)
,p_translate_from_id=>wwv_flow_api.id(34676802301337168)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40041713438080535)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34680476620337166.901305012)
,p_translate_from_id=>wwv_flow_api.id(34680476620337166)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40041938844080535)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34681081650337166.901305012)
,p_translate_from_id=>wwv_flow_api.id(34681081650337166)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40042146816080535)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34681673440337165.901305012)
,p_translate_from_id=>wwv_flow_api.id(34681673440337165)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40042375746080535)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34266794053219039.901305012)
,p_translate_from_id=>wwv_flow_api.id(34266794053219039)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40042503061080535)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34266898508219040.901305012)
,p_translate_from_id=>wwv_flow_api.id(34266898508219040)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40042789776080535)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34266967432219041.901305012)
,p_translate_from_id=>wwv_flow_api.id(34266967432219041)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40042971662080535)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34267878218219050.901305012)
,p_translate_from_id=>wwv_flow_api.id(34267878218219050)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40043125752080533)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34026047261587088.901305012)
,p_translate_from_id=>wwv_flow_api.id(34026047261587088)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40043353444080533)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34026695843587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34026695843587087)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40043527988080533)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34027880698587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34027880698587087)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40043727874080533)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426676930667304.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426676930667304)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40043959973080533)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407261182820938.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407261182820938)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40044151255080532)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407323792820939.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407323792820939)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
end;
/
begin
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40044363472080532)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407768322820943.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407768322820943)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40044575869080532)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34072084856228366.901305012)
,p_translate_from_id=>wwv_flow_api.id(34072084856228366)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40044741341080532)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34073752716228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34073752716228363)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40044918222080532)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34074364822228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34074364822228363)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40045173840080532)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34074937132228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34074937132228363)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40045354560080532)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34075554087228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34075554087228363)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40045590210080532)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34076177451228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34076177451228362)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40045781657080532)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34076766424228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34076766424228362)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40045902682080532)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34077942060228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34077942060228362)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40046172079080532)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34079139100228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34079139100228362)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40046325486080532)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29076655505826699.901305012)
,p_translate_from_id=>wwv_flow_api.id(29076655505826699)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40046557455080532)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29404392189820909.901305012)
,p_translate_from_id=>wwv_flow_api.id(29404392189820909)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'center'
,p_translate_from_text=>'center'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40046701551080532)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29111927509788994.901305012)
,p_translate_from_id=>wwv_flow_api.id(29111927509788994)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40048756225080531)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39258208834502744.901305012)
,p_translate_from_id=>wwv_flow_api.id(39258208834502744)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40048975277080531)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39258385761502745.901305012)
,p_translate_from_id=>wwv_flow_api.id(39258385761502745)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40049148908080531)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426444220667302.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426444220667302)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40049307481080528)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34026695843587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34026695843587087)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40049551880080528)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34027880698587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34027880698587087)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40049708694080528)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34028491778587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34028491778587087)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'button'
,p_translate_from_text=>'button'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40049928643080528)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34029667960587086.901305012)
,p_translate_from_id=>wwv_flow_api.id(34029667960587086)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'button'
,p_translate_from_text=>'button'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40050141844080528)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39426676930667304.901305012)
,p_translate_from_id=>wwv_flow_api.id(39426676930667304)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40050304406080528)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407768322820943.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407768322820943)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40050587621080527)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34074937132228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34074937132228363)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40050715568080527)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34075554087228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34075554087228363)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40050939505080527)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34076177451228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34076177451228362)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40051133800080527)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34077317688228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34077317688228362)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'button'
,p_translate_from_text=>'button'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40051384858080527)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34077942060228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34077942060228362)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40051503347080527)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34078523314228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34078523314228362)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'button'
,p_translate_from_text=>'button'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40051762766080527)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34079139100228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34079139100228362)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40051916624080527)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29077847583826698.901305012)
,p_translate_from_id=>wwv_flow_api.id(29077847583826698)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'button'
,p_translate_from_text=>'button'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40052107928080527)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29079030861826697.901305012)
,p_translate_from_id=>wwv_flow_api.id(29079030861826697)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'button'
,p_translate_from_text=>'button'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40052321499080527)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29113793293788993.901305012)
,p_translate_from_id=>wwv_flow_api.id(29113793293788993)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'button'
,p_translate_from_text=>'button'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40052515881080527)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29114908475788991.901305012)
,p_translate_from_id=>wwv_flow_api.id(29114908475788991)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'button'
,p_translate_from_text=>'button'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40053504878080526)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39258385761502745.901305012)
,p_translate_from_id=>wwv_flow_api.id(39258385761502745)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40053702518080524)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34028491778587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34028491778587087)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40053949492080524)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34029030188587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34029030188587087)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40054199385080524)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34029667960587086.901305012)
,p_translate_from_id=>wwv_flow_api.id(34029667960587086)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40054323088080524)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34030273299587086.901305012)
,p_translate_from_id=>wwv_flow_api.id(34030273299587086)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40054775104080524)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407648377820942.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407648377820942)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40054978886080524)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39427011308667308.901305012)
,p_translate_from_id=>wwv_flow_api.id(39427011308667308)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40055150850080524)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34074937132228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34074937132228363)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40055399573080524)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34075554087228363.901305012)
,p_translate_from_id=>wwv_flow_api.id(34075554087228363)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40055528389080524)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34077317688228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34077317688228362)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40055790533080524)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34077942060228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34077942060228362)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40055969204080524)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34078523314228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34078523314228362)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40056145312080524)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34079139100228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34079139100228362)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40056326865080523)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29077222802826698.901305012)
,p_translate_from_id=>wwv_flow_api.id(29077222802826698)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40056581895080523)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29077847583826698.901305012)
,p_translate_from_id=>wwv_flow_api.id(29077847583826698)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40056738109080523)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29078403661826697.901305012)
,p_translate_from_id=>wwv_flow_api.id(29078403661826697)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40056901757080523)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29079030861826697.901305012)
,p_translate_from_id=>wwv_flow_api.id(29079030861826697)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40057151157080523)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29079662777826697.901305012)
,p_translate_from_id=>wwv_flow_api.id(29079662777826697)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40057314520080523)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29113109886788993.901305012)
,p_translate_from_id=>wwv_flow_api.id(29113109886788993)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40057560913080523)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29113793293788993.901305012)
,p_translate_from_id=>wwv_flow_api.id(29113793293788993)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40057777678080523)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29114346391788993.901305012)
,p_translate_from_id=>wwv_flow_api.id(29114346391788993)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40057909865080523)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29114908475788991.901305012)
,p_translate_from_id=>wwv_flow_api.id(29114908475788991)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40058168851080523)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29115553156788990.901305012)
,p_translate_from_id=>wwv_flow_api.id(29115553156788990)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40058322907080523)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29065115613897303.901305012)
,p_translate_from_id=>wwv_flow_api.id(29065115613897303)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40058578558080519)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34028491778587087.901305012)
,p_translate_from_id=>wwv_flow_api.id(34028491778587087)
,p_translate_column_id=>428
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40058783131080519)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34029667960587086.901305012)
,p_translate_from_id=>wwv_flow_api.id(34029667960587086)
,p_translate_column_id=>428
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40058976649080519)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34077317688228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34077317688228362)
,p_translate_column_id=>428
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40059162764080519)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34078523314228362.901305012)
,p_translate_from_id=>wwv_flow_api.id(34078523314228362)
,p_translate_column_id=>428
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40059320844080519)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29077847583826698.901305012)
,p_translate_from_id=>wwv_flow_api.id(29077847583826698)
,p_translate_column_id=>428
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40059554191080519)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29079030861826697.901305012)
,p_translate_from_id=>wwv_flow_api.id(29079030861826697)
,p_translate_column_id=>428
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40059717767080519)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29113793293788993.901305012)
,p_translate_from_id=>wwv_flow_api.id(29113793293788993)
,p_translate_column_id=>428
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40059901360080519)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29114908475788991.901305012)
,p_translate_from_id=>wwv_flow_api.id(29114908475788991)
,p_translate_column_id=>428
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40060194311080491)
,p_page_id=>202
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34072559234228365.901305012)
,p_translate_from_id=>wwv_flow_api.id(34072559234228365)
,p_translate_column_id=>448
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Actions'
,p_translate_from_text=>'Actions'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40060344432080486)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34024842817587089.901305012)
,p_translate_from_id=>wwv_flow_api.id(34024842817587089)
,p_translate_column_id=>451
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
,p_translate_from_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40060565655080486)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29408169038820947.901305012)
,p_translate_from_id=>wwv_flow_api.id(29408169038820947)
,p_translate_column_id=>451
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
,p_translate_from_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40060773884080486)
,p_page_id=>210
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29075491030826701.901305012)
,p_translate_from_id=>wwv_flow_api.id(29075491030826701)
,p_translate_column_id=>451
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
,p_translate_from_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40060926421080486)
,p_page_id=>214
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29110789714788994.901305012)
,p_translate_from_id=>wwv_flow_api.id(29110789714788994)
,p_translate_column_id=>451
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
,p_translate_from_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40216498997595950)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15168277290724623.901305012)
,p_translate_from_id=>wwv_flow_api.id(15168277290724623)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40216633953595947)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15168123377724622.901305012)
,p_translate_from_id=>wwv_flow_api.id(15168123377724622)
,p_translate_column_id=>288
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>':P1_URL := apex_util.prepare_url(''f?p=100:220:&SESSION.:&DEBUG.'');'
,p_translate_from_text=>':P1_URL := apex_util.prepare_url(''f?p=100:220:&SESSION.:&DEBUG.'');'
);
end;
/
begin
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40216895476595947)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15168021256724621.901305012)
,p_translate_from_id=>wwv_flow_api.id(15168021256724621)
,p_translate_column_id=>288
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'window.location.href = $v(''P1_URL'');'
,p_translate_from_text=>'window.location.href = $v(''P1_URL'');'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40217606499595947)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15168123377724622.901305012)
,p_translate_from_id=>wwv_flow_api.id(15168123377724622)
,p_translate_column_id=>290
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P1_URL'
,p_translate_from_text=>'P1_URL'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40217993561595946)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15168123377724622.901305012)
,p_translate_from_id=>wwv_flow_api.id(15168123377724622)
,p_translate_column_id=>291
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40889958820877138)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39778110173497748.901305012)
,p_translate_from_id=>wwv_flow_api.id(39778110173497748)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Total Bugs'
,p_translate_from_text=>'Total Bugs'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40890129827877137)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(40544666627820609.901305012)
,p_translate_from_id=>wwv_flow_api.id(40544666627820609)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Total Codesmells'
,p_translate_from_text=>'Total Codesmells'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40890350629877137)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(40545051880820613.901305012)
,p_translate_from_id=>wwv_flow_api.id(40545051880820613)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Total Vulnerabilities'
,p_translate_from_text=>'Total Vulnerabilities'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40890508071877137)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39776332218497730.901305012)
,p_translate_from_id=>wwv_flow_api.id(39776332218497730)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'History Codesmells'
,p_translate_from_text=>'History Codesmells'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40890711812877137)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39776864266497735.901305012)
,p_translate_from_id=>wwv_flow_api.id(39776864266497735)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'History Vulnerabilities'
,p_translate_from_text=>'History Vulnerabilities'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40891464122877134)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39778110173497748.901305012)
,p_translate_from_id=>wwv_flow_api.id(39778110173497748)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT count(b.dedescr) as decount_bugs, b.dedescr',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
' and a.detypid = 1',
' group by b.dedescr',
''))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT count(b.dedescr) as decount_bugs, b.dedescr',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
' and a.detypid = 1',
' group by b.dedescr',
''))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40891641933877134)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(40544666627820609.901305012)
,p_translate_from_id=>wwv_flow_api.id(40544666627820609)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT count(b.dedescr) as decount_csm, b.dedescr',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
' and a.detypid = 2',
' group by b.dedescr',
''))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT count(b.dedescr) as decount_csm, b.dedescr',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
' and a.detypid = 2',
' group by b.dedescr',
''))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40891837119877134)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(40545051880820613.901305012)
,p_translate_from_id=>wwv_flow_api.id(40545051880820613)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT count(b.dedescr) as decount_vul, b.dedescr',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
' and a.detypid = 3',
' group by b.dedescr',
''))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT count(b.dedescr) as decount_vul, b.dedescr',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
' and a.detypid = 3',
' group by b.dedescr',
''))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40892092063877134)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39776332218497730.901305012)
,p_translate_from_id=>wwv_flow_api.id(39776332218497730)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT *',
'FROM (',
'  SELECT a.deproid, a.detypid, c.dedescr,a.decredt',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
')',
'PIVOT (',
'  count(*) as count',
'  FOR detypid IN (1,2,3)',
' )*/',
' ',
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 2',
'group by b.derunid, c.dedescr, b.detypid',
'order by b.derunid;',
''))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT *',
'FROM (',
'  SELECT a.deproid, a.detypid, c.dedescr,a.decredt',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
')',
'PIVOT (',
'  count(*) as count',
'  FOR detypid IN (1,2,3)',
' )*/',
' ',
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 2',
'group by b.derunid, c.dedescr, b.detypid',
'order by b.derunid;',
''))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40892298915877134)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39776864266497735.901305012)
,p_translate_from_id=>wwv_flow_api.id(39776864266497735)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT *',
'FROM (',
'  SELECT a.deproid, a.detypid, c.dedescr,a.decredt',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
')',
'PIVOT (',
'  count(*) as count',
'  FOR detypid IN (1,2,3)',
' )*/',
' ',
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 3',
'group by b.derunid, c.dedescr, b.detypid',
'order by b.derunid;',
''))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT *',
'FROM (',
'  SELECT a.deproid, a.detypid, c.dedescr,a.decredt',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
')',
'PIVOT (',
'  count(*) as count',
'  FOR detypid IN (1,2,3)',
' )*/',
' ',
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 3',
'group by b.derunid, c.dedescr, b.detypid',
'order by b.derunid;',
''))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40893030617876993)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(40544537977820608.901305012)
,p_translate_from_id=>wwv_flow_api.id(40544537977820608)
,p_translate_column_id=>106
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Total Bugs'
,p_translate_from_text=>'Total Bugs'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40893108265876993)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(40544944527820612.901305012)
,p_translate_from_id=>wwv_flow_api.id(40544944527820612)
,p_translate_column_id=>106
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Total Codesmells'
,p_translate_from_text=>'Total Codesmells'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40893369821876993)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(40545348090820616.901305012)
,p_translate_from_id=>wwv_flow_api.id(40545348090820616)
,p_translate_column_id=>106
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Total Vulnerabilities'
,p_translate_from_text=>'Total Vulnerabilities'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40894812876876988)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(40544537977820608.901305012)
,p_translate_from_id=>wwv_flow_api.id(40544537977820608)
,p_translate_column_id=>107
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'#DECOUNT_BUGS#'
,p_translate_from_text=>'#DECOUNT_BUGS#'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40895070278876988)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(40544944527820612.901305012)
,p_translate_from_id=>wwv_flow_api.id(40544944527820612)
,p_translate_column_id=>107
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'#DECOUNT_CSM#'
,p_translate_from_text=>'#DECOUNT_CSM#'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40895244110876988)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(40545348090820616.901305012)
,p_translate_from_id=>wwv_flow_api.id(40545348090820616)
,p_translate_column_id=>107
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'#DECOUNT_VUL#'
,p_translate_from_text=>'#DECOUNT_VUL#'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40895492834876787)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39777370254497740.901305012)
,p_translate_from_id=>wwv_flow_api.id(39777370254497740)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40895802691876747)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39777703209497744.901305012)
,p_translate_from_id=>wwv_flow_api.id(39777703209497744)
,p_translate_column_id=>288
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if($v(''P300_DEDESCR'')){',
'  apex.region("ResultId").call("getActions").invoke("reset-report");',
'',
'    setTimeout(function() {',
'        apex.region("ResultId").widget().interactiveGrid("addFilter", {',
'          type: ''column'',',
'          columnType: ''column'',',
'          columnName: ''DETYPDE'',',
'          operator: ''EQ'',',
'          value: $v(''P300_DEDESCR''),',
'          isCaseSensitive: false',
'        });',
'        $s(''P300_DEDESCR'', null);',
'    }, 1000);',
'}'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if($v(''P300_DEDESCR'')){',
'  apex.region("ResultId").call("getActions").invoke("reset-report");',
'',
'    setTimeout(function() {',
'        apex.region("ResultId").widget().interactiveGrid("addFilter", {',
'          type: ''column'',',
'          columnType: ''column'',',
'          columnName: ''DETYPDE'',',
'          operator: ''EQ'',',
'          value: $v(''P300_DEDESCR''),',
'          isCaseSensitive: false',
'        });',
'        $s(''P300_DEDESCR'', null);',
'    }, 1000);',
'}'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40896180800876564)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34677416540337167.901305012)
,p_translate_from_id=>wwv_flow_api.id(34677416540337167)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule'
,p_translate_from_text=>'Derulid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40896304486876562)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39774971927497716.901305012)
,p_translate_from_id=>wwv_flow_api.id(39774971927497716)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40896566748876562)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39776257529497729.901305012)
,p_translate_from_id=>wwv_flow_api.id(39776257529497729)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40896807062876560)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(29407582566820941.901305012)
,p_translate_from_id=>wwv_flow_api.id(29407582566820941)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'LOV'
,p_translate_from_text=>'LOV'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40897030453876560)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34673864616337169.901305012)
,p_translate_from_id=>wwv_flow_api.id(34673864616337169)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40897225746876560)
,p_page_id=>300
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(34677416540337167.901305012)
,p_translate_from_id=>wwv_flow_api.id(34677416540337167)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'LOV'
,p_translate_from_text=>'LOV'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40897507492876510)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39775453170497721.901305012)
,p_translate_from_id=>wwv_flow_api.id(39775453170497721)
,p_translate_column_id=>465
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Run ID'
,p_translate_from_text=>'Run ID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40897796021876510)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39775523251497722.901305012)
,p_translate_from_id=>wwv_flow_api.id(39775523251497722)
,p_translate_column_id=>465
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Total'
,p_translate_from_text=>'Total'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40897944438876510)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39776685593497733.901305012)
,p_translate_from_id=>wwv_flow_api.id(39776685593497733)
,p_translate_column_id=>465
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Run ID'
,p_translate_from_text=>'Run ID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40898166606876510)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39776797571497734.901305012)
,p_translate_from_id=>wwv_flow_api.id(39776797571497734)
,p_translate_column_id=>465
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Total'
,p_translate_from_text=>'Total'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40898368447876510)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39777111992497738.901305012)
,p_translate_from_id=>wwv_flow_api.id(39777111992497738)
,p_translate_column_id=>465
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Run ID'
,p_translate_from_text=>'Run ID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40898588930876510)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39777246462497739.901305012)
,p_translate_from_id=>wwv_flow_api.id(39777246462497739)
,p_translate_column_id=>465
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Total'
,p_translate_from_text=>'Total'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40898793342876508)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39776516123497732.901305012)
,p_translate_from_id=>wwv_flow_api.id(39776516123497732)
,p_translate_column_id=>466
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Graph'
,p_translate_from_text=>'Graph'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40898976969876508)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39777044213497737.901305012)
,p_translate_from_id=>wwv_flow_api.id(39777044213497737)
,p_translate_column_id=>466
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Graph'
,p_translate_from_text=>'Graph'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(40899100308876508)
,p_page_id=>1
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(39775340786497720.901305012)
,p_translate_from_id=>wwv_flow_api.id(39775340786497720)
,p_translate_column_id=>466
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Graph'
,p_translate_from_text=>'Graph'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46677323511760640)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>204.901305012
,p_translate_from_id=>204
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Profiles'
,p_translate_from_text=>'Profiles'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46677429398760639)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>205.901305012
,p_translate_from_id=>205
,p_translate_column_id=>5
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Detail profile'
,p_translate_from_text=>'Detail profile'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46677736435760637)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>204.901305012
,p_translate_from_id=>204
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Profiles'
,p_translate_from_text=>'Profiles'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46677927605760636)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>205.901305012
,p_translate_from_id=>205
,p_translate_column_id=>6
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Detail profile'
,p_translate_from_text=>'Detail profile'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46678204342760629)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42495302824122779.901305012)
,p_translate_from_id=>wwv_flow_api.id(42495302824122779)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Create'
,p_translate_from_text=>'Create'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46678499506760628)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42497011922122778.901305012)
,p_translate_from_id=>wwv_flow_api.id(42497011922122778)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Cancel'
,p_translate_from_text=>'Cancel'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46678612142760628)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42496958589122778.901305012)
,p_translate_from_id=>wwv_flow_api.id(42496958589122778)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Delete'
,p_translate_from_text=>'Delete'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46678892722760628)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42496807187122778.901305012)
,p_translate_from_id=>wwv_flow_api.id(42496807187122778)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Save'
,p_translate_from_text=>'Save'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46679046312760628)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42496797048122778.901305012)
,p_translate_from_id=>wwv_flow_api.id(42496797048122778)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Create'
,p_translate_from_text=>'Create'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46679206324760628)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15170336113724644.901305012)
,p_translate_from_id=>wwv_flow_api.id(15170336113724644)
,p_translate_column_id=>13
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Add rule'
,p_translate_from_text=>'Button.Add Rule'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46679569269760627)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169028379724631.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169028379724631)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Profile ID'
,p_translate_from_text=>'Deprofl Id'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46679708821760623)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42499076905122772.901305012)
,p_translate_from_id=>wwv_flow_api.id(42499076905122772)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Profile name'
,p_translate_from_text=>'Deprfnm'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46679926812760623)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541174671693424.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541174671693424)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Type ID'
,p_translate_from_text=>'Detypid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46680106483760623)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42498644093122777.901305012)
,p_translate_from_id=>wwv_flow_api.id(42498644093122777)
,p_translate_column_id=>14
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Profile ID'
,p_translate_from_text=>'Deprofl Id'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46680429124760618)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42502277710122770.901305012)
,p_translate_from_id=>wwv_flow_api.id(42502277710122770)
,p_translate_column_id=>18
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Action Processed.'
,p_translate_from_text=>'Action Processed.'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46680699954760615)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42538846552693401.901305012)
,p_translate_from_id=>wwv_flow_api.id(42538846552693401)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Used in profiles'
,p_translate_from_text=>'Region.UsedInProfiles'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46680882538760615)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539287553693405.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539287553693405)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Parameters per statement'
,p_translate_from_text=>'Region.ParametersPerStatement'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46681064786760615)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42489884887122786.901305012)
,p_translate_from_id=>wwv_flow_api.id(42489884887122786)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Profiles'
,p_translate_from_text=>'Profiles'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46681247716760615)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42496348707122778.901305012)
,p_translate_from_id=>wwv_flow_api.id(42496348707122778)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Detail profile'
,p_translate_from_text=>'Region.DetailProfile'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46681445498760615)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15168588666724626.901305012)
,p_translate_from_id=>wwv_flow_api.id(15168588666724626)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Projects per profile'
,p_translate_from_text=>'Region.ProjectsPerProfile'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46681642571760615)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169154405724632.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169154405724632)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Rules per profile'
,p_translate_from_text=>'Region.RulesPerProfile'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46681874969760615)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541564219693428.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541564219693428)
,p_translate_column_id=>20
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'Y'
,p_translate_to_text=>'Rules per project'
,p_translate_from_text=>'Region.RulesPerProject'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46682374816760613)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42538846552693401.901305012)
,p_translate_from_id=>wwv_flow_api.id(42538846552693401)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select      r.deprofl_id, p.deprfnm',
'from        drprrul r, drprofl p',
'where       r.deprofl_id = p.deprofl_id',
'and         r.derulid = :P201_DERULID;'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select      r.deprofl_id, p.deprfnm',
'from        drprrul r, drprofl p',
'where       r.deprofl_id = p.deprofl_id',
'and         r.derulid = :P201_DERULID;'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46682532124760613)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539287553693405.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539287553693405)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select        *',
'from        drparst',
'where        derlcid = :P203_DERLCID'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select        *',
'from        drparst',
'where        derlcid = :P203_DERLCID'))
);
end;
/
begin
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46682721265760613)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42489884887122786.901305012)
,p_translate_from_id=>wwv_flow_api.id(42489884887122786)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select DEPROFL_ID,',
'       DEPRFNM,',
'       DECREDT,',
'       DECREUS,',
'       DEWYZDT,',
'       DEWYZUS',
'  from DRPROFL'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select DEPROFL_ID,',
'       DEPRFNM,',
'       DECREDT,',
'       DECREUS,',
'       DEWYZDT,',
'       DEWYZUS',
'  from DRPROFL'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46682949464760613)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15168588666724626.901305012)
,p_translate_from_id=>wwv_flow_api.id(15168588666724626)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select       *',
'from         vwprfpr',
'where        deprofl_id = :P205_DEPROFL_ID'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select       *',
'from         vwprfpr',
'where        deprofl_id = :P205_DEPROFL_ID'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46683128811760613)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169154405724632.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169154405724632)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'with sel as (',
'    select C.DERULID,',
'           A.DERNAME,',
'           A.DEDESCR,',
'           min(v.deapenr) as  deapenr,',
'            d.dedescr as dertype_d',
'    from DRRULES A, DRPRRUL C , DRRULCE B, DRRTYPE D, drapexv v',
'    where A.DERULID = B.DERULID',
'    and   C.DERULID = A.DERULID',
'    and D.DETYPID = a.DETYPID',
'    and b.deapeid = v.deapeid',
'    and C.DEPROFL_ID = :P205_DEPROFL_ID',
'    group by c.derulid, a.dername, a.dedescr, d.dedescr',
')',
'select      s.*, v.deapexv as deapexv_min',
'from        sel s, drapexv v',
'where       s.deapenr = v.deapenr;'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'with sel as (',
'    select C.DERULID,',
'           A.DERNAME,',
'           A.DEDESCR,',
'           min(v.deapenr) as  deapenr,',
'            d.dedescr as dertype_d',
'    from DRRULES A, DRPRRUL C , DRRULCE B, DRRTYPE D, drapexv v',
'    where A.DERULID = B.DERULID',
'    and   C.DERULID = A.DERULID',
'    and D.DETYPID = a.DETYPID',
'    and b.deapeid = v.deapeid',
'    and C.DEPROFL_ID = :P205_DEPROFL_ID',
'    group by c.derulid, a.dername, a.dedescr, d.dedescr',
')',
'select      s.*, v.deapexv as deapexv_min',
'from        sel s, drapexv v',
'where       s.deapenr = v.deapenr;'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46683364814760613)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541564219693428.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541564219693428)
,p_translate_column_id=>21
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select        dername, dedescr, dertype_d',
'from          vwrlprp',
'where         deproid = :P215_DEPROID',
'and           deprofl_id = :P215_DEPROFL_ID'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select        dername, dedescr, dertype_d',
'from          vwrlprp',
'where         deproid = :P215_DEPROID',
'and           deprofl_id = :P215_DEPROFL_ID'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46683906889760603)
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42489430525122786.901305012)
,p_translate_from_id=>wwv_flow_api.id(42489430525122786)
,p_translate_column_id=>28
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Manage profiles'
,p_translate_from_text=>'Manage profiles'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46684123205760462)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15170482629724645.901305012)
,p_translate_from_id=>wwv_flow_api.id(15170482629724645)
,p_translate_column_id=>148
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Profile ID'
,p_translate_from_text=>'Deprofl Id'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46684485306760460)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15170482629724645.901305012)
,p_translate_from_id=>wwv_flow_api.id(15170482629724645)
,p_translate_column_id=>149
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Profile ID'
,p_translate_from_text=>'Deprofl Id'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46684892191760289)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169028379724631.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169028379724631)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46684970753760289)
,p_page_id=>217
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15170560586724646.901305012)
,p_translate_from_id=>wwv_flow_api.id(15170560586724646)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46685177082760289)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42499076905122772.901305012)
,p_translate_from_id=>wwv_flow_api.id(42499076905122772)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46685316243760289)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42499480241122771.901305012)
,p_translate_from_id=>wwv_flow_api.id(42499480241122771)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46685504576760289)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42499806761122771.901305012)
,p_translate_from_id=>wwv_flow_api.id(42499806761122771)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46685793546760289)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42500200574122771.901305012)
,p_translate_from_id=>wwv_flow_api.id(42500200574122771)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46685944958760289)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42500672184122771.901305012)
,p_translate_from_id=>wwv_flow_api.id(42500672184122771)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46686195189760289)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541174671693424.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541174671693424)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46686321401760288)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42498644093122777.901305012)
,p_translate_from_id=>wwv_flow_api.id(42498644093122777)
,p_translate_column_id=>268
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46686621844760285)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169028379724631.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169028379724631)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46686864498760285)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42499076905122772.901305012)
,p_translate_from_id=>wwv_flow_api.id(42499076905122772)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46687086301760285)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541174671693424.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541174671693424)
,p_translate_column_id=>269
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46687364578760283)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42499076905122772.901305012)
,p_translate_from_id=>wwv_flow_api.id(42499076905122772)
,p_translate_column_id=>270
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46687549818760282)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42499076905122772.901305012)
,p_translate_from_id=>wwv_flow_api.id(42499076905122772)
,p_translate_column_id=>271
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46687759851760271)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540601329693419.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540601329693419)
,p_translate_column_id=>278
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'REGION_SOURCE'
,p_translate_from_text=>'REGION_SOURCE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46687982387760271)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15170886946724649.901305012)
,p_translate_from_id=>wwv_flow_api.id(15170886946724649)
,p_translate_column_id=>278
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'PLSQL_CODE'
,p_translate_from_text=>'PLSQL_CODE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46688132616760271)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42502612993122770.901305012)
,p_translate_from_id=>wwv_flow_api.id(42502612993122770)
,p_translate_column_id=>278
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'CLEAR_CACHE_CURRENT_PAGE'
,p_translate_from_text=>'CLEAR_CACHE_CURRENT_PAGE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46688427706760268)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42501417948122770.901305012)
,p_translate_from_id=>wwv_flow_api.id(42501417948122770)
,p_translate_column_id=>279
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DRPROFL'
,p_translate_from_text=>'DRPROFL'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46688668014760267)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42502277710122770.901305012)
,p_translate_from_id=>wwv_flow_api.id(42502277710122770)
,p_translate_column_id=>279
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DRPROFL'
,p_translate_from_text=>'DRPROFL'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46688805768760266)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42501417948122770.901305012)
,p_translate_from_id=>wwv_flow_api.id(42501417948122770)
,p_translate_column_id=>280
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P205_DEPROFL_ID'
,p_translate_from_text=>'P205_DEPROFL_ID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46689075989760266)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42502277710122770.901305012)
,p_translate_from_id=>wwv_flow_api.id(42502277710122770)
,p_translate_column_id=>280
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'P205_DEPROFL_ID'
,p_translate_from_text=>'P205_DEPROFL_ID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46689385789760264)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15170886946724649.901305012)
,p_translate_from_id=>wwv_flow_api.id(15170886946724649)
,p_translate_column_id=>281
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    case :APEX$ROW_STATUS',
'    when ''D'' then',
'        delete drprrul',
'         where derulid = :DERULID',
'         and   deprofl_id = :DEPROFL_ID;',
'    end case;',
'end;'))
,p_translate_from_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    case :APEX$ROW_STATUS',
'    when ''D'' then',
'        delete drprrul',
'         where derulid = :DERULID',
'         and   deprofl_id = :DEPROFL_ID;',
'    end case;',
'end;'))
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46689540709760264)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42501417948122770.901305012)
,p_translate_from_id=>wwv_flow_api.id(42501417948122770)
,p_translate_column_id=>281
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DEPROFL_ID'
,p_translate_from_text=>'DEPROFL_ID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46689779921760264)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42502277710122770.901305012)
,p_translate_from_id=>wwv_flow_api.id(42502277710122770)
,p_translate_column_id=>281
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'DEPROFL_ID'
,p_translate_from_text=>'DEPROFL_ID'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46689990665760262)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540601329693419.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540601329693419)
,p_translate_column_id=>282
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46690149621760262)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15170886946724649.901305012)
,p_translate_from_id=>wwv_flow_api.id(15170886946724649)
,p_translate_column_id=>282
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46690426168760260)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540601329693419.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540601329693419)
,p_translate_column_id=>283
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46690681099760260)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15170886946724649.901305012)
,p_translate_from_id=>wwv_flow_api.id(15170886946724649)
,p_translate_column_id=>283
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46690978350760257)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540601329693419.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540601329693419)
,p_translate_column_id=>285
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46691516204760223)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42496348707122778.901305012)
,p_translate_from_id=>wwv_flow_api.id(42496348707122778)
,p_translate_column_id=>308
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46691755505760221)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42496348707122778.901305012)
,p_translate_from_id=>wwv_flow_api.id(42496348707122778)
,p_translate_column_id=>309
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46692059883760219)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42496348707122778.901305012)
,p_translate_from_id=>wwv_flow_api.id(42496348707122778)
,p_translate_column_id=>310
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46692319745760197)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42502277710122770.901305012)
,p_translate_from_id=>wwv_flow_api.id(42502277710122770)
,p_translate_column_id=>323
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'I:U:D'
,p_translate_from_text=>'I:U:D'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46692573486760196)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42502277710122770.901305012)
,p_translate_from_id=>wwv_flow_api.id(42502277710122770)
,p_translate_column_id=>324
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46692758457760045)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541368990693426.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541368990693426)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule type'
,p_translate_from_text=>'Dertype D'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46692843852760045)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42493484588122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42493484588122780)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Created by'
,p_translate_from_text=>'Decreus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46693076353760045)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169585722724636.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169585722724636)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Name'
,p_translate_from_text=>'Dername'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46693251446760045)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541763401693430.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541763401693430)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Name'
,p_translate_from_text=>'Dername'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46693488129760045)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539553369693408.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539553369693408)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule Code'
,p_translate_from_text=>'Derlcid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46693600643760045)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539735825693410.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539735825693410)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Parameter name'
,p_translate_from_text=>'Deparnm'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46693886028760045)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541929477693432.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541929477693432)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule type'
,p_translate_from_text=>'Dertype D'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46694038689760044)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539622762693409.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539622762693409)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Profile ID'
,p_translate_from_text=>'Deprofl Id'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46694299662760044)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540979353693422.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540979353693422)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'APEX Number'
,p_translate_from_text=>'Deapenr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46694448498760044)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42492829172122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42492829172122780)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Creation date'
,p_translate_from_text=>'Decredt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46694667078760044)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42494036669122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42494036669122780)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Mutation date'
,p_translate_from_text=>'Dewyzdt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46694805060760044)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15168826023724629.901305012)
,p_translate_from_id=>wwv_flow_api.id(15168826023724629)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Description'
,p_translate_from_text=>'Dedescr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46695095796760044)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540778441693420.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540778441693420)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Description'
,p_translate_from_text=>'Dedescr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46695201245760044)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169653291724637.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169653291724637)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Description'
,p_translate_from_text=>'Dedescr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46695431908760044)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169494285724635.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169494285724635)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule'
,p_translate_from_text=>'Derulid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46695677598760044)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541025029693423.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541025029693423)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Min. version'
,p_translate_from_text=>'Deapexv Min'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46695836234760044)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42494618808122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42494618808122780)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Mutation user'
,p_translate_from_text=>'Dewyzus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46696019959760044)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539149086693404.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539149086693404)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Profile name'
,p_translate_from_text=>'Deprfnm'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46696204621760044)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541407147693427.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541407147693427)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Rule type'
,p_translate_from_text=>'Dertype D'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46696484975760044)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540235120693415.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540235120693415)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Mutation user'
,p_translate_from_text=>'Dewyzus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46696686939760044)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539962134693412.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539962134693412)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Creation date'
,p_translate_from_text=>'Decredt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46696883586760044)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540126103693414.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540126103693414)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Mutation date'
,p_translate_from_text=>'Dewyzdt'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46697003304760044)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541842117693431.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541842117693431)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Description'
,p_translate_from_text=>'Dedescr'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46697276398760043)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541203466693425.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541203466693425)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Type ID'
,p_translate_from_text=>'Detypid'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46697495636760043)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539440715693407.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539440715693407)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Parameter ID'
,p_translate_from_text=>'Deparst Id'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46697626312760043)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42492288125122781.901305012)
,p_translate_from_id=>wwv_flow_api.id(42492288125122781)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Profile name'
,p_translate_from_text=>'Deprfnm'
);
end;
/
begin
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46697816532760043)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540063105693413.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540063105693413)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Created by'
,p_translate_from_text=>'Decreus'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46698069110760043)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539070252693403.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539070252693403)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Profile ID'
,p_translate_from_text=>'Deprofl Id'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46698275914760043)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539835621693411.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539835621693411)
,p_translate_column_id=>421
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Parameter value'
,p_translate_from_text=>'Deparvl'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46699271945760040)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541763401693430.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541763401693430)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46699422764760040)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169585722724636.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169585722724636)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46699697083760040)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541368990693426.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541368990693426)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46699883169760040)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15170787027724648.901305012)
,p_translate_from_id=>wwv_flow_api.id(15170787027724648)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46700074716760040)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42493484588122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42493484588122780)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46700215110760040)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541929477693432.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541929477693432)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46700433996760040)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15168968891724630.901305012)
,p_translate_from_id=>wwv_flow_api.id(15168968891724630)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46700662535760040)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42491696638122781.901305012)
,p_translate_from_id=>wwv_flow_api.id(42491696638122781)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46700835891760040)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169653291724637.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169653291724637)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46701063001760040)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540778441693420.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540778441693420)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46701278434760040)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15168760257724628.901305012)
,p_translate_from_id=>wwv_flow_api.id(15168760257724628)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46701430637760039)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42494618808122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42494618808122780)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46701628573760039)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539149086693404.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539149086693404)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46701874236760039)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541407147693427.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541407147693427)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46702074593760039)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541842117693431.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541842117693431)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46702286448760039)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540559904693418.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540559904693418)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46702485456760039)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42492288125122781.901305012)
,p_translate_from_id=>wwv_flow_api.id(42492288125122781)
,p_translate_column_id=>422
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46703299810760037)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539553369693408.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539553369693408)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46703442453760037)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169585722724636.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169585722724636)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46703685901760037)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541368990693426.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541368990693426)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46703850162760037)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541763401693430.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541763401693430)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46704040196760037)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42493484588122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42493484588122780)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46704219846760035)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15170787027724648.901305012)
,p_translate_from_id=>wwv_flow_api.id(15170787027724648)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46704434948760035)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541929477693432.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541929477693432)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46704652854760035)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539735825693410.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539735825693410)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46704868347760035)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539622762693409.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539622762693409)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'LOV'
,p_translate_from_text=>'LOV'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46705088394760035)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540778441693420.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540778441693420)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46705271735760035)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169653291724637.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169653291724637)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46705445447760035)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42494618808122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42494618808122780)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46705613836760035)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541407147693427.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541407147693427)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46705826589760035)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540235120693415.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540235120693415)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46706015960760035)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539149086693404.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539149086693404)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46706274964760035)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540126103693414.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540126103693414)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46706412350760035)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539962134693412.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539962134693412)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46706605795760035)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541842117693431.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541842117693431)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46706852717760034)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540559904693418.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540559904693418)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'Y'
,p_translate_from_text=>'Y'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46707040566760034)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42492288125122781.901305012)
,p_translate_from_id=>wwv_flow_api.id(42492288125122781)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46707201185760034)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540063105693413.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540063105693413)
,p_translate_column_id=>423
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'VALUE'
,p_translate_from_text=>'VALUE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46707936966760032)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541763401693430.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541763401693430)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46708179905760032)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169585722724636.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169585722724636)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46708319823760032)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541368990693426.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541368990693426)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46708515018760032)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15170787027724648.901305012)
,p_translate_from_id=>wwv_flow_api.id(15170787027724648)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46708746227760032)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42493484588122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42493484588122780)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46708937753760032)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540979353693422.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540979353693422)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46709132521760032)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541929477693432.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541929477693432)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46709351136760032)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540778441693420.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540778441693420)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46709509470760032)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169653291724637.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169653291724637)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46709752446760031)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169494285724635.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169494285724635)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46709922734760031)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42494618808122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42494618808122780)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46710123559760031)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539149086693404.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539149086693404)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46710398239760031)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541407147693427.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541407147693427)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46710591604760031)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541203466693425.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541203466693425)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46710719283760031)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541842117693431.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541842117693431)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46710973353760031)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540559904693418.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540559904693418)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46711168799760031)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42492288125122781.901305012)
,p_translate_from_id=>wwv_flow_api.id(42492288125122781)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46711370282760031)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539440715693407.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539440715693407)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46711566484760031)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539070252693403.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539070252693403)
,p_translate_column_id=>424
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'right'
,p_translate_from_text=>'right'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46712612450760028)
,p_page_id=>201
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539149086693404.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539149086693404)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46712881762760028)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169585722724636.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169585722724636)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46713098235760028)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15169653291724637.901305012)
,p_translate_from_id=>wwv_flow_api.id(15169653291724637)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46713218345760028)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42540778441693420.901305012)
,p_translate_from_id=>wwv_flow_api.id(42540778441693420)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46713479920760028)
,p_page_id=>200
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541368990693426.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541368990693426)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46713654265760028)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541407147693427.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541407147693427)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46713806502760028)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42492288125122781.901305012)
,p_translate_from_id=>wwv_flow_api.id(42492288125122781)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46714008929760028)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42492829172122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42492829172122780)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'button'
,p_translate_from_text=>'button'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46714271374760028)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42493484588122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42493484588122780)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46714415823760028)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42494036669122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42494036669122780)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'button'
,p_translate_from_text=>'button'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46714659428760028)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42494618808122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42494618808122780)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'TEXT'
,p_translate_from_text=>'TEXT'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46714805640760028)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541763401693430.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541763401693430)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46715055528760028)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541842117693431.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541842117693431)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46715247588760028)
,p_page_id=>215
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541929477693432.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541929477693432)
,p_translate_column_id=>425
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
end;
/
begin
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46715875713760025)
,p_page_id=>203
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42539835621693411.901305012)
,p_translate_from_id=>wwv_flow_api.id(42539835621693411)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46716008403760025)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(15168826023724629.901305012)
,p_translate_from_id=>wwv_flow_api.id(15168826023724629)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46716283469760025)
,p_page_id=>205
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42541025029693423.901305012)
,p_translate_from_id=>wwv_flow_api.id(42541025029693423)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46716431703760025)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42492829172122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42492829172122780)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46716637416760025)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42493484588122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42493484588122780)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46716800290760025)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42494036669122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42494036669122780)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'N'
,p_translate_from_text=>'N'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46717067425760025)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42494618808122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42494618808122780)
,p_translate_column_id=>426
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'BOTH'
,p_translate_from_text=>'BOTH'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46717232440760021)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42492829172122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42492829172122780)
,p_translate_column_id=>428
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46717426012760021)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42494036669122780.901305012)
,p_translate_from_id=>wwv_flow_api.id(42494036669122780)
,p_translate_column_id=>428
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'NONE'
,p_translate_from_text=>'NONE'
);
wwv_flow_api.create_translation(
 p_id=>wwv_flow_api.id(46717661812759988)
,p_page_id=>204
,p_translated_flow_id=>901305012
,p_translate_to_id=>wwv_flow_api.id(42491077053122783.901305012)
,p_translate_from_id=>wwv_flow_api.id(42491077053122783)
,p_translate_column_id=>451
,p_translate_to_lang_code=>'en'
,p_translation_specific_to_item=>'NO'
,p_template_translatable=>'N'
,p_translate_to_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
,p_translate_from_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
);
end;
/
prompt --application/shared_components/globalization/messages
begin
null;
end;
/
prompt --application/shared_components/globalization/dyntranslations
begin
null;
end;
/
prompt --application/shared_components/user_interface/shortcuts/delete_confirm_msg
begin
wwv_flow_api.create_shortcut(
 p_id=>wwv_flow_api.id(29034095074135069)
,p_shortcut_name=>'DELETE_CONFIRM_MSG'
,p_shortcut_type=>'TEXT_ESCAPE_JS'
,p_shortcut=>'Would you like to perform this delete action?'
);
end;
/
prompt --application/shared_components/security/authentications/application_express_authentication
begin
wwv_flow_api.create_authentication(
 p_id=>wwv_flow_api.id(28910012309135218)
,p_name=>'Application Express Authentication'
,p_scheme_type=>'NATIVE_APEX_ACCOUNTS'
,p_invalid_session_type=>'LOGIN'
,p_use_secure_cookie_yn=>'N'
,p_ras_mode=>0
);
end;
/
prompt --application/user_interfaces
begin
wwv_flow_api.create_user_interface(
 p_id=>wwv_flow_api.id(29030956062135106)
,p_ui_type_name=>'DESKTOP'
,p_display_name=>'Desktop'
,p_display_seq=>10
,p_use_auto_detect=>false
,p_is_default=>true
,p_theme_id=>42
,p_home_url=>'f?p=&APP_ID.:1:&SESSION.'
,p_login_url=>'f?p=&APP_ID.:LOGIN_DESKTOP:&SESSION.'
,p_theme_style_by_user_pref=>false
,p_global_page_id=>0
,p_navigation_list_id=>wwv_flow_api.id(28910865468135213)
,p_navigation_list_position=>'SIDE'
,p_navigation_list_template_id=>wwv_flow_api.id(28998212159135136)
,p_nav_list_template_options=>'#DEFAULT#'
,p_css_file_urls=>'#APP_IMAGES#app-icon.css?version=#APP_VERSION#'
,p_nav_bar_type=>'LIST'
,p_nav_bar_list_id=>wwv_flow_api.id(29030637967135107)
,p_nav_bar_list_template_id=>wwv_flow_api.id(28999682801135131)
,p_nav_bar_template_options=>'#DEFAULT#'
);
end;
/
prompt --application/user_interfaces/combined_files
begin
null;
end;
/
prompt --application/pages/page_00000
begin
wwv_flow_api.create_page(
 p_id=>0
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Global Page - Desktop'
,p_step_title=>'Global Page - Desktop'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'D'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210503145956'
);
end;
/
prompt --application/pages/page_00001
begin
wwv_flow_api.create_page(
 p_id=>1
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Home'
,p_alias=>'HOME'
,p_step_title=>'Coding Conventions Checker'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'NDMR'
,p_last_upd_yyyymmddhh24miss=>'20210530113807'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29042021766135027)
,p_plug_name=>'Coding Conventions Checker'
,p_icon_css_classes=>'app-icon'
,p_region_template_options=>'#DEFAULT#'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_api.id(28952270918135162)
,p_plug_display_sequence=>10
,p_plug_display_point=>'REGION_POSITION_01'
,p_plug_query_num_rows=>15
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(39255522007502717)
,p_plug_name=>'History Bugs'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT *',
'FROM (',
'  SELECT a.deproid, a.detypid, c.dedescr,a.decredt',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
')',
'PIVOT (',
'  count(*) as count',
'  FOR detypid IN (1,2,3)',
' )*/',
' ',
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid, b.decredt',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 1',
'group by b.derunid, c.dedescr, b.decredt, b.detypid',
'order by b.derunid;',
''))
,p_plug_source_type=>'NATIVE_JET_CHART'
,p_plug_query_num_rows=>15
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_jet_chart(
 p_id=>wwv_flow_api.id(39775250655497719)
,p_region_id=>wwv_flow_api.id(39255522007502717)
,p_chart_type=>'lineWithArea'
,p_animation_on_display=>'none'
,p_animation_on_data_change=>'none'
,p_orientation=>'vertical'
,p_data_cursor=>'auto'
,p_data_cursor_behavior=>'auto'
,p_hide_and_show_behavior=>'none'
,p_hover_behavior=>'none'
,p_stack=>'off'
,p_connect_nulls=>'Y'
,p_sorting=>'label-asc'
,p_fill_multi_series_gaps=>true
,p_zoom_and_scroll=>'off'
,p_tooltip_rendered=>'Y'
,p_show_series_name=>true
,p_show_group_name=>true
,p_show_value=>true
,p_show_label=>true
,p_legend_rendered=>'on'
,p_legend_position=>'auto'
);
wwv_flow_api.create_jet_chart_series(
 p_id=>wwv_flow_api.id(39775340786497720)
,p_chart_id=>wwv_flow_api.id(39775250655497719)
,p_seq=>10
,p_name=>'Graph'
,p_data_source_type=>'SQL'
,p_data_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 1',
'group by b.derunid, c.dedescr, b.detypid',
'order by b.derunid;'))
,p_series_name_column_name=>'TYPE'
,p_items_value_column_name=>'DECOUNT'
,p_items_label_column_name=>'DERUNID'
,p_line_style=>'solid'
,p_line_type=>'auto'
,p_marker_rendered=>'auto'
,p_marker_shape=>'auto'
,p_assigned_to_y2=>'off'
,p_items_label_rendered=>false
);
wwv_flow_api.create_jet_chart_axis(
 p_id=>wwv_flow_api.id(39775453170497721)
,p_chart_id=>wwv_flow_api.id(39775250655497719)
,p_axis=>'x'
,p_is_rendered=>'on'
,p_title=>'Run ID'
,p_format_scaling=>'auto'
,p_scaling=>'linear'
,p_baseline_scaling=>'zero'
,p_major_tick_rendered=>'on'
,p_minor_tick_rendered=>'off'
,p_tick_label_rendered=>'on'
,p_tick_label_rotation=>'auto'
,p_tick_label_position=>'outside'
);
wwv_flow_api.create_jet_chart_axis(
 p_id=>wwv_flow_api.id(39775523251497722)
,p_chart_id=>wwv_flow_api.id(39775250655497719)
,p_axis=>'y'
,p_is_rendered=>'on'
,p_title=>'Total'
,p_format_scaling=>'auto'
,p_scaling=>'linear'
,p_baseline_scaling=>'zero'
,p_position=>'auto'
,p_major_tick_rendered=>'on'
,p_minor_tick_rendered=>'off'
,p_tick_label_rendered=>'on'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(39776332218497730)
,p_plug_name=>'History Codesmells'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>60
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT *',
'FROM (',
'  SELECT a.deproid, a.detypid, c.dedescr,a.decredt',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
')',
'PIVOT (',
'  count(*) as count',
'  FOR detypid IN (1,2,3)',
' )*/',
' ',
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 2',
'group by b.derunid, c.dedescr, b.detypid',
'order by b.derunid;',
''))
,p_plug_source_type=>'NATIVE_JET_CHART'
,p_plug_query_num_rows=>15
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_jet_chart(
 p_id=>wwv_flow_api.id(39776437768497731)
,p_region_id=>wwv_flow_api.id(39776332218497730)
,p_chart_type=>'lineWithArea'
,p_animation_on_display=>'none'
,p_animation_on_data_change=>'none'
,p_orientation=>'vertical'
,p_data_cursor=>'auto'
,p_data_cursor_behavior=>'auto'
,p_hide_and_show_behavior=>'none'
,p_hover_behavior=>'none'
,p_stack=>'off'
,p_connect_nulls=>'Y'
,p_sorting=>'label-asc'
,p_fill_multi_series_gaps=>true
,p_zoom_and_scroll=>'off'
,p_tooltip_rendered=>'Y'
,p_show_series_name=>true
,p_show_group_name=>true
,p_show_value=>true
,p_show_label=>true
,p_legend_rendered=>'on'
,p_legend_position=>'auto'
);
wwv_flow_api.create_jet_chart_series(
 p_id=>wwv_flow_api.id(39776516123497732)
,p_chart_id=>wwv_flow_api.id(39776437768497731)
,p_seq=>10
,p_name=>'Graph'
,p_data_source_type=>'SQL'
,p_data_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 2',
'group by b.derunid, c.dedescr, b.detypid',
'order by b.derunid;',
''))
,p_series_name_column_name=>'TYPE'
,p_items_value_column_name=>'DECOUNT'
,p_items_label_column_name=>'DERUNID'
,p_line_style=>'solid'
,p_line_type=>'auto'
,p_marker_rendered=>'auto'
,p_marker_shape=>'auto'
,p_assigned_to_y2=>'off'
,p_items_label_rendered=>false
);
wwv_flow_api.create_jet_chart_axis(
 p_id=>wwv_flow_api.id(39776685593497733)
,p_chart_id=>wwv_flow_api.id(39776437768497731)
,p_axis=>'x'
,p_is_rendered=>'on'
,p_title=>'Run ID'
,p_format_scaling=>'auto'
,p_scaling=>'linear'
,p_baseline_scaling=>'zero'
,p_major_tick_rendered=>'on'
,p_minor_tick_rendered=>'off'
,p_tick_label_rendered=>'on'
,p_tick_label_rotation=>'auto'
,p_tick_label_position=>'outside'
);
wwv_flow_api.create_jet_chart_axis(
 p_id=>wwv_flow_api.id(39776797571497734)
,p_chart_id=>wwv_flow_api.id(39776437768497731)
,p_axis=>'y'
,p_is_rendered=>'on'
,p_title=>'Total'
,p_format_scaling=>'auto'
,p_scaling=>'linear'
,p_baseline_scaling=>'zero'
,p_position=>'auto'
,p_major_tick_rendered=>'on'
,p_minor_tick_rendered=>'off'
,p_tick_label_rendered=>'on'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(39776864266497735)
,p_plug_name=>'History Vulnerabilities'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>70
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT *',
'FROM (',
'  SELECT a.deproid, a.detypid, c.dedescr,a.decredt',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
')',
'PIVOT (',
'  count(*) as count',
'  FOR detypid IN (1,2,3)',
' )*/',
' ',
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 3',
'group by b.derunid, c.dedescr, b.detypid',
'order by b.derunid;',
''))
,p_plug_source_type=>'NATIVE_JET_CHART'
,p_plug_query_num_rows=>15
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_jet_chart(
 p_id=>wwv_flow_api.id(39776965629497736)
,p_region_id=>wwv_flow_api.id(39776864266497735)
,p_chart_type=>'lineWithArea'
,p_animation_on_display=>'none'
,p_animation_on_data_change=>'none'
,p_orientation=>'vertical'
,p_data_cursor=>'auto'
,p_data_cursor_behavior=>'auto'
,p_hide_and_show_behavior=>'none'
,p_hover_behavior=>'none'
,p_stack=>'off'
,p_connect_nulls=>'Y'
,p_sorting=>'label-asc'
,p_fill_multi_series_gaps=>true
,p_zoom_and_scroll=>'off'
,p_tooltip_rendered=>'Y'
,p_show_series_name=>true
,p_show_group_name=>true
,p_show_value=>true
,p_show_label=>true
,p_legend_rendered=>'on'
,p_legend_position=>'auto'
);
wwv_flow_api.create_jet_chart_series(
 p_id=>wwv_flow_api.id(39777044213497737)
,p_chart_id=>wwv_flow_api.id(39776965629497736)
,p_seq=>10
,p_name=>'Graph'
,p_data_source_type=>'SQL'
,p_data_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 3',
'group by b.derunid, c.dedescr, b.detypid;',
'',
''))
,p_series_name_column_name=>'TYPE'
,p_items_value_column_name=>'DECOUNT'
,p_items_label_column_name=>'DERUNID'
,p_line_style=>'solid'
,p_line_type=>'auto'
,p_marker_rendered=>'auto'
,p_marker_shape=>'auto'
,p_assigned_to_y2=>'off'
,p_items_label_rendered=>false
);
wwv_flow_api.create_jet_chart_axis(
 p_id=>wwv_flow_api.id(39777111992497738)
,p_chart_id=>wwv_flow_api.id(39776965629497736)
,p_axis=>'x'
,p_is_rendered=>'on'
,p_title=>'Run ID'
,p_format_scaling=>'auto'
,p_scaling=>'linear'
,p_baseline_scaling=>'zero'
,p_major_tick_rendered=>'on'
,p_minor_tick_rendered=>'off'
,p_tick_label_rendered=>'on'
,p_tick_label_rotation=>'auto'
,p_tick_label_position=>'outside'
);
wwv_flow_api.create_jet_chart_axis(
 p_id=>wwv_flow_api.id(39777246462497739)
,p_chart_id=>wwv_flow_api.id(39776965629497736)
,p_axis=>'y'
,p_is_rendered=>'on'
,p_title=>'Total'
,p_format_scaling=>'auto'
,p_scaling=>'linear'
,p_baseline_scaling=>'zero'
,p_position=>'auto'
,p_major_tick_rendered=>'on'
,p_minor_tick_rendered=>'off'
,p_tick_label_rendered=>'on'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(39778110173497748)
,p_name=>'Total Bugs'
,p_template=>wwv_flow_api.id(28956997224135161)
,p_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--removeHeader:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#:t-BadgeList--large:t-BadgeList--circular:t-BadgeList--fixed'
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT count(b.dedescr) as decount_bugs, b.dedescr',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
' and a.detypid = 1',
' group by b.dedescr',
''))
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(28975823088135145)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(40544537977820608)
,p_query_column_id=>1
,p_column_alias=>'DECOUNT_BUGS'
,p_column_display_sequence=>2
,p_column_heading=>'Total Bugs'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:300:&SESSION.::&DEBUG.:RP:P300_DEDESCR:#DEDESCR#'
,p_column_linktext=>'#DECOUNT_BUGS#'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(40544479384820607)
,p_query_column_id=>2
,p_column_alias=>'DEDESCR'
,p_column_display_sequence=>1
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(40544666627820609)
,p_name=>'Total Codesmells'
,p_template=>wwv_flow_api.id(28956997224135161)
,p_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--removeHeader:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#:t-BadgeList--large:t-BadgeList--circular:t-BadgeList--fixed'
,p_new_grid_row=>false
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT count(b.dedescr) as decount_csm, b.dedescr',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
' and a.detypid = 2',
' group by b.dedescr',
''))
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(28975823088135145)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(40544944527820612)
,p_query_column_id=>1
,p_column_alias=>'DECOUNT_CSM'
,p_column_display_sequence=>2
,p_column_heading=>'Total Codesmells'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:300:&SESSION.::&DEBUG.:RP:P300_DEDESCR:#DEDESCR#'
,p_column_linktext=>'#DECOUNT_CSM#'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(40544781209820610)
,p_query_column_id=>2
,p_column_alias=>'DEDESCR'
,p_column_display_sequence=>1
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(40545051880820613)
,p_name=>'Total Vulnerabilities'
,p_template=>wwv_flow_api.id(28956997224135161)
,p_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--removeHeader:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#:t-BadgeList--large:t-BadgeList--circular:t-BadgeList--fixed'
,p_new_grid_row=>false
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT count(b.dedescr) as decount_vul, b.dedescr',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
' and a.detypid = 3',
' group by b.dedescr',
''))
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(28975823088135145)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(40545348090820616)
,p_query_column_id=>1
,p_column_alias=>'DECOUNT_VUL'
,p_column_display_sequence=>2
,p_column_heading=>'Total Vulnerabilities'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:300:&SESSION.::&DEBUG.:RP:P300_DEDESCR:#DEDESCR#'
,p_column_linktext=>'#DECOUNT_VUL#'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(40545173650820614)
,p_query_column_id=>2
,p_column_alias=>'DEDESCR'
,p_column_display_sequence=>1
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15168277290724623)
,p_name=>'P1_URL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(29042021766135027)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(15167993230724620)
,p_name=>'Page Load - Open choose project'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
,p_display_when_type=>'PLSQL_EXPRESSION'
,p_display_when_cond=>':APP_PROJECT is null'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(15168123377724622)
,p_event_id=>wwv_flow_api.id(15167993230724620)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P1_URL := apex_util.prepare_url(''f?p=100:220:&SESSION.:&DEBUG.'');'
,p_attribute_03=>'P1_URL'
,p_attribute_04=>'N'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(15168021256724621)
,p_event_id=>wwv_flow_api.id(15167993230724620)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'window.location.href = $v(''P1_URL'');'
);
end;
/
prompt --application/pages/page_00200
begin
wwv_flow_api.create_page(
 p_id=>200
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Manage Rules'
,p_step_title=>'Manage Rules'
,p_step_sub_title=>'Manage Rules'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210716161446'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(34023606834587092)
,p_plug_name=>'Manage Rules'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select A.DERULID,',
'       A.DERNAME,',
'       A.DEGRAID,',
'       B.DEDESCR,',
'       A.DEDESCR as DEGRADDE,',
'       a.detypid,',
'       r.dedescr as dertype_d,',
'       A.DECREDT,',
'       A.DECREUS,',
'       A.DEWYZDT,',
'       A.DEWYZUS',
'  from DRRULES A, DRGRADA B, drrtype r',
'where A.DEGRAID = B.DEGRAID',
'and    a.detypid = r.detypid',
'order by A.DERNAME'))
,p_plug_source_type=>'NATIVE_IG'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34024842817587089)
,p_name=>'APEX$LINK'
,p_source_type=>'NONE'
,p_item_type=>'NATIVE_LINK'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>10
,p_value_alignment=>'CENTER'
,p_stretch=>'N'
,p_link_target=>'f?p=&APP_ID.:201:&SESSION.::&DEBUG.:RP,201:P201_DERULID:&DERULID.'
,p_link_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
,p_use_as_row_header=>false
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34026047261587088)
,p_name=>'DERULID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERULID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Derulid'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>30
,p_value_alignment=>'CENTER'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34026695843587087)
,p_name=>'DERNAME'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERNAME'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Dername'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>40
,p_value_alignment=>'CENTER'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_max_length=>250
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34027217883587087)
,p_name=>'DEGRAID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEGRAID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>50
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34027880698587087)
,p_name=>'DEDESCR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEDESCR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Dedescr'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>70
,p_value_alignment=>'CENTER'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_max_length=>250
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34028491778587087)
,p_name=>'DECREDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREDT'
,p_data_type=>'DATE'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DATE_PICKER'
,p_heading=>'Decredt'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>80
,p_value_alignment=>'CENTER'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34029030188587087)
,p_name=>'DECREUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Decreus'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>90
,p_value_alignment=>'CENTER'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>20
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34029667960587086)
,p_name=>'DEWYZDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEWYZDT'
,p_data_type=>'DATE'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DATE_PICKER'
,p_heading=>'Dewyzdt'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>100
,p_value_alignment=>'CENTER'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34030273299587086)
,p_name=>'DEWYZUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEWYZUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Dewyzus'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>110
,p_value_alignment=>'CENTER'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>20
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(39426676930667304)
,p_name=>'DEGRADDE'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEGRADDE'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Degradde'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>60
,p_value_alignment=>'CENTER'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_max_length=>250
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42541203466693425)
,p_name=>'DETYPID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DETYPID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Detypid'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>120
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42541368990693426)
,p_name=>'DERTYPE_D'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERTYPE_D'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Dertype D'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>130
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(34024193744587090)
,p_internal_uid=>34024193744587090
,p_is_editable=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(34024582325587090)
,p_interactive_grid_id=>wwv_flow_api.id(34024193744587090)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(34024603477587090)
,p_report_id=>wwv_flow_api.id(34024582325587090)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34025249087587089)
,p_view_id=>wwv_flow_api.id(34024603477587090)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(34024842817587089)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>55
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34026466895587087)
,p_view_id=>wwv_flow_api.id(34024603477587090)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(34026047261587088)
,p_is_visible=>false
,p_is_frozen=>false
,p_width=>78
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34027058742587087)
,p_view_id=>wwv_flow_api.id(34024603477587090)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(34026695843587087)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34027640094587087)
,p_view_id=>wwv_flow_api.id(34024603477587090)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(34027217883587087)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34028293517587087)
,p_view_id=>wwv_flow_api.id(34024603477587090)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(34027880698587087)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>123
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34028817043587087)
,p_view_id=>wwv_flow_api.id(34024603477587090)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(34028491778587087)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34029464476587086)
,p_view_id=>wwv_flow_api.id(34024603477587090)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(34029030188587087)
,p_is_visible=>false
,p_is_frozen=>false
,p_width=>102
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34030069444587086)
,p_view_id=>wwv_flow_api.id(34024603477587090)
,p_display_seq=>8
,p_column_id=>wwv_flow_api.id(34029667960587086)
,p_is_visible=>false
,p_is_frozen=>false
,p_width=>175
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34030606460587086)
,p_view_id=>wwv_flow_api.id(34024603477587090)
,p_display_seq=>9
,p_column_id=>wwv_flow_api.id(34030273299587086)
,p_is_visible=>false
,p_is_frozen=>false
,p_width=>100
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(39438488111601619)
,p_view_id=>wwv_flow_api.id(34024603477587090)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(39426676930667304)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>471
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(44941904422326071)
,p_view_id=>wwv_flow_api.id(34024603477587090)
,p_display_seq=>10
,p_column_id=>wwv_flow_api.id(42541203466693425)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(44942439500326069)
,p_view_id=>wwv_flow_api.id(34024603477587090)
,p_display_seq=>11
,p_column_id=>wwv_flow_api.id(42541368990693426)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34030975428587086)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(34023606834587092)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_EDIT'
,p_button_redirect_url=>'f?p=&APP_ID.:201:&SESSION.::&DEBUG.:201'
,p_grid_new_grid=>false
);
end;
/
prompt --application/pages/page_00201
begin
wwv_flow_api.create_page(
 p_id=>201
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Manage Rule'
,p_step_title=>'Manage Rule'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210810134023'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29405515958820921)
,p_plug_name=>'Statements per rule'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select A.DERLCID,',
'       A.DERULID,',
'       A.DEAPEID,',
'       C.DEAPEXV,',
'       A.DERLINK,',
'       A.DEAPBIV,',
'       A.DESTATE     ',
'from DRRULCE A , DRRULES B, DRAPEXV C',
'where A.DERULID = :P201_DERULID',
'and A.DEAPEID = C.DEAPEID',
'and B.DERULID = A.DERULID;'))
,p_plug_source_type=>'NATIVE_IG'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29407261182820938)
,p_name=>'DERLCID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERLCID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Derlcid'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>40
,p_value_alignment=>'CENTER'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29407323792820939)
,p_name=>'DERULID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERULID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Derulid'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>50
,p_value_alignment=>'CENTER'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29407444533820940)
,p_name=>'DEAPEID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPEID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>60
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29407582566820941)
,p_name=>'DERLINK'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERLINK'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Derlink'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>70
,p_value_alignment=>'LEFT'
,p_attribute_02=>'LOV'
,p_lov_type=>'PLSQL_FUNCTION_BODY'
,p_lov_source=>'return pck_link_types.get_lov();'
,p_lov_display_extra=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'LOV'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29407648377820942)
,p_name=>'DEAPBIV'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPBIV'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Deapbiv'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>80
,p_value_alignment=>'CENTER'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>30
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29407768322820943)
,p_name=>'DESTATE'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DESTATE'
,p_data_type=>'CLOB'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Destate'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>90
,p_value_alignment=>'CENTER'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29408169038820947)
,p_name=>'APEX$LINK'
,p_source_type=>'NONE'
,p_item_type=>'NATIVE_LINK'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>30
,p_value_alignment=>'CENTER'
,p_stretch=>'N'
,p_link_target=>'f?p=&APP_ID.:203:&SESSION.::&DEBUG.:RP:P203_DERLCID:&DERLCID.'
,p_link_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
,p_use_as_row_header=>false
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(39427011308667308)
,p_name=>'DEAPEXV'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPEXV'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Deapexv'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>100
,p_value_alignment=>'CENTER'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>15
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(29405610166820922)
,p_internal_uid=>29405610166820922
,p_is_editable=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(34099355509003185)
,p_interactive_grid_id=>wwv_flow_api.id(29405610166820922)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(34099439549003185)
,p_report_id=>wwv_flow_api.id(34099355509003185)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34106265983970957)
,p_view_id=>wwv_flow_api.id(34099439549003185)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(29407261182820938)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34106746197970956)
,p_view_id=>wwv_flow_api.id(34099439549003185)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(29407323792820939)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34107210858970955)
,p_view_id=>wwv_flow_api.id(34099439549003185)
,p_display_seq=>14
,p_column_id=>wwv_flow_api.id(29407444533820940)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34107763244970954)
,p_view_id=>wwv_flow_api.id(34099439549003185)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(29407582566820941)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>178
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34108254591970953)
,p_view_id=>wwv_flow_api.id(34099439549003185)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(29407648377820942)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>202
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34108742154970952)
,p_view_id=>wwv_flow_api.id(34099439549003185)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(29407768322820943)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34160552050793861)
,p_view_id=>wwv_flow_api.id(34099439549003185)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(29408169038820947)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>40
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(39446116258456755)
,p_view_id=>wwv_flow_api.id(34099439549003185)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(39427011308667308)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>231
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(34031963879587085)
,p_plug_name=>'Manage Rule'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(42538846552693401)
,p_plug_name=>'Region.UsedInProfiles'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select      r.deprofl_id, p.deprfnm',
'from        drprrul r, drprofl p',
'where       r.deprofl_id = p.deprofl_id',
'and         r.derulid = :P201_DERULID;'))
,p_plug_source_type=>'NATIVE_IG'
,p_ajax_items_to_submit=>'P201_DERULID'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42539070252693403)
,p_name=>'DEPROFL_ID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPROFL_ID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Deprofl Id'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>10
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42539149086693404)
,p_name=>'DEPRFNM'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPRFNM'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Deprfnm'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>20
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(42538994058693402)
,p_internal_uid=>42538994058693402
,p_is_editable=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(42544492353674052)
,p_interactive_grid_id=>wwv_flow_api.id(42538994058693402)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(42544528406674052)
,p_report_id=>wwv_flow_api.id(42544492353674052)
,p_view_type=>'GRID'
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42545001660674051)
,p_view_id=>wwv_flow_api.id(42544528406674052)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(42539070252693403)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42545563390674050)
,p_view_id=>wwv_flow_api.id(42544528406674052)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(42539149086693404)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34032401739587085)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(34031963879587085)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Save'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>'P201_DERULID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34032622778587085)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(34031963879587085)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:200:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34032359666587085)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(34031963879587085)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P201_DERULID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34032515752587085)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(34031963879587085)
,p_button_name=>'DELETE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--danger'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_execute_validations=>'N'
,p_button_condition=>'P201_DERULID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(29407840573820944)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(29405515958820921)
,p_button_name=>'CREATE_ST'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Create St'
,p_button_position=>'REGION_TEMPLATE_EDIT'
,p_button_redirect_url=>'f?p=&APP_ID.:203:&SESSION.::&DEBUG.:RP,203:P203_DERULID:&P201_DERULID.'
,p_button_condition=>'P201_DERULID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(34036966726587082)
,p_branch_name=>'Go To Page 200'
,p_branch_action=>'f?p=&APP_ID.:200:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'REQUEST_IN_CONDITION'
,p_branch_condition=>'DELETE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29404833304820914)
,p_name=>'P201_DECREUS'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(34031963879587085)
,p_use_cache_before_default=>'NO'
,p_source=>'DECREUS'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29404987872820915)
,p_name=>'P201_DEWYZDT'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(34031963879587085)
,p_use_cache_before_default=>'NO'
,p_source=>'DEWYZDT'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29405031218820916)
,p_name=>'P201_DEWYZUS'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(34031963879587085)
,p_use_cache_before_default=>'NO'
,p_source=>'DEWYZUS'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29405127966820917)
,p_name=>'P201_DERNAME'
,p_is_required=>true
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(34031963879587085)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Dername'
,p_source=>'DERNAME'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>250
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29405295312820918)
,p_name=>'P201_DEDESCR'
,p_is_required=>true
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(34031963879587085)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Dedescr'
,p_source=>'DEDESCR'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_cMaxlength=>250
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29405398360820919)
,p_name=>'P201_DEGRAID'
,p_is_required=>true
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(34031963879587085)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Degraid'
,p_source=>'DEGRAID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'return pck_gradatie.get_lov();'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29405427389820920)
,p_name=>'P201_DECREDT'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(34031963879587085)
,p_use_cache_before_default=>'NO'
,p_source=>'DECREDT'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(34034207929587083)
,p_name=>'P201_ROWID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(34031963879587085)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Rowid'
,p_source=>'ROWID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(29008583468135127)
,p_item_template_options=>'#DEFAULT#'
,p_protection_level=>'S'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(34034698486587083)
,p_name=>'P201_DERULID'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(34031963879587085)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Derulid'
,p_source=>'DERULID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(42541174671693424)
,p_name=>'P201_DETYPID'
,p_is_required=>true
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(34031963879587085)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Detypid'
,p_source=>'DETYPID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'return pck_types.get_lov();'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(39426884456667306)
,p_name=>'Hide details'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
,p_display_when_type=>'ITEM_IS_NULL'
,p_display_when_cond=>'P201_DERULID'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(39426948340667307)
,p_event_id=>wwv_flow_api.id(39426884456667306)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(29405515958820921)
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(34035786939587082)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from DRRULES'
,p_attribute_02=>'DRRULES'
,p_attribute_03=>'P201_DERULID'
,p_attribute_04=>'DERULID'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29404617555820912)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Vul creatie user en datum + ID'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P201_decreus := :APP_USER;',
':P201_decredt := sysdate;',
':P201_derulid := sqrules.nextval;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(34032359666587085)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29404755378820913)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Vul wijzigings user en datum'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P201_dewyzus := :APP_USER;',
':P201_dewyzdt := sysdate;',
''))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(34032401739587085)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(34036161691587082)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of DRRULES'
,p_attribute_02=>'DRRULES'
,p_attribute_03=>'P201_DERULID'
,p_attribute_04=>'DERULID'
,p_attribute_11=>'I:U:D'
,p_attribute_12=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Action Processed.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(34036584990587082)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(34032515752587085)
);
end;
/
prompt --application/pages/page_00202
begin
wwv_flow_api.create_page(
 p_id=>202
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Statements per rule'
,p_step_title=>'Statements per rule'
,p_step_sub_title=>'Statements per rule'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'NDMR'
,p_last_upd_yyyymmddhh24miss=>'20210511113743'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(34070858865228366)
,p_plug_name=>'Statements per rule'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select DERLCID,',
'       DERULID,',
'       DEAPEID,',
'       DERLINK,',
'       DEAPBIV,',
'       DESTATE,',
'       DETYPID,',
'       DECREDT,',
'       DECREUS,',
'       DEWYZDT,',
'       DEWYZUS',
'  from DRRULCE'))
,p_plug_source_type=>'NATIVE_IG'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34072084856228366)
,p_name=>'APEX$ROW_SELECTOR'
,p_item_type=>'NATIVE_ROW_SELECTOR'
,p_display_sequence=>10
,p_attribute_01=>'Y'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_is_primary_key=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34072559234228365)
,p_name=>'APEX$ROW_ACTION'
,p_item_type=>'NATIVE_ROW_ACTION'
,p_label=>'Actions'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>20
,p_value_alignment=>'CENTER'
,p_is_primary_key=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34073156204228365)
,p_name=>'DERLCID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERLCID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>30
,p_attribute_01=>'Y'
,p_enable_filter=>false
,p_is_primary_key=>true
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34073752716228363)
,p_name=>'DERULID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERULID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Derulid'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>40
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34074364822228363)
,p_name=>'DEAPEID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPEID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Deapeid'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>50
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34074937132228363)
,p_name=>'DERLINK'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERLINK'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Derlink'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>60
,p_value_alignment=>'LEFT'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>64
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34075554087228363)
,p_name=>'DEAPBIV'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPBIV'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Deapbiv'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>70
,p_value_alignment=>'LEFT'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>30
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34076177451228362)
,p_name=>'DESTATE'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DESTATE'
,p_data_type=>'CLOB'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Destate'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>80
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_max_length=>4000
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34076766424228362)
,p_name=>'DETYPID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DETYPID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Detypid'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>90
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34077317688228362)
,p_name=>'DECREDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREDT'
,p_data_type=>'DATE'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DATE_PICKER'
,p_heading=>'Decredt'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>100
,p_value_alignment=>'CENTER'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34077942060228362)
,p_name=>'DECREUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Decreus'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>110
,p_value_alignment=>'LEFT'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>20
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34078523314228362)
,p_name=>'DEWYZDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEWYZDT'
,p_data_type=>'DATE'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DATE_PICKER'
,p_heading=>'Dewyzdt'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>120
,p_value_alignment=>'CENTER'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34079139100228362)
,p_name=>'DEWYZUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEWYZUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Dewyzus'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>130
,p_value_alignment=>'LEFT'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>20
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(34071357164228366)
,p_internal_uid=>34071357164228366
,p_is_editable=>true
,p_edit_operations=>'i:u:d'
,p_lost_update_check_type=>'VALUES'
,p_add_row_if_empty=>true
,p_submit_checked_rows=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(34071723475228366)
,p_interactive_grid_id=>wwv_flow_api.id(34071357164228366)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(34071839500228366)
,p_report_id=>wwv_flow_api.id(34071723475228366)
,p_view_type=>'GRID'
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34072924758228365)
,p_view_id=>wwv_flow_api.id(34071839500228366)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(34072559234228365)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34073573447228363)
,p_view_id=>wwv_flow_api.id(34071839500228366)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(34073156204228365)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34074152749228363)
,p_view_id=>wwv_flow_api.id(34071839500228366)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(34073752716228363)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34074770332228363)
,p_view_id=>wwv_flow_api.id(34071839500228366)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(34074364822228363)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34075386901228363)
,p_view_id=>wwv_flow_api.id(34071839500228366)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(34074937132228363)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34075919830228363)
,p_view_id=>wwv_flow_api.id(34071839500228366)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(34075554087228363)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34076555895228362)
,p_view_id=>wwv_flow_api.id(34071839500228366)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(34076177451228362)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34077124204228362)
,p_view_id=>wwv_flow_api.id(34071839500228366)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(34076766424228362)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34077705916228362)
,p_view_id=>wwv_flow_api.id(34071839500228366)
,p_display_seq=>8
,p_column_id=>wwv_flow_api.id(34077317688228362)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34078375667228362)
,p_view_id=>wwv_flow_api.id(34071839500228366)
,p_display_seq=>9
,p_column_id=>wwv_flow_api.id(34077942060228362)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34078997113228362)
,p_view_id=>wwv_flow_api.id(34071839500228366)
,p_display_seq=>10
,p_column_id=>wwv_flow_api.id(34078523314228362)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34079518195228362)
,p_view_id=>wwv_flow_api.id(34071839500228366)
,p_display_seq=>11
,p_column_id=>wwv_flow_api.id(34079139100228362)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(34079715575228361)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(34070858865228366)
,p_process_type=>'NATIVE_IG_DML'
,p_process_name=>' - Save Interactive Grid Data'
,p_attribute_01=>'REGION_SOURCE'
,p_attribute_05=>'Y'
,p_attribute_06=>'Y'
,p_attribute_08=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
prompt --application/pages/page_00203
begin
wwv_flow_api.create_page(
 p_id=>203
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Form statement per rule'
,p_step_title=>'Form statement per rule'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210806155243'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(34085831334083520)
,p_plug_name=>'Form statement per rule'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(42539287553693405)
,p_plug_name=>'Region.ParametersPerStatement'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select        *',
'from        drparst',
'where        derlcid = :P203_DERLCID'))
,p_plug_source_type=>'NATIVE_IG'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'PLSQL_EXPRESSION'
,p_plug_display_when_condition=>':P203_DERLCID is not null'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42539440715693407)
,p_name=>'DEPARST_ID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPARST_ID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Deparst Id'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>30
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42539553369693408)
,p_name=>'DERLCID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERLCID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Derlcid'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>40
,p_value_alignment=>'LEFT'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42539622762693409)
,p_name=>'DEPROFL_ID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPROFL_ID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Deprofl Id'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>50
,p_value_alignment=>'LEFT'
,p_attribute_02=>'LOV'
,p_lov_type=>'PLSQL_FUNCTION_BODY'
,p_lov_source=>'return pck_profl.get_lov;'
,p_lov_display_extra=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'LOV'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42539735825693410)
,p_name=>'DEPARNM'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPARNM'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Deparnm'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>60
,p_value_alignment=>'LEFT'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42539835621693411)
,p_name=>'DEPARVL'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPARVL'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Deparvl'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>70
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>200
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42539962134693412)
,p_name=>'DECREDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREDT'
,p_data_type=>'DATE'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Decredt'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>90
,p_value_alignment=>'LEFT'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42540063105693413)
,p_name=>'DECREUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Decreus'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>100
,p_value_alignment=>'LEFT'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42540126103693414)
,p_name=>'DEWYZDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEWYZDT'
,p_data_type=>'DATE'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Dewyzdt'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>110
,p_value_alignment=>'LEFT'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42540235120693415)
,p_name=>'DEWYZUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEWYZUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Dewyzus'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>120
,p_value_alignment=>'LEFT'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42540482127693417)
,p_name=>'APEX$ROW_ACTION'
,p_item_type=>'NATIVE_ROW_ACTION'
,p_display_sequence=>20
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42540559904693418)
,p_name=>'APEX$ROW_SELECTOR'
,p_item_type=>'NATIVE_ROW_SELECTOR'
,p_display_sequence=>10
,p_attribute_01=>'Y'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42540778441693420)
,p_name=>'DEDESCR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEDESCR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Dedescr'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>80
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(42539313624693406)
,p_internal_uid=>42539313624693406
,p_is_editable=>true
,p_edit_operations=>'u'
,p_lost_update_check_type=>'VALUES'
,p_submit_checked_rows=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(42546763943509943)
,p_interactive_grid_id=>wwv_flow_api.id(42539313624693406)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(42546870451509943)
,p_report_id=>wwv_flow_api.id(42546763943509943)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42547385420509943)
,p_view_id=>wwv_flow_api.id(42546870451509943)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(42539440715693407)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42547879313509942)
,p_view_id=>wwv_flow_api.id(42546870451509943)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(42539553369693408)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42548302449509941)
,p_view_id=>wwv_flow_api.id(42546870451509943)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(42539622762693409)
,p_is_visible=>false
,p_is_frozen=>false
,p_width=>156.65625
,p_break_order=>5
,p_break_is_enabled=>true
,p_break_sort_direction=>'ASC'
,p_break_sort_nulls=>'LAST'
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42548841659509939)
,p_view_id=>wwv_flow_api.id(42546870451509943)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(42539735825693410)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>201.65625
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42549372213509938)
,p_view_id=>wwv_flow_api.id(42546870451509943)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(42539835621693411)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>537.171875
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42549803413509937)
,p_view_id=>wwv_flow_api.id(42546870451509943)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(42539962134693412)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42550358204509936)
,p_view_id=>wwv_flow_api.id(42546870451509943)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(42540063105693413)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42550827386509935)
,p_view_id=>wwv_flow_api.id(42546870451509943)
,p_display_seq=>8
,p_column_id=>wwv_flow_api.id(42540126103693414)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42551327023509934)
,p_view_id=>wwv_flow_api.id(42546870451509943)
,p_display_seq=>9
,p_column_id=>wwv_flow_api.id(42540235120693415)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42955030924890343)
,p_view_id=>wwv_flow_api.id(42546870451509943)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(42540482127693417)
,p_is_visible=>true
,p_is_frozen=>true
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(43019164420228056)
,p_view_id=>wwv_flow_api.id(42546870451509943)
,p_display_seq=>10
,p_column_id=>wwv_flow_api.id(42540778441693420)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34086300793083520)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(34085831334083520)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Apply Changes'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>'P203_DERLCID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_grid=>false
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34086544373083520)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(34085831334083520)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:201:&SESSION.::&DEBUG.:::'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34086247051083520)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(34085831334083520)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P203_DERLCID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_grid_new_grid=>false
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34086467756083520)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(34085831334083520)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#:t-Button--danger'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition=>'P203_DERLCID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(34088186822083519)
,p_branch_name=>'Go To Page 203'
,p_branch_action=>'f?p=&APP_ID.:203:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'PLSQL_EXPRESSION'
,p_branch_condition=>':REQUEST in (''CREATE'',''SAVE'')'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(42540836184693421)
,p_branch_name=>'Go To Page 201'
,p_branch_action=>'f?p=&APP_ID.:201:&SESSION.::&DEBUG.:::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>11
,p_branch_condition_type=>'PLSQL_EXPRESSION'
,p_branch_condition=>':REQUEST in (''DELETE'')'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(34088583739083518)
,p_name=>'P203_DERLCID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(34085831334083520)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Derlcid'
,p_source=>'DERLCID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(29008583468135127)
,p_item_template_options=>'#DEFAULT#'
,p_protection_level=>'S'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(34088910359083518)
,p_name=>'P203_DEAPEID'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(34085831334083520)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Deapeid'
,p_source=>'DEAPEID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'return pck_apex_version.get_lov();'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_protection_level=>'S'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(34089352387083517)
,p_name=>'P203_DERULID'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(34085831334083520)
,p_use_cache_before_default=>'NO'
,p_source=>'DERULID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(34089742850083517)
,p_name=>'P203_DERLINK'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(34085831334083520)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Derlink'
,p_source=>'DERLINK'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'return pck_link_types.get_lov();'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(34090165831083517)
,p_name=>'P203_DEAPBIV'
,p_is_required=>true
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(34085831334083520)
,p_use_cache_before_default=>'NO'
,p_item_default=>':APPLICATION'
,p_prompt=>'Deapbiv'
,p_source=>'DEAPBIV'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>30
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(34090520576083517)
,p_name=>'P203_DESTATE'
,p_is_required=>true
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(34085831334083520)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Destate'
,p_source=>'DESTATE'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>60
,p_cMaxlength=>4000
,p_cHeight=>10
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(34091367931083516)
,p_name=>'P203_DECREDT'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(34085831334083520)
,p_use_cache_before_default=>'NO'
,p_source=>'DECREDT'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(34091766789083516)
,p_name=>'P203_DECREUS'
,p_item_sequence=>100
,p_item_plug_id=>wwv_flow_api.id(34085831334083520)
,p_use_cache_before_default=>'NO'
,p_source=>'DECREUS'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(34092132661083516)
,p_name=>'P203_DEWYZDT'
,p_item_sequence=>110
,p_item_plug_id=>wwv_flow_api.id(34085831334083520)
,p_use_cache_before_default=>'NO'
,p_source=>'DEWYZDT'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(34092506541083516)
,p_name=>'P203_DEWYZUS'
,p_item_sequence=>120
,p_item_plug_id=>wwv_flow_api.id(34085831334083520)
,p_use_cache_before_default=>'NO'
,p_source=>'DEWYZUS'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(39430342639667341)
,p_name=>'P203_HELP'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(34085831334083520)
,p_prompt=>'Help'
,p_source=>'P203_HELP'
,p_source_type=>'ITEM'
,p_display_as=>'NATIVE_DISPLAY_ONLY'
,p_begin_on_new_line=>'N'
,p_colspan=>3
,p_field_template=>wwv_flow_api.id(29008583468135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'VALUE'
,p_attribute_04=>'Y'
);
wwv_flow_api.create_page_computation(
 p_id=>wwv_flow_api.id(34265372634219025)
,p_computation_sequence=>10
,p_computation_item=>'P203_DEAPBIV'
,p_computation_type=>'FUNCTION_BODY'
,p_computation=>'return '':''||:P203_DEAPBIV;'
,p_compute_when=>':P203_DEAPBIV not like '':%'''
,p_compute_when_type=>'PLSQL_EXPRESSION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(34265146708219023)
,p_validation_name=>'Check Valid SQL'
,p_validation_sequence=>10
,p_validation=>'return pck_checks.is_valid_query( :P203_DESTATE, :P203_DERLINK );'
,p_validation_type=>'FUNC_BODY_RETURNING_ERR_TEXT'
,p_associated_item=>wwv_flow_api.id(34090520576083517)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(34265226271219024)
,p_validation_name=>'Check Bind Variable'
,p_validation_sequence=>20
,p_validation=>'instr(:P203_DESTATE, :P203_DEAPBIV) > 0'
,p_validation_type=>'PLSQL_EXPRESSION'
,p_error_message=>'#LABEL# must be used in the validation query.'
,p_associated_item=>wwv_flow_api.id(34090165831083517)
,p_error_display_location=>'INLINE_WITH_FIELD_AND_NOTIFICATION'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(39429337679667331)
,p_name=>'On Change - Link'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P203_DERLINK'
,p_condition_element=>'P203_DERLINK'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(39430900726667347)
,p_event_id=>wwv_flow_api.id(39429337679667331)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P203_HELP := pck_help.help_message(:P203_DERLINK);'
,p_attribute_02=>'P203_DERLINK'
,p_attribute_03=>'P203_HELP'
,p_attribute_04=>'N'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(34093992542083515)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from DRRULCE'
,p_attribute_02=>'DRRULCE'
,p_attribute_03=>'P203_DERLCID'
,p_attribute_04=>'DERLCID'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29407912409820945)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Vul creatie user en datum'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P203_decreus := :APP_USER;',
':P203_decredt := sysdate;',
':P203_derlcid := sqrulce.nextval;',
''))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(34086247051083520)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29408034972820946)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Vul wijzigings user en datum'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P203_dewyzus := :APP_USER;',
':P203_dewyzdt := sysdate;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(34086300793083520)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(34095156904083515)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of DRRULCE'
,p_attribute_02=>'DRRULCE'
,p_attribute_03=>'P203_DERLCID'
,p_attribute_04=>'DERLCID'
,p_attribute_11=>'I:U:D'
,p_attribute_12=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Action Processed.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(42540382956693416)
,p_process_sequence=>60
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Extract parameters for statement'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'pck_rules.extract_parameters(',
'    i_rlcid => :P203_DERLCID',
');'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(34095532917083515)
,p_process_sequence=>70
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(34086467756083520)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(42540601329693419)
,p_process_sequence=>80
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(42539287553693405)
,p_process_type=>'NATIVE_IG_DML'
,p_process_name=>'Region.ParametersPerStatement - Save Interactive Grid Data'
,p_attribute_01=>'REGION_SOURCE'
,p_attribute_05=>'Y'
,p_attribute_06=>'Y'
,p_attribute_08=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
prompt --application/pages/page_00204
begin
wwv_flow_api.create_page(
 p_id=>204
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Profiles'
,p_step_title=>'Profiles'
,p_step_sub_title=>'Profielen'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210622175037'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(42489884887122786)
,p_plug_name=>'Profiles'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select DEPROFL_ID,',
'       DEPRFNM,',
'       DECREDT,',
'       DECREUS,',
'       DEWYZDT,',
'       DEWYZUS',
'  from DRPROFL'))
,p_plug_source_type=>'NATIVE_IG'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42491077053122783)
,p_name=>'APEX$LINK'
,p_source_type=>'NONE'
,p_item_type=>'NATIVE_LINK'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>10
,p_value_alignment=>'CENTER'
,p_link_target=>'f?p=&APP_ID.:205:&APP_SESSION.::&DEBUG.:RP,205:P205_DEPROFL_ID:&DEPROFL_ID.'
,p_link_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
,p_is_primary_key=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42491696638122781)
,p_name=>'DEPROFL_ID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPROFL_ID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>20
,p_attribute_01=>'Y'
,p_enable_filter=>false
,p_is_primary_key=>true
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42492288125122781)
,p_name=>'DEPRFNM'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPRFNM'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Deprfnm'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>30
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_max_length=>250
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42492829172122780)
,p_name=>'DECREDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREDT'
,p_data_type=>'DATE'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DATE_PICKER'
,p_heading=>'Decredt'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>40
,p_value_alignment=>'CENTER'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42493484588122780)
,p_name=>'DECREUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Decreus'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>50
,p_value_alignment=>'LEFT'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>20
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42494036669122780)
,p_name=>'DEWYZDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEWYZDT'
,p_data_type=>'DATE'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DATE_PICKER'
,p_heading=>'Dewyzdt'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>60
,p_value_alignment=>'CENTER'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42494618808122780)
,p_name=>'DEWYZUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEWYZUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Dewyzus'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>70
,p_value_alignment=>'LEFT'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>20
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(42490334871122785)
,p_internal_uid=>42490334871122785
,p_is_editable=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(42490744221122784)
,p_interactive_grid_id=>wwv_flow_api.id(42490334871122785)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(42490897002122784)
,p_report_id=>wwv_flow_api.id(42490744221122784)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42491464267122782)
,p_view_id=>wwv_flow_api.id(42490897002122784)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(42491077053122783)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>40
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42492078657122781)
,p_view_id=>wwv_flow_api.id(42490897002122784)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(42491696638122781)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42492642010122780)
,p_view_id=>wwv_flow_api.id(42490897002122784)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(42492288125122781)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42493266645122780)
,p_view_id=>wwv_flow_api.id(42490897002122784)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(42492829172122780)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42493869206122780)
,p_view_id=>wwv_flow_api.id(42490897002122784)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(42493484588122780)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42494443914122780)
,p_view_id=>wwv_flow_api.id(42490897002122784)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(42494036669122780)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42495050265122780)
,p_view_id=>wwv_flow_api.id(42490897002122784)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(42494618808122780)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(42495302824122779)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(42489884887122786)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_EDIT'
,p_button_redirect_url=>'f?p=&APP_ID.:205:&SESSION.::&DEBUG.:205'
,p_grid_new_grid=>false
);
end;
/
prompt --application/pages/page_00205
begin
wwv_flow_api.create_page(
 p_id=>205
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Detail profile'
,p_step_title=>'Detail profile'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210716165833'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15168588666724626)
,p_plug_name=>'Region.ProjectsPerProfile'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select       *',
'from         vwprfpr',
'where        deprofl_id = :P205_DEPROFL_ID'))
,p_plug_source_type=>'NATIVE_IG'
,p_ajax_items_to_submit=>'P205_DEPROFL_ID'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P205_DEPROFL_ID'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(15168760257724628)
,p_name=>'DEPROID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPROID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>10
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(15168826023724629)
,p_name=>'DEDESCR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEDESCR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Dedescr'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>20
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(15168968891724630)
,p_name=>'DEPROFL_ID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPROFL_ID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>30
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(15168674145724627)
,p_internal_uid=>15168674145724627
,p_is_editable=>false
,p_lazy_loading=>true
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(42510703272774732)
,p_interactive_grid_id=>wwv_flow_api.id(15168674145724627)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(42510884159774732)
,p_report_id=>wwv_flow_api.id(42510703272774732)
,p_view_type=>'GRID'
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42511336108774728)
,p_view_id=>wwv_flow_api.id(42510884159774732)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(15168760257724628)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42511828531774725)
,p_view_id=>wwv_flow_api.id(42510884159774732)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(15168826023724629)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42512346696774723)
,p_view_id=>wwv_flow_api.id(42510884159774732)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(15168968891724630)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(15169154405724632)
,p_plug_name=>'Region.RulesPerProfile'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'with sel as (',
'    select C.DERULID,',
'           A.DERNAME,',
'           A.DEDESCR,',
'           min(v.deapenr) as  deapenr,',
'            d.dedescr as dertype_d',
'    from DRRULES A, DRPRRUL C , DRRULCE B, DRRTYPE D, drapexv v',
'    where A.DERULID = B.DERULID',
'    and   C.DERULID = A.DERULID',
'    and D.DETYPID = a.DETYPID',
'    and b.deapeid = v.deapeid',
'    and C.DEPROFL_ID = :P205_DEPROFL_ID',
'    group by c.derulid, a.dername, a.dedescr, d.dedescr',
')',
'select      s.*, v.deapexv as deapexv_min',
'from        sel s, drapexv v',
'where       s.deapenr = v.deapenr;'))
,p_plug_source_type=>'NATIVE_IG'
,p_ajax_items_to_submit=>'P205_DEPROFL_ID'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P205_DEPROFL_ID'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(15169494285724635)
,p_name=>'DERULID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERULID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Derulid'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>30
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(15169585722724636)
,p_name=>'DERNAME'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERNAME'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Dername'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>40
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_max_length=>250
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(15169653291724637)
,p_name=>'DEDESCR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEDESCR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Dedescr'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>50
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_max_length=>250
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(15170650391724647)
,p_name=>'APEX$ROW_ACTION'
,p_item_type=>'NATIVE_ROW_ACTION'
,p_display_sequence=>20
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(15170787027724648)
,p_name=>'APEX$ROW_SELECTOR'
,p_item_type=>'NATIVE_ROW_SELECTOR'
,p_display_sequence=>10
,p_attribute_01=>'Y'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42540979353693422)
,p_name=>'DEAPENR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPENR'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Deapenr'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>60
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42541025029693423)
,p_name=>'DEAPEXV_MIN'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPEXV_MIN'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Deapexv Min'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>70
,p_value_alignment=>'LEFT'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>15
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42541407147693427)
,p_name=>'DERTYPE_D'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERTYPE_D'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Dertype D'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>80
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(15169249660724633)
,p_internal_uid=>15169249660724633
,p_is_editable=>true
,p_edit_operations=>'d'
,p_lost_update_check_type=>'VALUES'
,p_submit_checked_rows=>false
,p_lazy_loading=>true
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(42516769067105962)
,p_interactive_grid_id=>wwv_flow_api.id(15169249660724633)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(42516818821105962)
,p_report_id=>wwv_flow_api.id(42516769067105962)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(4944856769139)
,p_view_id=>wwv_flow_api.id(42516818821105962)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(42541407147693427)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42517345475105960)
,p_view_id=>wwv_flow_api.id(42516818821105962)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(15169494285724635)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42517887141105959)
,p_view_id=>wwv_flow_api.id(42516818821105962)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(15169585722724636)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42518316281105958)
,p_view_id=>wwv_flow_api.id(42516818821105962)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(15169653291724637)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(42531950578862312)
,p_view_id=>wwv_flow_api.id(42516818821105962)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(15170650391724647)
,p_is_visible=>true
,p_is_frozen=>true
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(44936432333435174)
,p_view_id=>wwv_flow_api.id(42516818821105962)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(42540979353693422)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(44936923671435170)
,p_view_id=>wwv_flow_api.id(42516818821105962)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(42541025029693423)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(42496348707122778)
,p_plug_name=>'Region.DetailProfile'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(42496807187122778)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(42496348707122778)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Save'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>'P205_DEPROFL_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_grid=>false
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(42497011922122778)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(42496348707122778)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:204:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(42496797048122778)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(42496348707122778)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P205_DEPROFL_ID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_grid_new_grid=>false
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(42496958589122778)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(42496348707122778)
,p_button_name=>'DELETE'
,p_button_action=>'REDIRECT_URL'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_redirect_url=>'javascript:apex.confirm(htmldb_delete_message,''DELETE'');'
,p_button_execute_validations=>'N'
,p_button_condition=>'P205_DEPROFL_ID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_grid_new_grid=>false
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(15170336113724644)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(15169154405724632)
,p_button_name=>'ADD_RULE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29008925477135126)
,p_button_image_alt=>'Button.Add Rule'
,p_button_position=>'REGION_TEMPLATE_NEXT'
,p_button_redirect_url=>'f?p=&APP_ID.:217:&SESSION.::&DEBUG.:RP,217:P217_DEPROFL_ID:&P205_DEPROFL_ID.'
,p_icon_css_classes=>'fa-plus'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(42503043465122769)
,p_branch_action=>'f?p=&APP_ID.:204:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'REQUEST_IN_CONDITION'
,p_branch_condition=>'SAVE,DELETE,CREATE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(42498644093122777)
,p_name=>'P205_DEPROFL_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(42496348707122778)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Deprofl Id'
,p_source=>'DEPROFL_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(29008583468135127)
,p_item_template_options=>'#DEFAULT#'
,p_protection_level=>'S'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(42499076905122772)
,p_name=>'P205_DEPRFNM'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(42496348707122778)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Deprfnm'
,p_source=>'DEPRFNM'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXTAREA'
,p_cSize=>60
,p_cMaxlength=>250
,p_cHeight=>4
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(42499480241122771)
,p_name=>'P205_DECREDT'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(42496348707122778)
,p_use_cache_before_default=>'NO'
,p_source=>'DECREDT'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(42499806761122771)
,p_name=>'P205_DECREUS'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(42496348707122778)
,p_use_cache_before_default=>'NO'
,p_source=>'DECREUS'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(42500200574122771)
,p_name=>'P205_DEWYZDT'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(42496348707122778)
,p_use_cache_before_default=>'NO'
,p_source=>'DEWYZDT'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(42500672184122771)
,p_name=>'P205_DEWYZUS'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(42496348707122778)
,p_use_cache_before_default=>'NO'
,p_source=>'DEWYZUS'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(42501417948122770)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from DRPROFL'
,p_attribute_02=>'DRPROFL'
,p_attribute_03=>'P205_DEPROFL_ID'
,p_attribute_04=>'DEPROFL_ID'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(42501841052122770)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Get PK'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin ',
'    if :P205_DEPROFL_ID is null then',
'        select "#OWNER#"."SQPROFL".nextval',
'          into :P205_DEPROFL_ID',
'          from sys.dual;',
'    end if;',
'end;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(42496797048122778)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(42502277710122770)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of DRPROFL'
,p_attribute_02=>'DRPROFL'
,p_attribute_03=>'P205_DEPROFL_ID'
,p_attribute_04=>'DEPROFL_ID'
,p_attribute_11=>'I:U:D'
,p_attribute_12=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Action Processed.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(42502612993122770)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(42496958589122778)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15170886946724649)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(15169154405724632)
,p_process_type=>'NATIVE_IG_DML'
,p_process_name=>'Region.RulesPerProfile - Save Interactive Grid Data'
,p_attribute_01=>'PLSQL_CODE'
,p_attribute_04=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    case :APEX$ROW_STATUS',
'    when ''D'' then',
'        delete drprrul',
'         where derulid = :DERULID',
'         and   deprofl_id = :DEPROFL_ID;',
'    end case;',
'end;'))
,p_attribute_05=>'Y'
,p_attribute_06=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
prompt --application/pages/page_00210
begin
wwv_flow_api.create_page(
 p_id=>210
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Manage Apex Versions'
,p_step_title=>'Manage Apex Versions'
,p_step_sub_title=>'Manage Apex Versions'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'NDMR'
,p_last_upd_yyyymmddhh24miss=>'20210521105723'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29074299873826705)
,p_plug_name=>'Manage Apex Versions'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select DEAPEID,',
'       DEAPEXV,',
'       DEAPENR,',
'       DECREDT,',
'       DECREUS,',
'       DEWYZDT,',
'       DEWYZUS',
'  from DRAPEXV'))
,p_plug_source_type=>'NATIVE_IG'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29075491030826701)
,p_name=>'APEX$LINK'
,p_source_type=>'NONE'
,p_item_type=>'NATIVE_LINK'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>10
,p_value_alignment=>'CENTER'
,p_stretch=>'N'
,p_link_target=>'f?p=&APP_ID.:211:&SESSION.::&DEBUG.:RP,211:P211_DEAPEID:&DEAPEID.'
,p_link_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
,p_use_as_row_header=>false
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29076655505826699)
,p_name=>'DEAPEID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPEID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Deapeid'
,p_heading_alignment=>'RIGHT'
,p_display_sequence=>30
,p_value_alignment=>'RIGHT'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29077222802826698)
,p_name=>'DEAPEXV'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPEXV'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Deapexv'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>50
,p_value_alignment=>'CENTER'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>15
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29077847583826698)
,p_name=>'DECREDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREDT'
,p_data_type=>'DATE'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DATE_PICKER'
,p_heading=>'Decredt'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>60
,p_value_alignment=>'CENTER'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29078403661826697)
,p_name=>'DECREUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Decreus'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>70
,p_value_alignment=>'CENTER'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>20
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29079030861826697)
,p_name=>'DEWYZDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEWYZDT'
,p_data_type=>'DATE'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DATE_PICKER'
,p_heading=>'Dewyzdt'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>80
,p_value_alignment=>'CENTER'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29079662777826697)
,p_name=>'DEWYZUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEWYZUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Dewyzus'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>90
,p_value_alignment=>'CENTER'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>20
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29404392189820909)
,p_name=>'DEAPENR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPENR'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Deapenr'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>40
,p_value_alignment=>'CENTER'
,p_attribute_03=>'center'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(29074734467826703)
,p_internal_uid=>29074734467826703
,p_is_editable=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(29075110768826702)
,p_interactive_grid_id=>wwv_flow_api.id(29074734467826703)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(29075200336826702)
,p_report_id=>wwv_flow_api.id(29075110768826702)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29075869945826700)
,p_view_id=>wwv_flow_api.id(29075200336826702)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(29075491030826701)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>52
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29077093709826698)
,p_view_id=>wwv_flow_api.id(29075200336826702)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(29076655505826699)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29077661851826698)
,p_view_id=>wwv_flow_api.id(29075200336826702)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(29077222802826698)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>259
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29078285572826698)
,p_view_id=>wwv_flow_api.id(29075200336826702)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(29077847583826698)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29078865090826697)
,p_view_id=>wwv_flow_api.id(29075200336826702)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(29078403661826697)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29079490851826697)
,p_view_id=>wwv_flow_api.id(29075200336826702)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(29079030861826697)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29080091672826697)
,p_view_id=>wwv_flow_api.id(29075200336826702)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(29079662777826697)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(33992839791929157)
,p_view_id=>wwv_flow_api.id(29075200336826702)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(29404392189820909)
,p_is_visible=>true
,p_is_frozen=>false
,p_sort_order=>1
,p_sort_direction=>'DESC'
,p_sort_nulls=>'FIRST'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(29080348373826697)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(29074299873826705)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_EDIT'
,p_button_redirect_url=>'f?p=&APP_ID.:211:&SESSION.::&DEBUG.:211'
,p_grid_new_grid=>false
);
end;
/
prompt --application/pages/page_00211
begin
wwv_flow_api.create_page(
 p_id=>211
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Manage Apex Version'
,p_step_title=>'Manage Apex Version'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210531103713'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29081388327826696)
,p_plug_name=>'Manage Apex Version'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(29081836824826696)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(29081388327826696)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Save'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>'P211_DEAPEID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(29082037944826696)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(29081388327826696)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:210:&SESSION.::&DEBUG.:::'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(29081792767826696)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(29081388327826696)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P211_DEAPEID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(29081977540826696)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(29081388327826696)
,p_button_name=>'DELETE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--danger'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_condition=>'P211_DEAPEID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(29089835558826693)
,p_branch_action=>'f?p=&APP_ID.:210:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'REQUEST_IN_CONDITION'
,p_branch_condition=>'SAVE,DELETE,CREATE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29065669273897308)
,p_name=>'P211_DECREDT'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(29081388327826696)
,p_use_cache_before_default=>'NO'
,p_source=>'DECREDT'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29065761787897309)
,p_name=>'P211_DECREUS'
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(29081388327826696)
,p_use_cache_before_default=>'NO'
,p_source=>'DECREUS'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29065853011897310)
,p_name=>'P211_DEWYZDT'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(29081388327826696)
,p_use_cache_before_default=>'NO'
,p_source=>'DEWYZDT'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29065973436897311)
,p_name=>'P211_DEWYZUS'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(29081388327826696)
,p_use_cache_before_default=>'NO'
,p_source=>'DEWYZUS'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29084020572826695)
,p_name=>'P211_DEAPEID'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(29081388327826696)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Deapeid'
,p_source=>'DEAPEID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29084401693826695)
,p_name=>'P211_DEAPEXV'
,p_is_required=>true
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(29081388327826696)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Deapexv'
,p_source=>'DEAPEXV'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>15
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29404460001820910)
,p_name=>'P211_DEAPENR'
,p_is_required=>true
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(29081388327826696)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Deapenr'
,p_source=>'DEAPENR'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_NUMBER_FIELD'
,p_cSize=>32
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_03=>'left'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29088699042826694)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from DRAPEXV'
,p_attribute_02=>'DRAPEXV'
,p_attribute_03=>'P211_DEAPEID'
,p_attribute_04=>'DEAPEID'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29065455081897306)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Vul creatie user en datum + ID'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P211_decreus := :APP_USER;',
':P211_decredt := sysdate;',
':P211_deapeid := sqapexv.nextval;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(29081792767826696)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29065544506897307)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Vul wijziging user en datum'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P211_dewyzus := :APP_USER;',
':P211_dewyzdt := sysdate;',
''))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(29081836824826696)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29089035435826694)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of DRAPEXV'
,p_attribute_02=>'DRAPEXV'
,p_attribute_03=>'P211_DEAPEID'
,p_attribute_04=>'DEAPEID'
,p_attribute_11=>'I:U:D'
,p_attribute_12=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Action Processed.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29089460823826694)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(29081977540826696)
);
end;
/
prompt --application/pages/page_00214
begin
wwv_flow_api.create_page(
 p_id=>214
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Manage Projects'
,p_step_title=>'Manage Projects'
,p_step_sub_title=>'Manage Projects'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210526112716'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29109531489788995)
,p_plug_name=>'Manage Projects'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select P.DEPROID,',
'       A.DEAPEXV,',
'       P.DEDESCR,',
'       P.DECREDT,',
'       P.DECREUS,',
'       P.DEWYZDT,',
'       P.DEWYZUS,',
'       P.DEAPEID',
'--''<a style="color:#000000;" onclick="javascript:l_printen = 1;" href="''|| apex_util.prepare_url(p_url => ''f?p=&APP_ID.:216:&APP_SESSION.::::P216_DEPROID,P216_DEDESCR:''|| deproid || '','' || dedescr) ||''"><i class="fa fa-print fa-2x" aria-hidden="true"'
||'></i></a>'' as label3',
'  from DRPROJE P, DRAPEXV A',
'where P.DEAPEID = A.DEAPEID'))
,p_plug_source_type=>'NATIVE_IG'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29065115613897303)
,p_name=>'DEAPEXV'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPEXV'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Deapexv'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>100
,p_value_alignment=>'CENTER'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>15
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29110789714788994)
,p_name=>'APEX$LINK'
,p_source_type=>'NONE'
,p_item_type=>'NATIVE_LINK'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>10
,p_value_alignment=>'CENTER'
,p_stretch=>'N'
,p_link_target=>'f?p=&APP_ID.:215:&SESSION.::&DEBUG.:RP:P215_DEPROID,P215_DEAPEID:&DEPROID.,&DEAPEID.'
,p_link_text=>'<img src="#IMAGE_PREFIX#app_ui/img/icons/apex-edit-pencil.png" class="apex-edit-pencil" alt="Edit">'
,p_use_as_row_header=>false
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29111927509788994)
,p_name=>'DEPROID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPROID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Deproid'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>30
,p_value_alignment=>'CENTER'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29113109886788993)
,p_name=>'DEDESCR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEDESCR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Dedescr'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>50
,p_value_alignment=>'CENTER'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>30
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29113793293788993)
,p_name=>'DECREDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREDT'
,p_data_type=>'DATE'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DATE_PICKER'
,p_heading=>'Decredt'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>60
,p_value_alignment=>'CENTER'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29114346391788993)
,p_name=>'DECREUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Decreus'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>70
,p_value_alignment=>'CENTER'
,p_attribute_05=>'BOTH'
,p_is_required=>true
,p_max_length=>20
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29114908475788991)
,p_name=>'DEWYZDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEWYZDT'
,p_data_type=>'DATE'
,p_is_query_only=>false
,p_item_type=>'NATIVE_DATE_PICKER'
,p_heading=>'Dewyzdt'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>80
,p_value_alignment=>'CENTER'
,p_attribute_04=>'button'
,p_attribute_05=>'N'
,p_attribute_07=>'NONE'
,p_is_required=>false
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(29115553156788990)
,p_name=>'DEWYZUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEWYZUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXT_FIELD'
,p_heading=>'Dewyzus'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>90
,p_value_alignment=>'CENTER'
,p_attribute_05=>'BOTH'
,p_is_required=>false
,p_max_length=>20
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(39427613999667314)
,p_name=>'DEAPEID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPEID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>110
,p_attribute_01=>'Y'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(29110097337788995)
,p_internal_uid=>29110097337788995
,p_is_editable=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(29110443605788994)
,p_interactive_grid_id=>wwv_flow_api.id(29110097337788995)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(29110583021788994)
,p_report_id=>wwv_flow_api.id(29110443605788994)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29111127496788994)
,p_view_id=>wwv_flow_api.id(29110583021788994)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(29110789714788994)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>40
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29112363266788993)
,p_view_id=>wwv_flow_api.id(29110583021788994)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(29111927509788994)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29113507187788993)
,p_view_id=>wwv_flow_api.id(29110583021788994)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(29113109886788993)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29114116320788993)
,p_view_id=>wwv_flow_api.id(29110583021788994)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(29113793293788993)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29114720194788991)
,p_view_id=>wwv_flow_api.id(29110583021788994)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(29114346391788993)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29115372656788990)
,p_view_id=>wwv_flow_api.id(29110583021788994)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(29114908475788991)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29115915961788990)
,p_view_id=>wwv_flow_api.id(29110583021788994)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(29115553156788990)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(29127924546752347)
,p_view_id=>wwv_flow_api.id(29110583021788994)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(29065115613897303)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(39453880171058176)
,p_view_id=>wwv_flow_api.id(29110583021788994)
,p_display_seq=>11
,p_column_id=>wwv_flow_api.id(39427613999667314)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(29116252307788990)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(29109531489788995)
,p_button_name=>'CREATE'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_EDIT'
,p_button_redirect_url=>'f?p=&APP_ID.:215:&SESSION.::&DEBUG.:215'
,p_grid_new_grid=>false
);
end;
/
prompt --application/pages/page_00215
begin
wwv_flow_api.create_page(
 p_id=>215
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Manage Project'
,p_step_title=>'Manage Project'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210810145727'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29117224403788990)
,p_plug_name=>'Manage Project'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(39256930946502731)
,p_plug_name=>'Applications per project'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody:margin-top-sm'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select A.APPLICATION_ID,',
'       A.APPLICATION_NAME,',
'       B.DEAPPID,',
'       B.DEPROID',
'from APEX_APPLICATIONS A, DRPRAPP B',
'where A.APPLICATION_ID = B.DEAPPID',
'and B.DEPROID = :P215_DEPROID;'))
,p_plug_source_type=>'NATIVE_IG'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_plug_display_when_condition=>'P215_DEPROID'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(39258208834502744)
,p_name=>'P215_APPLICATION_ID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'APPLICATION_ID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_NUMBER_FIELD'
,p_heading=>'Application Id'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>30
,p_value_alignment=>'CENTER'
,p_stretch=>'N'
,p_attribute_03=>'right'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(39258385761502745)
,p_name=>'P215_APPLICATION_NAME'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'APPLICATION_NAME'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Application Name'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>40
,p_value_alignment=>'CENTER'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_max_length=>255
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(39426366820667301)
,p_name=>'APEX$ROW_ACTION'
,p_item_type=>'NATIVE_ROW_ACTION'
,p_display_sequence=>20
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(39426444220667302)
,p_name=>'APEX$ROW_SELECTOR'
,p_item_type=>'NATIVE_ROW_SELECTOR'
,p_display_sequence=>10
,p_attribute_01=>'Y'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(39428531797667323)
,p_name=>'DEAPPID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPPID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>50
,p_attribute_01=>'Y'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(39428670191667324)
,p_name=>'DEPROID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPROID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>60
,p_attribute_01=>'Y'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>true
,p_include_in_export=>false
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(39257142100502733)
,p_internal_uid=>39257142100502733
,p_is_editable=>true
,p_edit_operations=>'d'
,p_delete_authorization_scheme=>wwv_flow_api.id(29033676742135075)
,p_lost_update_check_type=>'VALUES'
,p_submit_checked_rows=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(39363339651113800)
,p_interactive_grid_id=>wwv_flow_api.id(39257142100502733)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(39363482502113800)
,p_report_id=>wwv_flow_api.id(39363339651113800)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(39363913385113799)
,p_view_id=>wwv_flow_api.id(39363482502113800)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(39258208834502744)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>448.4444580078125
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(39364471983113798)
,p_view_id=>wwv_flow_api.id(39363482502113800)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(39258385761502745)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>911.8889694213867
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(39432344787664088)
,p_view_id=>wwv_flow_api.id(39363482502113800)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(39426366820667301)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(39490171779212864)
,p_view_id=>wwv_flow_api.id(39363482502113800)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(39428531797667323)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(39490658699212862)
,p_view_id=>wwv_flow_api.id(39363482502113800)
,p_display_seq=>4
,p_column_id=>wwv_flow_api.id(39428670191667324)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(42541564219693428)
,p_plug_name=>'Region.RulesPerProject'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select        dername, dedescr, dertype_d',
'from          vwrlprp',
'where         deproid = :P215_DEPROID',
'and           deprofl_id = :P215_DEPROFL_ID'))
,p_plug_source_type=>'NATIVE_IG'
,p_ajax_items_to_submit=>'P215_DEPROID,P215_DEPROFL_ID'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'PLSQL_EXPRESSION'
,p_plug_display_when_condition=>':P215_DEPROID is not null and :P215_DEPROFL_ID is not null'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42541763401693430)
,p_name=>'DERNAME'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERNAME'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Dername'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>10
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42541842117693431)
,p_name=>'DEDESCR'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEDESCR'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Dedescr'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>20
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(42541929477693432)
,p_name=>'DERTYPE_D'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERTYPE_D'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>false
,p_item_type=>'NATIVE_TEXTAREA'
,p_heading=>'Dertype D'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>30
,p_value_alignment=>'LEFT'
,p_attribute_01=>'Y'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'BOTH'
,p_is_required=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>true
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(42541645298693429)
,p_internal_uid=>42541645298693429
,p_is_editable=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_fixed_row_height=>true
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(46669506211353177)
,p_interactive_grid_id=>wwv_flow_api.id(42541645298693429)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(46669630846353176)
,p_report_id=>wwv_flow_api.id(46669506211353177)
,p_view_type=>'GRID'
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(46670110136353171)
,p_view_id=>wwv_flow_api.id(46669630846353176)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(42541763401693430)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(46670660241353168)
,p_view_id=>wwv_flow_api.id(46669630846353176)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(42541842117693431)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(46671172856353166)
,p_view_id=>wwv_flow_api.id(46669630846353176)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(42541929477693432)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(29117787587788990)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(29117224403788990)
,p_button_name=>'SAVE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Save'
,p_button_position=>'REGION_TEMPLATE_CHANGE'
,p_button_condition=>'P215_DEPROID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'UPDATE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(29117946813788990)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(29117224403788990)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_button_redirect_url=>'f?p=&APP_ID.:214:&SESSION.::&DEBUG.:::'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(29117612058788990)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(29117224403788990)
,p_button_name=>'CREATE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Create'
,p_button_position=>'REGION_TEMPLATE_CREATE'
,p_button_condition=>'P215_DEPROID'
,p_button_condition_type=>'ITEM_IS_NULL'
,p_database_action=>'INSERT'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(29117881820788990)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(29117224403788990)
,p_button_name=>'DELETE'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#:t-Button--danger'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Delete'
,p_button_position=>'REGION_TEMPLATE_DELETE'
,p_button_condition=>'P215_DEPROID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
,p_database_action=>'DELETE'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(39257060349502732)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(39256930946502731)
,p_button_name=>'ADD_ST_1'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Add Application'
,p_button_position=>'REGION_TEMPLATE_EDIT'
,p_button_redirect_url=>'f?p=&APP_ID.:218:&SESSION.::&DEBUG.:RP,218:P215_DEPROID:&P215_DEPROID.'
,p_button_condition=>'P215_DEPROID'
,p_button_condition_type=>'ITEM_IS_NOT_NULL'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(29126419774788987)
,p_branch_name=>'Go To Page 214'
,p_branch_action=>'f?p=&APP_ID.:214:&SESSION.&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>1
,p_branch_condition_type=>'REQUEST_IN_CONDITION'
,p_branch_condition=>'DELETE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15169028379724631)
,p_name=>'P215_DEPROFL_ID'
,p_item_sequence=>50
,p_item_plug_id=>wwv_flow_api.id(29117224403788990)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Deprofl Id'
,p_source=>'DEPROFL_ID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'return pck_profl.get_lov;'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(29008583468135127)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29119906758788989)
,p_name=>'P215_DEPROID'
,p_is_required=>true
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(29117224403788990)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Deproid'
,p_source=>'DEPROID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29120325710788989)
,p_name=>'P215_DEAPEID'
,p_is_required=>true
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(29117224403788990)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Deapeid'
,p_source=>'DEAPEID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'return pck_apex_version.get_lov();'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'YES'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29120722589788989)
,p_name=>'P215_DEDESCR'
,p_is_required=>true
,p_item_sequence=>40
,p_item_plug_id=>wwv_flow_api.id(29117224403788990)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Dedescr'
,p_source=>'DEDESCR'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>32
,p_cMaxlength=>250
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29121153741788988)
,p_name=>'P215_DECREDT'
,p_item_sequence=>60
,p_item_plug_id=>wwv_flow_api.id(29117224403788990)
,p_use_cache_before_default=>'NO'
,p_source=>'DECREDT'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29121528476788988)
,p_name=>'P215_DECREUS'
,p_item_sequence=>70
,p_item_plug_id=>wwv_flow_api.id(29117224403788990)
,p_use_cache_before_default=>'NO'
,p_source=>'DECREUS'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29121900790788988)
,p_name=>'P215_DEWYZDT'
,p_item_sequence=>80
,p_item_plug_id=>wwv_flow_api.id(29117224403788990)
,p_use_cache_before_default=>'NO'
,p_source=>'DEWYZDT'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29122379309788988)
,p_name=>'P215_DEWYZUS'
,p_item_sequence=>90
,p_item_plug_id=>wwv_flow_api.id(29117224403788990)
,p_use_cache_before_default=>'NO'
,p_source=>'DEWYZUS'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(39427285098667310)
,p_name=>'Hide details'
,p_event_sequence=>10
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
,p_display_when_type=>'ITEM_IS_NULL'
,p_display_when_cond=>'P215_DEPROID'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(39427415756667312)
,p_event_id=>wwv_flow_api.id(39427285098667310)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'Y'
,p_action=>'NATIVE_HIDE'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(39256930946502731)
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29125260617788987)
,p_process_sequence=>10
,p_process_point=>'AFTER_HEADER'
,p_process_type=>'NATIVE_FORM_FETCH'
,p_process_name=>'Fetch Row from DRPROJE'
,p_attribute_02=>'DRPROJE'
,p_attribute_03=>'P215_DEPROID'
,p_attribute_04=>'DEPROID'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29065214356897304)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Vul creatie user en datum'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P215_decreus := :APP_USER;',
':P215_decredt := sysdate;',
':P215_deproid := sqproje.nextval;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(29117612058788990)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29065394812897305)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Vul wijzigings user en datum'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P215_dewyzus := :APP_USER;',
':P215_dewyzdt := sysdate;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(29117787587788990)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29125611403788987)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_FORM_PROCESS'
,p_process_name=>'Process Row of DRPROJE'
,p_attribute_02=>'DRPROJE'
,p_attribute_03=>'P215_DEPROID'
,p_attribute_04=>'DEPROID'
,p_attribute_11=>'I:U:D'
,p_attribute_12=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Action Processed.'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29126020999788987)
,p_process_sequence=>40
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'reset page'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_when_button_id=>wwv_flow_api.id(29117881820788990)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(39426558939667303)
,p_process_sequence=>50
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(39256930946502731)
,p_process_type=>'NATIVE_IG_DML'
,p_process_name=>'Applications per project - Save Interactive Grid Data'
,p_attribute_01=>'TABLE'
,p_attribute_03=>'DRPRAPP'
,p_attribute_05=>'Y'
,p_attribute_06=>'Y'
,p_attribute_08=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
prompt --application/pages/page_00216
begin
wwv_flow_api.create_page(
 p_id=>216
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Manage rules for projects'
,p_page_mode=>'MODAL'
,p_step_title=>'Manage rules for projects'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_javascript_file_urls=>'#WORKSPACE_IMAGES#JS/93502.js'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'NDMR'
,p_last_upd_yyyymmddhh24miss=>'20210506212725'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29316067573630901)
,p_plug_name=>'Manage rules for projects'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(29316521513630906)
,p_name=>'Labels'
,p_region_name=>'LABELS'
,p_parent_plug_id=>wwv_flow_api.id(29316067573630901)
,p_template=>wwv_flow_api.id(28956997224135161)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Form--slimPadding:t-Form--stretchInputs'
,p_component_template_options=>'#DEFAULT#:t-Report--altRowsDefault:t-Report--rowHighlight'
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select    a.deproid,c.derulid, c.dername, c.dedescr as description, b.destate, b.derlcid,',
'          APEX_ITEM.CHECKBOX2( ',
'              p_idx => 2,',
'              p_value => decode(d.derulid, null, ''N'', ''J''), ',
'              p_attributes => case when decode(d.derulid, null, ''N'', ''J'') = ''J'' then ''CHECKED '' else '' '' end ||  ',
'              ''onChange="change_checkbox('''''' || c.derulid || '''''','''''' || decode(d.derulid, null, ''N'', ''J'') || '''''');"''                 ',
'          ) as DEOPLJN',
'from      drproje a, ',
'          drrulce b, ',
'          drrules c,',
'          drprrul d',
'where     a.deapeid = b.deapeid',
'and       b.derulid = c.derulid',
'and       c.derulid = d.derulid (+)',
'and       a.deproid (+) = :P216_DEPROID',
'order by  a.deproid',
';',
'',
'',
''))
,p_ajax_enabled=>'Y'
,p_ajax_items_to_submit=>'P216_DEPROID'
,p_query_row_template=>wwv_flow_api.id(28979442408135144)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29404067578820906)
,p_query_column_id=>1
,p_column_alias=>'DEPROID'
,p_column_display_sequence=>5
,p_column_heading=>'Deproid'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29317943177630920)
,p_query_column_id=>2
,p_column_alias=>'DERULID'
,p_column_display_sequence=>1
,p_column_heading=>'Derulid'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29318014362630921)
,p_query_column_id=>3
,p_column_alias=>'DERNAME'
,p_column_display_sequence=>2
,p_column_heading=>'Dername'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29318279205630923)
,p_query_column_id=>4
,p_column_alias=>'DESCRIPTION'
,p_column_display_sequence=>3
,p_column_heading=>'Description'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29404167518820907)
,p_query_column_id=>5
,p_column_alias=>'DESTATE'
,p_column_display_sequence=>6
,p_column_heading=>'Destate'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29404247410820908)
,p_query_column_id=>6
,p_column_alias=>'DERLCID'
,p_column_display_sequence=>7
,p_column_heading=>'Derlcid'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29403949682820905)
,p_query_column_id=>7
,p_column_alias=>'DEOPLJN'
,p_column_display_sequence=>4
,p_column_heading=>'Deopljn'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29316997309630910)
,p_plug_name=>'Buttons'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28936438753135170)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29316114592630902)
,p_name=>'P216_DEPROID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(29316067573630901)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Deproid'
,p_source=>'DEPROID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(29008583468135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29316461016630905)
,p_name=>'P216_DEDESCR'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(29316067573630901)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Dedescr'
,p_source=>'DEDESCR'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(29008583468135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29317593687630916)
,p_name=>'P216_MESSAGE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(29316067573630901)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(29316233344630903)
,p_name=>'On Change - ID'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P216_DEPROID'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(29316306093630904)
,p_event_id=>wwv_flow_api.id(29316233344630903)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(29316521513630906)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(29317623290630917)
,p_name=>'On change - Message'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P216_MESSAGE'
,p_condition_element=>'P216_MESSAGE'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(29317706112630918)
,p_event_id=>wwv_flow_api.id(29317623290630917)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'customAlert(''E'', document.getElementById(''P216_MESSAGE'').value, 0);',
''))
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(29403628156820902)
,p_name=>'Refresh labels'
,p_event_sequence=>30
,p_triggering_element_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_element=>'document'
,p_bind_type=>'bind'
,p_bind_event_type=>'custom'
,p_bind_event_type_custom=>'refresh_labels'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(29403717068820903)
,p_event_id=>wwv_flow_api.id(29403628156820902)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(29316521513630906)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29403821960820904)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_CLOSE_WINDOW'
,p_process_name=>'Close Dialog'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29317222165630913)
,p_process_sequence=>10
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'change_checkbox'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if apex_application.g_f02(1) = ''N'' then',
'    -- Indien aanvinken --> toevoegen aan DRPPRUL',
'    insert into DRPRRUL',
'    (deproid, derulid, decredt, decreus)',
'    values',
'    (:P216_DEPROID , apex_application.g_f01(1), sysdate, :APP_USER);',
'else ',
'    -- Indien uitvinken --> verwijderen uit DRPPRUL',
'    delete from DRPRRUL',
'    where  deproid = :P216_DEPROID',
'    and    derulid = apex_application.g_f01(1);',
'end if;',
'',
'commit;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
prompt --application/pages/page_00217
begin
wwv_flow_api.create_page(
 p_id=>217
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Add Statements'
,p_step_title=>'Add Statements'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Add Statements'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function checkallf01() {',
'  if ($("#checkall").is('':checked'')) { ',
'    $("input[name=f01]").prop("checked", true);',
'  } else { ',
'    $("input[name=f01]").prop("checked", false);',
'  }',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_help_text=>'<p>To track standards for application(s) check the application and click the <b>Add Applications</b> button.</p>'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210806155048'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1481924344399434654)
,p_plug_name=>'Add Statements'
,p_region_template_options=>'#DEFAULT#:js-showMaximizeButton'
,p_plug_template=>wwv_flow_api.id(28955876667135161)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select apex_item.checkbox(1,A.DERULID) cb,',
'       A.DERULID,',
'       A.DERNAME,',
'       A.DEDESCR,',
'       B.DERLCID,',
'       B.DESTATE,',
'       a.DETYPID,',
'       C.DEPROFL_ID,',
'       D.DEDESCR DETYPDE',
'from DRRULES A, DRRULCE B, DRPROFL C, DRRTYPE D',
'where (C.DEPROFL_ID, A.DERULID) not in (select DEPROFL_ID, DERULID from DRPRRUL)',
'and B.DERULID = A.DERULID',
'and A.DERULID = B.DERULID',
'and C.DEPROFL_ID = :P217_DEPROFL_ID',
'and D.DETYPID = a.DETYPID'))
,p_plug_source_type=>'NATIVE_IR'
,p_ajax_items_to_submit=>'P217_DEPROFL_ID'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(1481924543448434655)
,p_name=>'Add Applications'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_save_rpt_public=>'Y'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML:EMAIL'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MIKE'
,p_internal_uid=>1481924543448434655
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34355197641804111)
,p_db_column_name=>'CB'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'<input type="checkbox" onclick="checkallf01();" id="checkall" />'
,p_allow_sorting=>'N'
,p_allow_filtering=>'N'
,p_allow_highlighting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_hide=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34263912197219011)
,p_db_column_name=>'DERULID'
,p_display_order=>14
,p_column_identifier=>'G'
,p_column_label=>'Derulid'
,p_column_type=>'NUMBER'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34264074079219012)
,p_db_column_name=>'DERNAME'
,p_display_order=>24
,p_column_identifier=>'H'
,p_column_label=>'Dername'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34264142002219013)
,p_db_column_name=>'DEDESCR'
,p_display_order=>34
,p_column_identifier=>'I'
,p_column_label=>'Dedescr'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34264286026219014)
,p_db_column_name=>'DERLCID'
,p_display_order=>44
,p_column_identifier=>'J'
,p_column_label=>'Derlcid'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34264496117219016)
,p_db_column_name=>'DESTATE'
,p_display_order=>64
,p_column_identifier=>'L'
,p_column_label=>'Destate'
,p_allow_sorting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_pivot=>'N'
,p_column_type=>'CLOB'
,p_column_alignment=>'CENTER'
,p_rpt_show_filter_lov=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34264586240219017)
,p_db_column_name=>'DETYPID'
,p_display_order=>74
,p_column_identifier=>'M'
,p_column_label=>'Detypid'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39430297642667340)
,p_db_column_name=>'DETYPDE'
,p_display_order=>94
,p_column_identifier=>'O'
,p_column_label=>'Detypde'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(15170482629724645)
,p_db_column_name=>'DEPROFL_ID'
,p_display_order=>104
,p_column_identifier=>'P'
,p_column_label=>'Deprofl Id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(1481925036115434656)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'343563'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>100
,p_report_columns=>'CB:DERNAME:DEDESCR:DESTATE:DETYPDE::DEPROFL_ID'
,p_sort_column_1=>'APPLICATION_ID'
,p_sort_direction_1=>'ASC'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34357473452804110)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(1481924344399434654)
,p_button_name=>'ADD_STATEMENTS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Add Statements'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34264646013219018)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(1481924344399434654)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Cancel'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:205:&SESSION.::&DEBUG.:RP::'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34356741411804110)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(1481924344399434654)
,p_button_name=>'RESET_REPORT'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29008925477135126)
,p_button_image_alt=>'Reset'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:217:&SESSION.::&DEBUG.:28,RIR::'
,p_icon_css_classes=>'fa-undo-alt'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(34359630393804108)
,p_branch_name=>'back to statements'
,p_branch_action=>'f?p=&APP_ID.:205:&SESSION.::&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15170560586724646)
,p_name=>'P217_DEPROFL_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1481924344399434654)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(34358877650804109)
,p_validation_name=>'Must select statement'
,p_validation_sequence=>10
,p_validation=>'wwv_flow.g_f01.count >= 1'
,p_validation_type=>'PLSQL_EXPRESSION'
,p_error_message=>'Please check an statement to add.'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(34359181734804109)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'add rules'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    f number;',
'    g number := :P217_DEPROFL_ID;',
'begin',
'    for i in 1..wwv_flow.g_f01.count loop',
'        f := wwv_flow.g_f01(i);',
'        insert into drprrul(deprofl_id, derulid, decredt, decreus) values (g,f,sysdate,:APP_USER);',
'    end loop;',
'end;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Statement(s) added'
);
end;
/
prompt --application/pages/page_00218
begin
wwv_flow_api.create_page(
 p_id=>218
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Add Applications'
,p_step_title=>'Add Applications'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Add Applications'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function checkallf01() {',
'  if ($("#checkall").is('':checked'')) { ',
'    $("input[name=f01]").prop("checked", true);',
'  } else { ',
'    $("input[name=f01]").prop("checked", false);',
'  }',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_help_text=>'<p>To track standards for application(s) check the application and click the <b>Add Applications</b> button.</p>'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210810122013'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1521289983605535955)
,p_plug_name=>'Add Applications'
,p_region_template_options=>'#DEFAULT#:js-showMaximizeButton'
,p_plug_template=>wwv_flow_api.id(28955876667135161)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct apex_item.checkbox(1,A.APPLICATION_ID) cb,',
'       A.APPLICATION_ID,',
'       A.APPLICATION_NAME',
'      ',
'from APEX_APPLICATIONS A',
'where (A.APPLICATION_ID, :P215_DEPROID) not in (select DEAPPID, DEPROID from DRPRAPP)',
'',
'',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(1521290182654535956)
,p_name=>'Add Applications'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_save_rpt_public=>'Y'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML:EMAIL'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MIKE'
,p_internal_uid=>1521290182654535956
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39369168635101293)
,p_db_column_name=>'CB'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'<input type="checkbox" onclick="checkallf01();" id="checkall" />'
,p_allow_sorting=>'N'
,p_allow_filtering=>'N'
,p_allow_highlighting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_hide=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39258453876502746)
,p_db_column_name=>'APPLICATION_ID'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Application Id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39258549187502747)
,p_db_column_name=>'APPLICATION_NAME'
,p_display_order=>24
,p_column_identifier=>'O'
,p_column_label=>'Application Name'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(1521290675321535957)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'393695'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>100
,p_report_columns=>'CB:APPLICATION_ID:APPLICATION_NAME'
,p_sort_column_1=>'APPLICATION_ID'
,p_sort_direction_1=>'ASC'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(39370779918101290)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(1521289983605535955)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Cancel'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=&APP_ID.:215:&SESSION.::&DEBUG.:RP::'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(39370366429101290)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(1521289983605535955)
,p_button_name=>'ADD_APPLICATIONS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Add Applications'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(39369976710101291)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(1521289983605535955)
,p_button_name=>'RESET_REPORT'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29008925477135126)
,p_button_image_alt=>'Reset'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:218:&SESSION.::&DEBUG.:28,RIR::'
,p_icon_css_classes=>'fa-undo-alt'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(39372034233101286)
,p_branch_name=>'back to applications'
,p_branch_action=>'f?p=&APP_ID.:215:&SESSION.::&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(39371275384101287)
,p_validation_name=>'Must select application'
,p_validation_sequence=>10
,p_validation=>'wwv_flow.g_f01.count >= 1'
,p_validation_type=>'PLSQL_EXPRESSION'
,p_error_message=>'Please check an application to add.'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(39371593834101287)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'add applications'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    f number;',
'    g number := :P215_DEPROID;',
'begin',
'    for i in 1..wwv_flow.g_f01.count loop',
'        f := wwv_flow.g_f01(i);',
'        insert into drprapp(deproid, deappid, decredt, decreus) values (g,f,sysdate, v(''APP_USER''));',
'    end loop;',
'end;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Application(s) added'
);
end;
/
prompt --application/pages/page_00220
begin
wwv_flow_api.create_page(
 p_id=>220
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Choose Project'
,p_page_mode=>'MODAL'
,p_step_title=>'Choose Project'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>'var htmldb_delete_message=''"DELETE_CONFIRM_MSG"'';'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210526113243'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(39874500802883406)
,p_plug_name=>'Choose Project'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28935454342135171)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(39875204544883406)
,p_plug_name=>'Buttons'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28936438753135170)
,p_plug_display_sequence=>10
,p_plug_display_point=>'REGION_POSITION_03'
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(39875680165883406)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(39875204544883406)
,p_button_name=>'CANCEL'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Cancel'
,p_button_position=>'REGION_TEMPLATE_CLOSE'
,p_grid_new_grid=>false
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(39774201117497709)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(39875204544883406)
,p_button_name=>'CONFIRM'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Confirm'
,p_button_position=>'REGION_TEMPLATE_NEXT'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(15167877415724619)
,p_branch_name=>'Go to Dashboard'
,p_branch_action=>'f?p=&APP_ID.:1:&SESSION.::&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(39878020111883403)
,p_name=>'P220_DEPROID'
,p_is_required=>true
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(39874500802883406)
,p_prompt=>'Deproid'
,p_display_as=>'NATIVE_SELECT_LIST'
,p_lov=>'return pck_projects.get_lov();'
,p_lov_display_null=>'YES'
,p_cHeight=>1
,p_field_template=>wwv_flow_api.id(29008861129135127)
,p_item_template_options=>'#DEFAULT#'
,p_warn_on_unsaved_changes=>'I'
,p_lov_display_extra=>'NO'
,p_attribute_01=>'NONE'
,p_attribute_02=>'N'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(39875706547883406)
,p_name=>'Cancel Dialog'
,p_event_sequence=>10
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(39875680165883406)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(39876580006883404)
,p_event_id=>wwv_flow_api.id(39875706547883406)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_DIALOG_CANCEL'
,p_stop_execution_on_error=>'Y'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(15167719115724618)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Set app_project'
,p_process_sql_clob=>':APP_PROJECT := :P220_DEPROID;'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
prompt --application/pages/page_00300
begin
wwv_flow_api.create_page(
 p_id=>300
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Run Result'
,p_step_title=>'Results'
,p_step_sub_title=>'Run Result'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function openInBuilder(url) {',
'    apex.navigation.openInNewWindow(url,',
'        ''APEX_BUILDER'',',
'        {altSuffix:''&APX_BLDR_SESSION.'',favorTabbedBrowsing: true});',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'NDMR'
,p_last_upd_yyyymmddhh24miss=>'20210609092023'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(34671456281337179)
,p_plug_name=>'Results'
,p_region_name=>'ResultId'
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select ''<a href="javascript:openInBuilder('''''' ||pck_checks.build_link( derslid, deappid, derefid) ||',
'        '''''')" class="t-Button t-Button--stretch">Go to</a>'' as edit_link,',
'        /* <span class="fa fa-mail-forward"></span> */',
'       A.DERSLID,',
'       A.DERUNID,',
'       A.DEAPPID,',
'       A.DEPAGEN,',
'       A.DEREGIO,',
'       A.DEITEMN,',
'       A.DELABEL,',
'       A.DERULID,',
'       B.DERNAME,',
'       A.DEGRAID,',
'       C.DEDESCR  as DEGRADE,',
'       A.DETYPID,',
'       D.DEDESCR as DETYPDE,',
'       A.DESTAID,',
'       A.DEPROID,',
'       E.DEDESCR as DEPRODE,',
'       A.DEIDENT,',
'       A.DECREDT,',
'       A.DECREUS,',
'       A.DERLINK,',
'       A.DEREFID',
'  from DRRESUL A, DRRULES B, DRGRADA C, DRRTYPE D, DRPROJE E',
'  where A.DERULID = B.DERULID',
'  and   A.DEGRAID = C.DEGRAID',
'  and   A.DETYPID = D.DETYPID',
'  and   A.DEPROID = E.DEPROID ',
'  and   A.DEPROID = :APP_PROJECT;',
''))
,p_plug_source_type=>'NATIVE_IG'
,p_ajax_items_to_submit=>'P300_DEDESCR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34266794053219039)
,p_name=>'DERNAME'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERNAME'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Dername'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>110
,p_value_alignment=>'CENTER'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34266898508219040)
,p_name=>'DEGRADE'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEGRADE'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Degrade'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>120
,p_value_alignment=>'CENTER'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34266967432219041)
,p_name=>'DETYPDE'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DETYPDE'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Detypde'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>150
,p_value_alignment=>'CENTER'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34267878218219050)
,p_name=>'DEPRODE'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPRODE'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Deprode'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>210
,p_value_alignment=>'CENTER'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34673299311337169)
,p_name=>'DERSLID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERSLID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>30
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34673864616337169)
,p_name=>'DERUNID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERUNID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Derunid'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>40
,p_value_alignment=>'LEFT'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34674488859337168)
,p_name=>'DEAPPID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEAPPID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Deappid'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>50
,p_value_alignment=>'CENTER'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34675042818337168)
,p_name=>'DEPAGEN'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPAGEN'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Depagen'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>60
,p_value_alignment=>'CENTER'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34675684226337168)
,p_name=>'DEREGIO'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEREGIO'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Deregio'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>70
,p_value_alignment=>'CENTER'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34676269277337168)
,p_name=>'DEITEMN'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEITEMN'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Deitemn'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>80
,p_value_alignment=>'CENTER'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34676802301337168)
,p_name=>'DELABEL'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DELABEL'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Delabel'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>90
,p_value_alignment=>'CENTER'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34677416540337167)
,p_name=>'DERULID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERULID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Derulid'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>100
,p_value_alignment=>'LEFT'
,p_attribute_02=>'LOV'
,p_lov_type=>'PLSQL_FUNCTION_BODY'
,p_lov_source=>'return pck_rules.get_lov;'
,p_lov_display_extra=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'LOV'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>false
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34678032462337167)
,p_name=>'DEGRAID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEGRAID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>130
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34678600301337166)
,p_name=>'DETYPID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DETYPID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>140
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34679252176337166)
,p_name=>'DESTAID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DESTAID'
,p_data_type=>'NUMBER'
,p_is_query_only=>false
,p_item_type=>'NATIVE_SELECT_LIST'
,p_heading=>'Destaid'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>160
,p_value_alignment=>'CENTER'
,p_is_required=>true
,p_lov_type=>'PLSQL_FUNCTION_BODY'
,p_lov_source=>'return pck_status.get_lov();'
,p_lov_display_extra=>true
,p_lov_display_null=>true
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'LOV'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_duplicate_value=>true
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34679820139337166)
,p_name=>'DEPROID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEPROID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>170
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34680476620337166)
,p_name=>'DEIDENT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEIDENT'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Deident'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>180
,p_value_alignment=>'CENTER'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_lov_type=>'NONE'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34681081650337166)
,p_name=>'DECREDT'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREDT'
,p_data_type=>'DATE'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Decredt'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>190
,p_value_alignment=>'CENTER'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_is_required=>false
,p_filter_date_ranges=>'ALL'
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(34681673440337165)
,p_name=>'DECREUS'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DECREUS'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_DISPLAY_ONLY'
,p_heading=>'Decreus'
,p_heading_alignment=>'CENTER'
,p_display_sequence=>200
,p_value_alignment=>'CENTER'
,p_attribute_02=>'VALUE'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_enable_pivot=>false
,p_is_primary_key=>false
,p_include_in_export=>true
,p_escape_on_http_output=>true
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(39774179589497708)
,p_name=>'EDIT_LINK'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'EDIT_LINK'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_LINK'
,p_heading_alignment=>'LEFT'
,p_display_sequence=>220
,p_value_alignment=>'LEFT'
,p_enable_filter=>true
,p_filter_operators=>'C:S:CASE_INSENSITIVE:REGEXP'
,p_filter_is_required=>false
,p_filter_text_case=>'MIXED'
,p_filter_exact_match=>true
,p_filter_lov_type=>'DISTINCT'
,p_use_as_row_header=>false
,p_enable_sort_group=>false
,p_is_primary_key=>false
,p_include_in_export=>false
,p_display_condition_type=>'ITEM_IS_NOT_NULL'
,p_display_condition=>'APP_BUILDER_SESSION'
,p_escape_on_http_output=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(39774971927497716)
,p_name=>'DERLINK'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DERLINK'
,p_data_type=>'VARCHAR2'
,p_is_query_only=>true
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>230
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_include_in_export=>false
);
wwv_flow_api.create_region_column(
 p_id=>wwv_flow_api.id(39776257529497729)
,p_name=>'DEREFID'
,p_source_type=>'DB_COLUMN'
,p_source_expression=>'DEREFID'
,p_data_type=>'NUMBER'
,p_is_query_only=>true
,p_item_type=>'NATIVE_HIDDEN'
,p_display_sequence=>240
,p_attribute_01=>'Y'
,p_filter_is_required=>false
,p_use_as_row_header=>false
,p_enable_sort_group=>true
,p_is_primary_key=>false
,p_include_in_export=>false
);
wwv_flow_api.create_interactive_grid(
 p_id=>wwv_flow_api.id(34671955299337179)
,p_internal_uid=>34671955299337179
,p_is_editable=>true
,p_edit_operations=>'u'
,p_lost_update_check_type=>'VALUES'
,p_submit_checked_rows=>false
,p_lazy_loading=>false
,p_requires_filter=>false
,p_show_nulls_as=>'-'
,p_pagination_type=>'SCROLL'
,p_show_total_row_count=>true
,p_show_toolbar=>true
,p_enable_save_public_report=>false
,p_enable_subscriptions=>true
,p_enable_download=>true
,p_enable_mail_download=>true
,p_fixed_header=>'PAGE'
,p_show_icon_view=>false
,p_show_detail_view=>false
);
wwv_flow_api.create_ig_report(
 p_id=>wwv_flow_api.id(34672374221337178)
,p_interactive_grid_id=>wwv_flow_api.id(34671955299337179)
,p_type=>'PRIMARY'
,p_default_view=>'GRID'
,p_show_row_number=>false
,p_settings_area_expanded=>true
);
wwv_flow_api.create_ig_report_view(
 p_id=>wwv_flow_api.id(34672490143337178)
,p_report_id=>wwv_flow_api.id(34672374221337178)
,p_view_type=>'GRID'
,p_stretch_columns=>true
,p_srv_exclude_null_values=>false
,p_srv_only_display_columns=>true
,p_edit_mode=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34673622606337169)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>2
,p_column_id=>wwv_flow_api.id(34673299311337169)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34674255336337168)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>0
,p_column_id=>wwv_flow_api.id(34673864616337169)
,p_is_visible=>false
,p_is_frozen=>false
,p_width=>74
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34674814520337168)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>5
,p_column_id=>wwv_flow_api.id(34674488859337168)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>91
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34675450798337168)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>6
,p_column_id=>wwv_flow_api.id(34675042818337168)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>78
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34676080777337168)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(34675684226337168)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>196
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34676656520337168)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>7
,p_column_id=>wwv_flow_api.id(34676269277337168)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>152
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34677228194337167)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>9
,p_column_id=>wwv_flow_api.id(34676802301337168)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>185
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34677835487337167)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>10
,p_column_id=>wwv_flow_api.id(34677416540337167)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>380
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34678468176337166)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>10
,p_column_id=>wwv_flow_api.id(34678032462337167)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34679069510337166)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>11
,p_column_id=>wwv_flow_api.id(34678600301337166)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34679661755337166)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>21
,p_column_id=>wwv_flow_api.id(34679252176337166)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>111
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34680270312337166)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>13
,p_column_id=>wwv_flow_api.id(34679820139337166)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34680811750337166)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>13
,p_column_id=>wwv_flow_api.id(34680476620337166)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34681445972337166)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>18
,p_column_id=>wwv_flow_api.id(34681081650337166)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34682028588337165)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>16
,p_column_id=>wwv_flow_api.id(34681673440337165)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34683248036313899)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>14
,p_column_id=>wwv_flow_api.id(34266794053219039)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34683778086313896)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>15
,p_column_id=>wwv_flow_api.id(34266898508219040)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>82
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(34684264995313895)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>17
,p_column_id=>wwv_flow_api.id(34266967432219041)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>91
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(39252572077818033)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>3
,p_column_id=>wwv_flow_api.id(34267878218219050)
,p_is_visible=>false
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(39885435352047363)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>1
,p_column_id=>wwv_flow_api.id(39774179589497708)
,p_is_visible=>true
,p_is_frozen=>false
,p_width=>99
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(40267831787536891)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>20
,p_column_id=>wwv_flow_api.id(39774971927497716)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_ig_report_column(
 p_id=>wwv_flow_api.id(40406262788076783)
,p_view_id=>wwv_flow_api.id(34672490143337178)
,p_display_seq=>21
,p_column_id=>wwv_flow_api.id(39776257529497729)
,p_is_visible=>true
,p_is_frozen=>false
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(39427743103667315)
,p_plug_name=>'Project'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--scrollBody'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_plug_display_condition_type=>'NEVER'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(39428140124667319)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(34671456281337179)
,p_button_name=>'START'
,p_button_action=>'DEFINED_BY_DA'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Start'
,p_button_position=>'REGION_TEMPLATE_EDIT'
,p_button_execute_validations=>'N'
,p_warn_on_unsaved_changes=>null
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(39428008993667318)
,p_name=>'P300_DEPROID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(34671456281337179)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(39777370254497740)
,p_name=>'P300_DEDESCR'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(34671456281337179)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(39428248612667320)
,p_name=>'START_RUN'
,p_event_sequence=>10
,p_triggering_element_type=>'BUTTON'
,p_triggering_button_id=>wwv_flow_api.id(39428140124667319)
,p_bind_type=>'bind'
,p_bind_event_type=>'click'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(39428322063667321)
,p_event_id=>wwv_flow_api.id(39428248612667320)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'begin',
'    pck_checks.start_run(:APP_PROJECT,:APP_USER);',
'end;'))
,p_attribute_02=>'P300_DEPROID'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(39428454433667322)
,p_event_id=>wwv_flow_api.id(39428248612667320)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(34671456281337179)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(39777605374497743)
,p_name=>'Filtering'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(39777703209497744)
,p_event_id=>wwv_flow_api.id(39777605374497743)
,p_event_result=>'TRUE'
,p_action_sequence=>30
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if($v(''P300_DEDESCR'')){',
'  apex.region("ResultId").call("getActions").invoke("reset-report");',
'',
'    setTimeout(function() {',
'        apex.region("ResultId").widget().interactiveGrid("addFilter", {',
'          type: ''column'',',
'          columnType: ''column'',',
'          columnName: ''DETYPDE'',',
'          operator: ''EQ'',',
'          value: $v(''P300_DEDESCR''),',
'          isCaseSensitive: false',
'        });',
'        $s(''P300_DEDESCR'', null);',
'    }, 1000);',
'}'))
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(34267364179219045)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_region_id=>wwv_flow_api.id(34671456281337179)
,p_process_type=>'NATIVE_IG_DML'
,p_process_name=>'Run Result - Save Interactive Grid Data'
,p_attribute_01=>'REGION_SOURCE'
,p_attribute_05=>'Y'
,p_attribute_06=>'Y'
,p_attribute_08=>'Y'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
prompt --application/pages/page_09999
begin
wwv_flow_api.create_page(
 p_id=>9999
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Login Page'
,p_alias=>'LOGIN_DESKTOP'
,p_step_title=>'Coding Conventions Checker - Sign In'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'OFF'
,p_step_template=>wwv_flow_api.id(28915702668135184)
,p_page_template_options=>'#DEFAULT#'
,p_page_is_public_y_n=>'Y'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210503145957'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29035184224135054)
,p_plug_name=>'Coding Conventions Checker'
,p_icon_css_classes=>'app-icon'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28956456576135161)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_attribute_01=>'N'
,p_attribute_02=>'TEXT'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29039822450135034)
,p_plug_name=>'Language Selector'
,p_parent_plug_id=>wwv_flow_api.id(29035184224135054)
,p_region_template_options=>'#DEFAULT#'
,p_component_template_options=>'#DEFAULT#'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_api.id(28935454342135171)
,p_plug_display_sequence=>20
,p_plug_display_point=>'BODY'
,p_plug_source=>'apex_lang.emit_language_selector_list;'
,p_plug_source_type=>'NATIVE_PLSQL'
,p_plug_query_num_rows=>15
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(29037994104135040)
,p_button_sequence=>40
,p_button_plug_id=>wwv_flow_api.id(29035184224135054)
,p_button_name=>'LOGIN'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Sign In'
,p_button_position=>'REGION_TEMPLATE_NEXT'
,p_button_alignment=>'LEFT'
,p_grid_new_grid=>false
,p_grid_new_row=>'Y'
,p_grid_new_column=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29035574074135049)
,p_name=>'P9999_USERNAME'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(29035184224135054)
,p_prompt=>'username'
,p_placeholder=>'username'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>40
,p_cMaxlength=>100
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(29008249445135128)
,p_item_icon_css_classes=>'fa-user'
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'N'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'NONE'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29035962092135048)
,p_name=>'P9999_PASSWORD'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(29035184224135054)
,p_prompt=>'password'
,p_placeholder=>'password'
,p_display_as=>'NATIVE_PASSWORD'
,p_cSize=>40
,p_cMaxlength=>100
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(29008249445135128)
,p_item_icon_css_classes=>'fa-key'
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'Y'
,p_attribute_02=>'Y'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29037019371135044)
,p_name=>'P9999_REMEMBER'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(29035184224135054)
,p_prompt=>'Remember username'
,p_display_as=>'NATIVE_CHECKBOX'
,p_named_lov=>'LOGIN_REMEMBER_USERNAME'
,p_lov=>'.'||wwv_flow_api.id(29036286403135048)||'.'
,p_label_alignment=>'RIGHT'
,p_field_template=>wwv_flow_api.id(29008249445135128)
,p_item_template_options=>'#DEFAULT#'
,p_lov_display_extra=>'NO'
,p_help_text=>wwv_flow_string.join(wwv_flow_t_varchar2(
'<p>',
'If you select this checkbox, the application will save your username in a persistent browser cookie named "LOGIN_USERNAME_COOKIE".',
'When you go to the login page the next time,',
'the username field will be automatically populated with this value.',
'</p>',
'<p>',
'If you deselect this checkbox and your username is already saved in the cookie,',
'the application will overwrite it with an empty value.',
'You can also use your browser''s developer tools to completely remove the cookie.',
'</p>'))
,p_attribute_01=>'1'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29038795589135036)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Set Username Cookie'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex_authentication.send_login_username_cookie (',
'    p_username => lower(:P9999_USERNAME),',
'    p_consent  => :P9999_REMEMBER = ''Y'' );'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29038342293135036)
,p_process_sequence=>20
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Login'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'apex_authentication.login(',
'    p_username => :P9999_USERNAME,',
'    p_password => :P9999_PASSWORD );'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29039579020135035)
,p_process_sequence=>30
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_SESSION_STATE'
,p_process_name=>'Clear Page(s) Cache'
,p_attribute_01=>'CLEAR_CACHE_CURRENT_PAGE'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29039128953135035)
,p_process_sequence=>10
,p_process_point=>'BEFORE_HEADER'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'Get Username Cookie'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
':P9999_USERNAME := apex_authentication.get_login_username_cookie;',
':P9999_REMEMBER := case when :P9999_USERNAME is not null then ''Y'' end;'))
);
end;
/
prompt --application/end_environment
begin
wwv_flow_api.import_end(p_auto_install_sup_obj => nvl(wwv_flow_application_install.get_auto_install_sup_obj, false));
commit;
end;
/
set verify on feedback on define on
prompt  ...done
