prompt --application/pages/page_00216
begin
wwv_flow_api.create_page(
 p_id=>216
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Manage rules for projects'
,p_page_mode=>'MODAL'
,p_step_title=>'Manage rules for projects'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_javascript_file_urls=>'#WORKSPACE_IMAGES#JS/93502.js'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'NDMR'
,p_last_upd_yyyymmddhh24miss=>'20210506212725'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29316067573630901)
,p_plug_name=>'Manage rules for projects'
,p_region_template_options=>'#DEFAULT#:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Form--slimPadding:t-Form--leftLabels'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(29316521513630906)
,p_name=>'Labels'
,p_region_name=>'LABELS'
,p_parent_plug_id=>wwv_flow_api.id(29316067573630901)
,p_template=>wwv_flow_api.id(28956997224135161)
,p_display_sequence=>20
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--removeHeader:t-Region--noBorder:t-Region--scrollBody:t-Form--slimPadding:t-Form--stretchInputs'
,p_component_template_options=>'#DEFAULT#:t-Report--altRowsDefault:t-Report--rowHighlight'
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select    a.deproid,c.derulid, c.dername, c.dedescr as description, b.destate, b.derlcid,',
'          APEX_ITEM.CHECKBOX2( ',
'              p_idx => 2,',
'              p_value => decode(d.derulid, null, ''N'', ''J''), ',
'              p_attributes => case when decode(d.derulid, null, ''N'', ''J'') = ''J'' then ''CHECKED '' else '' '' end ||  ',
'              ''onChange="change_checkbox('''''' || c.derulid || '''''','''''' || decode(d.derulid, null, ''N'', ''J'') || '''''');"''                 ',
'          ) as DEOPLJN',
'from      drproje a, ',
'          drrulce b, ',
'          drrules c,',
'          drprrul d',
'where     a.deapeid = b.deapeid',
'and       b.derulid = c.derulid',
'and       c.derulid = d.derulid (+)',
'and       a.deproid (+) = :P216_DEPROID',
'order by  a.deproid',
';',
'',
'',
''))
,p_ajax_enabled=>'Y'
,p_ajax_items_to_submit=>'P216_DEPROID'
,p_query_row_template=>wwv_flow_api.id(28979442408135144)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_query_num_rows_type=>'NEXT_PREVIOUS_LINKS'
,p_pagination_display_position=>'BOTTOM_RIGHT'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29404067578820906)
,p_query_column_id=>1
,p_column_alias=>'DEPROID'
,p_column_display_sequence=>5
,p_column_heading=>'Deproid'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29317943177630920)
,p_query_column_id=>2
,p_column_alias=>'DERULID'
,p_column_display_sequence=>1
,p_column_heading=>'Derulid'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29318014362630921)
,p_query_column_id=>3
,p_column_alias=>'DERNAME'
,p_column_display_sequence=>2
,p_column_heading=>'Dername'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29318279205630923)
,p_query_column_id=>4
,p_column_alias=>'DESCRIPTION'
,p_column_display_sequence=>3
,p_column_heading=>'Description'
,p_use_as_row_header=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29404167518820907)
,p_query_column_id=>5
,p_column_alias=>'DESTATE'
,p_column_display_sequence=>6
,p_column_heading=>'Destate'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29404247410820908)
,p_query_column_id=>6
,p_column_alias=>'DERLCID'
,p_column_display_sequence=>7
,p_column_heading=>'Derlcid'
,p_use_as_row_header=>'N'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(29403949682820905)
,p_query_column_id=>7
,p_column_alias=>'DEOPLJN'
,p_column_display_sequence=>4
,p_column_heading=>'Deopljn'
,p_use_as_row_header=>'N'
,p_display_as=>'WITHOUT_MODIFICATION'
,p_derived_column=>'N'
,p_include_in_export=>'N'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29316997309630910)
,p_plug_name=>'Buttons'
,p_region_template_options=>'#DEFAULT#'
,p_plug_template=>wwv_flow_api.id(28936438753135170)
,p_plug_display_sequence=>10
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'REGION_POSITION_03'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29316114592630902)
,p_name=>'P216_DEPROID'
,p_item_sequence=>20
,p_item_plug_id=>wwv_flow_api.id(29316067573630901)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Deproid'
,p_source=>'DEPROID'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(29008583468135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29316461016630905)
,p_name=>'P216_DEDESCR'
,p_item_sequence=>30
,p_item_plug_id=>wwv_flow_api.id(29316067573630901)
,p_use_cache_before_default=>'NO'
,p_prompt=>'Dedescr'
,p_source=>'DEDESCR'
,p_source_type=>'DB_COLUMN'
,p_display_as=>'NATIVE_TEXT_FIELD'
,p_cSize=>30
,p_field_template=>wwv_flow_api.id(29008583468135127)
,p_item_template_options=>'#DEFAULT#'
,p_attribute_01=>'N'
,p_attribute_02=>'Y'
,p_attribute_03=>'N'
,p_attribute_04=>'TEXT'
,p_attribute_05=>'BOTH'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(29317593687630916)
,p_name=>'P216_MESSAGE'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(29316067573630901)
,p_use_cache_before_default=>'NO'
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'N'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(29316233344630903)
,p_name=>'On Change - ID'
,p_event_sequence=>10
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P216_DEPROID'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(29316306093630904)
,p_event_id=>wwv_flow_api.id(29316233344630903)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(29316521513630906)
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(29317623290630917)
,p_name=>'On change - Message'
,p_event_sequence=>20
,p_triggering_element_type=>'ITEM'
,p_triggering_element=>'P216_MESSAGE'
,p_condition_element=>'P216_MESSAGE'
,p_triggering_condition_type=>'NOT_NULL'
,p_bind_type=>'bind'
,p_bind_event_type=>'change'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(29317706112630918)
,p_event_id=>wwv_flow_api.id(29317623290630917)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>wwv_flow_string.join(wwv_flow_t_varchar2(
'customAlert(''E'', document.getElementById(''P216_MESSAGE'').value, 0);',
''))
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(29403628156820902)
,p_name=>'Refresh labels'
,p_event_sequence=>30
,p_triggering_element_type=>'JAVASCRIPT_EXPRESSION'
,p_triggering_element=>'document'
,p_bind_type=>'bind'
,p_bind_event_type=>'custom'
,p_bind_event_type_custom=>'refresh_labels'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(29403717068820903)
,p_event_id=>wwv_flow_api.id(29403628156820902)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_REFRESH'
,p_affected_elements_type=>'REGION'
,p_affected_region_id=>wwv_flow_api.id(29316521513630906)
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29403821960820904)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_CLOSE_WINDOW'
,p_process_name=>'Close Dialog'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(29317222165630913)
,p_process_sequence=>10
,p_process_point=>'ON_DEMAND'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'change_checkbox'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'if apex_application.g_f02(1) = ''N'' then',
'    -- Indien aanvinken --> toevoegen aan DRPPRUL',
'    insert into DRPRRUL',
'    (deproid, derulid, decredt, decreus)',
'    values',
'    (:P216_DEPROID , apex_application.g_f01(1), sysdate, :APP_USER);',
'else ',
'    -- Indien uitvinken --> verwijderen uit DRPPRUL',
'    delete from DRPRRUL',
'    where  deproid = :P216_DEPROID',
'    and    derulid = apex_application.g_f01(1);',
'end if;',
'',
'commit;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
end;
/
