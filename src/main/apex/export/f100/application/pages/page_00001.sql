prompt --application/pages/page_00001
begin
wwv_flow_api.create_page(
 p_id=>1
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Home'
,p_alias=>'HOME'
,p_step_title=>'Coding Conventions Checker'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_autocomplete_on_off=>'OFF'
,p_page_template_options=>'#DEFAULT#'
,p_last_updated_by=>'NDMR'
,p_last_upd_yyyymmddhh24miss=>'20210530113807'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(29042021766135027)
,p_plug_name=>'Coding Conventions Checker'
,p_icon_css_classes=>'app-icon'
,p_region_template_options=>'#DEFAULT#'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_api.id(28952270918135162)
,p_plug_display_sequence=>10
,p_plug_display_point=>'REGION_POSITION_01'
,p_plug_query_num_rows=>15
,p_attribute_01=>'N'
,p_attribute_02=>'HTML'
,p_attribute_03=>'Y'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(39255522007502717)
,p_plug_name=>'History Bugs'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>50
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT *',
'FROM (',
'  SELECT a.deproid, a.detypid, c.dedescr,a.decredt',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
')',
'PIVOT (',
'  count(*) as count',
'  FOR detypid IN (1,2,3)',
' )*/',
' ',
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid, b.decredt',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 1',
'group by b.derunid, c.dedescr, b.decredt, b.detypid',
'order by b.derunid;',
''))
,p_plug_source_type=>'NATIVE_JET_CHART'
,p_plug_query_num_rows=>15
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_jet_chart(
 p_id=>wwv_flow_api.id(39775250655497719)
,p_region_id=>wwv_flow_api.id(39255522007502717)
,p_chart_type=>'lineWithArea'
,p_animation_on_display=>'none'
,p_animation_on_data_change=>'none'
,p_orientation=>'vertical'
,p_data_cursor=>'auto'
,p_data_cursor_behavior=>'auto'
,p_hide_and_show_behavior=>'none'
,p_hover_behavior=>'none'
,p_stack=>'off'
,p_connect_nulls=>'Y'
,p_sorting=>'label-asc'
,p_fill_multi_series_gaps=>true
,p_zoom_and_scroll=>'off'
,p_tooltip_rendered=>'Y'
,p_show_series_name=>true
,p_show_group_name=>true
,p_show_value=>true
,p_show_label=>true
,p_legend_rendered=>'on'
,p_legend_position=>'auto'
);
wwv_flow_api.create_jet_chart_series(
 p_id=>wwv_flow_api.id(39775340786497720)
,p_chart_id=>wwv_flow_api.id(39775250655497719)
,p_seq=>10
,p_name=>'Graph'
,p_data_source_type=>'SQL'
,p_data_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 1',
'group by b.derunid, c.dedescr, b.detypid',
'order by b.derunid;'))
,p_series_name_column_name=>'TYPE'
,p_items_value_column_name=>'DECOUNT'
,p_items_label_column_name=>'DERUNID'
,p_line_style=>'solid'
,p_line_type=>'auto'
,p_marker_rendered=>'auto'
,p_marker_shape=>'auto'
,p_assigned_to_y2=>'off'
,p_items_label_rendered=>false
);
wwv_flow_api.create_jet_chart_axis(
 p_id=>wwv_flow_api.id(39775453170497721)
,p_chart_id=>wwv_flow_api.id(39775250655497719)
,p_axis=>'x'
,p_is_rendered=>'on'
,p_title=>'Run ID'
,p_format_scaling=>'auto'
,p_scaling=>'linear'
,p_baseline_scaling=>'zero'
,p_major_tick_rendered=>'on'
,p_minor_tick_rendered=>'off'
,p_tick_label_rendered=>'on'
,p_tick_label_rotation=>'auto'
,p_tick_label_position=>'outside'
);
wwv_flow_api.create_jet_chart_axis(
 p_id=>wwv_flow_api.id(39775523251497722)
,p_chart_id=>wwv_flow_api.id(39775250655497719)
,p_axis=>'y'
,p_is_rendered=>'on'
,p_title=>'Total'
,p_format_scaling=>'auto'
,p_scaling=>'linear'
,p_baseline_scaling=>'zero'
,p_position=>'auto'
,p_major_tick_rendered=>'on'
,p_minor_tick_rendered=>'off'
,p_tick_label_rendered=>'on'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(39776332218497730)
,p_plug_name=>'History Codesmells'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>60
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT *',
'FROM (',
'  SELECT a.deproid, a.detypid, c.dedescr,a.decredt',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
')',
'PIVOT (',
'  count(*) as count',
'  FOR detypid IN (1,2,3)',
' )*/',
' ',
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 2',
'group by b.derunid, c.dedescr, b.detypid',
'order by b.derunid;',
''))
,p_plug_source_type=>'NATIVE_JET_CHART'
,p_plug_query_num_rows=>15
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_jet_chart(
 p_id=>wwv_flow_api.id(39776437768497731)
,p_region_id=>wwv_flow_api.id(39776332218497730)
,p_chart_type=>'lineWithArea'
,p_animation_on_display=>'none'
,p_animation_on_data_change=>'none'
,p_orientation=>'vertical'
,p_data_cursor=>'auto'
,p_data_cursor_behavior=>'auto'
,p_hide_and_show_behavior=>'none'
,p_hover_behavior=>'none'
,p_stack=>'off'
,p_connect_nulls=>'Y'
,p_sorting=>'label-asc'
,p_fill_multi_series_gaps=>true
,p_zoom_and_scroll=>'off'
,p_tooltip_rendered=>'Y'
,p_show_series_name=>true
,p_show_group_name=>true
,p_show_value=>true
,p_show_label=>true
,p_legend_rendered=>'on'
,p_legend_position=>'auto'
);
wwv_flow_api.create_jet_chart_series(
 p_id=>wwv_flow_api.id(39776516123497732)
,p_chart_id=>wwv_flow_api.id(39776437768497731)
,p_seq=>10
,p_name=>'Graph'
,p_data_source_type=>'SQL'
,p_data_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 2',
'group by b.derunid, c.dedescr, b.detypid',
'order by b.derunid;',
''))
,p_series_name_column_name=>'TYPE'
,p_items_value_column_name=>'DECOUNT'
,p_items_label_column_name=>'DERUNID'
,p_line_style=>'solid'
,p_line_type=>'auto'
,p_marker_rendered=>'auto'
,p_marker_shape=>'auto'
,p_assigned_to_y2=>'off'
,p_items_label_rendered=>false
);
wwv_flow_api.create_jet_chart_axis(
 p_id=>wwv_flow_api.id(39776685593497733)
,p_chart_id=>wwv_flow_api.id(39776437768497731)
,p_axis=>'x'
,p_is_rendered=>'on'
,p_title=>'Run ID'
,p_format_scaling=>'auto'
,p_scaling=>'linear'
,p_baseline_scaling=>'zero'
,p_major_tick_rendered=>'on'
,p_minor_tick_rendered=>'off'
,p_tick_label_rendered=>'on'
,p_tick_label_rotation=>'auto'
,p_tick_label_position=>'outside'
);
wwv_flow_api.create_jet_chart_axis(
 p_id=>wwv_flow_api.id(39776797571497734)
,p_chart_id=>wwv_flow_api.id(39776437768497731)
,p_axis=>'y'
,p_is_rendered=>'on'
,p_title=>'Total'
,p_format_scaling=>'auto'
,p_scaling=>'linear'
,p_baseline_scaling=>'zero'
,p_position=>'auto'
,p_major_tick_rendered=>'on'
,p_minor_tick_rendered=>'off'
,p_tick_label_rendered=>'on'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(39776864266497735)
,p_plug_name=>'History Vulnerabilities'
,p_region_template_options=>'#DEFAULT#:t-Region--scrollBody'
,p_escape_on_http_output=>'Y'
,p_plug_template=>wwv_flow_api.id(28956997224135161)
,p_plug_display_sequence=>70
,p_include_in_reg_disp_sel_yn=>'Y'
,p_plug_new_grid_row=>false
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'/*SELECT *',
'FROM (',
'  SELECT a.deproid, a.detypid, c.dedescr,a.decredt',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
')',
'PIVOT (',
'  count(*) as count',
'  FOR detypid IN (1,2,3)',
' )*/',
' ',
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 3',
'group by b.derunid, c.dedescr, b.detypid',
'order by b.derunid;',
''))
,p_plug_source_type=>'NATIVE_JET_CHART'
,p_plug_query_num_rows=>15
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_jet_chart(
 p_id=>wwv_flow_api.id(39776965629497736)
,p_region_id=>wwv_flow_api.id(39776864266497735)
,p_chart_type=>'lineWithArea'
,p_animation_on_display=>'none'
,p_animation_on_data_change=>'none'
,p_orientation=>'vertical'
,p_data_cursor=>'auto'
,p_data_cursor_behavior=>'auto'
,p_hide_and_show_behavior=>'none'
,p_hover_behavior=>'none'
,p_stack=>'off'
,p_connect_nulls=>'Y'
,p_sorting=>'label-asc'
,p_fill_multi_series_gaps=>true
,p_zoom_and_scroll=>'off'
,p_tooltip_rendered=>'Y'
,p_show_series_name=>true
,p_show_group_name=>true
,p_show_value=>true
,p_show_label=>true
,p_legend_rendered=>'on'
,p_legend_position=>'auto'
);
wwv_flow_api.create_jet_chart_series(
 p_id=>wwv_flow_api.id(39777044213497737)
,p_chart_id=>wwv_flow_api.id(39776965629497736)
,p_seq=>10
,p_name=>'Graph'
,p_data_source_type=>'SQL'
,p_data_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select b.detypid , c.dedescr as type, sum(decount) as decount, b.derunid',
'from drrunlg a, drrunst b, drrtype c',
'where a.derunid = b.derunid',
'and deproid  = :APP_PROJECT',
'and b.detypid = c.detypid',
'and b.detypid = 3',
'group by b.derunid, c.dedescr, b.detypid;',
'',
''))
,p_series_name_column_name=>'TYPE'
,p_items_value_column_name=>'DECOUNT'
,p_items_label_column_name=>'DERUNID'
,p_line_style=>'solid'
,p_line_type=>'auto'
,p_marker_rendered=>'auto'
,p_marker_shape=>'auto'
,p_assigned_to_y2=>'off'
,p_items_label_rendered=>false
);
wwv_flow_api.create_jet_chart_axis(
 p_id=>wwv_flow_api.id(39777111992497738)
,p_chart_id=>wwv_flow_api.id(39776965629497736)
,p_axis=>'x'
,p_is_rendered=>'on'
,p_title=>'Run ID'
,p_format_scaling=>'auto'
,p_scaling=>'linear'
,p_baseline_scaling=>'zero'
,p_major_tick_rendered=>'on'
,p_minor_tick_rendered=>'off'
,p_tick_label_rendered=>'on'
,p_tick_label_rotation=>'auto'
,p_tick_label_position=>'outside'
);
wwv_flow_api.create_jet_chart_axis(
 p_id=>wwv_flow_api.id(39777246462497739)
,p_chart_id=>wwv_flow_api.id(39776965629497736)
,p_axis=>'y'
,p_is_rendered=>'on'
,p_title=>'Total'
,p_format_scaling=>'auto'
,p_scaling=>'linear'
,p_baseline_scaling=>'zero'
,p_position=>'auto'
,p_major_tick_rendered=>'on'
,p_minor_tick_rendered=>'off'
,p_tick_label_rendered=>'on'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(39778110173497748)
,p_name=>'Total Bugs'
,p_template=>wwv_flow_api.id(28956997224135161)
,p_display_sequence=>20
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--removeHeader:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#:t-BadgeList--large:t-BadgeList--circular:t-BadgeList--fixed'
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT count(b.dedescr) as decount_bugs, b.dedescr',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
' and a.detypid = 1',
' group by b.dedescr',
''))
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(28975823088135145)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(40544537977820608)
,p_query_column_id=>1
,p_column_alias=>'DECOUNT_BUGS'
,p_column_display_sequence=>2
,p_column_heading=>'Total Bugs'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:300:&SESSION.::&DEBUG.:RP:P300_DEDESCR:#DEDESCR#'
,p_column_linktext=>'#DECOUNT_BUGS#'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(40544479384820607)
,p_query_column_id=>2
,p_column_alias=>'DEDESCR'
,p_column_display_sequence=>1
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(40544666627820609)
,p_name=>'Total Codesmells'
,p_template=>wwv_flow_api.id(28956997224135161)
,p_display_sequence=>30
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--removeHeader:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#:t-BadgeList--large:t-BadgeList--circular:t-BadgeList--fixed'
,p_new_grid_row=>false
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT count(b.dedescr) as decount_csm, b.dedescr',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
' and a.detypid = 2',
' group by b.dedescr',
''))
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(28975823088135145)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(40544944527820612)
,p_query_column_id=>1
,p_column_alias=>'DECOUNT_CSM'
,p_column_display_sequence=>2
,p_column_heading=>'Total Codesmells'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:300:&SESSION.::&DEBUG.:RP:P300_DEDESCR:#DEDESCR#'
,p_column_linktext=>'#DECOUNT_CSM#'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(40544781209820610)
,p_query_column_id=>2
,p_column_alias=>'DEDESCR'
,p_column_display_sequence=>1
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_api.create_report_region(
 p_id=>wwv_flow_api.id(40545051880820613)
,p_name=>'Total Vulnerabilities'
,p_template=>wwv_flow_api.id(28956997224135161)
,p_display_sequence=>40
,p_include_in_reg_disp_sel_yn=>'Y'
,p_region_template_options=>'#DEFAULT#:t-Region--noPadding:t-Region--removeHeader:t-Region--scrollBody'
,p_component_template_options=>'#DEFAULT#:t-BadgeList--large:t-BadgeList--circular:t-BadgeList--fixed'
,p_new_grid_row=>false
,p_display_point=>'BODY'
,p_source_type=>'NATIVE_SQL_REPORT'
,p_query_type=>'SQL'
,p_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'  SELECT count(b.dedescr) as decount_vul, b.dedescr',
'  FROM drresul a, drrtype b, drproje c',
'  where destaid != 2',
'  and a.detypid = b.detypid',
'  and a.deproid = c.deproid',
'  and a.deproid = :APP_PROJECT',
' and a.detypid = 3',
' group by b.dedescr',
''))
,p_ajax_enabled=>'Y'
,p_query_row_template=>wwv_flow_api.id(28975823088135145)
,p_query_num_rows=>15
,p_query_options=>'DERIVED_REPORT_COLUMNS'
,p_query_show_nulls_as=>'-'
,p_csv_output=>'N'
,p_prn_output=>'N'
,p_sort_null=>'L'
,p_plug_query_strip_html=>'N'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(40545348090820616)
,p_query_column_id=>1
,p_column_alias=>'DECOUNT_VUL'
,p_column_display_sequence=>2
,p_column_heading=>'Total Vulnerabilities'
,p_use_as_row_header=>'N'
,p_column_link=>'f?p=&APP_ID.:300:&SESSION.::&DEBUG.:RP:P300_DEDESCR:#DEDESCR#'
,p_column_linktext=>'#DECOUNT_VUL#'
,p_disable_sort_column=>'N'
,p_derived_column=>'N'
,p_include_in_export=>'Y'
);
wwv_flow_api.create_report_columns(
 p_id=>wwv_flow_api.id(40545173650820614)
,p_query_column_id=>2
,p_column_alias=>'DEDESCR'
,p_column_display_sequence=>1
,p_hidden_column=>'Y'
,p_derived_column=>'N'
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15168277290724623)
,p_name=>'P1_URL'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(29042021766135027)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_da_event(
 p_id=>wwv_flow_api.id(15167993230724620)
,p_name=>'Page Load - Open choose project'
,p_event_sequence=>20
,p_bind_type=>'bind'
,p_bind_event_type=>'ready'
,p_display_when_type=>'PLSQL_EXPRESSION'
,p_display_when_cond=>':APP_PROJECT is null'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(15168123377724622)
,p_event_id=>wwv_flow_api.id(15167993230724620)
,p_event_result=>'TRUE'
,p_action_sequence=>10
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_EXECUTE_PLSQL_CODE'
,p_attribute_01=>':P1_URL := apex_util.prepare_url(''f?p=100:220:&SESSION.:&DEBUG.'');'
,p_attribute_03=>'P1_URL'
,p_attribute_04=>'N'
,p_stop_execution_on_error=>'Y'
,p_wait_for_result=>'Y'
);
wwv_flow_api.create_page_da_action(
 p_id=>wwv_flow_api.id(15168021256724621)
,p_event_id=>wwv_flow_api.id(15167993230724620)
,p_event_result=>'TRUE'
,p_action_sequence=>20
,p_execute_on_page_init=>'N'
,p_action=>'NATIVE_JAVASCRIPT_CODE'
,p_attribute_01=>'window.location.href = $v(''P1_URL'');'
);
end;
/
