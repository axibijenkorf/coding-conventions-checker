prompt --application/pages/page_00217
begin
wwv_flow_api.create_page(
 p_id=>217
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Add Statements'
,p_step_title=>'Add Statements'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Add Statements'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function checkallf01() {',
'  if ($("#checkall").is('':checked'')) { ',
'    $("input[name=f01]").prop("checked", true);',
'  } else { ',
'    $("input[name=f01]").prop("checked", false);',
'  }',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_help_text=>'<p>To track standards for application(s) check the application and click the <b>Add Applications</b> button.</p>'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210806155048'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1481924344399434654)
,p_plug_name=>'Add Statements'
,p_region_template_options=>'#DEFAULT#:js-showMaximizeButton'
,p_plug_template=>wwv_flow_api.id(28955876667135161)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select apex_item.checkbox(1,A.DERULID) cb,',
'       A.DERULID,',
'       A.DERNAME,',
'       A.DEDESCR,',
'       B.DERLCID,',
'       B.DESTATE,',
'       a.DETYPID,',
'       C.DEPROFL_ID,',
'       D.DEDESCR DETYPDE',
'from DRRULES A, DRRULCE B, DRPROFL C, DRRTYPE D',
'where (C.DEPROFL_ID, A.DERULID) not in (select DEPROFL_ID, DERULID from DRPRRUL)',
'and B.DERULID = A.DERULID',
'and A.DERULID = B.DERULID',
'and C.DEPROFL_ID = :P217_DEPROFL_ID',
'and D.DETYPID = a.DETYPID'))
,p_plug_source_type=>'NATIVE_IR'
,p_ajax_items_to_submit=>'P217_DEPROFL_ID'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(1481924543448434655)
,p_name=>'Add Applications'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_save_rpt_public=>'Y'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML:EMAIL'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MIKE'
,p_internal_uid=>1481924543448434655
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34355197641804111)
,p_db_column_name=>'CB'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'<input type="checkbox" onclick="checkallf01();" id="checkall" />'
,p_allow_sorting=>'N'
,p_allow_filtering=>'N'
,p_allow_highlighting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_hide=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34263912197219011)
,p_db_column_name=>'DERULID'
,p_display_order=>14
,p_column_identifier=>'G'
,p_column_label=>'Derulid'
,p_column_type=>'NUMBER'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34264074079219012)
,p_db_column_name=>'DERNAME'
,p_display_order=>24
,p_column_identifier=>'H'
,p_column_label=>'Dername'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34264142002219013)
,p_db_column_name=>'DEDESCR'
,p_display_order=>34
,p_column_identifier=>'I'
,p_column_label=>'Dedescr'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34264286026219014)
,p_db_column_name=>'DERLCID'
,p_display_order=>44
,p_column_identifier=>'J'
,p_column_label=>'Derlcid'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34264496117219016)
,p_db_column_name=>'DESTATE'
,p_display_order=>64
,p_column_identifier=>'L'
,p_column_label=>'Destate'
,p_allow_sorting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_pivot=>'N'
,p_column_type=>'CLOB'
,p_column_alignment=>'CENTER'
,p_rpt_show_filter_lov=>'N'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(34264586240219017)
,p_db_column_name=>'DETYPID'
,p_display_order=>74
,p_column_identifier=>'M'
,p_column_label=>'Detypid'
,p_column_type=>'NUMBER'
,p_display_text_as=>'HIDDEN'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39430297642667340)
,p_db_column_name=>'DETYPDE'
,p_display_order=>94
,p_column_identifier=>'O'
,p_column_label=>'Detypde'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(15170482629724645)
,p_db_column_name=>'DEPROFL_ID'
,p_display_order=>104
,p_column_identifier=>'P'
,p_column_label=>'Deprofl Id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'RIGHT'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(1481925036115434656)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'343563'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>100
,p_report_columns=>'CB:DERNAME:DEDESCR:DESTATE:DETYPDE::DEPROFL_ID'
,p_sort_column_1=>'APPLICATION_ID'
,p_sort_direction_1=>'ASC'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34357473452804110)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(1481924344399434654)
,p_button_name=>'ADD_STATEMENTS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Add Statements'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34264646013219018)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(1481924344399434654)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Cancel'
,p_button_position=>'BELOW_BOX'
,p_button_alignment=>'LEFT'
,p_button_redirect_url=>'f?p=&APP_ID.:205:&SESSION.::&DEBUG.:RP::'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(34356741411804110)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(1481924344399434654)
,p_button_name=>'RESET_REPORT'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29008925477135126)
,p_button_image_alt=>'Reset'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:217:&SESSION.::&DEBUG.:28,RIR::'
,p_icon_css_classes=>'fa-undo-alt'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(34359630393804108)
,p_branch_name=>'back to statements'
,p_branch_action=>'f?p=&APP_ID.:205:&SESSION.::&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_api.create_page_item(
 p_id=>wwv_flow_api.id(15170560586724646)
,p_name=>'P217_DEPROFL_ID'
,p_item_sequence=>10
,p_item_plug_id=>wwv_flow_api.id(1481924344399434654)
,p_display_as=>'NATIVE_HIDDEN'
,p_attribute_01=>'Y'
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(34358877650804109)
,p_validation_name=>'Must select statement'
,p_validation_sequence=>10
,p_validation=>'wwv_flow.g_f01.count >= 1'
,p_validation_type=>'PLSQL_EXPRESSION'
,p_error_message=>'Please check an statement to add.'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(34359181734804109)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'add rules'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    f number;',
'    g number := :P217_DEPROFL_ID;',
'begin',
'    for i in 1..wwv_flow.g_f01.count loop',
'        f := wwv_flow.g_f01(i);',
'        insert into drprrul(deprofl_id, derulid, decredt, decreus) values (g,f,sysdate,:APP_USER);',
'    end loop;',
'end;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Statement(s) added'
);
end;
/
