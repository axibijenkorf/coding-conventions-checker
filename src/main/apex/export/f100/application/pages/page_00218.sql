prompt --application/pages/page_00218
begin
wwv_flow_api.create_page(
 p_id=>218
,p_user_interface_id=>wwv_flow_api.id(29030956062135106)
,p_name=>'Add Applications'
,p_step_title=>'Add Applications'
,p_reload_on_submit=>'A'
,p_warn_on_unsaved_changes=>'N'
,p_step_sub_title=>'Add Applications'
,p_step_sub_title_type=>'TEXT_WITH_SUBSTITUTIONS'
,p_first_item=>'AUTO_FIRST_ITEM'
,p_autocomplete_on_off=>'ON'
,p_javascript_code=>wwv_flow_string.join(wwv_flow_t_varchar2(
'function checkallf01() {',
'  if ($("#checkall").is('':checked'')) { ',
'    $("input[name=f01]").prop("checked", true);',
'  } else { ',
'    $("input[name=f01]").prop("checked", false);',
'  }',
'}'))
,p_page_template_options=>'#DEFAULT#'
,p_protection_level=>'C'
,p_help_text=>'<p>To track standards for application(s) check the application and click the <b>Add Applications</b> button.</p>'
,p_last_updated_by=>'KMRT'
,p_last_upd_yyyymmddhh24miss=>'20210810122013'
);
wwv_flow_api.create_page_plug(
 p_id=>wwv_flow_api.id(1521289983605535955)
,p_plug_name=>'Add Applications'
,p_region_template_options=>'#DEFAULT#:js-showMaximizeButton'
,p_plug_template=>wwv_flow_api.id(28955876667135161)
,p_plug_display_sequence=>10
,p_plug_display_point=>'BODY'
,p_query_type=>'SQL'
,p_plug_source=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select distinct apex_item.checkbox(1,A.APPLICATION_ID) cb,',
'       A.APPLICATION_ID,',
'       A.APPLICATION_NAME',
'      ',
'from APEX_APPLICATIONS A',
'where (A.APPLICATION_ID, :P215_DEPROID) not in (select DEAPPID, DEPROID from DRPRAPP)',
'',
'',
''))
,p_plug_source_type=>'NATIVE_IR'
,p_plug_query_options=>'DERIVED_REPORT_COLUMNS'
);
wwv_flow_api.create_worksheet(
 p_id=>wwv_flow_api.id(1521290182654535956)
,p_name=>'Add Applications'
,p_max_row_count=>'1000000'
,p_max_row_count_message=>'The maximum row count for this report is #MAX_ROW_COUNT# rows.  Please apply a filter to reduce the number of records in your query.'
,p_no_data_found_message=>'No data found.'
,p_allow_save_rpt_public=>'Y'
,p_allow_report_categories=>'N'
,p_show_nulls_as=>'-'
,p_pagination_type=>'ROWS_X_TO_Y'
,p_pagination_display_pos=>'BOTTOM_RIGHT'
,p_report_list_mode=>'TABS'
,p_show_detail_link=>'N'
,p_show_notify=>'Y'
,p_show_calendar=>'N'
,p_download_formats=>'CSV:HTML:EMAIL'
,p_allow_exclude_null_values=>'N'
,p_allow_hide_extra_columns=>'N'
,p_icon_view_columns_per_row=>1
,p_owner=>'MIKE'
,p_internal_uid=>1521290182654535956
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39369168635101293)
,p_db_column_name=>'CB'
,p_display_order=>4
,p_column_identifier=>'D'
,p_column_label=>'<input type="checkbox" onclick="checkallf01();" id="checkall" />'
,p_allow_sorting=>'N'
,p_allow_filtering=>'N'
,p_allow_highlighting=>'N'
,p_allow_ctrl_breaks=>'N'
,p_allow_aggregations=>'N'
,p_allow_computations=>'N'
,p_allow_charting=>'N'
,p_allow_group_by=>'N'
,p_allow_hide=>'N'
,p_column_type=>'STRING'
,p_display_text_as=>'WITHOUT_MODIFICATION'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39258453876502746)
,p_db_column_name=>'APPLICATION_ID'
,p_display_order=>14
,p_column_identifier=>'N'
,p_column_label=>'Application Id'
,p_column_type=>'NUMBER'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_column(
 p_id=>wwv_flow_api.id(39258549187502747)
,p_db_column_name=>'APPLICATION_NAME'
,p_display_order=>24
,p_column_identifier=>'O'
,p_column_label=>'Application Name'
,p_column_type=>'STRING'
,p_column_alignment=>'CENTER'
);
wwv_flow_api.create_worksheet_rpt(
 p_id=>wwv_flow_api.id(1521290675321535957)
,p_application_user=>'APXWS_DEFAULT'
,p_report_seq=>10
,p_report_alias=>'393695'
,p_status=>'PUBLIC'
,p_is_default=>'Y'
,p_display_rows=>100
,p_report_columns=>'CB:APPLICATION_ID:APPLICATION_NAME'
,p_sort_column_1=>'APPLICATION_ID'
,p_sort_direction_1=>'ASC'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(39370779918101290)
,p_button_sequence=>10
,p_button_plug_id=>wwv_flow_api.id(1521289983605535955)
,p_button_name=>'CANCEL'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_image_alt=>'Cancel'
,p_button_position=>'BELOW_BOX'
,p_button_redirect_url=>'f?p=&APP_ID.:215:&SESSION.::&DEBUG.:RP::'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(39370366429101290)
,p_button_sequence=>30
,p_button_plug_id=>wwv_flow_api.id(1521289983605535955)
,p_button_name=>'ADD_APPLICATIONS'
,p_button_action=>'SUBMIT'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29009020462135125)
,p_button_is_hot=>'Y'
,p_button_image_alt=>'Add Applications'
,p_button_position=>'BELOW_BOX'
);
wwv_flow_api.create_page_button(
 p_id=>wwv_flow_api.id(39369976710101291)
,p_button_sequence=>20
,p_button_plug_id=>wwv_flow_api.id(1521289983605535955)
,p_button_name=>'RESET_REPORT'
,p_button_action=>'REDIRECT_PAGE'
,p_button_template_options=>'#DEFAULT#'
,p_button_template_id=>wwv_flow_api.id(29008925477135126)
,p_button_image_alt=>'Reset'
,p_button_position=>'RIGHT_OF_IR_SEARCH_BAR'
,p_button_redirect_url=>'f?p=&APP_ID.:218:&SESSION.::&DEBUG.:28,RIR::'
,p_icon_css_classes=>'fa-undo-alt'
);
wwv_flow_api.create_page_branch(
 p_id=>wwv_flow_api.id(39372034233101286)
,p_branch_name=>'back to applications'
,p_branch_action=>'f?p=&APP_ID.:215:&SESSION.::&DEBUG.:RP::&success_msg=#SUCCESS_MSG#'
,p_branch_point=>'AFTER_PROCESSING'
,p_branch_type=>'REDIRECT_URL'
,p_branch_sequence=>10
);
wwv_flow_api.create_page_validation(
 p_id=>wwv_flow_api.id(39371275384101287)
,p_validation_name=>'Must select application'
,p_validation_sequence=>10
,p_validation=>'wwv_flow.g_f01.count >= 1'
,p_validation_type=>'PLSQL_EXPRESSION'
,p_error_message=>'Please check an application to add.'
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
);
wwv_flow_api.create_page_process(
 p_id=>wwv_flow_api.id(39371593834101287)
,p_process_sequence=>10
,p_process_point=>'AFTER_SUBMIT'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'add applications'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'declare',
'    f number;',
'    g number := :P215_DEPROID;',
'begin',
'    for i in 1..wwv_flow.g_f01.count loop',
'        f := wwv_flow.g_f01(i);',
'        insert into drprapp(deproid, deappid, decredt, decreus) values (g,f,sysdate, v(''APP_USER''));',
'    end loop;',
'end;'))
,p_error_display_location=>'INLINE_IN_NOTIFICATION'
,p_process_success_message=>'Application(s) added'
);
end;
/
