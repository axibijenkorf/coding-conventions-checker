set define off verify off feedback off
whenever sqlerror exit sql.sqlcode rollback
begin
wwv_flow_api.import_begin (
 p_version_yyyy_mm_dd=>'2018.04.04'
,p_release=>'18.1.0.00.45'
,p_default_workspace_id=>23268441850672677
,p_default_application_id=>100
,p_default_owner=>'AXICCCDEV'
);
end;
/
 
prompt APPLICATION 100 - Coding Conventions Checker


