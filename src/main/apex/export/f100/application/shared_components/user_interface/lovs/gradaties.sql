prompt --application/shared_components/user_interface/lovs/gradaties
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(29140166001654578)
,p_lov_name=>'GRADATIES'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select     degraid, dedescr',
'from       drgrada',
'order by   dedescr'))
);
end;
/
