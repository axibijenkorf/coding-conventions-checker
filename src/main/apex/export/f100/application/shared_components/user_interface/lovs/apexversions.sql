prompt --application/shared_components/user_interface/lovs/apexversions
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(29140382425652695)
,p_lov_name=>'APEXVERSIONS'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select     deapeid, deapexv',
'from       drapexv',
'order by   deapexv'))
);
end;
/
