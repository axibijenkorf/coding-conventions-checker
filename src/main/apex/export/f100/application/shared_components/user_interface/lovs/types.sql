prompt --application/shared_components/user_interface/lovs/types
begin
wwv_flow_api.create_list_of_values(
 p_id=>wwv_flow_api.id(29140744167648633)
,p_lov_name=>'TYPES'
,p_lov_query=>wwv_flow_string.join(wwv_flow_t_varchar2(
'select     detypid, dedescr',
'from       drrtype',
'order by   dedescr'))
);
end;
/
