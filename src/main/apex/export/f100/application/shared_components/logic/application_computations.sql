prompt --application/shared_components/logic/application_computations
begin
wwv_flow_api.create_flow_computation(
 p_id=>wwv_flow_api.id(40214425537634817)
,p_computation_sequence=>10
,p_computation_item=>'APP_PROJECT_NAME'
,p_computation_point=>'BEFORE_HEADER'
,p_computation_type=>'QUERY'
,p_computation_processed=>'REPLACE_EXISTING'
,p_computation=>'select dedescr from drproje where deproid = :APP_PROJECT'
);
end;
/
