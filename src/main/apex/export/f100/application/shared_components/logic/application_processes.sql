prompt --application/shared_components/logic/application_processes
begin
wwv_flow_api.create_flow_process(
 p_id=>wwv_flow_api.id(40062561943021734)
,p_process_sequence=>1
,p_process_point=>'ON_NEW_INSTANCE'
,p_process_type=>'NATIVE_PLSQL'
,p_process_name=>'After login'
,p_process_sql_clob=>wwv_flow_string.join(wwv_flow_t_varchar2(
'-- If the session is started as an APEX developer session, use the "fake" language',
'if wwv_flow.g_edit_cookie_session_id is not null then',
'    apex_util.set_session_lang(p_lang => ''af'');',
'else',
'    apex_util.set_session_lang(p_lang => ''en'');',
'end if;'))
);
end;
/
