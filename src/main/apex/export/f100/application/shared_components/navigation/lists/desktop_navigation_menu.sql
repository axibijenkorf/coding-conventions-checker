prompt --application/shared_components/navigation/lists/desktop_navigation_menu
begin
wwv_flow_api.create_list(
 p_id=>wwv_flow_api.id(28910865468135213)
,p_name=>'Desktop Navigation Menu'
,p_list_status=>'PUBLIC'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(29041044846135029)
,p_list_item_display_sequence=>10
,p_list_item_link_text=>'Home'
,p_list_item_link_target=>'f?p=&APP_ID.:1:&APP_SESSION.::&DEBUG.:'
,p_list_item_icon=>'fa-home'
,p_list_item_current_type=>'TARGET_PAGE'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(29073829068826705)
,p_list_item_display_sequence=>30
,p_list_item_link_text=>'Manage Apex Versions'
,p_list_item_link_target=>'f?p=&APP_ID.:210:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'fa-apex'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'210,211'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(34023202441587092)
,p_list_item_display_sequence=>50
,p_list_item_link_text=>'Manage Rules'
,p_list_item_link_target=>'f?p=&APP_ID.:200:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'fa-check'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'200,201'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(42489430525122786)
,p_list_item_display_sequence=>55
,p_list_item_link_text=>'Manage profiles'
,p_list_item_link_target=>'f?p=&APP_ID.:204:&SESSION.::&DEBUG.::::'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'204,205'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(29090647822816867)
,p_list_item_display_sequence=>60
,p_list_item_link_text=>'Manage Projects'
,p_list_item_link_target=>'f?p=&APP_ID.:214:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'fa-folders'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'214,215'
);
wwv_flow_api.create_list_item(
 p_id=>wwv_flow_api.id(34671044905337180)
,p_list_item_display_sequence=>70
,p_list_item_link_text=>'Results'
,p_list_item_link_target=>'f?p=&APP_ID.:300:&SESSION.::&DEBUG.::::'
,p_list_item_icon=>'fa-tasks'
,p_list_item_current_type=>'COLON_DELIMITED_PAGE_LIST'
,p_list_item_current_for_pages=>'300'
);
end;
/
