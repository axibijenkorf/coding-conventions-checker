#!/bin/sh
set +x

SCHEMA=$1
PASSWORD=$2
URL=$3

cd src/main/database/logger/

RETVAL=`sqlplus -silent "$SCHEMA/$PASSWORD@$URL" <<EOF
SET PAGESIZE 0 FEEDBACK OFF VERIFY OFF HEADING OFF ECHO OFF
    select      *
    from        user_objects
    where       object_name = 'LOGGER';
EXIT;
EOF`

if [ -z "$RETVAL" ]; then
  sqlplus "$SCHEMA/$PASSWORD@$URL" << EOF

    @"logger_install.sql"
    @"logger_grants.sql"
    /
    commit;

    exit
EOF
else
    echo "Logger already installed"
fi