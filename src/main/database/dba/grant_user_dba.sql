DEFINE DBA_USER = devadm ;

-- QUOTAS
alter user &DBA_USER quota unlimited on users;
grant unlimited tablespace to &DBA_USER with admin option;
grant alter user to &DBA_USER with admin option;

-- ROLES
grant connect to &DBA_USER with admin option;
grant resource to &DBA_USER with admin option;
grant select_catalog_role to &DBA_USER with admin option;
grant scheduler_admin to &DBA_USER with admin option;
grant aq_administrator_role to &DBA_USER with admin option;
grant debug connect session to &DBA_USER with admin option;
grant debug any procedure to &DBA_USER with admin option;

-- SYSTEM GRANTS
grant execute any procedure to &DBA_USER with admin option;
grant create public synonym to &DBA_USER with admin option;
grant create any directory to &DBA_USER with admin option;
grant create external job to &DBA_USER with admin option;
grant create job to &DBA_USER with admin option;
grant drop any context to &DBA_USER with admin option;
grant create any context to &DBA_USER with admin option;
grant create dimension to &DBA_USER with admin option;
grant create indextype to &DBA_USER with admin option;
grant create operator to &DBA_USER with admin option;
grant create type to &DBA_USER with admin option;
grant create materialized view to &DBA_USER with admin option;
grant create any trigger to &DBA_USER with admin option;
grant create trigger to &DBA_USER with admin option;
grant create procedure to &DBA_USER with admin option;
grant create sequence to &DBA_USER with admin option;
grant create view to &DBA_USER with admin option;
grant create any synonym to &DBA_USER with admin option;
grant create synonym to &DBA_USER with admin option;
grant create cluster to &DBA_USER with admin option;
grant create table to &DBA_USER with admin option;
grant create user to &DBA_USER with admin option;
grant unlimited tablespace to &DBA_USER with admin option;
grant create session to &DBA_USER with admin option;
grant alter session to &DBA_USER with admin option;
grant create any directory to &DBA_USER with admin option;
grant drop any directory to &DBA_USER with admin option;
grant datapump_exp_full_database to &DBA_USER with admin option;
grant datapump_imp_full_database to &DBA_USER with admin option;


-- GRANTS
grant select on sys.dba_source to &DBA_USER  with grant option;
grant select on sys.dba_tables to &DBA_USER  with grant option;
grant select on sys.dba_objects to &DBA_USER with grant option;
grant select on sys.dba_constraints to &DBA_USER  with grant option;
grant select on sys.dba_cons_columns to &DBA_USER  with grant option;
grant select on sys.dba_tab_columns to &DBA_USER with grant option;
grant select on sys.dba_identifiers to &DBA_USER  with grant option;
grant execute on sys.dbms_lock to &DBA_USER with grant option;
grant select on sys.dba_context to &DBA_USER  with grant option;
grant select on sys.v_$globalcontext to &DBA_USER with grant option;
grant execute on sys.dbms_crypto to &DBA_USER with grant option;
grant select on dba_context to &DBA_USER with grant option;
grant select on dba_constraints to &DBA_USER with grant option;
grant select on sys.dba_triggers to &DBA_USER with grant option;
grant select on sys.dba_objects to &DBA_USER with grant option;
grant select on sys.dba_tab_privs to &DBA_USER with grant option;
grant select on sys.dba_tab_columns to &DBA_USER with grant option;
grant select on sys.dba_col_comments to &DBA_USER with grant option;
grant select on v_$session to &DBA_USER with grant option;
grant apex_administrator_role to &DBA_USER with admin option;
grant execute on sys.dbms_aq to &DBA_USER with grant option;
grant execute on sys.dbms_aqadm to &DBA_USER with grant option;
grant select on sys.dba_synonyms to &DBA_USER with grant option;
grant execute on sys.dbms_network_acl_admin to &DBA_USER with grant option;
grant select on sys.dba_network_acls to &DBA_USER;
grant select on sys.dba_network_acl_privileges to &DBA_USER;
grant execute on utl_file to &DBA_USER with grant option;

-- audit tablespace aanmaken
grant create tablespace to &DBA_USER;
grant drop tablespace to &DBA_USER;
grant manage tablespace to &DBA_USER;