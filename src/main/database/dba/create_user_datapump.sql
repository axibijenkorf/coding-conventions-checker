begin
    execute immediate
    q'[
        create user ${datapump_user} identified by ${datapump_password}
        default tablespace users
        temporary tablespace temp
    ]';
exception
    when others then
        if sqlcode = -1920 then
            null;  -- user exists
        else
            raise;
        end if;
end;
/

