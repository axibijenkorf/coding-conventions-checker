-- QUOTAS
alter user ${tools_user} quota unlimited on users;

-- ROLES
grant connect to ${tools_user};
grant resource to ${tools_user};

-- GRANTS
grant create any context to ${tools_user};
grant scheduler_admin to ${tools_user};
grant select_catalog_role to ${tools_user};
alter user ${tools_user} default role all;
grant create cluster to ${tools_user};
grant create dimension to ${tools_user};
grant create external job to ${tools_user};
grant create indextype to ${tools_user};
grant create job to ${tools_user};
grant create materialized view to ${tools_user};
grant create operator to ${tools_user};
grant create procedure to ${tools_user};
grant create sequence to ${tools_user};
grant create session to ${tools_user};
grant create any synonym to ${tools_user};
grant create table to ${tools_user};
grant create trigger to ${tools_user};
grant create type to ${tools_user};
grant create view to ${tools_user};
grant apex_administrator_role to ${tools_user};
grant select on dba_objects to ${tools_user};
grant select on dba_identifiers to ${tools_user};
grant execute on  sys.dbms_network_acl_admin to ${tools_user};
