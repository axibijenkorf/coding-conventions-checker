declare
v_count     number;
v_stmnt     varchar2(4000);
begin

select  count(*)
into    v_count
from    all_users
where   username = '${prj_user}';

if v_count = 0 then
    v_stmnt := q'[CREATE USER ${prj_user} IDENTIFIED BY "${prj_password}"
    DEFAULT TABLESPACE USERS
    TEMPORARY TABLESPACE TEMP]';
    execute immediate v_stmnt;
end if;

end;
/
