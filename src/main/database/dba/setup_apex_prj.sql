declare
    v_count     number;
    c_wspid     constant number(30) := ${apex_workspace_id};    -- Workspace ID
begin     
    select  count(*) into v_count
    from    apex_workspace_schemas
    where   workspace_id = c_wspid
    and     schema = upper('${prj_user}');

    if v_count = 0 then

        apex_util.set_security_group_id(p_security_group_id => c_wspid);

        apex_instance_admin.add_schema(
            p_workspace    => '${apex_workspace}',
            p_schema       => upper('${prj_user}')
        );

    end if;
end;
/