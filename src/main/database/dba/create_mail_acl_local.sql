DECLARE
  l_acl       VARCHAR2(100) := 'Mail';
  l_desc      VARCHAR2(100) := 'Mail';
  l_principal VARCHAR2(30)  := '${apex_user}'; -- upper case
  --l_prjusr    VARCHAR2(30)  := '${prj_user}'; -- upper case
  l_host      VARCHAR2(100) := 'localhost';
BEGIN
    DBMS_NETWORK_ACL_ADMIN.append_host_ace (
    host        => l_host,
    lower_port  => 1025,
    upper_port  => 1025,
    ace         => xs$ace_type(privilege_list => xs$name_list('connect'),
                              principal_name => l_principal,
                              principal_type => xs_acl.ptype_db));

    -- DBMS_NETWORK_ACL_ADMIN.append_host_ace (
    -- host        => l_host,
    -- lower_port  => 1025,
    -- upper_port  => 1025,
    -- ace         => xs$ace_type(privilege_list => xs$name_list('connect'),
    --                           principal_name => l_prjusr,
    --                           principal_type => xs_acl.ptype_db));

  COMMIT;
END;
/

begin
    apex_instance_admin.set_parameter('SMTP_HOST_ADDRESS','localhost');
    apex_instance_admin.set_parameter('SMTP_HOST_PORT', 1025);
end;
/