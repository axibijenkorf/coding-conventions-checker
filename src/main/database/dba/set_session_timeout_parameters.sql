begin
    apex_instance_admin.set_workspace_parameter('${apex_workspace}', 'MAX_SESSION_IDLE_SEC', ${max_session_idle_sec});
    apex_instance_admin.set_workspace_parameter('${apex_workspace}', 'MAX_SESSION_LENGTH_SEC', ${max_session_length_sec});
end;
/