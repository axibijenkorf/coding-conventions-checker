-- QUOTAS
alter user ${prj_user} quota unlimited on users
/

-- ROLES
grant connect to ${prj_user}
/
grant resource to ${prj_user}
/

-- GRANTS
grant scheduler_admin to ${prj_user}
/
grant select_catalog_role to ${prj_user}
/
alter user ${prj_user} default role all
/
grant create cluster to ${prj_user}
/
grant create dimension to ${prj_user}
/
grant create external job to ${prj_user}
/
grant create indextype to ${prj_user}
/
grant create job to ${prj_user}
/
grant create materialized view to ${prj_user}
/
grant create operator to ${prj_user}
/
grant create procedure to ${prj_user}
/
grant create sequence to ${prj_user}
/
grant create session to ${prj_user}
/
grant create any synonym to ${prj_user}
/
grant create table to ${prj_user}
/
grant create trigger to ${prj_user}
/
grant create type to ${prj_user}
/
grant create view to ${prj_user}
/
grant execute on dbms_lock to ${prj_user}
/
grant aq_administrator_role to ${prj_user}
/
grant execute on dbms_aq to ${prj_user}
/
grant execute on dbms_aqadm to ${prj_user}
/
begin
    dbms_aqadm.grant_system_privilege('ENQUEUE_ANY', '${prj_user}');
    dbms_aqadm.grant_system_privilege('DEQUEUE_ANY', '${prj_user}');
end;
/
grant select on v$session to ${prj_user}
/