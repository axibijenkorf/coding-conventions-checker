declare
    c_wspid     constant number(30) := ${apex_workspace_id};    -- Workspace ID
    v_tlusr     varchar2(50) := '${tools_user}';                -- Tools schema
    v_count     pls_integer;                                    -- Tools user(s) found
begin
    apex_instance_admin.add_workspace (
        p_workspace_id       => c_wspid,
        p_workspace          => '${apex_workspace}',
        p_primary_schema     => '${prj_user}'
    );

    select      count(*)
    into        v_count
    from        all_users
    where       username = '${tools_user}';

    if v_count > 0 then
        apex_instance_admin.add_schema(
            p_workspace    => '${apex_workspace}',
            p_schema       => '${tools_user}'
        );
    end if;

    apex_util.set_security_group_id(c_wspid);

    apex_util.create_user(
        p_user_name                     => 'ADMIN',
        p_first_name                    => 'AXI',
        p_last_name                     => 'Administrator',
        p_web_password                  => 'Dune2019!',
        p_developer_privs               => 'ADMIN:CREATE:DATA_LOADER:EDIT:HELP:MONITOR:SQL',
        p_change_password_on_first_use  => 'N'
    );
end;
/