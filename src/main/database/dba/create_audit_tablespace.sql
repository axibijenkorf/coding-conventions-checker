declare
    e_inuse exception;
    pragma exception_init(e_inuse, -1543);
begin
    execute immediate 'create tablespace ${audit_tablespace} ' ||
        'datafile ''${dbf_folder}${audit_tablespace}01.dbf'' ' ||
        'size 10m reuse autoextend on';
exception
    when e_inuse then
        dbms_output.put_line('Tablespace already exists - we will reuse it');
    when others then
        raise;
end;
/