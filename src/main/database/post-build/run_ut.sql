declare
   v_failed_unit_tests varchar2(32767);
   v_flag varchar2(1) := 'N';
   v_coverage  varchar2(32767);
begin

    for rec in (select * from  ut.run()) loop
        if rec.column_value ='Failures:' then
            v_flag := 'Y';
        end if;
        if v_flag ='Y' then
          v_failed_unit_tests := substr(v_failed_unit_tests || rec.column_value ||chr(10),1,32767);
        end if;
    end loop;

    if v_failed_unit_tests is not null then
        v_failed_unit_tests := substr(chr(10)||lpad('-',72,'-')||chr(10)||v_failed_unit_tests,1,32767);
        v_failed_unit_tests := substr(v_failed_unit_tests ||chr(10)||lpad('-',72,'-'),1,32767);
        raise_application_error(-20000, v_failed_unit_tests || chr(10));
    end if;

    if '${code_coverage}' is not null then
        for rec in (select column_value from ut.run(':all',ut_coverage_html_reporter())
                    where rownum < 3) loop
            v_coverage := rec.column_value;
        end loop;
        v_coverage := substr(v_coverage,instr(v_coverage,'covered_percent',1,1)+25, 100);
        v_coverage := substr(v_coverage,instr(v_coverage,'>',1,1)+1,instr(v_coverage,'<',1,1)-instr(v_coverage,'>',1,1)-2);
        dbms_output.put_line ('behaalde code coverage: '||v_coverage);
        dbms_output.put_line ('te behalen code coverage: '||'${code_coverage}');
        if v_coverage < to_number('${code_coverage}') then
            v_failed_unit_tests := substr(chr(10)||lpad('-',72,'-')||chr(10)||'!!! behaalde code coverage: '||v_coverage ||',  te behalen: '||'${code_coverage}',1,32767);
            v_failed_unit_tests := substr(v_failed_unit_tests ||chr(10)||lpad('-',72,'-'),1,32767);
            raise_application_error(-20000,v_failed_unit_tests|| chr(10) );
        end if;
    end if;
end;
/