declare
  v_compilation_errors varchar2(32767);
  v_total   number;
  v_count   number;
  cursor cur_invalid_objects is
      select    object_type,object_name,
                listagg(
                    nvl2(
                        er.line,
                        '    =>('||lpad(er.line,5)||','||lpad(er.position,3)||') '||
                        --only take the first line of a multiline error message
                        substr(
                            text,
                            0,
                            decode(
                                instr(text,chr(10)),
                                0,
                                length(text),
                                instr(text,chr(10))-1
                            )
                        ),
                        null
                    ),
                    chr(10)
                ) within group (order by er.sequence) as err_detail,
                count(*) over () as Total
      from      user_objects ob,user_errors er
      where     status = 'INVALID'
      and       er.attribute in ('ERROR', 'WARNING')
      and       er.name(+) = ob.object_name
      and       er.type(+) = ob.object_type
      group by  object_type,object_name
      order by  object_type,object_name;
begin
    v_count := 1;
    for rec_invalid_object in cur_invalid_objects loop
        v_total := rec_invalid_object.total;
        v_compilation_errors := v_compilation_errors || chr(10) || '  ' || rpad(rec_invalid_object.object_type, 20) || rec_invalid_object.object_name;

         --make sure there is still some room to add the detailed error message
        if  rec_invalid_object.err_detail is not null and
        (length(v_compilation_errors) + length(rec_invalid_object.err_detail) +((v_total - v_count)*80)) < 2000  then
            v_compilation_errors := v_compilation_errors || chr(10) || rec_invalid_object.err_detail;
        end if;
        v_count := v_count+1;
    end loop;

    if v_compilation_errors is not null then
        v_compilation_errors := chr(10) || chr(10) || v_total||' Invalid objects in schema ' || user || ':' || v_compilation_errors;
        raise_application_error(-20000, v_compilation_errors || chr(10));
    end if;
end;
/