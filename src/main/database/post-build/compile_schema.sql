begin
    dbms_utility.compile_schema(
    schema          => user,
    compile_all     => false,
    reuse_settings  => true
    );
end;
/