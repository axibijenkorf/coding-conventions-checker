create table drrtype
(detypid    number(15,0)    not null
,dedescr    varchar2(250)    not null
,decredt    date            not null
,decreus    varchar2(20)    not null
,dewyzdt    date
,dewyzus    varchar2(20)
);

comment on column drrtype.detypid is 'ID type';
comment on column drrtype.dedescr is 'Omschrijving';
comment on column drrtype.decredt is 'Creatie datum';
comment on column drrtype.decreus is 'Creatie user';
comment on column drrtype.dewyzdt is 'Datum laatste wijziging';
comment on column drrtype.dewyzus is 'User laatste wijziging';
comment on table drrtype is 'Types';
