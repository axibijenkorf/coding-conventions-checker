create table drapexv
(deapeid    number(15,0)    not null
,deapexv    varchar2(15)    not null
,deapenr    number(15,1)    not null
,decredt    date            not null
,decreus    varchar2(20)    not null
,dewyzdt    date
,dewyzus    varchar2(20)
);


comment on column drapexv.deapeid is 'ID Apex versie';
comment on column drapexv.deapexv is 'Omschrijving Apex versie';
comment on column drapexv.deapenr is 'Apexnummer';
comment on column drapexv.decredt is 'Creatie datum';
comment on column drapexv.decreus is 'Creatie user';
comment on column drapexv.dewyzdt is 'Datum laatste wijziging';
comment on column drapexv.dewyzus is 'Gebruiker laatste wijziging';
comment on table drapexv is 'Tabel voor Apexversies';
