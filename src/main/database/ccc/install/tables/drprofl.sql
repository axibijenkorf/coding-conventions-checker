create table drprofl(
    deprofl_id  number(30,0)
        default on null sqprofl.nextval,
    deprfnm     varchar2(250) not null,
    decredt     date not null,
    decreus     varchar2(20) not null,
    dewyzdt     date,
    dewyzus     varchar2(20)
);

comment on column drprofl.deprofl_id is 'Profiel ID';
comment on column drprofl.deprfnm is 'Profiel naam';
comment on column drprofl.decredt is 'Creatie datum';
comment on column drprofl.decreus is 'Creatie user';
comment on column drprofl.dewyzdt is 'Datum laatste wijziging';
comment on column drprofl.dewyzus is 'User laatste wijziging';
comment on table drprofl is 'Profielen';