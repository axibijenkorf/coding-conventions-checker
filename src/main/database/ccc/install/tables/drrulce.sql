
create table drrulce
(derlcid    number(15,0)    not null
,derulid    number(15,0)    not null
,deapeid    number(15,0)    not null
,derlink    varchar2(64)
,deapbiv    varchar2(30)    not null
,destate    clob            not null
,decredt    date            not null
,decreus    varchar2(20)    not null
,dewyzdt    date
,dewyzus    varchar2(20)
);

comment on column drrulce.derlcid is 'ID regelcode';
comment on column drrulce.derulid is 'ID regel';
comment on column drrulce.deapeid is 'ID Apex versie';
comment on column drrulce.derlink is 'Link type';
comment on column drrulce.deapbiv is 'Application Bind Variable';
comment on column drrulce.destate is 'Statement';
comment on column drrulce.decredt is 'Creatie datum';
comment on column drrulce.decreus is 'Creatie user';
comment on column drrulce.dewyzdt is 'Datum laatste wijziging';
comment on column drrulce.dewyzus is 'User laatste wijziging';
comment on table drrulce is 'Codes van de regels';