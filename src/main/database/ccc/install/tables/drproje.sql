create table drproje
(deproid    number(15,0)    not null
,deapeid    number(15,0)    not null
,dedescr    varchar2(30)    not null
,deprofl_id number(30,0)
,decredt    date            not null
,decreus    varchar2(20)    not null
,dewyzdt    date
,dewyzus    varchar2(20)
);

comment on column drproje.deproid is 'ID project';
comment on column drproje.deapeid is 'ID Apex versie';
comment on column drproje.dedescr is 'Omschrijving van het project';
comment on column drproje.deprofl_id is 'Profiel ID';
comment on column drproje.decredt is 'Creatie datum';
comment on column drproje.decreus is 'Creatie user';
comment on column drproje.dewyzdt is 'Datum laatste wijziging';
comment on column drproje.dewyzus is 'User laatste wijziging';
comment on table drproje is 'Projecten';
