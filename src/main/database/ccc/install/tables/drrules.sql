
create table drrules
(derulid    number(15,0)    not null
,dername    varchar2(250)   not null
,degraid    number(15,0)    not null
,detypid    number(15,0)    not null
,dedescr    varchar2(250)   not null
,decredt    date            not null
,decreus    varchar2(20)    not null
,dewyzdt    date
,dewyzus    varchar2(20)
);

comment on column drrules.derulid is 'ID Regel';
comment on column drrules.dername is 'Regelnaam';
comment on column drrules.degraid is 'ID gradatie';
comment on column drrules.dedescr is 'Omschrijving regel';
comment on column drrules.detypid is 'ID Type';
comment on column drrules.decredt is 'Creatie datum';
comment on column drrules.decreus is 'Creatie user';
comment on column drrules.dewyzdt is 'Datum laatste wijziging';
comment on column drrules.dewyzus is 'User laatste wijziging';
comment on table drrules is 'Tabel voor de regels';
