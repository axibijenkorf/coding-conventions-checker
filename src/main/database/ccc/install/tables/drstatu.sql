
create table drstatu
(destaid    number(15,0)    not null
,dedescr    varchar2(250)    not null
,decredt    date            not null
,decreus    varchar2(20)    not null
,dewyzdt    date
,dewyzus    varchar2(20)
);

comment on column drstatu.destaid is 'ID status';
comment on column drstatu.dedescr is 'Omschrijving';
comment on column drstatu.decredt is 'Creatie datum';
comment on column drstatu.decreus is 'Creatie user';
comment on column drstatu.dewyzdt is 'Datum laatste wijziging';
comment on column drstatu.dewyzus is 'User laatste wijziging';
comment on table drstatu is 'Statussen';



