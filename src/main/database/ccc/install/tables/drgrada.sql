create table drgrada
(degraid    number(15,0)    not null
,dedescr    varchar2(250)    not null
,decredt    date            not null
,decreus    varchar2(20)    not null
,dewyzdt    date
,dewyzus    varchar2(20)
);

comment on column drgrada.degraid is 'ID gradatie';
comment on column drgrada.dedescr is 'Omschrijving';
comment on column drgrada.decredt is 'Creatie datum';
comment on column drgrada.decreus is 'Creatie user';
comment on column drgrada.dewyzdt is 'Datum laatste wijziging';
comment on column drgrada.dewyzus is 'User laatste wijziging';
comment on table drgrada is 'Gradaties';



