create table drprrul
(deprofl_id    number(15,0)    not null
,derulid    number(15,0)    not null
,decredt    date            not null
,decreus    varchar2(20)    not null
,dewyzdt    date
,dewyzus    varchar2(20)
);

comment on column drprrul.deprofl_id is 'Profiel ID';
comment on column drprrul.derulid is 'ID Regel';
comment on column drprrul.decredt is 'Creatie datum';
comment on column drprrul.decreus is 'Creatie user';
comment on column drprrul.dewyzdt is 'Datum laatste wijziging';
comment on column drprrul.dewyzus is 'User laatste wijziging';
comment on table drprrul is 'Regels per profiel';
