create table drresul
(derslid    number(15,0)    not null
,derunid    number(15,0)    not null
,deappid    number(15,0)    not null
,depagen    varchar2(60)
,deregio    varchar2(60)
,deitemn    varchar2(60)
,delabel    varchar2(60)
,derulid    number(15,0)    not null
,degraid    number(15,0)    not null
,detypid    number(15,0)    not null
,destaid    number(15,0)    not null
,deproid    number(15,0)    not null
,deident    varchar2(100)
,decredt    date            not null
,decreus    varchar2(20)    not null
,derlink    varchar2(64)
,derefid    number(30,0)
);

comment on column drresul.derslid is 'ID resultaten';
comment on column drresul.derunid is 'ID run';
comment on column drresul.deappid is 'ID applicatie';
comment on column drresul.depagen is 'Paginanaam';
comment on column drresul.deregio is 'Regionaam';
comment on column drresul.deitemn is 'Itemnaam';
comment on column drresul.delabel is 'Labelnaam';
comment on column drresul.derulid is 'ID regel';
comment on column drresul.degraid is 'ID gradatie';
comment on column drresul.detypid is 'ID type';
comment on column drresul.destaid is 'ID status';
comment on column drresul.deproid is 'ID project';
comment on column drresul.decredt is 'Unieke identifier van het APEX Object';
comment on column drresul.decredt is 'Creatie datum';
comment on column drresul.decreus is 'Creatie user';
comment on column drresul.derlink is 'Link';
comment on column drresul.derefid is 'Apex Item ID';
comment on table drresul is 'Resultaten van de run';
