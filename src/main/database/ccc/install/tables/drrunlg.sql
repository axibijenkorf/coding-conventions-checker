create table drrunlg
(derunid    number(15,0)    not null
,deproid    number(15,0)    not null
,debegdt    date            not null
,deenddt    date
,decreus    varchar2(20)    not null
);

comment on column drrunlg.derunid is 'ID Run';
comment on column drrunlg.deproid is 'ID Project';
comment on column drrunlg.debegdt is 'Start tijdstip';
comment on column drrunlg.deenddt is 'Eind tijdstip';
comment on column drrunlg.decreus is 'Creatie user';
comment on table drrunlg is 'Run log';
