create table drlinkt
(delinid    number(15,0)    not null
,dedescr    varchar2(250)    not null
,delinva    varchar2(250)    not null
,decredt    date            not null
,decreus    varchar2(20)    not null
,dewyzdt    date
,dewyzus    varchar2(20)
);

comment on column drlinkt.delinid is 'ID type';
comment on column drlinkt.dedescr is 'Omschrijving';
comment on column drlinkt.delinva is 'Return waarde';
comment on column drlinkt.decredt is 'Creatie datum';
comment on column drlinkt.decreus is 'Creatie user';
comment on column drlinkt.dewyzdt is 'Datum laatste wijziging';
comment on column drlinkt.dewyzus is 'User laatste wijziging';
comment on table drlinkt is 'Link Types';
