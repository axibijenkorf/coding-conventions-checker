create table drprapp
(deproid    number(15,0)    not null
,deappid    number(15,0)    not null
,decredt    date            not null
,decreus    varchar2(20)    not null
,dewyzdt    date
,dewyzus    varchar2(20)
);

comment on column drprapp.deproid is 'ID project';
comment on column drprapp.deappid is 'ID Application';
comment on column drprapp.decredt is 'Creatie datum';
comment on column drprapp.decreus is 'Creatie user';
comment on column drprapp.dewyzdt is 'Datum laatste wijziging';
comment on column drprapp.dewyzus is 'User laatste wijziging';
comment on table drprapp is 'Applicaties per project';
