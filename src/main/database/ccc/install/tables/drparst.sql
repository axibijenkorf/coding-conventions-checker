create table drparst(
    deparst_id  number(15,0)
        default on null sqparst.nextval,
    derlcid     number(15,0) not null,
    deprofl_id  number(15,0) not null,
    deparnm     varchar2(200) not null,
    deparvl     varchar2(200),
    dedescr     varchar2(4000),
    decredt     date not null,
    decreus     varchar2(20) not null,
    dewyzdt     date,
    dewyzus     varchar2(20)
);

comment on column drparst.deparst_id is 'Parameter per statement ID';
comment on column drparst.derlcid is 'Rule code ID';
comment on column drparst.deprofl_id is 'Profiel ID';
comment on column drparst.deparnm is 'Parameter naam';
comment on column drparst.deparvl is 'Parameter waarde';
comment on column drparst.decredt is 'Creatie datum';
comment on column drparst.decreus is 'Creatie user';
comment on column drparst.dewyzdt is 'Datum laatste wijziging';
comment on column drparst.dewyzus is 'User laatste wijziging';
comment on table drparst is 'Parameters per statement';