create table drrunst
(derstid    number(15,0)    not null
,derunid    number(15,0)    not null
,degraid    number(15,0)
,detypid    number(15,0)
,decount    number(15,0)
,decredt    date            not null
,decreus    varchar2(20)    not null
,dewyzdt    date
,dewyzus    varchar2(20)
);

comment on column drrunst.derstid is 'ID run status';
comment on column drrunst.derunid is 'ID run';
comment on column drrunst.degraid is 'ID gradatie';
comment on column drrunst.detypid is 'ID type';
comment on column drrunst.decount is 'Aantal';
comment on column drrunst.decredt is 'Creatie datum';
comment on column drrunst.decreus is 'Creatie user';
comment on column drrunst.dewyzdt is 'Datum laatste wijziging';
comment on column drrunst.dewyzus is 'User laatste wijziging';
comment on table drrunst is 'Status per run';
