create or replace trigger drparst_bir_audit
before insert on drparst
for each row
begin
    :new.decreus := coalesce(sys_context('APEX$SESSION', 'app_user'), sys_context('USERENV','OS_USER'), upper(user));
    :new.decredt := current_timestamp;
end;
/