create or replace trigger drprofl_bur_audit
before update on drprofl
for each row
begin
    :new.dewyzus := coalesce(sys_context('APEX$SESSION', 'app_user'), sys_context('USERENV','OS_USER'), upper(user));
    :new.dewyzdt := current_timestamp;
end;
/