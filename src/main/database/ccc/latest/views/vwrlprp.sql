create or replace view vwrlprp
as
with sel as (
    select  c.derulid,
            a.dername,
            a.dedescr,
            d.dedescr as dertype_d,
            v1.deapexv,
            c.deprofl_id,
            p.deproid,
            b.derlcid,
            row_number() over (partition by a.derulid, c.deprofl_id, p.deproid order by v1.deapenr desc) as rn
    from    drrules a, drprrul c , drrulce b, drrtype d, drapexv v1,
            vwprfpr p, drproje j, drapexv v2
    where   a.derulid = b.derulid
    and     c.derulid = a.derulid
    and     d.detypid = a.detypid
    and     b.deapeid = v1.deapeid
    and     c.deprofl_id = p.deprofl_id
    and     p.deproid = j.deproid
    and     j.deapeid = v2.deapeid
    and     v1.deapenr <= v2.deapenr
)
select  *
from    sel
where   rn = 1
/
comment on table vwrlprp is 'Rules van toepassing obv profiel en project'
/