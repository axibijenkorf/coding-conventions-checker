create or replace force view vwprfpr
as
select      p.deproid,
            p.dedescr,
            f.deprofl_id
from        drproje p, drprofl f
where       p.deprofl_id = f.deprofl_id
/