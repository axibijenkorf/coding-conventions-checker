create or replace package body pck_checks as
    -- Private helper function.
    function clean_query( p_query in clob ) return clob;
    -- Do any necessary clean-up.
    function clean_query( p_query in clob ) return clob is
        l_query clob := p_query;
    begin
        loop
            if substr(l_query,-1) in (chr(10),chr(13),';',' ','/') then
                l_query := substr(l_query,1,length(l_query)-1);
            else
                exit;
            end if;
        end loop;
        return l_query;
    end clean_query;

    -- test if query is valid
    function is_valid_query( p_query in clob, p_derlink in varchar2 ) return varchar2 is
    -- declaratie
        l_source_query  clob := p_query;
        l_source_queryv clob;
        l_report_cursor integer;
    begin
    --Controle of het statement goed is opgesteld
        if l_source_query is not null then

            --Controle op gebruik van application_id bij derlink = 1 (=application)
            if p_derlink = 1
            -- Opvangen hoe het statement precies begint door de trim functie
                    and substr(upper(ltrim(l_source_query)),1,21) != 'SELECT APPLICATION_ID'
                    and substr(upper(ltrim(l_source_query)),1,19) != 'WITH APPLICATION_ID' then
                return 'Query must begin with SELECT APPLICATION_ID or WITH APPLICATION_ID';
            end if;

            --Controle op gebruik van Page_id bij derlink = 2 (=page)
            if p_derlink = 2
                    and substr(upper(ltrim(l_source_query)),1,14) != 'SELECT PAGE_ID'
                    and substr(upper(ltrim(l_source_query)),1,12) != 'WITH PAGE_ID' then
                return 'Query must begin with SELECT PAGE_ID or WITH PAGE_ID';
            end if;

                        --Controle op gebruik van regio_id bij derlink = 3 (=region)
            if p_derlink = 3
                    and substr(upper(ltrim(l_source_query)),1,16) != 'SELECT REGION_ID'
                    and substr(upper(ltrim(l_source_query)),1,14) != 'WITH REGION_ID' then
                return 'Query must begin with SELECT REGION_ID or WITH REGION_ID';
            end if;

                        --Controle op gebruik van item_id bij derlink = 4 (=item)
            if p_derlink = 4
                    and substr(upper(ltrim(l_source_query)),1,14) != 'SELECT ITEM_ID'
                    and substr(upper(ltrim(l_source_query)),1,12) != 'WITH ITEM_ID' then
                return 'Query must begin with SELECT ITEM_ID or WITH ITEM_ID';
            end if;

                        --Controle op gebruik van application_item_id bij derlink = 5 (=application item)
            if p_derlink = 5
                    and substr(upper(ltrim(l_source_query)),1,26) != 'SELECT APPLICATION_ITEM_ID'
                    and substr(upper(ltrim(l_source_query)),1,14) != 'WITH APPLICATION_ITEM_ID' then
                return 'Query must begin with SELECT APPLICATION_ITEM_ID or WITH APPLICATION_ITEM_ID';
            end if;

                        --Controle op gebruik van button_id bij derlink = 6 (=button)
            if p_derlink = 6
                    and substr(upper(ltrim(l_source_query)),1,16) != 'SELECT BUTTON_ID'
                    and substr(upper(ltrim(l_source_query)),1,14) != 'WITH BUTTON_ID' then
                return 'Query must begin with SELECT BUTTON_ID or WITH BUTTON_ID';
            end if;

                        --Controle op gebruik van list_id bij derlink = 7 (=List)
            if p_derlink = 7
                    and substr(upper(ltrim(l_source_query)),1,14) != 'SELECT LIST_ID'
                    and substr(upper(ltrim(l_source_query)),1,12) != 'WITH LIST_ID' then
                return 'Query must begin with SELECT LIST_ID or WITH LIST_ID';
            end if;

                        --Controle op gebruik van List_entry_id bij derlink = 8 (=list entry)
            if p_derlink = 8
                    and substr(upper(ltrim(l_source_query)),1,20) != 'SELECT LIST_ENTRY_ID'
                    and substr(upper(ltrim(l_source_query)),1,18) != 'WITH LIST_ENTRY_ID' then
                return 'Query must begin with SELECT LIST_ENTRY_ID or WITH LIST_ENTRY_ID';
            end if;


            l_source_query := clean_query( l_source_query );
            --Doorgeven van code via een variabele , die niet moet gecontroleerd worden voor SQL injectie
            l_source_queryv := sys.dbms_assert.noop( str => l_source_query );
            begin
            --cursor openen
                l_report_cursor := sys.dbms_sql.open_cursor;
            --mogelijk maken via de parse functie om sql's uit te voeren
            --cursor ID / sql statement / language
                sys.dbms_sql.parse( l_report_cursor, l_source_queryv, SYS.DBMS_SQL.NATIVE );
            --cursor sluiten
                sys.dbms_sql.close_cursor(l_report_cursor);
            exception when others then
            --indien foutcode, dan cursor meteen sluiten
                if sys.dbms_sql.is_open( l_report_cursor ) then
                    sys.dbms_sql.close_cursor( l_report_cursor );
                end if;
                return sqlerrm;
            end;
        end if;
        return null;
    end is_valid_query;



    -- Procedure om de tests op te starten

    procedure start_run (
        i_proid in number,
        i_usrnm in varchar2 default coalesce(sys_context('APEX$SESSION', 'app_user'), sys_context('USERENV','OS_USER'), upper(user))
    ) is
        l_query clob;
        l_cursor integer := null;
        l_feedback integer;

        --cursor opmaken voor de rules die er moeten gechecked worden.
        cursor tst_csr
        is
        select a.derulid,
            a.dername,
            a.dedescr,
            a.degraid,
            b.derlcid,
            b.deapeid,
            b.destate,
            a.detypid,
            b.derlink,
            b.deapbiv,
            e.deproid,
            d.deappid,
            e.deprofl_id
        from drrules a, drrulce b, drprrul c, drprapp d, drproje e, vwrlprp p
        where a.derulid = b.derulid
        and   b.derulid = c.derulid
        and   e.deprofl_id = c.deprofl_id
        and   e.deproid = d.deproid
        and   e.deproid = p.deproid
        and   e.deprofl_id = p.deprofl_id
        and   p.derlcid = b.derlcid
        and   d.deproid = i_proid;

        tst_rec tst_csr%ROWTYPE;
        l_message    varchar2(4000);
        l_status     varchar2(30);
        l_identifier varchar2(4000);
        l_pass_cnt   number;
        l_total      number;
        l_start      timestamp with local time zone;
        l_end        timestamp with local time zone;
        l_duration   interval day to second;
        l_dur_ms     number;
        l_resul      drresul%rowtype;
        v_runid      number;
        v_rstid      number;

       --opslaan in de resultatentabel
        procedure save_status(
            p_deappid   in number,
            p_depagen   in number,
            p_deregio   in varchar2,
            p_deitemn   in varchar2,
            p_delabel   in varchar2,
            p_start     in date,
            p_end       in date,
            v_runid     in number,
            p_derulid   in number,
            p_deproid   in number,
            p_derlink   in varchar2,
            p_derefid   in number
        )
        is
          --   p_status in number, p_duration in number ) is
        begin

            merge into drresul dest
            using ( select a.derlcid,
                           i_proid deproid,
                           p_derulid derulid,
                           p_deappid deappid,
                           b.detypid,
                           b.degraid,
                           p_depagen depagen,
                           p_deitemn deitemn,
                           p_delabel delabel,
                           p_deregio deregio,
                           p_derlink derlink,
                           p_derefid derefid
                    from drrulce a , drrules b, drproje c
                    where a.derulid = b.derulid
                    and b.derulid = p_derulid
                    and c.deproid = i_proid
                    and a.deapeid = c.deapeid) src
            on ( dest.deappid = src.deappid
                and dest.derulid = src.derulid
                and dest.depagen = src.depagen
                and dest.deitemn = src.deitemn

                )
            when not matched then
                insert (dest.derslid, dest.derunid, dest.derulid, dest.degraid, dest.deappid, dest.detypid,
                    dest.destaid, dest.decreus, dest.decredt, dest.deproid, dest.depagen, dest.deitemn, dest.delabel,
                    dest.deregio, dest.derlink, dest.derefid)
                values (sqresul.nextval, v_runid, src.derulid, src.degraid, p_deappid, src.detypid,
                    '1', i_usrnm, sysdate, src.deproid, p_depagen, p_deitemn, p_delabel,
                    p_deregio, p_derlink, p_derefid
            );
        end save_status;
    begin
        -- Run ID bepalen
        v_runid := sqrunst.nextval;
        l_start := localtimestamp;

        -- Data verwijderen uit voorgaande runs.
        delete from drresul
        --De false positives laten staan in de lijst
        --De data van de projecten die niet gelopen hebben, mogen niet gedelete worden
        where      destaid != 2
        and        deproid = i_proid;

          for tst_rec in tst_csr loop
            begin
                -- Open de cursor
                l_cursor := sys.dbms_sql.open_cursor;
                -- SQL statement aan de cursor koppelen
                sys.dbms_sql.parse( l_cursor, tst_rec.destate, dbms_sql.native);

                -- Input bind variabelen definiëren
                --BIND_VARIABLE(<Cursor_ID>, <Placeholder_name>, <Variable | Value>);
                sys.dbms_sql.bind_variable( l_cursor, tst_rec.deapbiv, tst_rec.deappid); --applicatieid
                apex_debug.message(tst_rec.deapbiv);
                apex_debug.message(tst_rec.deappid);
                apex_debug.message(tst_rec.destate);
                for rec in (
                    select      deparnm, deparvl
                    from        drparst
                    where       derlcid = tst_rec.derlcid
                    and         deprofl_id = tst_rec.deprofl_id)
                loop
                    sys.dbms_sql.bind_variable( l_cursor, rec.deparnm, rec.deparvl);
                end loop;
                -- Output variabelen definiëren
                --DBMS_SQL.DEFINE_COLUMN(<Cursor_id>, <Relative_position>, <Defined_variable> );
                sys.dbms_sql.define_column(
                    c           => l_cursor,
                    position    => 1,
                    column      => l_status,
                    column_size => 30
                );
                --sql uitvoeren
                l_feedback := sys.dbms_sql.execute( l_cursor );

                loop
                    l_resul := null;
                    l_feedback := sys.dbms_sql.fetch_rows( l_cursor );
                    exit when l_feedback = 0;
                    sys.dbms_sql.column_value( l_cursor, 1, l_status );
                    l_total := l_total + 1;

                    if l_status is not null then
                        case tst_rec.derlink
                        --wanneer het om de applicatie binding variabele gaat
                        WHEN '1' THEN

                            select application_id
                            into l_resul.deappid
                            from apex_applications
                            where application_id = l_status ;

                        --wanneer het om de PAGE binding variabele gaat
                        WHEN '2' THEN

                            select application_id, page_id
                            into l_resul.deappid, l_resul.depagen
                            from apex_application_pages
                            where application_id = tst_rec.deappid
                            and  page_id = l_status;

                        --wanneer het om de REGION binding variabele gaat
                        WHEN '3' THEN

                            select application_id, page_id, region_name
                            into l_resul.deappid, l_resul.depagen, l_resul.deregio
                            from apex_application_page_regions
                            where region_id = l_status ;

                        --wanneer het om de PAGE ITEM binding variabele gaat
                            WHEN '4' THEN
                        -- dbms_output.put_line (tst_rec.derlink);
                            select application_id, page_id, region , item_name
                            into l_resul.deappid, l_resul.depagen, l_resul.deregio, l_resul.deitemn
                            from apex_application_page_items
                            where item_id = l_status;

                        --wanneer het om de APPLICATION ITEM binding variabele gaat

                            WHEN '5' THEN

                            select application_id, item_name
                            into l_resul.deappid, l_resul.deitemn
                            from apex_application_items
                            where application_item_id = l_status ;

                        --wanneer het om de BUTTON binding variabele gaat
                            WHEN '6' THEN

                            select application_id, page_id, region , label, button_name
                            into l_resul.deappid, l_resul.depagen, l_resul.deregio, l_resul.delabel, l_resul.deitemn
                            from apex_application_page_buttons
                            where button_id = l_status ;


                        --wanneer het om de LIST binding variabele gaat
                            WHEN '7' THEN

                            select application_id, list_name
                            into l_resul.deappid, l_resul.delabel
                            from apex_application_lists
                            where list_id = l_status ;

                        --wanneer het om de LIST ENTRY binding variabele gaat
                            WHEN '8' THEN

                            select application_id, list_name, entry_text
                            into l_resul.deappid, l_resul.delabel, l_resul.deitemn
                            from apex_application_list_entries
                            where list_entry_id = l_status ;
                        l_pass_cnt := l_pass_cnt + 1;
                        end case;

                    dbms_output.put_line ('begin');
                    dbms_output.put_line (l_resul.depagen);
                    dbms_output.put_line (l_resul.deregio);
                    dbms_output.put_line (l_resul.deitemn);
                    dbms_output.put_line (l_resul.delabel);
                    dbms_output.put_line (v_runid);
                    dbms_output.put_line (tst_rec.derlink);
                    dbms_output.put_line ('end');

                    save_status(
                        p_deappid => l_resul.deappid,
                        p_depagen => l_resul.depagen,
                        p_deregio => l_resul.deregio,
                        p_deitemn => l_resul.deitemn,
                        p_delabel => l_resul.delabel,
                        p_derulid => tst_rec.derulid,
                        p_deproid => i_proid,
                        p_start => l_start,
                        p_end => l_end,
                        v_runid => v_runid,
                        p_derlink => tst_rec.derlink,
                        p_derefid => l_status
                        );
                    end if;
                end loop;

                --cursor sluiten
                sys.dbms_sql.close_cursor( l_cursor );

                l_end := localtimestamp;
                l_duration := localtimestamp - l_start;
                l_dur_ms := extract(   day from l_duration) * 24*60*60*1000
                            + extract(  hour from l_duration) * 60*60*1000
                            + extract(minute from l_duration) * 60*1000
                            + extract(second from l_duration) * 1000;



                if sys.dbms_sql.is_open( l_cursor ) then
                    sys.dbms_sql.close_cursor( l_cursor );
                end if;
            end;
        end loop;
        --data wegschrijven in de logtabel voor de runs

        insert into drrunlg (derunid, deproid, debegdt, deenddt, decreus)
        values (v_runid, i_proid , l_start,l_end, i_usrnm);

        --data wegschrijven in de logtabel voor de status per runs
        v_rstid := sqrunst.nextval;
        insert into drrunst (derstid, derunid, degraid, detypid, decount, decredt, decreus)
        select v_rstid, derunid, degraid, detypid , count(*), sysdate, i_usrnm
        from drresul
        where    derunid = v_runid
        group by derunid, degraid, detypid;
    exception
        when others then
            if sys.dbms_sql.is_open( l_cursor ) then
                sys.dbms_sql.close_cursor( l_cursor );
            end if;
            raise;
    end start_run;

    function build_link( p_derslid in number, p_application_id in number, p_derefid in number)
            return varchar2 is
        l_app number;
        l_page number;
        l_builder_session number := v('APP_BUILDER_SESSION');
        l_link varchar2(4000) := null;
        l_version number;
    begin
        if l_builder_session is null then
            -- Not logged in to the builder; bail out.
            return l_link;
        end if;
        -- Do things differently depending on the APEX version.
        for c1 in ( select to_number(substr(version_no,0,instr(version_no,'.')-1)) vrsn from apex_release ) loop
            l_version := c1.vrsn;
        end loop;
        if l_version >= 5 then
            l_app := 4000;
            l_page := 4500;
        end if;
        for c1 in ( select derlink from drresul where derslid = p_derslid ) loop
            case c1.derlink
            --APPLICATION
            when '1' then
                l_app := 4000;
                l_page := 1;
                l_link := ':::RP:FB_FLOW_ID,F4000_P1_FLOW,P0_FLOWPAGE:'
                    ||p_application_id||','||p_application_id||','||p_application_id;
            --PAGE /OK
            when '2' then
                if l_version < 5 then
                    l_app := 4000;
                    l_page := 4150;
                    l_link := '::::FB_FLOW_ID,FB_FLOW_PAGE_ID,F4000_P4150_GOTO_PAGE:'
                        ||p_application_id||','||p_derefid||','||p_derefid;
                else
                    l_link := '::NO:1,4150:FB_FLOW_ID,FB_FLOW_PAGE_ID,F4000_P1_FLOW,F4000_P4150_GOTO_PAGE,F4000_P1_PAGE'
                        ||':'||p_application_id||','||p_derefid||','||p_application_id||','||p_derefid||','||p_derefid
                        ||'#5000:'||p_derefid;
                end if;
            --REGION
            when '3' then
                for c2 in ( select page_id
                            from apex_application_page_regions
                            where application_id = p_application_id
                                and region_id = p_derefid ) loop
                    if l_version < 5 then
                        l_app := 4000;
                        l_page := 4651;
                        l_link := ':::RP,4651,960,420,601,4050,27,196,121,232,695,754,832,287,2000'
                            ||':FB_FLOW_ID,FB_FLOW_PAGE_ID,F4000_P4651_ID:'
                            ||p_application_id||','||c2.page_id||','||p_derefid;
                    else
                        l_link := '::NO:1,4150:FB_FLOW_ID,FB_FLOW_PAGE_ID,F4000_P1_FLOW,F4000_P4150_GOTO_PAGE,F4000_P1_PAGE'
                            ||':'||p_application_id||','||c2.page_id||','||p_application_id||','||c2.page_id||','||c2.page_id
                            ||'#5110:'||p_derefid;
                    end if;
                end loop;
            --PAGE ITEM
            when '4' then
                for c2 in ( select page_id
                            from apex_application_page_items
                            where application_id = p_application_id
                                and item_id = p_derefid ) loop
                    if l_version < 5 then
                        l_app := 4000;
                        l_page := 4311;
                        l_link := ':::RP,4311:FB_FLOW_ID,FB_FLOW_PAGE_ID,F4000_P4311_ID:'
                            ||p_application_id||','||c2.page_id||','||p_derefid;
                    else
                        l_link := '::NO:1,4150:FB_FLOW_ID,FB_FLOW_PAGE_ID,F4000_P1_FLOW,F4000_P4150_GOTO_PAGE,F4000_P1_PAGE'
                            ||':'||p_application_id||','||c2.page_id||','||p_application_id||','||c2.page_id||','||c2.page_id
                            ||'#5120:'||p_derefid;
                    end if;
                end loop;
            --APP ITEM
            when '5' then
                l_app := 4000;
                l_page := 4303;
                l_link := '::::FB_FLOW_ID,F4000_P4303_ID:'
                    ||p_application_id||','||p_derefid;
            --BUTTON
            when '6' then
                for c2 in ( select page_id
                            from apex_application_page_buttons
                            where application_id = p_application_id
                                and button_id = p_derefid ) loop
                    if l_version < 5 then
                        l_app := 4000;
                        l_page := 4314;
                        l_link := ':::RP,4314:FB_FLOW_ID,FB_FLOW_PAGE_ID,F4000_P4314_ID:'
                            ||p_application_id||','||c2.page_id||','||p_derefid;
                    else
                        l_link := '::NO:1,4150:FB_FLOW_ID,FB_FLOW_PAGE_ID,F4000_P1_FLOW,F4000_P4150_GOTO_PAGE,F4000_P1_PAGE'
                            ||':'||p_application_id||','||c2.page_id||','||p_application_id||','||c2.page_id||','||c2.page_id
                            ||'#5130:'||p_derefid;
                    end if;
                end loop;
            --LIST
            when '7' then
                l_app := 4000;
                l_page := 4050;
                l_link := ':::4050:FB_FLOW_ID,F4000_P4050_LIST_ID:'
                    ||p_application_id||','||p_derefid;
            --LIST_ENTRY
            when '8' then
                l_app := 4000;
                l_page := 4052;
                l_link := ':::4050,4052:FB_FLOW_ID,F4000_P4052_ID:'
                    ||p_application_id||','||p_derefid;
            else
                -- Someone tried to link to a component we don't support yet.
                null;
            end case;
        end loop;
        if l_link is not null then
            l_link := 'f?p='||l_app||':'||l_page||':'||l_builder_session||l_link;
        end if;
        return l_link;
    end build_link;

end pck_checks;
/