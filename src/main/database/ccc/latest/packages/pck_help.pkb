create or replace package body pck_help as

    -- Return help messages
    function help_message( p_derlink in number ) return varchar2 is

    begin
--    dbms_output.put_line(p_derlink);
            if p_derlink is null or p_derlink > 8 then
                return 'Er is geen help tekst voorzien ';
             end if;

            if p_derlink = 1 then
                return 'select application_id
                        from apex_applications
                        where application_id = :APPLICATION;';
             end if;

            if p_derlink = 2 then
                return 'select page_id
                        from apex_application_pages
                        where application_id = :APPLICATION;';
             end if;

            if p_derlink = 3 then
                return 'select region_id
                        from apex_application_page_regions
                        where application_id = :APPLICATION;';
             end if;


            if p_derlink = 4 then
                return 'select item_id
                        from apex_application_page_items
                        where application_id = :APPLICATION;';
             end if;

            if p_derlink = 5 then
                return 'select application_item_id
                        from apex_application_items
                        where application_id = :APPLICATION;';
             end if;

            if p_derlink = 6 then
                return 'select button_id
                        from apex_application_page_buttons
                        where application_id = :APPLICATION;';
             end if;

            if p_derlink = 7 then
                return 'select list_id
                        from apex_application_lists
                        where application_id = :APPLICATION;';
             end if;

             if p_derlink = 8 then
                return 'select list_entry_id
                        from apex_application_list_entries
                        where application_id = :APPLICATION;';
             end if;

    end help_message;

end pck_help;
/