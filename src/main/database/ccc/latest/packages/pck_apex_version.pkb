create or replace package body pck_apex_version
as
    function get_lov
    return varchar2
    is
        r_value     varchar2(4000);
    begin
        r_value := q'{
            select      deapexv, deapeid
            from        drapexv
            order by    deapexv
        }';
        return r_value;
    end get_lov;

end pck_apex_version;
/