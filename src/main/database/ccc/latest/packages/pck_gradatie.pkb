create or replace package body pck_gradatie
as
    function get_lov
    return varchar2
    is
        r_value     varchar2(4000);
    begin
        r_value := q'{
            select      dedescr, degraid
            from        drgrada
            order by    dedescr
        }';
        return r_value;
    end get_lov;

end pck_gradatie;
/