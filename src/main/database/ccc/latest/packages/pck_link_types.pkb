create or replace package body pck_link_types
as
    function get_lov
    return varchar2
    is
        r_value     varchar2(4000);
    begin
        r_value := q'{
            select      dedescr, delinid
            from        drlinkt
            order by    dedescr
        }';
        return r_value;
    end get_lov;

end pck_link_types;
/