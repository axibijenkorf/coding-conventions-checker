create or replace package pck_rules
as
    function get_lov
    return varchar2;

    procedure extract_parameters(
        i_rlcid     in number
    );

end pck_rules;
/