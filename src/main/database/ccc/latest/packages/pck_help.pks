create or replace package pck_help
as
    function help_message( p_derlink in number )
    return varchar2;

end pck_help;
/