create or replace package body pck_profl
as

    function get_lov
    return clob
    is
        r_value     varchar2(4000);
    begin
        r_value := q'{
            select      deprfnm, deprofl_id
            from        drprofl
        }';
        return r_value;
    end get_lov;

end pck_profl;
/