create or replace package body pck_rules
as
    function get_lov
    return varchar2
    is
        r_value     varchar2(4000);
    begin
        r_value := q'{
            select      dedescr, derulid
            from        drrules
        }';
        return r_value;
    end get_lov;

    procedure extract_parameters(
        i_rlcid     in number
    )
    is
        v_regex     varchar2(50) := ':[A-Za-z0-9_]*';
        v_apbiv     drrulce.deapbiv%type;
    begin
        begin
            select      deapbiv
            into        v_apbiv
            from        drrulce
            where       derlcid = i_rlcid;
        exception
            when no_data_found then
                v_apbiv := ':APPLICATION';
        end;

        delete
        from    drparst
        where   deparst_id in (
        select  deparst_id
        from (
        with sel as (
                select to_char(destate) as stmnt
                from drrulce
                where derlcid = i_rlcid
            )
            select      regexp_substr(stmnt, v_regex, 1, level) as param
            from        sel
            connect by  regexp_substr(stmnt, v_regex, 1, level) is not null
        ) a, drparst b
        where   a.param(+) = b.deparnm
        and     a.param is null
        and     nvl(a.param, '@') != v_apbiv);

        for rec in (select *
            from (
            with sel as (
                    select to_char(destate) as stmnt
                    from drrulce
                    where derlcid = i_rlcid
                )
                select      regexp_substr(stmnt, v_regex, 1, level) as param
                from        sel
                connect by  regexp_substr(stmnt, v_regex, 1, level) is not null
            )
            where   param != v_apbiv
        ) loop
            for rec2 in (select deprofl_id from drprofl) loop
                merge into drparst p
                using(
                    select      rec2.deprofl_id as deprofl_id, rec.param as param, i_rlcid as derlcid
                    from        dual
                ) d
                on (
                    p.deprofl_id = d.deprofl_id
                    and p.deparnm = d.param
                    and p.derlcid = d.derlcid
                )
                when not matched then
                    insert(derlcid, deprofl_id, deparnm, decredt, decreus)
                    values(i_rlcid, rec2.deprofl_id, rec.param, systimestamp, nvl(v('APP_USER'), 'AUTO'));
            end loop;
        end loop;
    end extract_parameters;

end pck_rules;
/