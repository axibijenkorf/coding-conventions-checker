create or replace package pck_checks
 as
    g_collection varchar2(255) := 'pck_checks';
    function is_valid_query( p_query in clob, p_derlink in varchar2 ) return varchar2;
    procedure start_run (
        i_proid in number,
        i_usrnm in varchar2 default coalesce(sys_context('APEX$SESSION', 'app_user'), sys_context('USERENV','OS_USER'), upper(user))
    );
    function build_link( p_derslid in number, p_application_id in number, p_derefid in number)

        return varchar2;
end pck_checks;
/