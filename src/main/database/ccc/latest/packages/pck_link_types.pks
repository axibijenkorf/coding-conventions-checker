create or replace package pck_link_types
as
    function get_lov
    return varchar2;

end pck_link_types;
/