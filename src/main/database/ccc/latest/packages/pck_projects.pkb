create or replace package body pck_projects
as
    function get_lov
    return varchar2
    is
        r_value     varchar2(4000);
    begin
        r_value := q'{
            select      dedescr, deproid
            from        drproje
            order by    dedescr
        }';
        return r_value;
    end get_lov;

end pck_projects;
/