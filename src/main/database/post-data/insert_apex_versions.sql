insert into drapexv (deapeid, deapexv, deapenr, decredt, decreus) values (sqapexv.nextval, 'APEX 19.1','20190331', sysdate, 'INSTALL');
insert into drapexv (deapeid, deapexv, deapenr, decredt, decreus) values (sqapexv.nextval, 'APEX 20.1','20200331', sysdate, 'INSTALL');
insert into drapexv (deapeid, deapexv, deapenr, decredt, decreus) values (sqapexv.nextval, 'APEX 19.2','20191004', sysdate, 'INSTALL');
insert into drapexv (deapeid, deapexv, deapenr, decredt, decreus) values (sqapexv.nextval, 'APEX 20.2','20201001', sysdate, 'INSTALL');
insert into drapexv (deapeid, deapexv, deapenr, decredt, decreus) values (sqapexv.nextval, 'APEX 5.1','20160824', sysdate, 'INSTALL');
insert into drapexv (deapeid, deapexv, deapenr, decredt, decreus) values (sqapexv.nextval, 'APEX 5.0','20130101', sysdate, 'INSTALL');
insert into drapexv (deapeid, deapexv, deapenr, decredt, decreus) values (sqapexv.nextval, 'APEX 18.1','20180404', sysdate, 'INSTALL');