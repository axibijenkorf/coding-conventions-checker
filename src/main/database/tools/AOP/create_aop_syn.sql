-- AOP
create or replace synonym aop_api_pkg for aop_api20_pkg;
create or replace synonym aop_plsql_pkg for aop_plsql20_pkg;
create or replace synonym aop_convert_pkg for aop_convert20_pkg;