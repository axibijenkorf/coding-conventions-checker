DECLARE
  l_acl       VARCHAR2(100) := 'AOP';
  l_desc      VARCHAR2(100) := 'AOP';
  l_principal VARCHAR2(30)  := '${apex_user}'; -- upper case
  l_tlsusr    VARCHAR2(30)  := '${tools_user}'; -- upper case
  l_host      VARCHAR2(100) := 'localhost';
BEGIN
    DBMS_NETWORK_ACL_ADMIN.append_host_ace (
    host        => l_host,
    lower_port  => 8010,
    upper_port  => 8010,
    ace         => xs$ace_type(privilege_list => xs$name_list('http'),
                              principal_name => l_principal,
                              principal_type => xs_acl.ptype_db));

    DBMS_NETWORK_ACL_ADMIN.append_host_ace (
    host        => l_host,
    lower_port  => 8010,
    upper_port  => 8010,
    ace         => xs$ace_type(privilege_list => xs$name_list('http'),
                              principal_name => l_tlsusr,
                              principal_type => xs_acl.ptype_db));

  COMMIT;
END;
/