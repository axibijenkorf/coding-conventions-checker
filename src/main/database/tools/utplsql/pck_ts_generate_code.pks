create or replace package pck_ts_generate_code
as

    function generate_ut_package(
        i_schem     in varchar2,
        i_pkgnm     in varchar2,
        i_bdspc     in varchar2 default 'BOTH',
        i_foldr     in varchar2
    ) return clob;

end pck_ts_generate_code;
/
