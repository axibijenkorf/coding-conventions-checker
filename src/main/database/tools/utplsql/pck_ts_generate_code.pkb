create or replace package body pck_ts_generate_code
as
    gc_scope_prefix constant varchar2(31)   := lower($$plsql_unit) || '.';
    gc_tab_length   constant simple_integer := 4;
    gc_tab_01       constant varchar2(100)  := rpad(' ', gc_tab_length * 1, ' ');
    gc_tab_02       constant varchar2(100)  := rpad(' ', gc_tab_length * 2, ' ');
    gc_tab_03       constant varchar2(100)  := rpad(' ', gc_tab_length * 3, ' ');
    gc_tab_04       constant varchar2(100)  := rpad(' ', gc_tab_length * 4, ' ');
    gc_tab_05       constant varchar2(100)  := rpad(' ', gc_tab_length * 5, ' ');
    gc_tab_06       constant varchar2(100)  := rpad(' ', gc_tab_length * 6, ' ');
    gc_tab_07       constant varchar2(100)  := rpad(' ', gc_tab_length * 7, ' ');
    -- If more tabs are needed create new constant gc_tab_xx
    gc_flxst        constant varchar2(30)   := '%\_FLEX\_%';
    gc_collect_lim  constant simple_integer := 10000;
    gc_unique_abv   constant varchar2(1)    := 'U';
    gc_l_params     constant varchar2(100)  := 'l_params            logger.tab_param;';
    gc_l_scope      constant varchar2(100)  := 'l_scope             logger_logs.scope%type := gc_scope_prefix || ''';
    gc_l_version    constant varchar2(100)  := '1.1.6';
    gc_l_view_v     constant varchar2(100)  := '1.0.0';

    gv_pckge         user_objects.object_name%type;          -- Package name
    gv_prmky         user_tab_columns.column_name%type;         -- Primary key column name
    gv_vwnam         user_objects.object_name%type;          -- View name
    gv_apinm         user_objects.object_name%type;          -- API package name
    gv_prdfx         varchar2(10);                           -- Product prefix
    gv_tblnm         user_objects.object_name%type;          -- Table name
    gv_varnm         varchar2(100);                          -- Variable name
    gv_lngtb         user_objects.object_name%type;          -- Language table name
    gv_lngvr         varchar2(100);                          -- Variable for language table rowtype
    gv_lngex         boolean;                                -- Language table exists
    gv_strpk         varchar2(100);                          -- Stripped primary key column name
    gv_apiln         varchar2(200);                          -- API Package name of language table
    gv_rtvar         varchar2(100);                          -- Constant for defining rowtype as in parameter
    gv_actcl         boolean;                                -- Table has actif column
    gv_lngpk         varchar2(200);                          -- Language table primary key column

    /**
    * Generate a template for writing unit tests on this pakcage
    * In order for your identifiers to be available in the dba_identifiers table,
    * execute these statements:
    *
    * alter session set plscope_settings = 'identifiers:all';
    * alter package <package_name> compile;
    *
    *
    *
    * @issue    CC-461
    *
    * @author   KMRT
    * @created  31-10-2018
    * @param    i_schem     Schema name
    * @param    i_pkgnm     Package name
    * @return   Template code for unit test package
    */
    function generate_ut_package(
        i_schem     in varchar2,
        i_pkgnm     in varchar2,
        i_bdspc     in varchar2 default 'BOTH',
        i_foldr     in varchar2
    ) return clob
    is
        l_scope     logger_logs.scope%type := gc_scope_prefix || 'generate_view_links';
        l_params    logger.tab_param;
        v_pspec     clob;                   --generated package spec
        v_pbody     clob;                   --generated package body

        v_prccl     varchar2(4000);         --proc to call
        v_prcnm     varchar2(1000);         --procedure name
        v_pkgnm     varchar2(200);          --package name
        r_stmnt     clob;                   -- Generated statement
    begin
        logger.append_param(l_params, 'i_schem', i_schem);
        logger.append_param(l_params, 'i_pkgnm', i_pkgnm);
        logger.log_start(null, l_scope, null, l_params);
        v_pkgnm := 'ut_' || regexp_replace(lower(i_pkgnm), '^pck_');

        -- Header general
        v_pspec := v_pspec || 'create or replace package ' || v_pkgnm || utl_tcp.crlf
                || 'as' || utl_tcp.crlf
                || gc_tab_01 || '-- %suite(' || v_pkgnm || ')' || utl_tcp.crlf
                || gc_tab_01 || '-- %suitepath(all.'||i_foldr||')' || utl_tcp.crlf || utl_tcp.crlf;

        v_pspec := v_pspec ||
                gc_tab_01 || '-- %beforeall' || utl_tcp.crlf ||
                gc_tab_01 || 'procedure global_setup;' || utl_tcp.crlf || utl_tcp.crlf ||
                gc_tab_01 || '-- %afterall' || utl_tcp.crlf ||
                gc_tab_01 || 'procedure global_cleanup;' || utl_tcp.crlf || utl_tcp.crlf ||
                gc_tab_01 || '-- %beforeeach' || utl_tcp.crlf ||
                gc_tab_01 || 'procedure test_setup;' || utl_tcp.crlf || utl_tcp.crlf ||
                gc_tab_01 || '-- %aftereach' || utl_tcp.crlf ||
                gc_tab_01 || 'procedure test_cleanup;' || utl_tcp.crlf || utl_tcp.crlf;

        -- Body general
        v_pbody := v_pbody || 'create or replace package body ' || v_pkgnm || utl_tcp.crlf
        || 'as' || utl_tcp.crlf;

        v_pbody := v_pbody ||
                gc_tab_01 || 'procedure global_setup' || utl_tcp.crlf ||
                gc_tab_01 || 'is' || utl_tcp.crlf ||
                gc_tab_01 || 'begin' || utl_tcp.crlf ||
                gc_tab_02 || 'null;' || utl_tcp.crlf ||
                gc_tab_01 || 'end;' || utl_tcp.crlf || utl_tcp.crlf ||
                gc_tab_01 || 'procedure global_cleanup' || utl_tcp.crlf ||
                gc_tab_01 || 'is' || utl_tcp.crlf ||
                gc_tab_01 || 'begin' || utl_tcp.crlf ||
                gc_tab_02 || 'null;' || utl_tcp.crlf ||
                gc_tab_01 || 'end;' || utl_tcp.crlf || utl_tcp.crlf ||
                gc_tab_01 || 'procedure test_setup' || utl_tcp.crlf ||
                gc_tab_01 || 'is' || utl_tcp.crlf ||
                gc_tab_01 || 'begin' || utl_tcp.crlf ||
                gc_tab_02 || 'null;' || utl_tcp.crlf ||
                gc_tab_01 || 'end;' || utl_tcp.crlf || utl_tcp.crlf ||
                gc_tab_01 || 'procedure test_cleanup' || utl_tcp.crlf ||
                gc_tab_01 || 'is' || utl_tcp.crlf ||
                gc_tab_01 || 'begin' || utl_tcp.crlf ||
                gc_tab_02 || 'null;' || utl_tcp.crlf ||
                gc_tab_01 || 'end;' || utl_tcp.crlf || utl_tcp.crlf;

        -- Get each procedure and function in the package
        for rec in (
            select  count(*) over (partition by owner, name order by owner, name, line) rn, i1.*,
                    (
                        select  listagg(name|| ' => ', ', '|| utl_tcp.crlf || gc_tab_04) within group (order by usage_id)
                        from    dba_identifiers i2
                        where   owner = upper(i_schem)
                        and     object_name = upper(i_pkgnm)
                        and     object_type = 'PACKAGE'
                        and     usage = 'DECLARATION'
                        and     usage_context_id = i1.usage_id
                    ) as params
            from    dba_identifiers i1
            where   object_type = 'PACKAGE'
            and     owner = upper(i_schem)
            and     object_name = upper(i_pkgnm)
            and     type in ('FUNCTION', 'PROCEDURE')
            order   by line asc
        ) loop
                v_prcnm := lower(replace(rec.name,'FNC_','PRC_') || '_test'||rec.rn );
                -- Add each record to the spec
                v_pspec := v_pspec ||
                        gc_tab_01 || '-- %test(' || lower(rec.name) || ': )' || utl_tcp.crlf ||
                        gc_tab_01 || 'procedure ' || v_prcnm || ';' || utl_tcp.crlf || utl_tcp.crlf;
                -- Add each record to the body
                v_pbody := v_pbody ||
                        gc_tab_01 || 'procedure ' || v_prcnm || utl_tcp.crlf ||
                        gc_tab_01 || 'is' || utl_tcp.crlf ||
                        gc_tab_01 || 'begin' || utl_tcp.crlf;

                v_prccl := lower(i_pkgnm || '.' || rec.name);
                -- Add the parameters to the procedure call
                if length(rec.params) > 0 then
                    v_prccl := v_prccl || '(' || utl_tcp.crlf ||
                            gc_tab_03 ||  gc_tab_01 || lower(rec.params) || utl_tcp.crlf ||
                            gc_tab_03 || ')';
                end if;

                case
                    -- Add expectation to function
                    when rec.type = 'FUNCTION' then
                    v_pbody := v_pbody || '/* ' || utl_tcp.crlf ||
                            gc_tab_02 || 'ut.expect(' || utl_tcp.crlf ||
                            gc_tab_03 || v_prccl ||utl_tcp.crlf ||
                            gc_tab_02 || ').to_equal(1);' || utl_tcp.crlf ||
                            gc_tab_02 || '*/ ' || utl_tcp.crlf ||
                            gc_tab_02 || 'ut.expect(1).to_equal(1);'|| utl_tcp.crlf;
                    when rec.type = 'PROCEDURE' then
                    v_pbody := v_pbody || '/* ' || utl_tcp.crlf ||
                            gc_tab_02 || v_prccl || ';' || utl_tcp.crlf ||
                            gc_tab_02 || '*/ ' || utl_tcp.crlf ||
                            gc_tab_02 || 'ut.expect(1).to_equal(1);'|| utl_tcp.crlf;
                end case;

                v_pbody := v_pbody ||
                        gc_tab_01 || 'end ' || v_prcnm || ';' || utl_tcp.crlf || utl_tcp.crlf;
            end loop;
        -- End tag for package spec and body
        v_pspec  :=  v_pspec || gc_tab_01 || 'end;' || utl_tcp.crlf || '/' || utl_tcp.crlf || utl_tcp.crlf;
        v_pbody  :=  v_pbody || gc_tab_01 || 'end;' || utl_tcp.crlf || '/' || utl_tcp.crlf || utl_tcp.crlf;

        case i_bdspc
            when 'BOTH' then
                r_stmnt := v_pspec || v_pbody;
            when 'SPEC' then
                r_stmnt := v_pspec;
            when 'BODY' then
                r_stmnt := v_pbody;
            else
                r_stmnt := v_pspec || v_pbody;
        end case;

        logger.log_end(null, l_scope, null, l_params);
        return r_stmnt;
    exception
        when others then
            logger.log_exception(null, l_scope, null, l_params);
            raise;
    end;



end pck_ts_generate_code;
/
