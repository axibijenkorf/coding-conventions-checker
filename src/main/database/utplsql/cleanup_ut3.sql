declare


begin

for i in 1..10 loop
    for rec in (select * from user_objects) loop
        begin
            execute immediate 'drop '||rec.object_type||' '||rec.object_name;
        exception when others then
            null;
        end;
    end loop;
end loop;

end;
/
exit;
