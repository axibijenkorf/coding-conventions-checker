env=$1

cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "$env"

sqlplus "$db_ut_username/$db_ut_password@$db_sqlplusurl" @uninstall.sql $db_ut_username;
sqlplus "$db_ut_username/$db_ut_password@$db_sqlplusurl" @cleanup_ut3.sql $db_ut_username;
sqlplus "$db_ut_username/$db_ut_password@$db_sqlplusurl" @install.sql $db_ut_username;
sqlplus "$db_sys_username/$db_sys_password@$db_sqlplusurl" @create_synonyms_and_grants_for_public.sql $db_ut_username;
