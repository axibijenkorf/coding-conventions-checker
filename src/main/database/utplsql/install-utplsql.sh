#!/bin/sh
set +x

SCHEMA=$1
PASSWORD=$2
UT_USER=$3
UT_PASSWORD=$4
URL=$5

cd src/main/database/utplsql/

RETVAL=`sqlplus -silent "$SCHEMA/$PASSWORD@$URL" <<EOF
SET PAGESIZE 0 FEEDBACK OFF VERIFY OFF HEADING OFF ECHO OFF
    select      *
    from        all_users
    where       username = '$UT_USER';
EXIT;
EOF`

if [ -z "$RETVAL" ]; then
    sqlplus "$SCHEMA/$PASSWORD@$URL" << EOF

    @"create_utplsql_owner.sql" $UT_USER $UT_PASSWORD users
    /
    commit;

    exit
EOF
    sqlplus "$UT_USER/$UT_PASSWORD@$URL" << EOF

    @"install.sql" $UT_USER
    /
    commit;

    exit
EOF
    sqlplus "$UT_USER/$UT_PASSWORD@$URL" << EOF

    @create_synonyms_and_grants_for_public.sql $UT_USER
    /
    commit;

    exit
EOF
else
    echo "utPLSQL already installed"
fi