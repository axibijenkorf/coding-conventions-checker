sudo su - root
echo "Scripts will run with user: $USER"
cd /tmp

yum install -y yum-utils zip unzip net-tools wget tar

libre_version='6.4.6'
aop_version='20.1.4'

echo "===> Download AOP + LibreOffice"
echo "===> Downloading AOP:  https://s3-eu-west-1.amazonaws.com/apexofficeprint/linux/aop_linux_v${aop_version}.zip"
wget -nv -nc "https://s3-eu-west-1.amazonaws.com/apexofficeprint/linux/aop_linux_v${aop_version}.zip"
echo "===> Downloading LibreOffice: https://download.documentfoundation.org/libreoffice/stable/${libre_version}/rpm/x86_64/LibreOffice_${libre_version}_Linux_x86-64_rpm.tar.gz"
wget -nv -nc "https://download.documentfoundation.org/libreoffice/stable/${libre_version}/rpm/x86_64/LibreOffice_${libre_version}_Linux_x86-64_rpm.tar.gz"

echo "===> unpack and install LibreOffice"
# make sure no old versions exist
yum remove openoffice* libreoffice*

tar -xvf LibreOffice_${libre_version}*

yum localinstall /tmp/LibreOffice_${libre_version}*_Linux_x86-64_rpm/RPMS/*.rpm -y
yum install cairo.x86_64 -y
yum install cups.x86_64 -y
yum install mesa-libGL.x86_64 -y

yum install java-1.8.0-openjdk.x86_64 -y

# make soffice available to your user
# create symbolic link
ln -s /opt/libreoffice*/program/soffice /usr/sbin/soffice

# check the version of LibreOffice and try to run a conversion
soffice --version
soffice --headless --invisible --convert-to pdf --outdir /tmp some_document.docx

# if you get: Fontconfig warning: ignoring UTF-8: not a valid region tag
echo "$LC_CTYPE"
# |-> you probably have UTF-8 defined; unset it
export LC_CTYPE=""

#aop_version=$(ls /tmp/ | grep -oP 'aop_linux_\K.*(?=\.zip)')
echo "===> unpack and install AOP $aop_version"
unzip /tmp/aop_linux*

mkdir /opt/aop/
mkdir /opt/aop/$aop_version

cp -r /tmp/v$aop_version/server /opt/aop/$aop_version/
chmod -R 777 /opt/aop/

unlink latest
ln -s /opt/aop/$aop_version /opt/aop/latest

cp "/vagrant/scripts/aop_config.json" "/opt/aop/$aop_version/server/aop_config.json"

echo "===> Start AOP"

if [ ! -f /etc/systemd/system/aop.service ]
then
cat >> /etc/systemd/system/aop.service << 'EOF'
[Unit]
Description=APEX Office Print
After=network.target

[Service]
Type=simple
User=root
Group=root

Environment=AOP_HOME=/opt/aop/latest/server  ### Please see comment below
Environment=AOP_PROCESS_NAME=APEXOfficePrint
Environment=AOP_EXECUTABLE_NAME=APEXOfficePrintLinux64
Environment=AOP_PORT=8010

Environment=PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/bin/
SyslogIdentifier=apexofficeprint
Restart=always
RestartSec=30
TimeoutStartSec=30
TimeoutStopSec=30

ExecStart=/usr/bin/env ${AOP_HOME}/${AOP_EXECUTABLE_NAME} -p ${AOP_PORT} -s ${AOP_HOME}

ExecStop=/usr/bin/env pkill ${AOP_PROCESS_NAME}

[Install]
WantedBy=multi-user.target
EOF
fi

systemctl daemon-reload

systemctl enable aop

systemctl start aop

echo "===> Remove downloaded files"

rm -f /tmp/LibreOffice_*.tar.gz
rm -R -f /tmp/LibreOffice_*

rm -f /tmp/aop_linux_*.zip
rm -R -f /tmp/v$aop_version

yum clean packages