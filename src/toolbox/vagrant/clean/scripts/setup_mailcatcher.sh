sudo su - root

echo "===> download and update related packages"
#yum -y update
# yum -y install ruby
# yum -y install rubygems

yum-config-manager --enable ol7_optional_latest
yum -y install gcc gcc-c++
yum -y install sqlite-devel ruby-devel

yum clean packages

echo "===> Install mailcatcher"
gem install mailcatcher --no-document

echo "===> Start mailcatcher"
if [ ! -f /etc/systemd/system/mailcatcher.service ]
then
cat >> /etc/systemd/system/mailcatcher.service << EOF
[Unit]
Description=Ruby MailCatcher
Documentation=http://mailcatcher.me/
 
[Service]
Type=simple
User=root
Group=root
Environment=PATH=/usr/local/bin:/usr/bin:/usr/local/sbin:/usr/sbin:/bin/
ExecStart=/usr/local/bin/mailcatcher --foreground --http-ip=0.0.0.0 
Restart=always
 
[Install]
WantedBy=multi-user.target
EOF
fi

systemctl daemon-reload

systemctl enable mailcatcher

systemctl start mailcatcher


