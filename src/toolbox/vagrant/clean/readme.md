# Vagrant Clean DB environment


With this vagrant setup in this folder we want to install a clean DB environment where you can run your project deploy (maven) for a clean install.

## What is Vagrant?

If you don't know how to use Vagrant, you might want to read this introduction.

* [Vagrant: Getting started](https://www.vagrantup.com/intro/getting-started/index.html)
* [Oracle Base: A Beginner's Guide](https://oracle-base.com/articles/vm/vagrant-a-beginners-guide)
* [Oracle Base: A Beginner's Guide video](https://www.youtube.com/watch?v=3LHS118_LQ4)
## Setup environment

The steps described beneath are also automated with a script initiate.sh. You can run this script in bash.

### Install Virtual Box and Vagrant

Software can be found on S: drive: S:\Tools\Software\Vagrant\Software)
1. Install VirtualBox
1. Install Vagrant

### Import box

This is a clean virtual machine with:

* Oracle Container DB 19.3
* Apex 19.1
* Apache tomcat 9.0
* ORDS 19.4
* SQLcl 19.4
* AOP 19.3.2
* Mailcatcher

choose your box on S:\Tools\Software\Vagrant\Boxes
the file name of the boxes consists of 3 parts
* 1st part -> oracle linux version (ol7)
* 2nd part -> oracle version (orcl183)
* 3th part => apex version (apx191)

import box in vagrant:
 `vagrant box add ol7_orcl183_apx191 /s/Tools/Software/Vagrant/Boxes/ol7_orcl183_apx191.box`

### startup your custom vagrant
1. Go to the clean project folder: `cd src/toolbox/vagrant/clean`
1. Startup vagrant box `initiate.sh`
1. You are ready to go and work on the database. Have fun

## Start and stop db
If the database is not running in the vagrant vm. The processes can be started with these commands:

`vagrant ssh` \
`sudo su - oracle` \
`cd /home/oracle/scripts/`\
`sh start_all.sh` \
or \
`sh stop_all.sh`

## Start MailCatcher and AOP
If the MailCatcher and AOP servers aren't running, use the following command to start them:

`vagrant provision`

## Environment

### Container database

* connect string: localhost:1521/cdb1
* ORACLE_SID=cdb1
* SYS_PASSWORD="SysPassword1"
* DATA_DIR=/u02/oradata

### pluggable database

* connect string: localhost:1521/pdb1
* SYS_PASSWORD="SysPassword1"
* PDB_NAME="pdb1"
* PDB_PASSWORD="PdbPassword1"

### APEX
* workspace="INTERNAL"
* username ="admin"
* password="ApexPassword1"
* apex: http://localhost:8080/ords

### Apex Office Print
APEX Office Print makes printing and exporting of data in Oracle Application Express (APEX) easy. Make a template in Office, HTML or Text, choose data from a database and merge them into one. You can easily export the result with office or PDF and have your document in no time. It saves you time and effort by creating templates in which you can integrate your data.

* official site: https://www.apexofficeprint.com/
* local server: http://localhost:8010

### Mailcatcher
MailCatcher runs a super simple SMTP server which catches any message sent to it to display in a web interface. Run mailcatcher, set your favourite app to deliver to smtp://localhost:1025 instead of your default SMTP server, then check out http://localhost:1080 to see the mail that's arrived so far.

* official site: https://mailcatcher.me/
* local server: http://localhost:1080

## Snapshots
Save snapshot `vagrant snapshot save initial`

Restore snapshot `vagrant snapshot restore initial`

## Other commands

* shell on vagrant machine: `vagrant ssh`
* Shutdown vm machine: `vagrant halt`
* Start  vm machine: `vagrant up`
* Suspend vm machine: `vagrant suspend`
* Resume vm machine: `vagrant resume`
* Delete  vm machine: `vagrant destroy`
* export box `vagrant package --output cc_latest.box`
* global status `vagrant global-status`
* https://www.vagrantup.com/docs/cli/