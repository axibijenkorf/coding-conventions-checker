project_root=$(git rev-parse --show-toplevel)
#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "local"
vm_name="Clean"
vagrant_box="ol7_orcl193_apx191"
box_path="//axi.intra/public/Tools/Software/Vagrant/Boxes"

#import vagrant box
echo -e  "${GREEN}====> import vagrant box${RESET}"
cur_box=$(vagrant box list | grep $vagrant_box)

if [ ! -z "$cur_box" ] ; then
    echo "${GREEN}vagrant box (${vagrant_box}) already exists in vagrant repo: ${cur_box} ${RESET}"
else
    echo "${GREEN}vagrant box (${vagrant_box}) does not exists in vagrant repo. Please import it. ${RESET}"
fi

prompt=''
until  [ "${prompt,,}" = "y" ] || [ "${prompt,,}" = "n" ] ;
do
   read -p "${YELLOW}Do you want to import a box? [y or n]: ${RESET}" prompt
done

if [ "${prompt,,}" == 'y' ]; then
    read -p $'We will import the box from the default folder: '$box_path$'\nPlease provide another folder where we can find the boxes.\nOr leave empty when you want to use the default: '${RESET} input_dir
    if [ ! -z "$input_dir" ] ; then
        box_path=$input_dir
    fi
    vagrant box add ${vagrant_box} "${box_path}/${vagrant_box}.box" --force
fi

echo -e  "${GREEN}====> Start vagrant box${RESET}"
#start vagrant
vagrant up

#take initial snapshot
echo -e  "${GREEN}====> Take initial snapshot${RESET}"
vagrant snapshot save $vm_name initial


echo -e  "${GREEN}====> install and run dba scripts${RESET}"
#install dba user
sqlplus "sys/SysPassword1@$db_sqlplusurl as sysdba" << EOF

    @"../../../../src/main/database/dba/create_user_dba.sql"
    @"../../../../src/main/database/dba/grant_user_dba.sql"
    @"../../../../src/main/database/dba/grant_user_dba_dev.sql"
    /

    exit
EOF

echo -e  "${GREEN}====> take DBA snapshot${RESET}"
vagrant snapshot save $vm_name dba

#mvn install -P local
