# Vagrant latest DB environment

mofe info in clean/readme.md

to use a latest vagrant box, make sure you follow following steps.
1. startup clean vagrant box
1. install latest build on local database
1. export box `vagrant package --output latest.box`
1. save your box on your project folder on the S-drive
1. change initiate.sh script to refer to your new created box
1. change var_box and var_vm_name in vagrantfile if needed
1. run initiate.sh