cls

$rootpath = git rev-parse --show-toplevel
$rootpath = $rootpath.Replace("/","\")

. "$rootpath\src\toolbox\repo\generate_vars.ps1"
cls

$env= read-host "enter profile you want to load (dev, test or prd)"
$env= $env.ToUpper()

switch($env){
    "TEST" {. "$rootpath\conf\env\generated\test.ps1"}
    "PROD" {. "$rootpath\conf\env\generated\prd.ps1"
            $env="PRD"}
    "PRD" {. "$rootpath\conf\env\generated\prd.ps1"}
    "DEV" {. "$rootpath\conf\env\generated\dev.ps1"}
    default {. "$rootpath\conf\env\generated\test.ps1"}
}

write-host $env
#write-host $test_db_sys_password
$username = "sys"
$password = "SysPassword1"
$datasource = "localhost:1521/pdb1"

$v_file = "CLONE_$env"

if (!(Test-Path "$v_file.sql"))
{
    $loc = $db_sqlplusUrl.IndexOf(":")
    $var = $db_sqlplusUrl.Split("/")
    $i_src_user = Read-Host "Enter Schema which you want to clone"
    $i_dst_user = Read-Host "Enter destination schema (does not need to exist)"
    $i_src_admin = $db_sys_username # read-host "Enter admin user that has full export privs for source schema"
    $i_src_pwd  = $db_sys_password #read-host "Enter password for admin user"
    $i_host = $db_sqlplusUrl.Substring(0,$loc) #read-host "Enter host (ex 192.168.103.37)"
    $i_sid = $var[1]
    $i_db_link = $env
    $i_max_rows = "10"

    New-Item "$v_file.sql"

    $v_content ="set serveroutput on size 1000000;
    spool $rootpath\src\toolbox\data\clone-data\$v_file.log
    begin
        prc_migrate_data(
        i_src_user => '$i_src_user',
        i_dst_user=> '$i_dst_user',
        i_src_admin => '$i_src_admin',
        i_src_pwd => '$i_src_pwd',
        i_db_link => '$i_db_link',
        i_max_rows => $i_max_rows,
        i_host => '$i_host',
        i_sid => '$i_sid',
        i_date => null);
    end;
    /
    spool off;
    ;
    exit;
     "
     Add-Content "$v_file.sql" $v_content

} else {
    write-host -ForegroundColor "yellow" "$v_file.sql already exists, the settings of the file will be used"
}

$execute = read-host  "Please review file $v_file.sql and hit enter to execute, enter N to abort"

if ($execute -eq "N")
{
    write-host -ForegroundColor "yellow" "data clone was aborted"
}
else {
    sqlplus $username/$password@$datasource as sysdba "@$rootpath\src\toolbox\data\clone-data\prc_migrate_data.sql"
    sqlplus $username/$password@$datasource as sysdba "@$rootpath\src\toolbox\data\clone-data\$v_file.sql"
    write-host "data clone is completed, please check log file"
    write-host "@$rootpath\src\toolbox\data\clone-data\$v_file.log"
}

write-host -ForegroundColor "green" "script completed"