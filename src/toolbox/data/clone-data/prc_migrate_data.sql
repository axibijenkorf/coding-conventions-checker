create or replace procedure prc_migrate_data(
    i_src_user in varchar2,
    i_dst_user in varchar2,
    i_src_admin in varchar2,
    i_src_pwd in varchar2,
    i_db_link in varchar2,
    i_max_rows in number default 1000,
    i_host in varchar2,
    i_sid in varchar2,
    i_date in varchar2 default '20180101'
)
as
    l_hnd    NUMBER; -- job handle
    l_js     user_datapump_jobs.state%TYPE; -- to hold job status
    l_q      VARCHAR2(1) := chr(39); -- single quote
    ind      NUMBER;-- Loop index number
    --l_job_name varchar2(100); -- Job Name
    percent_done NUMBER;-- Percentage of job complete
    job_state VARCHAR2(30);--Keeps track of job state
    le      ku$_LogEntry;-- work-in-progress and error messages
    js      ku$_JobStatus; -- Job status from get_status
    sts     ku$_Status; -- Status object returned by get_status

    type    tp_spaces is table of varchar2(500);
    cfetchedrows  tp_spaces;

    type rc_col  is record (
        col1     varchar2(255),
        col2     varchar2(4000)
    );
    type gt_col   is table of rc_col;
    ctables   gt_col;

    v_count  number;
    l_host   VARCHAR2(100) := trim(i_host); --'192.168.103.37';
    l_acl    varchar2(200);
    l_desc   varchar2(200);
    l_principal   varchar2(255);
    v_sql    varchar2(4000);

BEGIN

    /* create acl if needed */
    -- Create the new ACL.
    select  count(*)
    into    v_count
    from    dba_network_acls
    where   host =l_host;

    l_acl := replace(l_host,'.','_');
    l_desc := l_acl;
    l_acl := l_acl||'.xml';
    l_principal := user;

    if v_count = 0 then
        dbms_network_acl_admin.create_acl(l_acl, l_desc, l_principal, TRUE, 'connect');
        dbms_network_acl_admin.add_privilege(l_acl, l_principal, TRUE, 'resolve');
        dbms_network_acl_admin.assign_acl(l_acl, l_host);
        dbms_output.put_line('created acl for: '||l_host);
    else
        dbms_output.put_line('acl for '||l_host||' already exists');
    end if;

    select  count(*)
    into    v_count
    from    dba_db_links
    where   db_link = i_db_link;

    if v_count = 0 then
        v_sql := 'CREATE DATABASE LINK '||i_db_link||
                ' CONNECT TO '||i_src_admin||' IDENTIFIED BY '||i_src_pwd||' '||
                ' USING ''(DESCRIPTION = (ADDRESS = (PROTOCOL = TCP)(HOST = '||i_host||')(PORT = 1521)) (CONNECT_DATA = (SID = '||i_sid||') ))'' ';
        execute immediate v_sql;
        dbms_output.put_line('created db link '||i_db_link);
    else
        dbms_output.put_line('db link '||i_db_link||' already exists');
    end if;

    l_hnd := dbms_datapump.open ('IMPORT','SCHEMA', i_db_link);
    dbms_datapump.set_parallel(handle => l_hnd, degree => 8);
    dbms_datapump.set_parameter(l_hnd, 'FLASHBACK_SCN', dbms_flashback.get_system_change_number);
    dbms_datapump.metadata_filter(l_hnd, 'SCHEMA_LIST', l_q || i_src_user || l_q);

    if i_src_user <> i_dst_user then
        dbms_datapump.metadata_remap(l_hnd,'REMAP_SCHEMA',i_src_user,i_dst_user);
        dbms_output.put_line('remap schema '||i_src_user||' to '||i_dst_user);
    end if;

    execute immediate   'select  a.table_name, '||
                        ' ''where '' ||listagg(case when column_name in (''DEWYZDM'',''DEACTDM'')  and data_type =''DATE'' then '||
                        ' ''nvl(''||column_name||'',sysdate) > to_date('''''||i_date||''''',''''YYYYMMDD'''') or '' end ) '||
                        ' within group (order by column_name) ||'' rownum < '||i_max_rows||' '' cols '||
                        ' from    all_tab_columns@'||i_db_link||' a, all_tables@'||i_db_link||' b '||
                        ' where   a.owner = b.owner '||
                        ' and     a.table_name = b.table_name '||
                        ' and     a.owner ='''||i_src_user||''' '||
                        ' group   by a.table_name  '
    bulk collect into ctables;

    for i in ctables.first..ctables.last loop
        dbms_output.put_line('subquery: '||ctables(i).col1||' '||ctables(i).col2);
        dbms_datapump.data_filter (l_hnd, 'SUBQUERY', ctables(i).col2 ,ctables(i).col1,i_src_user);
    end loop;

    execute immediate 'select distinct tablespace_name from all_tables@'||i_db_link||' where owner = :i_src_user'
    bulk collect into cfetchedrows
    using i_src_user;
    if cfetchedrows.last > 0 then
    for i in cfetchedrows.first..cfetchedrows.last loop
        dbms_datapump.metadata_remap (l_hnd,'REMAP_TABLESPACE',cfetchedrows(i),'USERS');
        dbms_output.put_line('remap tablespace '||cfetchedrows(i)||' to '||'USERS');
    end loop;
    end if;

    execute immediate 'select tablespace_name from all_indexes@'||i_db_link||' where owner = :i_src_user minus '||
                      'select tablespace_name from all_tables@'||i_db_link||' where owner = :i_src_user'
    bulk collect into cfetchedrows
    using i_src_user,i_src_user;
    if cfetchedrows.last > 0 then
    for i in cfetchedrows.first..cfetchedrows.last loop
        dbms_datapump.metadata_remap (l_hnd,'REMAP_TABLESPACE',cfetchedrows(i),'USERS');
        dbms_output.put_line('remap tablespace '||cfetchedrows(i)||' to '||'USERS');
    end loop;
    end if;

    -- schemas remappen ivm grants voor diegene die niet bestaan in de huidige omgeving
    execute immediate 'select grantee from dba_tab_privs@'||i_db_link||' where owner =:i_src_user minus select username from all_users '
    bulk collect into cfetchedrows
    using i_src_user;
    for i in cfetchedrows.first..cfetchedrows.last loop
        if cfetchedrows(i) <> i_dst_user then
            dbms_datapump.metadata_remap (l_hnd,'REMAP_SCHEMA',cfetchedrows(i),'DEVADM');
            dbms_output.put_line('remap schema '||cfetchedrows(i)||' to devadm');
        end if;
    end loop;

    dbms_output.put_line('');
    dbms_output.put_line('**************************************************');
    dbms_output.put_line('start import ');
    dbms_output.put_line('**************************************************');


    dbms_datapump.start_job(l_hnd);

    percent_done := 0;
    job_state := 'UNDEFINED';

    while (job_state != 'COMPLETED') and (job_state != 'STOPPED') LOOP

    dbms_datapump.get_status(l_hnd,
    dbms_datapump.ku$_status_job_error +
    dbms_datapump.ku$_status_job_status +
    dbms_datapump.ku$_status_wip,-1,job_state,sts);
    js := sts.job_status;

    if js.percent_done != percent_done then
   --dbms_output.put_line('*** job percent done = '|| to_char(js.percent_done));
    percent_done := js.percent_done;
    end if;

    if (bitand(sts.mask,dbms_datapump.ku$_status_wip) != 0) then
    le := sts.wip;
    else
    if (bitand(sts.mask,dbms_datapump.ku$_status_job_error) != 0) then
    le := sts.error;
    else
    le := null;
    end if;
    end if;

    if le is not null then
        ind := le.first;
        while ind is not null loop
            dbms_output.put_line(le(ind).logtext);
            ind := le.next(ind);
        end loop;
    end if;

    end loop;

    dbms_output.put_line('job has completed');
    dbms_output.put_line('final job state = ' ||job_state);
    dbms_datapump.detach (l_hnd);

end;
/

exit;
