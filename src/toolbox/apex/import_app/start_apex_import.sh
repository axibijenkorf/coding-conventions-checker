#!/bin/bash
env=$1
user=$2

cur_dir=$(pwd)
project_folder=$(git rev-parse --show-toplevel)

exitCode=0

cd "$project_folder"

chmod +x conf/env/*.sh
chmod +x src/toolbox/apex/import_app/*.sh
#initiate global variables from properties files
. "$project_folder/conf/env/initiate_vars.sh" "$env" "$user"

workspace=$apex_workspace
schema=$db_username
exectype=$apex_importtype
export_dir="$apex_install_workdir"
## Mogelijkheid om applicatie onder een ander nummer te installeren bijv. wanneer test en productie op dezelfde database draaien
app_offset=$apex_application_offset
export NLS_LANG=AMERICAN_AMERICA.UTF8

## werkfolder import aanmaken indien nodig
import_dir="$project_folder/src/main/apex/import/"
mkdir -p "${import_dir}"

## werkfolder leeg maken
rm -f "${import_dir}/"*.sql
rm -f "${import_dir}/"*.xml

## alle gevonde apex apps kopieren
cp "src/main/apex/changelog/applications.xml" "${import_dir}applications.xml"
cp "$project_folder/${export_dir}"*.sql "${import_dir}"

## Oplijsten van alle APEX files
diff_files="$(ls -1rS ${import_dir}*.sql | sort -n )"
tot=$(echo $diff_files | grep -o " " | wc -l)
cnt=0

echo "---- Preparing ${tot} import files ----"

echo "start: $(date -u)"

for file in $diff_files
do
    tempfile="$(basename $file)"
    initialiseren=""
    finaliseren=""
    changeset=""

    if [[ "$tempfile" = "workspace_files.sql" ]]; then

        ## We gebruiken een workspace export voor CBOM_DW (datawedge oplossing HHT)
        application="workspace_files"

        initialiseren=$initialiseren"declare \n"
        initialiseren=$initialiseren"lco_workspace      constant apex_workspaces.workspace%type        := '$workspace'; \n"
        initialiseren=$initialiseren"lco_schema         constant varchar2(30)                          := '$schema'; \n"
        initialiseren=$initialiseren"l_workspace_id apex_workspaces.workspace_id%type; \n"
        initialiseren=$initialiseren"begin\n"
        initialiseren=$initialiseren"l_workspace_id := apex_util.find_security_group_id (p_workspace => upper(lco_workspace)); \n"
        initialiseren=$initialiseren"apex_application_install.set_workspace_id( l_workspace_id );\n"
        initialiseren=$initialiseren"apex_application_install.generate_offset;\n"
        initialiseren=$initialiseren"apex_application_install.set_schema(upper(lco_schema));\n"
        initialiseren=$initialiseren"end;\n"
        initialiseren=$initialiseren"/"

        NEW_APPNR="workspace_files"
    elif [[ "$tempfile" == f*.sql ]]; then

        ## Bestandsnaam = f[applicatienummer].sql
        application=$(echo "${tempfile%.*}" | awk -F'[f]' '{print $2}')

            if [ -z "$app_offset" ]; then
                NEW_APPNR=$application
            else
                NEW_APPNR=$(($application+$app_offset*1000000))
            fi

        ## Alias van de applicatie ophalen uit APEX export bestand
        alias="$(grep 'p_alias=>nvl(wwv_flow_application_install.get_application_alias,' ${file} |  awk -F'['\'']' '{print $2}')"

        ## Nieuwe versie
        initialiseren=$initialiseren"declare \n"
        initialiseren=$initialiseren"lco_application_id constant apex_applications.application_id%type := '$NEW_APPNR'; \n"
        initialiseren=$initialiseren"lco_workspace      constant apex_workspaces.workspace%type        := '$workspace'; \n"
        initialiseren=$initialiseren"lco_schema         constant varchar2(30)                          := '$schema'; \n"
        initialiseren=$initialiseren"l_workspace_id apex_workspaces.workspace_id%type; \n"
        initialiseren=$initialiseren"begin \n"
        initialiseren=$initialiseren"l_workspace_id := apex_util.find_security_group_id (p_workspace => upper(lco_workspace)); \n"
        initialiseren=$initialiseren"apex_application_install.set_workspace_id(l_workspace_id); \n"
        initialiseren=$initialiseren"apex_application_install.set_application_id(lco_application_id); \n"
        initialiseren=$initialiseren"apex_application_install.generate_offset; \n"
        initialiseren=$initialiseren"apex_application_install.set_schema(upper(lco_schema)); \n"
        initialiseren=$initialiseren"apex_application_install.set_auto_install_sup_obj(true); \n"
        initialiseren=$initialiseren"end; \n"
        initialiseren=$initialiseren"/"

        ## Statements die na installatie moeten uitgevoerd worden
        finaliseren=$finaliseren"begin \n"
        finaliseren=$finaliseren"for c1 in (select workspace_id \n"
        finaliseren=$finaliseren"                from apex_workspaces \n"
        finaliseren=$finaliseren"                where workspace = '$workspace') loop \n"
        finaliseren=$finaliseren"    apex_util.set_security_group_id( c1.workspace_id ); \n"
        finaliseren=$finaliseren"    exit; \n"
        finaliseren=$finaliseren"end loop; \n"
        finaliseren=$finaliseren"for c2 in ( \n"
        finaliseren=$finaliseren"select      primary_application_id, translated_app_language \n"
        finaliseren=$finaliseren"from        apex_application_trans_map \n"
        finaliseren=$finaliseren"where       primary_application_id = '$application') loop \n"
        finaliseren=$finaliseren"apex_lang.publish_application( \n"
        finaliseren=$finaliseren"    p_application_id => c2.primary_application_id, \n"
        finaliseren=$finaliseren"    p_language => c2.translated_app_language ); \n"
        finaliseren=$finaliseren"end loop; \n"
        finaliseren=$finaliseren"commit; \n"
        finaliseren=$finaliseren"end; \n"
        finaliseren=$finaliseren"/"
    fi

        ## Initialisatie toevoegen aan APEX export file
        sed -i -e 1"s#.*#\n$initialiseren\n&#" $file

        if [[ "${exectype,,}" == "maven" ]]; then

            ## Zorgen dat SQL*Plus commando's in APEX export file genegeerd worden
            sed -i -e 's/^prompt .*/--ignoreLines:start\n&\n--ignoreLines:end/' -e 's/^set define .*/--ignoreLines:start\n&\n--ignoreLines:end/' -e 's/^set verify .*/--ignoreLines:start\n&\n--ignoreLines:end/' -e 's/^whenever sqlerror .*/--ignoreLines:start\n&\n--ignoreLines:end/' $file

            ## Zorgen dat APEX export file aanzien wordt als een Liquibase changeset
            sed -i -e 1"s/.*/--liquibase formatted sql\n--changeset axi:application-"$NEW_APPNR" splitStatements:true endDelimiter:/" -e 's/endDelimiter:$/&(\?m)\^\/\$ runOnChange:true/' $file

            # replace/escape \_ with '||chr(92)||'_
            sed -i -e 's/\\_/\x27\|\|chr(92)\|\|\x27_/g' $file
            # replace/escape '\' with '||chr(92)||'
            sed -i -e 's/\x27\\\x27/\x27\x27\|\|chr(92)\|\|\x27\x27/g' $file
        fi

    ## Finalisatie (niet voor workspace file) toevoegen aan APEX export file
    if [[ "$tempfile" == f*.sql ]]; then
        echo -e $finaliseren >> $file
    fi

    ## APEX export file toevoegen aan Liquibase changelog voor APEX
    if [[ "$exectype" == "sqlcl" ]]; then
        changeset=$changeset'<changeSet author="axi" id="application-'$NEW_APPNR'" failOnError="true"> \n'
        changeset=$changeset'<n0:runOracleScript objectName="'$tempfile'" ownerName="CC" sourceType="FILE"> \n'
        changeset=$changeset'  <n0:source> \n'
        changeset=$changeset'    <![CDATA[src/main/apex/import/'$tempfile']]> \n'
        changeset=$changeset'  </n0:source> \n'
        changeset=$changeset'</n0:runOracleScript> \n'
        changeset=$changeset'</changeSet>'

        sed -i "$ i\    \n$changeset" ${import_dir}applications.xml
    else
        sed -i '$ i\    <include file="src/main/apex/import/'$tempfile'"/>' ${import_dir}applications.xml
    fi
done

echo "end: $(date -u)"

cd "$project_folder"

if [[ "$exectype" == "sqlcl" ]]; then
    ./src/toolbox/apex/import_app/import_sqlcl.sh "$env" "$user"
else
    mvn -f pom_import_apex.xml -P "$env" install "-Dimport_username=$db_username" "-Dimport_password=$db_password"
fi
exitCode=$?

## werkfolder leeg maken
rm -f "${import_dir}/"*.sql

cd "$cur_dir"
exit $exitCode