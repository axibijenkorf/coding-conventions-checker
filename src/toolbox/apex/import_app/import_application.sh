#!/bin/bash
set +x

WORKSPACE=$1
SCHEMA=$2
PASSWORD=$3
URL=$4
EXPORT_DIR="$5"

if [ -n "$6" ]; then
    APP_OFFSET=$6
fi

project_folder=$(git rev-parse --show-toplevel)
cd "$project_folder"

# check if an initial install was already executed
if [ -f "${EXPORT_DIR}first_run_done.txt" ]; then

    if  [ -z "$GIT_PREVIOUS_SUCCESSFUL_COMMIT" ]; then
        # get last commit from latest tag
        echo "Get commit form tag = $(git describe --abbrev=0)"
        PREV_SUCCES_COMMIT=$(git show-ref -s  `git describe --abbrev=0`)
    else
        # get last succesful commit from jenkins
        PREV_SUCCES_COMMIT=$GIT_PREVIOUS_SUCCESSFUL_COMMIT
    fi
    echo "Previous succesful build commit Hash =  ${PREV_SUCCES_COMMIT}"
    #list all application files changed since the previous commit
    diff_files=`git diff-tree -r --no-commit-id --name-only --diff-filter=ACMRT ${PREV_SUCCES_COMMIT} HEAD src/main/apex/ | grep -e "src/main/apex/.*.sql" | grep -v "src/main/apex/f[.]*"`
else
    diff_files=`find ${EXPORT_DIR}*.sql -printf "%p\n"`
fi

cd src/toolbox/apex/import_app/

tot=$(echo $diff_files | grep -o " " | wc -l)
echo $tot
cnt=0

for i in ${diff_files}
do
   if [[ $i == *"$5"f*.sql ]] && [[ $i != *"$5"*/* ]]; then
        fname=`basename $i`
        if [ "$fname" != ".gitignore" ]; then
            file=${fname%.*}
            appnr=${file:1}
             ((cnt+=1))

            if [ -z "$APP_OFFSET" ]; then
                NEW_APPNR=$appnr
            else
                NEW_APPNR=$(($appnr+$APP_OFFSET*1000000))
            fi

             echo "--==== Importing apex app $appnr as $NEW_APPNR - $cnt of $tot ====--"
             echo ${project_folder}/$i

            sqlplus "$SCHEMA/$PASSWORD@$URL" << EOF
            SET ESCCHAR @
            @"import_application.sql" "${NEW_APPNR}" ${WORKSPACE} ${SCHEMA}
            /
            @"${project_folder}/$i"
            /
            begin
            for rec in (select * from apex_application_trans_map where primary_application_id = ${NEW_APPNR} ) loop
                apex_lang.publish_application (
                p_application_id    =>  rec.primary_application_id,
                p_language          =>  rec.translated_app_language
                );
            end loop;
            end;
            /
            commit;

            exit
EOF
        fi
   fi
done
sqlplus "$SCHEMA/$PASSWORD@$URL" << EOF

    @"import_application.sql" "${NEW_APPNR}" ${WORKSPACE} ${SCHEMA}
    /
    @"${project_folder}/${EXPORT_DIR}workspace_files.sql"
    /
    commit;

    exit
EOF

if [ ! -f "${project_folder}/${EXPORT_DIR}first_run_done.txt" ]; then
    2> "${project_folder}/${EXPORT_DIR}first_run_done.txt"
fi