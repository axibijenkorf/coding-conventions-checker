#!/bin/bash
env=$1
user=$2

cur_dir=$(pwd)
project_folder=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_folder/conf/env/initiate_vars.sh" "$env" "$user"

cd "$project_folder"

# Delete log files older then 7 days
cd src/main/apex/changelog/
find . -name "*.log" -type f -mtime +7 -exec rm -f {} \;

cd "$project_folder"

sql "$db_username/$db_password@$db_sqlplusurl" << EOF
    lb update src/main/apex/import/applications.xml
EOF

cd "$cur_dir"