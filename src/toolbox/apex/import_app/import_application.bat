@echo off

set FROM_APP_ID=%1
set TO_APP_ID=%2
set WORKSPACE=%3
set SCHEMA=%4
set PASSWORD=%5
set URL=%6

(
echo @"import_application.sql" %TO_APP_ID% %WORKSPACE% %SCHEMA%
echo /
echo @".\..\..\..\..\src\main\apex\export\f%FROM_APP_ID%.sql"
echo /
echo commit;
echo exit
) | sqlplus %SCHEMA%/%PASSWORD%@%URL%