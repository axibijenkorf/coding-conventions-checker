env=$2
user=$3

cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "$env" "$user"

#Syntax voor cmd
#export_app.sh <directory> <db_connectie> <schema> <paswoord> "<applicaties>" <workspace_id>
#Workspace id enkel wanneer je ook de workspace files wil exporteren
apps=""
apps=$1

workspace_id=$apex_workspace_id

echo -e  "${GREEN}====> We will import the apex applications: ${apps}${RESET}"
echo -e  "${GREEN}====> Into DB: $db_sqlplusurl : $db_username${RESET}"
echo -e  "${GREEN}====> Workspace: $apex_workspace_id"

prompt=''
until  [ "${prompt,,}" = "y" ] || [ "${prompt,,}" = "n" ] ;
do
   read -p "${YELLOW}Do you want to continue? [y or n]: ${RESET}" prompt
done

#if parameter contains |
#retrieve appliation id from parameter
tot=$(echo $apps | grep -o "|" | wc -l)
if [ $tot == 1 ]
then
apps=$(echo $apps| cut -d'|' -f 1)
fi

if [ -n "$apps" ]; then
    bash import_application.sh "$apex_workspace_id" "$db_username" "$db_password" "$db_sqlplusurl" "${apex_install_workdir}" "$apex_application_offset" "$apps"
else
    echo "No apps given"
fi


