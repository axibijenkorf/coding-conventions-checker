declare
  lco_application_id constant apex_applications.application_id%type := '&&1';
  lco_workspace      constant apex_workspaces.workspace%type        := '&&2';
  lco_schema         constant varchar2(30)                          := '&&3';

  l_workspace_id apex_workspaces.workspace_id%type;
begin
  l_workspace_id := apex_util.find_security_group_id (p_workspace => upper(lco_workspace));

  apex_application_install.set_workspace_id(l_workspace_id);
  apex_application_install.set_application_id(lco_application_id);
  apex_application_install.generate_offset;
  apex_application_install.set_schema(upper(lco_schema));
  APEX_APPLICATION_INSTALL.SET_AUTO_INSTALL_SUP_OBJ(true);
end;