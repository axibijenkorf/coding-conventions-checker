#!/bin/bash
echo $1
echo $2
echo $3
echo $4
echo $5
apexDir=$1
db=$2
user=$3
password=$4
appIds=$5
appIdsArray=($appIds)
workspaceId=$6
commando=""
apps=();
OS="`uname`"

echo $OS;

# string to array
OIFS=$IFS
IFS=","

cd $apexDir

for ((i = 0; i < ${#appIdsArray[@]}; ++i))
do
  apps+=(${appIdsArray[$i]})
  command="apex export -applicationid ${appIdsArray[$i]} -expTranslations -expPubReports -split"
  echo $command
  echo $command | sql $user/$password@$db
done

#cd $apexDir
if [ -n "$workspaceId" ]; then
    command="apex export -workspaceid ${workspaceId} -expFiles"
    echo $command
    echo $command | sql $user/$password@$db
    mv files_"$workspaceId".sql workspace_files.sql
fi

IFS=$OIFS

IFS=$'\n'; set -f
#loopen over de apps
for i in "${apps[@]}"
do
  echo "$i"
  for f in $(find "./f${i}" -name '*.sql'); do
    echo $f;
    filename=$(basename -- "$f")
    if [ $filename = "install.sql" ]; then

      appdir="${PWD}/f${i}/"
      appdir=${appdir:1}
      appdir=$(echo "$appdir" | sed 's/\//\\\//g')
      echo "filename"
      echo $filename
      echo "appdir"
      echo $appdir
      echo "replace path"
      regex="s/${appdir}//g"
      if [ $OS != "Darwin" ]; then
        sed -i $regex $f
      else
        sed -i '' $regex $f
      fi
    fi
    if [ $OS != "Darwin" ]; then
      sed -i '/^ *--/d' $f
    else
      sed -i '' '/^ *--/d' $f
    fi
  done

  for f in $(find . -name "*f${i}*.sql" -and -not -path "./f${i}"); do
    echo $f;
    if [ $OS != "Darwin" ]; then
      sed -i '/^ *--/d' $f
    else
      sed -i '' '/^ *--/d' $f
    fi
  done

done
unset IFS; set +f

echo $PWD
