env=$1
user=$2

cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "$env" "$user"

#Syntax voor cmd
#export_app.sh <directory> <db_connectie> <schema> <paswoord> "<applicaties>" <workspace_id>
#Workspace id enkel wanneer je ook de workspace files wil exporteren
sh export_app.sh ../../../main/apex/export/ $db_sqlplusurl $db_username $db_password "" $apex_workspace_id

#select workspace_id
#from apex_workspaces
#where upper(workspace) = upper('DISCOVERY');