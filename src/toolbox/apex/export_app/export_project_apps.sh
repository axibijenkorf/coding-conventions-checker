env=$2
user=$3

cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "$env" "$user"

echo -e "${GREEN}"
echo "           _____  ________   __  ________   _______   ____  _____ _______   "
echo "     /\   |  __ \|  ____\ \ / / |  ____\ \ / /  __ \ / __ \|  __ \__   __|  "
echo "    /  \  | |__) | |__   \ V /  | |__   \ V /| |__) | |  | | |__) | | |     "
echo "   / /\ \ |  ___/|  __|   > <   |  __|   > < |  ___/| |  | |  _  /  | |     "
echo "  / ____ \| |    | |____ / . \  | |____ / . \| |    | |__| | | \ \  | |     "
echo " /_/    \_\_|    |______/_/ \_\ |______/_/ \_\_|     \____/|_|  \_\ |_|     "
echo "                                                                            "
echo -e "${RESET}"


#Syntax voor cmd
#export_app.sh <directory> <db_connectie> <schema> <paswoord> "<applicaties>" <workspace_id>
#Workspace id enkel wanneer je ook de workspace files wil exporteren
apps=""
apps=$1

workspace_id=$apex_workspace_id

#if parameter contains |
#retrieve appliation id from parameter
tot=$(echo $apps | grep -o "|" | wc -l)
if [ $tot == 1 ]
then
apps=$(echo $apps| cut -d'|' -f 1)
fi

#if parameter is empty then export all apps
#Get all applications for full export
#select      listagg(application_id, ',') within group (order by application_id)
#from        apex_applications
#where       application_group = 'AXI_CC'
if ! [ -n "$apps" ]; then
    apps="$apex_apps"
fi

#export workspace
echo -e "${GREEN}Exporting workspace $workspace_id ${RESET}"
bash export_app.sh ../../../main/apex/export/ $db_sqlplusurl $db_username $db_password "" $apex_workspace_id

tot=$(echo $apps | grep -o "," | wc -l)
((tot+=1))
cnt=0

for i in ${apps//,/ }
do
    ((cnt+=1))
    echo -e "${GREEN}--==== Exporting apex app $i - $cnt of $tot ====--${RESET}"
    # call your procedure/other scripts here below
    bash export_app.sh ../../../main/apex/export/ $db_sqlplusurl $db_username $db_password  "$i"
done
