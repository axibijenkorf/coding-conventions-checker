#!/bin/bash
project_root=$(git rev-parse --show-toplevel)
apps=$1
message=$2

chmod -R 755 conf/env/

cd "$project_root"
cd src/toolbox/apex/export_app/

bash export_project_apps.sh "$apps" dev

cd "$project_root"
cd src/main/apex/export/

git add .

git commit -m "$message"