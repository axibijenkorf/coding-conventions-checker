# script to initiate new db folder in main/database folder
# copy structure from #repo.folder# to new folder (name given by user)
# add variables to properties files in conf/env


# this function will add content to a file if the file does not exists it will be created
# i_file = fullname of the file that needs to be created/modified (should include full directory)
# i_content = content that should be added to the file
# i_after_text = if given, the content will added below the provided string. In case the string is available multiple times in the file.
#                the content will be added after each occurrence of the string.
# i_key = if provided, the function will first check whether the "key" string is already available. if so, the content is not written.
function add_file_content {
    Param ($i_file, $i_content, $i_after_text, $i_key)

    $v_found = "N"
    if (!($i_key -eq $null))
    {
    Select-String -Path $i_file -Pattern $i_key | Foreach-Object {
        $v_found ="Y"
        }
    }
    else {
        $v_found ="N"
    }

    if ($v_found -eq "N")
    {
        if (!($i_content -eq $null))
        {
            if ($i_after_text -eq $null -or $i_after_text -eq "" )
            {
            Add-Content $i_file $i_content
            }
            else {
                $v_content = $i_after_text + '
                ' + $i_content + '
                '
                ((Get-Content -path $i_file -Raw) -replace $i_after_text,$v_content ) | Set-Content -Path $i_file
            }
        }
    }


}

function replace_file_content {
    Param ($i_file, $i_origstring, $i_newstring)

    Select-String -Path $i_file -Pattern $i_origstring | Foreach-Object {
            $v_found ="Y"
            }
    if ($v_found -eq "Y")
    {
        ((Get-Content -path $i_file -Raw) -replace $i_origstring,$i_newstring ) | Set-Content -Path $i_file
        write-host -ForegroundColor "green" "$i_file"
        Write-Host -ForegroundColor "green" "$i_origstring replaced with $i_newstring"
    }
}


function create_folder {
Param ($i_folder)

    if (!(Test-Path $i_folder))
    {
        mkdir $i_folder
        Write-Host -ForegroundColor "green" "$i_folder created"
    }
    else {
        Write-Host -ForegroundColor "yellow" "$i_folder already exist"
    }

}

function create_file {
Param ($i_file, $i_content)

    if (!(Test-Path $i_file))
    {
        New-Item $i_file
        Write-Host -ForegroundColor "green" "$i_file created"

        if (!($i_content -eq $null))
        {
            add_file_content $i_file $i_content
        }
    } else {
        Write-Host -ForegroundColor "yellow" "$i_file already exist"
    }
}

cls
Write-Host "start script"

$rootpath = git rev-parse --show-toplevel
$rootpath = $rootpath.Replace("/","\")

$foldername = Read-Host "Enter folder name for schema"
$db_schema = Read-Host "Enter database schema"

create_folder "$rootpath\src\main\database\$foldername"
create_folder "$rootpath\src\main\database\$foldername\drop"
create_folder "$rootpath\src\main\database\$foldername\drop\constraints"
create_file "$rootpath\src\main\database\$foldername\drop\constraints\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\drop\grants"
create_file "$rootpath\src\main\database\$foldername\drop\grants\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\drop\directories"
create_file "$rootpath\src\main\database\$foldername\drop\directories\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\drop\indexes"
create_file "$rootpath\src\main\database\$foldername\drop\indexes\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\drop\job_classes"
create_file "$rootpath\src\main\database\$foldername\drop\job_classes\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\drop\jobs"
create_file "$rootpath\src\main\database\$foldername\drop\jobs\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\drop\processes"
create_file "$rootpath\src\main\database\$foldername\drop\processes\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\drop\queues"
create_file "$rootpath\src\main\database\$foldername\drop\queues\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\drop\ref_contraints"
create_file "$rootpath\src\main\database\$foldername\drop\ref_contraints\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\drop\sequences"
create_file "$rootpath\src\main\database\$foldername\drop\sequences\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\drop\tables"
create_file "$rootpath\src\main\database\$foldername\drop\tables\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\drop\triggers"
create_file "$rootpath\src\main\database\$foldername\drop\triggers\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\drop\views"
create_file "$rootpath\src\main\database\$foldername\drop\views\.gitkeep"

create_folder "$rootpath\src\main\database\$foldername"
create_folder "$rootpath\src\main\database\$foldername\install"
create_folder "$rootpath\src\main\database\$foldername\install\constraints"
create_file "$rootpath\src\main\database\$foldername\install\constraints\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\credentials"
create_file "$rootpath\src\main\database\$foldername\install\credentials\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\data"
create_file "$rootpath\src\main\database\$foldername\install\data\init.js"
create_folder "$rootpath\src\main\database\$foldername\install\data\init"
create_file "$rootpath\src\main\database\$foldername\install\data\init\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\directories"
create_file "$rootpath\src\main\database\$foldername\install\directories\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\indexes"
create_file "$rootpath\src\main\database\$foldername\install\indexes\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\job_classes"
create_file "$rootpath\src\main\database\$foldername\install\job_classes\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\jobs"
create_file "$rootpath\src\main\database\$foldername\install\jobs\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\processes"
create_file "$rootpath\src\main\database\$foldername\install\processes\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\programs"
create_file "$rootpath\src\main\database\$foldername\install\programs\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\queues"
create_file "$rootpath\src\main\database\$foldername\install\queues\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\ref_contraints"
create_file "$rootpath\src\main\database\$foldername\install\ref_contraints\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\schedules"
create_file "$rootpath\src\main\database\$foldername\install\schedules\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\sequences"
create_file "$rootpath\src\main\database\$foldername\install\sequences\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\install\tables"
create_file "$rootpath\src\main\database\$foldername\install\tables\.gitkeep"

create_folder "$rootpath\src\main\database\$foldername\latest"
create_folder "$rootpath\src\main\database\$foldername\latest\functions"
create_file "$rootpath\src\main\database\$foldername\latest\functions\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\latest\grants"
create_file "$rootpath\src\main\database\$foldername\latest\grants\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\latest\packages"
create_file "$rootpath\src\main\database\$foldername\latest\packages\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\latest\procedures"
create_file "$rootpath\src\main\database\$foldername\latest\procedures\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\latest\synonyms"
create_file "$rootpath\src\main\database\$foldername\latest\synonyms\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\latest\triggers"
create_file "$rootpath\src\main\database\$foldername\latest\triggers\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\latest\types"
create_file "$rootpath\src\main\database\$foldername\latest\types\.gitkeep"
create_folder "$rootpath\src\main\database\$foldername\latest\views"
create_file "$rootpath\src\main\database\$foldername\latest\views\.gitkeep"

create_folder "$rootpath\src\main\database\changelog\$foldername"
create_folder "$rootpath\src\test\database\$foldername"
create_folder "$rootpath\src\test\database\$foldername\packages"
create_folder "$rootpath\src\test\database\$foldername\changelog"
create_file "$rootpath\src\test\database\$foldername\packages\.gitkeep"

$masterxml = '<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog
                                       http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.0.xsd">

<!--
    <changeSet id="example_file_spec" author="axi" runAlways="false" runOnChange="true" failOnError="true" context="!prod">
        <sqlFile splitStatements="false" path="src/test/database/' + $foldername + '/packages/example_file.pks" endDelimiter="(?m)^/$" />
    </changeSet>
-->

</databaseChangeLog> '
create_file "$rootpath\src\test\database\$foldername\changelog\master.xml" $masterxml

$xmlbasecontent = '<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog
                                       http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.0.xsd">
<!--
endDelimiter="(?m)^/$"      -> optional (only add when your scripts uses sql and plsql)
context="dev"               -> optional (only add when scripts does not need to run in each environment)
splitStatements="false"     -> optional (used for installation of packages)
runAlways="false"           -> optional
runOnChange="true"          -> optional
failOnError="true"          -> optional
-->
<!--
    <changeSet id="example_file" author="axi" runAlways="false" runOnChange="true" failOnError="true">
        <sqlFile splitStatements="false" path="src/main/database/' + $foldername + '/#folder#/example_file.sql" endDelimiter="(?m)^/$" />
    </changeSet>
-->
</databaseChangeLog>'

$xmlsynonym ='<changeSet id="' + $foldername + '_logger_${log_user}" author="axi">
<sqlFile path="src/main/database/' + $foldername + '/latest/synonyms/logger.sql" />
</changeSet>'

$xmlcontent = $xmlbasecontent.Replace("#folder#","install/constraints")
create_file "$rootpath\src\main\database\changelog\$foldername\constraints.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","install/indexes")
create_file "$rootpath\src\main\database\changelog\$foldername\indexes.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","install/jobs")
create_file "$rootpath\src\main\database\changelog\$foldername\jobs.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","install/ref_constraints")
create_file "$rootpath\src\main\database\changelog\$foldername\ref_constraints.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","install/sequences")
create_file "$rootpath\src\main\database\changelog\$foldername\sequences.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","install/tables")
create_file "$rootpath\src\main\database\changelog\$foldername\tables.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","latest/functions")
create_file "$rootpath\src\main\database\changelog\$foldername\functions.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","latest/grants")
create_file "$rootpath\src\main\database\changelog\$foldername\grants.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","latest/packages")
create_file "$rootpath\src\main\database\changelog\$foldername\package_bodies.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","latest/packages")
create_file "$rootpath\src\main\database\changelog\$foldername\package_specs.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","latest/procedures")
create_file "$rootpath\src\main\database\changelog\$foldername\procedures.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","latest/synonyms")
$xmlcontent = $xmlbasecontent.Replace("-->","--> $xmlsynonym")
create_file "$rootpath\src\main\database\changelog\$foldername\synonyms.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","latest/triggers")
create_file "$rootpath\src\main\database\changelog\$foldername\triggers.xml" $xmlcontent
$xmlcontent = $xmlbasecontent.Replace("#folder#","latest/views")
create_file "$rootpath\src\main\database\changelog\$foldername\views.xml" $xmlcontent

$xmlcontent = '<?xml version="1.0" encoding="UTF-8"?>
<databaseChangeLog
    xmlns="http://www.liquibase.org/xml/ns/dbchangelog"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.liquibase.org/xml/ns/dbchangelog
                                       http://www.liquibase.org/xml/ns/dbchangelog/dbchangelog-3.0.xsd">
 <include file="src/main/database/changelog/' + $foldername + '/sequences.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/tables.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/package_specs.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/views.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/constraints.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/indexes.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/ref_constraints.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/functions.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/procedures.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/package_bodies.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/triggers.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/synonyms.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/grants.xml" />
 <include file="src/main/database/changelog/' + $foldername + '/jobs.xml" />
 <include file="src/test/database/' + $foldername + '/changelog/master.xml" />
 <include file="src/main/database/changelog/post-build/errors.xml" />
</databaseChangeLog>'
create_file "$rootpath\src\main\database\changelog\$foldername\master.xml" $xmlcontent

$syncontent = 'create or replace synonym LOGGER for '+'$'+'{log_user}.LOGGER;
create or replace synonym LOGGER_LOGS for '+'$'+'{log_user}.LOGGER_LOGS;
create or replace synonym LOGGER_LOGS_5_MIN for '+'$'+'{log_user}.LOGGER_LOGS_5_MIN;
create or replace synonym LOGGER_LOGS_60_MIN for '+'$'+'{log_user}.LOGGER_LOGS_60_MIN;
create or replace synonym LOGGER_LOGS_APEX_ITEMS for '+'$'+'{log_user}.LOGGER_LOGS_APEX_ITEMS;
create or replace synonym LOGGER_LOGS_TERSE for '+'$'+'{log_user}.LOGGER_LOGS_TERSE;
create or replace synonym LOGGER_PREFS for '+'$'+'{log_user}.LOGGER_PREFS;
create or replace synonym LOGGER_PREFS_BY_CLIENT_ID for '+'$'+'{log_user}.LOGGER_PREFS_BY_CLIENT_ID;
create or replace synonym pck_ts_generate_code for '+'$'+'{log_user}.pck_ts_generate_code;'
create_file "$rootpath\src\main\database\$foldername\latest\synonyms\logger.sql" $syncontent

$prop_content = '

# added by create_db_folder.ps1 for ' + $foldername + '
db.prj.username=' + $db_schema + '
db.' + $foldername + '.username=' + $db_schema + '
db.' + $foldername + '.password=' + $db_schema + '
db.' + $foldername + '.url=jdbc:oracle:thin:@localhost:1521/pdb1
db.' + $foldername + '.sqlplusUrl=localhost:1521/pdb1
liquibase-' + $foldername + '-dba.skip=false
liquibase-' + $foldername + '.skip=false
liquibase-' + $foldername + '-ut.skip=true
liquibase-' + $foldername + '-ut.min-coverage= '

add_file_content "$rootpath\conf\env\build.properties" $prop_content $null "db.$foldername"
add_file_content "$rootpath\conf\env\dev.properties" $prop_content  $null "db.$foldername"
add_file_content "$rootpath\conf\env\local.properties" $prop_content  $null "db.$foldername"
add_file_content "$rootpath\conf\env\prd.properties" $prop_content  $null "db.$foldername"
add_file_content "$rootpath\conf\env\test.properties" $prop_content  $null "db.$foldername"

$mvn_content = '
                    <execution>
                        <id>liquibase-' + $foldername + '</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>update</goal>
                        </goals>
                        <configuration>
                            <skip>${liquibase-' + $foldername + '.skip}</skip>
                            <driver>${liquibase.driver}</driver>
                            <url>${db.' + $foldername + '.url}</url>
                            <username>${db.' + $foldername + '.username}</username>
                            <password>${db.' + $foldername + '.password}</password>
                            <changeLogFile>src/main/database/changelog/' + $foldername + '/master.xml</changeLogFile>
                            <promptOnNonLocalDatabase>${liquibase.prompt}</promptOnNonLocalDatabase>
                            <verbose>${liquibase.verbose}</verbose>
                            <contexts>${liquibase.context}</contexts>
                            <databaseChangeLogTableName>${liquibase.databasechangelog}</databaseChangeLogTableName>
                            <databaseChangeLogLockTableName>${liquibase.databasechangeloglock}</databaseChangeLogLockTableName>
                            <expressionVars>
                                <property>
                                    <name>project_user</name>
                                    <value>${db.' + $foldername + '.username}</value>
                                </property>
                                <property>
                                    <name>apex_workspace</name>
                                    <value>${apex.workspace}</value>
                                </property>
                                <property>
                                    <name>apex_base_url</name>
                                    <value>${apex.base.url}</value>
                                </property>
                                <property>
                                    <name>audit_tablespace</name>
                                    <value>${db.sys.audit_tablespace}</value>
                                </property>
                                <property>
                                    <name>tools_user</name>
                                    <value>${db.tools.username}</value>
                                </property>
                                <property>
                                    <name>cc_user</name>
                                    <value>${db.cc.username}</value>
                                </property>
                                <property>
                                    <name>log_user</name>
                                    <value>${db.logger.username}</value>
                                </property>
                            </expressionVars>
                        </configuration>
                    </execution>
                    '

add_file_content "$rootpath\pom.xml" $mvn_content "<!-- DO_NOT_REMOVE_THIS_LINE_PRJ -->" "liquibase-$foldername"

$mvn_content = '
                    <execution>
                        <id>liquibase-dba-' + $foldername + '</id>
                        <phase>compile</phase>
                        <goals>
                            <goal>update</goal>
                        </goals>
                        <configuration>
                            <skip>${liquibase-' + $foldername + '-dba.skip}</skip>
                            <driver>${liquibase.driver}</driver>
                            <url>${db.url}</url>
                            <username>${db.sys.username}</username>
                            <password>${db.sys.password}</password>
                            <changeLogFile>src/main/database/changelog/dba_prj.xml</changeLogFile>
                            <promptOnNonLocalDatabase>${liquibase.prompt}</promptOnNonLocalDatabase>
                            <verbose>${liquibase.verbose}</verbose>
                            <contexts>${liquibase.context}</contexts>
                            <databaseChangeLogTableName>${liquibase.databasechangelog}</databaseChangeLogTableName>
                           <databaseChangeLogLockTableName>${liquibase.databasechangeloglock}</databaseChangeLogLockTableName>
                            <expressionVars>
                                <property>
                                    <name>prj_user</name>
                                    <value>${db.' + $foldername + '.username}</value>
                                </property>
                                <property>
                                    <name>prj_password</name>
                                    <value>${db.' + $foldername + '.password}</value>
                                </property>
                                <property>
                                    <name>audit_tablespace</name>
                                    <value>${db.sys.audit_tablespace}</value>
                                </property>
                                 <property>
                                    <name>tools_user</name>
                                    <value>${db.tools.username}</value>
                                </property>
                                 <property>
                                    <name>apex_workspace</name>
                                    <value>${apex.workspace}</value>
                                </property>
                                 <property>
                                    <name>apex_workspace_id</name>
                                    <value>${apex.workspace.id}</value>
                                </property>
                            </expressionVars>
                        </configuration>
                    </execution>'
add_file_content "$rootpath\pom.xml" $mvn_content "<!-- DO_NOT_REMOVE_THIS_LINE_DBA -->" "liquibase-dba-$foldername"


$mvn_content ='<execution>
                                <id>liquibase-' + $foldername + '-ut</id>
                                <phase>compile</phase>
                                <goals>
                                    <goal>update</goal>
                                </goals>
                                <configuration>
                                    <skip>${liquibase-' + $foldername + '-ut.skip}</skip>
                                    <driver>${liquibase.driver}</driver>
                                    <url>${db.' + $foldername + '.url}</url>
                                    <username>${db.' + $foldername + '.username}</username>
                                    <password>${db.' + $foldername + '.password}</password>
                                    <changeLogFile>src/main/database/changelog/post-build/run_utplsql.xml</changeLogFile>
                                    <promptOnNonLocalDatabase>${liquibase.prompt}</promptOnNonLocalDatabase>
                                    <verbose>${liquibase.verbose}</verbose>
                                    <contexts>${liquibase.context}</contexts>
                                    <databaseChangeLogTableName>${liquibase.databasechangelog}</databaseChangeLogTableName>
                                    <databaseChangeLogLockTableName>${liquibase.databasechangeloglock}</databaseChangeLogLockTableName>
                                    <expressionVars>
                                    <property>
                                      <name>code_coverage</name>
                                      <value>${liquibase-imp-ut.min-coverage}</value>
                                     </property>
                            </expressionVars>
                                </configuration>
                            </execution>'

add_file_content "$rootpath\pom.xml" $mvn_content "<!-- DO_NOT_REMOVE_THIS_LINE_PRJ_UT -->" "liquibase-$foldername-ut"


#task connecties replacen
replace_file_content "$rootpath\.vscode\tasks.json" "#DEFAULTSCHEMA#" $foldername
replace_file_content "$rootpath\.vscode\tasks.json" "#SCHEMAENTRY#" $foldername

$cobertura_source = "ut_utils.append_to_list(l_result, '<source>$rootpath/src/main/database/$foldername/latest/packages</source>'); --source$foldername"
add_file_content "$rootpath\src\main\database\utplsql\reporters\ut_coverage_cobertura_reporter.tpb" $cobertura_source "-- AXI - ADD SOURCES - DO NOT REMOVE THIS LINE" "source$foldername"


Write-Host "end script"