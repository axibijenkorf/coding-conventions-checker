#!/bin/sh
env=$1
file=$2
user=$3

cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "$env" "$user"

file="$project_root/$file"
echo $file
## Need to set nls_lang for special characters
 export NLS_LANG=.AL32UTF8

echo "user: $db_username"
#echo "pwd: $db_password"
echo "db: $db_sqlplusurl"
# run sqlplus, execute the script, then get the error list and exit
sqlplus "$db_username/$db_password@$db_sqlplusurl" << EOF
@$file
@_show_errors.sql
exit;
EOF