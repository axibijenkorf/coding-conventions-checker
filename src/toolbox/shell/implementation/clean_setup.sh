# Here we start with a clean repository and add an upstream and pull the latest release from upstream
project_root=$(git rev-parse --show-toplevel)
#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "global"

if [ ! ${project_type^^} == "SPECIFIC" ]
then
    echo "This script can only be user for project type SPECIFIC!"
    return 1
fi

echo -e  "${GREEN}====> Check variables"${RESET}
if [ -z $git_upstream ] ; then
   read -p  ${RED}$'git.upstream is not defined in global.properties!'${RESET}
   code "$project_root/conf/env/global.properties"
   return 1
fi

git remote add upstream $git_upstream

git fetch

git checkout -b master

git pull upstream master

git push -u origin master

git checkout -b stable

git push -u origin stable

git checkout -b integration

git push -u origin integration

git checkout -b develop

git push -u origin develop

git fetch

git flow init -d