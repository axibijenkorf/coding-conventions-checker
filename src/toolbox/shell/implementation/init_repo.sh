project_root=$(git rev-parse --show-toplevel)
#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "global"
cur_dir=$(pwd)

read -p ${YELLOW}$'Enter project type. Choose either STANDARD or SPECIFIC.\nPlease provide the project type:\n'${RESET} project_type
case ${project_type^^} in
   "STANDARD")
      ;;
   "SPECIFIC")
      ;;
   *)
      echo "Sorry, only project type STANDARD or SPECIFIC allowed, exiting."
      return 1 ;;
esac

read -p ${YELLOW}$'Enter project alias. Usually an abbreviation of 3 or 4 characters for customer name (A59, SRN).\nPlease provide the project alias:\n'${RESET} project_alias

#set project properties in global.properties, and set DB connections to #
echo -e "${GREEN}====> Set project vars in global.properties${RESET}"
sed -i "s/project.type=.*/project.type=${project_type^^}/g" "$project_root/conf/env/global.properties"
sed -i "s/project.alias=.*/project.alias=${project_alias^^}/g" "$project_root/conf/env/global.properties"

#set db.url and db.sqlplusUrl to # in all properties files to prevent Jenkins running on the wrong DB
cd "${project_root}/conf/env/"
for f in *.properties; do   
   sed -i 's/db.url=\(.*\)/db.url=#\1/g' "$project_root/conf/env/$f" 
   sed -i 's/db.sqlplusUrl=\(.*\)/db.sqlplusUrl=#\1/g' "$project_root/conf/env/$f"  
done
cd $cur_dir

git add "$project_root/conf/env/*.properties"
git commit -m "Set initial vars in properties files"
git push
echo "added global.properties to GIT"

echo -e  "${GREEN}====> Check variables"${RESET}
if [ -z $git_upstream ] ; then
   read -p  ${RED}$'git.upstream is not defined in global.properties!'${RESET}
   code "$project_root/conf/env/global.properties"
   return 1
fi

echo -e "${GREEN}====> add Upstream repository${RESET}"
up_repo=$(git remote -v |  grep -Eo '*upstream*' | wc -l)
if [ $up_repo -eq 0 ]
then
    echo -e "add upstream"
    git remote add upstream $git_upstream
fi
git fetch upstream

echo -e "${GREEN}====> Remove develop branch from origin${RESET}"
git checkout master
#delete remote branch
git push origin --delete develop
git fetch

if [ ${project_type^^} == "SPECIFIC" ]
then
   echo -e "${GREEN}====> Get latest release to develop${RESET}"
   git checkout master
   git checkout -b develop
   git push -u origin develop
   git fetch
fi

echo -e "${GREEN}====> Removing unnecessary branches${RESET}"
case ${project_type^^} in
   "STANDARD")
      git branch -r | grep -Eo '*origin/.*'| grep -Ev 'master|stable' | awk -Forigin/ ' {print $2 $3}' | xargs -I {} git push origin :{};;
   "SPECIFIC")
      git branch -r | grep -Eo '*origin/.*'| grep -Ev 'master|develop|integration|stable' | awk -Forigin/ ' {print $2 $3}' | xargs -I {} git push origin :{};;
esac

#create stable branch if it doesn't exist
echo -e "${GREEN}====> stable branch${RESET}"
branch=$(git ls-remote --heads | grep -Eo '*stable*')
if [ -z "$branch" ]
then
   echo "Create Stable branch"
   git checkout master
   git checkout -b stable
   git push -u origin stable
   git fetch
fi

if [ ${project_type^^} == "SPECIFIC" ]
then
   #create integration branch if it doesn't exist
   echo -e "${GREEN}====> integration branch${RESET}"
   branch=$(git ls-remote --heads | grep -Eo '*integration*')
   if [ -z "$branch" ]
   then
      echo "Create integration branch"
      git checkout master
      git checkout -b integration
      git push -u origin integration
      git fetch
   fi
fi

echo -e "${GREEN}====> init git flow${RESET}"
git flow init -d

