project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "global"

RED=$'\e[31m'
GREEN=$'\e[32m'
RESET=$'\e[0m'
YELLOW=$'\e[33m'

echo -e  "${GREEN}====> Check variables"${RESET}
if [ -z $git_upstream ] ; then
   read -p  ${RED}$'git.upstream is not defined in global.properties!'${RESET}
   code "$project_root/conf/env/global.properties"
   return 1
fi

cd "$project_root"
#don't start when you still have files you need to commit
echo -e "${GREEN}====> Check unstaged files${RESET}"
UNSTAGED=$(git status -s | wc -l)
if [ "$UNSTAGED" -gt 0 ] ; then
   read -p ${RED}$'There are '$UNSTAGED$' unstaged files.\nPLease add and commit them.\nPress [ENTER] to exit. '${RESET}
   return 1
fi

if [ ${project_type^^} == "SPECIFIC" ]
then
    #get all the latest commits from develop
    echo -e "${GREEN}====> Get all the latest commits from develop branch${RESET}"
    if git checkout develop &&
        git fetch origin develop &&
        [ `git rev-list HEAD...origin/develop --count` != 0 ]
    then
        git pull
    else
        echo -e "Already up to date"
    fi
fi

echo -e "${GREEN}====> Upstream repository${RESET}"
up_repo=$(git remote -v |  grep -Eo '*upstream*' | wc -l)
if [ $up_repo -eq 0 ]
then
    echo -e "add upstream"
    git remote add upstream $git_upstream
fi
git fetch upstream

echo -e "${GREEN}====> Run prepare_upstream.sh${RESET}"
#check if there is a prepare_release.sh file and run it
FILE=$project_root/src/toolbox/release/prepare_upstream.sh
if test -f "$FILE"; then
    #go to root project folder
    cd "$project_root/src/toolbox/release/"
    prepare_upstream.sh
else
    echo -e "no prepare_upstream.sh found"
fi
cd "$project_root"

echo -e "${GREEN}====> Checkout stable${RESET}"
#check if stable branch exists on remote
branch=$(git ls-remote --heads | grep -Eo '*stable*')
if [ -z "$branch" ]
then
    echo -e "Create stable branch"
    git branch stable
    git push --set-upstream origin stable
    git fetch
else
    exists=`git show-ref refs/heads/stable`
    if [ -n "$exists" ]; then
        echo 'branch exists!'
        git checkout stable
    else
        git checkout -b stable --track origin/stable
    fi
    git pull
fi

BRANCH=$(git branch | sed -nr 's/\*\s(.*)/\1/p')

#exit if you'r not on stable
if [ -z $BRANCH ] || [ $BRANCH != "stable" ]; then
    echo -e "${RED}You're not an stable branch. Checkout did not succeed. Maybe because of files you didn't commit${RESET}"
    return 1
fi

echo -e "${GREEN}====> Get latest commit from upstream${RESET}"
git pull upstream master

echo -e "${GREEN}====> Check for merge conflicts${RESET}"
CONFLICTS=$(git ls-files -u | wc -l)
until [ "$CONFLICTS" -eq 0 ] ;
do
    read -p ${RED}$'There are '$CONFLICTS$' merge conflicts.\nSolve the conflicts and then press [ENTER] to carry on. '${RESET}
    CONFLICTS=$(git ls-files -u | wc -l)
done


UNSTAGED=$(git status -s | wc -l)
until [ "$UNSTAGED" -eq 0 ] ;
do
   read -p ${RED}$'There are '$UNSTAGED$' unstaged files.\nPLease add and commit them.\nPress [ENTER] to carry on. '${RESET}
   UNSTAGED=$(git status -s | wc -l)
done


echo -e "${GREEN}====> Push stable${RESET}"
git push --tags
git push

#for specific project type merge into integration and develop
if [ ${project_type^^} == "SPECIFIC" ]
then
    echo -e "${GREEN}====> Go to integration branch${RESET}"
    #check if integration branch exists on remote
    branch=$(git ls-remote --heads | grep -Eo '*integration*')
    if [ -z "$branch" ]
    then
        echo -e "Create integration branch from develop"
        git checkout develop
        git checkout -b integration
        git push --set-upstream origin integration
        git fetch
    else
        exists=`git show-ref refs/heads/integration`
        if [ -n "$exists" ]; then
            echo 'branch exists!'
            git checkout integration
        else
            git checkout -b integration --track origin/integration
        fi
        git pull
        git merge develop
    fi


    echo -e "${GREEN}====>Check for merge conflicts ${RESET}"
    CONFLICTS=$(git ls-files -u | wc -l)
    until [ "$CONFLICTS" -eq 0 ] ;
    do
        read -p ${RED}$'There are '$CONFLICTS$' merge conflicts.\nSolve the conflicts and then press [ENTER] to carry on. '${RESET}
        CONFLICTS=$(git ls-files -u | wc -l)
    done

    UNSTAGED=$(git status -s | wc -l)
    until [ "$UNSTAGED" -eq 0 ] ;
    do
    read -p ${RED}$'There are '$UNSTAGED$' unstaged files.\nPLease add and commit them.\nPress [ENTER] to carry on. '${RESET}
    UNSTAGED=$(git status -s | wc -l)
    done

    echo -e "${GREEN}====> Merge stable into integration${RESET}"
    git merge stable

    echo -e "${GREEN}====> Check for merge conflicts${RESET}"
    CONFLICTS=$(git ls-files -u | wc -l)
    until [ "$CONFLICTS" -eq 0 ] ;
    do
        read -p ${RED}$'There are '$CONFLICTS$' merge conflicts.\nSolve the conflicts and then press [ENTER] to carry on. '${RESET}
        CONFLICTS=$(git ls-files -u | wc -l)
    done

    UNSTAGED=$(git status -s | wc -l)
    until [ "$UNSTAGED" -eq 0 ] ;
    do
        read -p ${RED}$'There are '$UNSTAGED$' unstaged files.\nPLease add and commit them.\nPress [ENTER] to carry on. '${RESET}
        UNSTAGED=$(git status -s | wc -l)
    done

    echo -e "${GREEN}====> Push integration${RESET}"
    git push

    if [ ! -z $jenkins_url ] ; then
        start "$jenkins_url/job/integration/"
        read -p ${YELLOW}$'The integration branch has been pushed.\nPlease take look at the Jenkins job\nIf it succeeded then you can press [ENTER] to carry on. '${RESET}
    fi


    echo -e "${GREEN}====> Merge integration into develop${RESET}"
    git checkout develop
    git merge integration

    echo -e "${GREEN}====>Check for merge conflicts${RESET}"
    CONFLICTS=$(git ls-files -u | wc -l)
    until [ "$CONFLICTS" -eq 0 ] ;
    do
        read -p ${RED}$'There are '$CONFLICTS$' merge conflicts.\nSolve the conflicts and then press [ENTER] to carry on. '${RESET}
        CONFLICTS=$(git ls-files -u | wc -l)
    done

    UNSTAGED=$(git status -s | wc -l)
    until [ "$UNSTAGED" -eq 0 ] ;
    do
    read -p ${RED}$'There are '$UNSTAGED$' unstaged files.\nPLease add and commit them.\nPress [ENTER] to carry on. '${RESET}
    UNSTAGED=$(git status -s | wc -l)
    done

    echo -e "${GREEN}====> Push develop${RESET}"
    git push

    if [ ! -z $jenkins_url ] ; then
        start "$jenkins_url/job/develop/"
        read -p ${YELLOW}$'The develop branch has been pushed.\nPlease take look at the Jenkins job\nIf it succeeded then you can press [ENTER] to carry on. '${RESET}
    fi
fi

#for specific project type merge into integration and develop
if [ ${project_type^^} == "STANDARD" ]
then
    #get all the latest commits from branche master
    echo -e  "${GREEN}====> Get all the latest commits from master branch${RESET}"
    if git checkout master &&
        git fetch origin master &&
        [ `git rev-list HEAD...origin/master --count` != 0 ]
    then
         git pull
    else
        echo "Already up to date"
    fi

    #make sure you're on the master branch
    echo -e  "${GREEN}====> Checkout master${RESET}"
    git checkout master

    BRANCH=$(git branch | sed -nr 's/\*\s(.*)/\1/p')

    #exit if you'r not on master
    if [ -z $BRANCH ] || [ $BRANCH != "master" ]; then
        echo -e "${RED}You're not an master branch. Checkout did not succeed. Maybe because of files you didn't commit${RESET}"
        return 1
    fi

    echo -e "${GREEN}====> Merge stable into master${RESET}"
    git merge stable

    echo -e "${GREEN}====> Check for merge conflicts${RESET}"
    CONFLICTS=$(git ls-files -u | wc -l)
    until [ "$CONFLICTS" -eq 0 ] ;
    do
        read -p ${RED}$'There are '$CONFLICTS$' merge conflicts.\nSolve the conflicts and then press [ENTER] to carry on. '${RESET}
        CONFLICTS=$(git ls-files -u | wc -l)
    done

    UNSTAGED=$(git status -s | wc -l)
    until [ "$UNSTAGED" -eq 0 ] ;
    do
        read -p ${RED}$'There are '$UNSTAGED$' unstaged files.\nPLease add and commit them.\nPress [ENTER] to carry on. '${RESET}
        UNSTAGED=$(git status -s | wc -l)
    done

    echo -e "${GREEN}====> Push master${RESET}"
    git push
    git push --tags

    if [ ! -z $jenkins_url ] ; then
        start "$jenkins_url/job/master/"
        read -p ${YELLOW}$'The master branch has been pushed.\nPlease take look at the Jenkins job\nIf it succeeded then you can press [ENTER] to carry on. '${RESET}
    fi
fi

echo -e "${GREEN}====> Run after_upstream.sh${RESET}"
#check if there is an after_release file and run it
FILE=$project_root/src/toolbox/shell/release/after_upstream.sh
if test -f "$FILE"; then
    #go to root project folder
    cd "$project_root/src/toolbox/shell/release/"
    . after_upstream.sh
else
    echo -e "no after_upstream.sh found${RESET}"
fi

echo -e "${GREEN}====> FINISHED${RESET}"
