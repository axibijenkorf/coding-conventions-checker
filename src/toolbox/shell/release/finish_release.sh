#!/bin/bash
project_root=$(git rev-parse --show-toplevel)
#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "global"
cur_dir=$(pwd)

if [ ! ${project_type^^} == "SPECIFIC" ]
then
    echo "This script can only be user for project type SPECIFIC!"
    return 1
fi

RED=$'\e[31m'
GREEN=$'\e[32m'
RESET=$'\e[0m'
YELLOW=$'\e[33m'

#don't start when you still have files you need to commit
echo -e  "${GREEN}====> Check unstaged files${RESET}"
UNSTAGED=$(git status -s | wc -l)
if [ "$UNSTAGED" -gt 0 ] ; then
   read -p  ${RED}$'There are '$UNSTAGED$' unstaged files.\nPLease add and commit them.\nPress [ENTER] to exit. '${RESET}
   return 1
fi

#check if git flow is initiated
echo -e  "${GREEN}====> Check GIT flow install"${RESET}
CONFLICTS=$(git config --get-regexp gitflow.prefix| wc -l)
if [ "$CONFLICTS" -eq 0 ] ; then
   read -p  ${RED}$'GIT FLOW is not initiated. Please install git flow for this repo by running git flow init -d'${RESET}
   return 1
fi

#get all the latest commits from branches master and develop
echo -e  "${GREEN}====> Get all the latest commits from develop and master branch${RESET}"
if git checkout master &&
    git fetch origin master &&
    [ `git rev-list HEAD...origin/master --count` != 0 ]
then
     git pull
else
    echo "Already up to date"
fi

if git checkout develop &&
    git fetch origin develop &&
    [ `git rev-list HEAD...origin/develop --count` != 0 ]
then
     git pull
else
    echo "Already up to date"
fi

read -p ${YELLOW}$'Which release do you want to finish?\nPlease provide the release tag (cc.19.10.1):\n'${RESET} rel

#make sure you're on the release branch
echo -e  "${GREEN}====> Checkout release  $rel${RESET}"
git checkout release/$rel

BRANCH=$(git branch | sed -nr 's/\*\s(.*)/\1/p')

#exit if you'r not on  release branch
if [ -z $BRANCH ] || [ $BRANCH != "release/$rel" ]; then
    echo  ${RED}"You're not an develop branch. Checkout did not succeed. Maybe because of files you didn't commit${RESET}"
    return 1
fi

echo -e  "${GREEN}====> Finish release $rel ${RESET}"
git flow release finish -m "$rel" $rel

echo -e "${GREEN}====>Check for merge conflicts${RESET}"
CONFLICTS=$(git ls-files -u | wc -l)
until [ "$CONFLICTS" -eq 0 ] ;
do
    read -p  ${RED}$'There are '$CONFLICTS$' merge conflicts.\nSolve the conflicts and then press [ENTER] to carry on. '${RESET}
    CONFLICTS=$(git ls-files -u | wc -l)
done

UNSTAGED=$(git status -s | wc -l)
until [ "$UNSTAGED" -eq 0 ] ;
do
   read -p  ${RED}$'There are '$UNSTAGED$' unstaged files.\nPLease add and commit them.\nPress [ENTER] to carry on. '${RESET}
   UNSTAGED=$(git status -s | wc -l)
done

echo -e  "${GREEN}====> Push master${RESET}"
git checkout master
git push

echo -e  "${GREEN}====> Push develop${RESET}"
git checkout develop
git push
git push --tags


if [ ! -z $jenkins_url ] ; then
    echo -e  "${GREEN}====> check the JENKINS builds${RESET}"
    start $jenkins_url
fi

echo -e  "${GREEN}====> Run after_release.sh${RESET}"
#check if there is an after_release file and run it
FILE=$project_root/src/toolbox/shell/release/after_release.sh
if test -f "$FILE"; then
    #go to root project folder
    cd "$project_root/src/toolbox/shell/release/"
    after_release.sh $rel
else
    echo "no after_release.sh found"
fi

echo -e  "${GREEN}====> You have finished releasing${RESET}"

cd "$cur_dir"