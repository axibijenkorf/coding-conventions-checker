#!/bin/bash
project_root=$(git rev-parse --show-toplevel)
cur_dir=$(pwd)
#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "global"

if [ ! ${project_type^^} == "SPECIFIC" ]
then
    echo "This script can only be user for project type SPECIFIC!"
    return 1
fi

RED=$'\e[31m'
GREEN=$'\e[32m'
RESET=$'\e[0m'
YELLOW=$'\e[33m'

#don't start when you still have files you need to commit
echo -e  "${GREEN}====> Check unstaged files${RESET}"
UNSTAGED=$(git status -s | wc -l)
if [ "$UNSTAGED" -gt 0 ] ; then
   read -p  ${RED}$'There are '$UNSTAGED$' unstaged files.\nPLease add and commit them.\nPress [ENTER] to exit. '${RESET}
   return 1
fi

#check if git flow is initiated
echo -e  "${GREEN}====> Check GIT flow install"${RESET}
CONFLICTS=$(git config --get-regexp gitflow.prefix| wc -l)
if [ "$CONFLICTS" -eq 0 ] ; then
   read -p  ${RED}$'GIT FLOW is not initiated. Please install git flow for this repo by running git flow init -d'${RESET}
   return 1
fi

#get all the latest commits from branches master and develop
echo -e  "${GREEN}====> Get all the latest commits from develop branch${RESET}"

if git checkout develop &&
    git fetch origin develop &&
    [ `git rev-list HEAD...origin/develop --count` != 0 ]
then
     git pull
else
    echo "Already up to date"
fi

#make sure you're on develop
echo -e  "${GREEN}====> Checkout develop${RESET}"
git checkout develop

BRANCH=$(git branch | sed -nr 's/\*\s(.*)/\1/p')

#exit if you'r not on develop
if [ -z $BRANCH ] || [ $BRANCH != "develop" ]; then
    echo  ${RED}"You're not an develop branch. Checkout did not succeed. Maybe because of files you didn't commit${RESET}"
    return 1
fi

echo -e  "${GREEN}====> Run prepare_release.sh${RESET}"
#check if there is a prepare_release.sh file and run it
FILE=$project_root/src/toolbox/shell/release/prepare_release.sh
if test -f "$FILE"; then
    prompt=''
    until  [ "${prompt,,}" = "y" ] || [ "${prompt,,}" = "n" ] ;
    do
        read -p "${YELLOW}Do you want to run prepare release? [y or n]: ${RESET}" prompt
    done

    if [ "${prompt,,}" == 'y' ]; then
         #go to root project folder
        cd "$project_root/src/toolbox/shell/release/"
        . prepare_release.sh
    fi
else
    echo "no prepare_release.sh found"
fi
cd "$project_root"


read -p ${YELLOW}$'Would you like to start a new release?\nPlease provide the release tag (cc.19.10.1):\n'${RESET} rel

echo -e  "${GREEN}====> Start release branch${RESET}"
#start release branch
git flow release start $rel

echo -e "${GREEN}====>Check for merge conflicts${RESET}"
CONFLICTS=$(git ls-files -u | wc -l)
until [ "$CONFLICTS" -eq 0 ] ;
do
    read -p  ${RED}$'There are '$CONFLICTS$' merge conflicts.\nSolve the conflicts and then press [ENTER] to carry on. '${RESET}
    CONFLICTS=$(git ls-files -u | wc -l)
done

UNSTAGED=$(git status -s | wc -l)
until [ "$UNSTAGED" -eq 0 ] ;
do
   read -p  ${RED}$'There are '$UNSTAGED$' unstaged files.\nPLease add and commit them.\nPress [ENTER] to carry on. '${RESET}
   UNSTAGED=$(git status -s | wc -l)
done

echo -e  "${GREEN}====> Publish release${RESET}"
git flow release publish $rel

if [ ! -z $jenkins_url ] ; then
    start $jenkins_url'job/release%252F'$rel'/'
    read -p ${YELLOW}$'The release branch has been created and published.\nPlease take look at Jenkins job "release/'$rel$'"\nIf it succeeded then you can press [ENTER] to finish the release. '${RESET}
fi

echo -e  "${GREEN}====> Vagrant clean and latest install"

read -p ${YELLOW}$'Please run a maven install on the vagrant Boxes:\n*Latest\n*Clean\nIf everything is OK you can press [ENTER] to finish the release. '${RESET}

echo -e  "${GREEN}====> You have started releasing${RESET}"

cd "$cur_dir"