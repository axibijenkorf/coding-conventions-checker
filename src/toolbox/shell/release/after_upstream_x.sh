cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "dev" cc
. "$project_root/conf/env/initiate_vars.sh" "global"

if [ ! ${project_type^^} == "SPECIFIC" ]
then
    echo "This script can only be user for project type SPECIFIC!"
    return 1
fi

RED=$'\e[31m'
GREEN=$'\e[32m'
RESET=$'\e[0m'
YELLOW=$'\e[33m'

echo -e  "${GREEN}====> Apex nitro publish workspace files"${RESET}
Apex-nitro publish $nitro_project

echo -e  "${GREEN}====> export workspace files"${RESET}
cd "$project_root"
cd src/toolbox/apex/export_app
. export_ws_files.sh dev

#export workspace files
echo -e  "${GREEN}====> Add to GIT repo"${RESET}
cd "$project_root"

UNSTAGED=$(git status -s | wc -l)
if [ "$UNSTAGED" -gt 0 ] ; then
    cd "$project_root"
    git add .
    git commit -m "Export workspace files after new Release"
    git push
    echo "added to GIT"
fi