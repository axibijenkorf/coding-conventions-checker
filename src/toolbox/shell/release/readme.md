start_release.sh

prepare_release.sh
Met dit script ga je ervoor zorgen dat alles in orde is vooralleer een nieuwe release te starten
- masterdata exporteren
- apex applicaties publishen
- apex applicaties exporteren
- versie nummer in masterdata goed zetten

full_release.sh

finish_release.sh

after_upstream.sh

import_upstream_release.sh
Dit script dient gestart te worden als je een nieuwe update wenst binnen te halen van de upstream (repository waarvan je geforked hebt)
