cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "dev"
. "$project_root/conf/env/initiate_vars.sh" "global"

if [ ! ${project_type^^} == "SPECIFIC" ]
then
    echo "This script can only be user for project type SPECIFIC!"
    return 1
fi

RED=$'\e[31m'
GREEN=$'\e[32m'
RESET=$'\e[0m'
YELLOW=$'\e[33m'

read -p "${YELLOW}Are you sure to start a new release? Please make sure everything is committed in GIT and press [ENTER] to carry on. ${RESET}"

echo -e  "${GREEN}====> go to develop branch${RESET}"
#make sure you're on develop
git checkout develop

BRANCH=$(git branch | sed -nr 's/\*\s(.*)/\1/p')

#exit if you'r not on stable
if [ -z $BRANCH ] || [ $BRANCH != "develop" ]; then
    echo "You're not an develop branch. Checkout did not succeed. Maybe because of files you didn't commit"
    return 1
fi

echo -e  "${GREEN}====> Publish wit apex-nitro${RESET}"
#go to root project folder
cd "$project_root"

apex-nitro publish $nitro_project
echo "Nitro published"


echo -e  "${GREEN}====> Publish translations with AVA${RESET}"
#. "$project_root/src/toolbox/database/APEX app translation/publish_apps.sh" $global_apex_apps dev
sqlplus "$db_ava_username/$db_ava_password@$db_sqlplusurl" << EOF
begin
    for rec in (
        select  pr.*
        from    ava_t_project pr, apex_applications apx
        where   apx.application_id = pr.app_id
        and     application_group = 'AXI_CC'
    )
    loop
        PCK_AVA.SEED_PROJECT(rec.id,1);
    end loop;
end;
/


begin
    for rec in (
        select  tr.*
        from    ava_t_trans tr,
                ava_t_project pr,
                apex_applications apx
        where   apx.application_id = pr.app_id
        and     application_group = 'AXI_CC'
        and     tr.project_id = pr.id
    )
    loop
        PCK_AVA.SYNC_STRINGS_FROM_LOCAL_REPO( rec.id, 1);
    end loop;
end;
/
commit
/
exit
EOF
echo "AVA Synced"

read -p ${YELLOW}$'Did the ava sync succeeded?\nPress [ENTER] to carry on. '${RESET}


echo -e  "${GREEN}====> Export apex apps${RESET}"
cd src/toolbox/apex/export_app/

#export all apps
. export_project_apps.sh "" dev

echo "Apex apps exported"
read -p ${YELLOW}$'Did the apex export succeeded?\nPress [ENTER] to carry on and add it to git '${RESET}


UNSTAGED=$(git status -s | wc -l)
if [ "$UNSTAGED" -gt 0 ] ; then
   cd "$project_root"
    git add .
    git commit -m "Full export apex apps before release"
    git push
    echo "added to GIT"
fi

echo -e  "${GREEN}====> Export masterdata${RESET}"
cd "$project_root"
cd src/toolbox/nodejs/masterdata/
. sync_ref.sh

UNSTAGED=$(git status -s | wc -l)
if [ "$UNSTAGED" -gt 0 ] ; then
    cd "$project_root"
    git add .
    git commit -m "Export masterdata"
    git push
    echo "added to GIT"
fi
cd "$cur_dir"
