#!/bin/bash

RED=$'\e[31m'
GREEN=$'\e[32m'
RESET=$'\e[0m'
YELLOW=$'\e[33m'
MAGENTA=$'\e[35m'

dir=$1
cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel || :)

if [ ! -z ${project_root} ]
then
	parent_dir="$(dirname "$project_root")"
	parent_msg="${RED}OR${YELLOW} If you push enter we will use the default folder: ${parent_dir}"
else
	parent_dir=''
	parent_msg=''
fi
echo "$parent_dir"

prompt=''
until  [ ! -z "${dir}" ] ;
do
   	read -p "${YELLOW}Please give the base folder you want to synchronize (Ex: /c/repos/) ${parent_msg}${RESET}"$'\n' prompt
   	if  [ ! -z "${prompt}" ]
	then
		dir="${prompt}"
	elif [ ! -z "${parent_dir}" ]
	then
		dir="${parent_dir}"
	else
		dir=""
	fi
done

# No directory has been provided, use current
if [ -z "$dir" ]
then
    dir="`pwd`"
fi

# Make sure directory ends with "/"
if [[ $dir != */ ]]
then
	dir="$dir/*"
else
	dir="$dir*"
fi

# Loop all sub-directories
for f in $dir
do
    # Only interested in directories
	[ -d "${f}" ] || continue

	# Check if directory has a git repositories
	if [ -d "$f/.git" ]
	then
		mod=0
		cd "${f}"
		repo_name=$(basename `git rev-parse --show-toplevel`)

		echo -e  "\e[1m\e[41m\e[97m-----===== Repo: ${repo_name} =====-----${RESET}"
		echo "$f"
		echo -e  "${GREEN}===> Fetch from GIT${RESET}"
		git fetch

		#get current branch
		cur_branch=$(git rev-parse --abbrev-ref HEAD)
		echo -e  "${GREEN}===> Current branch: ${cur_branch}${RESET}"

		# Check for modified files
		if [ $(git status | grep modified -c) -ne 0 ]
		then
			mod=1

			echo -e  "${RED}===> There are modified files, you still need to commit! ${RESET}"
			git diff --name-status
		fi

		# Check for untracked files
		if [ $(git status | grep Untracked -c) -ne 0 ]
		then
			mod=1
			echo -e  "${RED}===> There are untracked files, you still need to commit! ${RESET}"
			git ls-files --others --exclude-standard
		fi

		# Check for unpushed changes
		if [ $(git status | grep 'Your branch is ahead' -c) -ne 0 ]
		then
				mod=1
				echo -e  "${RED}===> Unpushed files ${RESET}"
				git log --branches --not --remotes
		fi

		# Check if everything is ok
		if [ $mod -eq 0 ]
		then
			echo -e  "${GREEN}===> Nothing to commit ${RESET}"
		fi

		total_differences=0
		sync_branches=()
		line_color=${MAGENTA}
		echo -e  "${GREEN}===> Overview Branches: ${RESET}"
		printf "${MAGENTA}%-40s | %-5s | %-6s | %-40s | %-30s\n${RESET}" "BRANCH" "AHEAD" "BEHIND" "REMOTE" "LAST COMMIT"
		printf "${MAGENTA}%130s${RESET}\n" | tr " " "-"

		while read local remote last_commit
		do
			[ -z "$remote" ] && continue
			ahead=`git rev-list ${remote}..${local} --count 2>/dev/null || continue`
    		behind=`git rev-list ${local}..${remote} --count 2>/dev/null || continue`
			total_differences=$(($ahead + $behind + $total_differences))

			if [ "$ahead" == 0 ] && [ "$behind" -gt 0 ] && [ $mod -eq 0 ]; then
				sync_branches+=("$local")
				line_color=${RED}
			elif [ "$local" = "$cur_branch" ]; then
				line_color=${YELLOW}
			else
				line_color=${MAGENTA}
			fi

			printf "${line_color}%-40s | %-5s | %-6s | %-40s | %-30s\n${RESET}" "$local" "$ahead" "$behind" "$remote" "$last_commit"


		done < <(git for-each-ref --format="%(refname:short) %(upstream:short) %(committerdate:relative)" refs/heads)

		if [ "$total_differences" == 0 ]; then
			echo -e "${GREEN}===> Everything is synchronized.$RESET"
		elif [ -n "$sync_branches" ]; then

			for value in "${sync_branches[@]}"
			do
     			echo -e  "${GREEN}===> checkout and pull branch ${value} ${RESET}"
				git checkout $value -q
				git pull
			done

			#return to current branch after pulling some branches
			git checkout ${cur_branch}
		fi


		cd ../
	else

		echo -e  "${RED}===> Not a git repository ${RESET}"
	fi

	echo
done

cd "$cur_dir"