#!/bin/bash
env=$1
user=$2

cur_dir=$(pwd)
project_folder=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_folder/conf/env/initiate_vars.sh" "$env" "$user"

cd "$project_folder"

sql "$db_username/$db_password@$db_sqlplusurl" << EOF
    lb clearchecksums src/main/database/changelog/${user}/master.xml
    exit
EOF

cd "$cur_dir"