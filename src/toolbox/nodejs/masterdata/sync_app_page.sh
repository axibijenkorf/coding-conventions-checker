#!/bin/bash
app=$1
cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "ref"
. "$project_root/conf/env/initiate_vars.sh" "dev"

cd "$project_root"

until   [ ! -z "${app,,}" ];
do
   read -p "${YELLOW}Which application do you want to sync? PLease give a app id: ${RESET}" app
done

echo -e  "${GREEN}Getting app alias for application ${app}${RESET}"
echo -e  "${GREEN}==================================================${RESET}"
app_alias=`sqlplus -s "$dev_db_username/$dev_db_password@$dev_db_sqlplusurl" <<EOF
    set pages 0 lines 120 trimout on trimspool on tab off echo off verify off feed off serverout on
    var mavar varchar2(100);
    select  alias
    from    apex_applications
    where   application_id = ${app};
    print mavar;
    exit;
EOF
`

#exit if we are not able to finde app alias
if [ -z "$app_alias" ]; then
    cd "$cur_dir"
    echo  ${RED}"We're not able to find an alias for your apllication ${app}${RESET}"
    return 1
fi

echo -e  "${GREEN}Replacing application in settings file ${RESET}"
echo -e  "${GREEN}==================================================${RESET}"
echo "Application alias: $app_alias"

sed -i "s/.*and dealias = '*'.*/                                and dealias = '${app_alias^^}'/g" "src/main/database/cc/install/data/cc_app_page.js"

echo -e  "${GREEN}Exporting masterdata from dev environment${RESET}"
echo -e  "${GREEN}==================================================${RESET}"
masterdata -c "$dev_db_username/$dev_db_password@$dev_db_sqlplusurl" -e -f "src/main/database/cc/install/data/cc_app_page.js" "$@"

cd "$project_root"
echo -e  "${GREEN}Importing masterdata into ref environment${RESET}"
echo -e  "${GREEN}==================================================${RESET}"
masterdata -c "$ref_db_username/$ref_db_password@$ref_db_sqlplusurl" -i -f "src/main/database/cc/install/data/cc_app_page.js" "$@"

cd "$cur_dir"