#!/bin/bash
#env=$1

cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

OPTIND=1
while getopts "b:u" opt; do
  case ${opt,,} in
    b) env="$OPTARG"
    ;;
    u) user="$OPTARG"
    ;;
  esac
done

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "$env" "$user"

echo "run masterdata in $env"
echo ==================================================

#remove environment from arguments
count=0
param=""
prev_arg=""
for arg in "$@"
do
    if ([[ "$arg" != "-b" && "$prev_arg" != "-b" ]] && [[ "$arg" != "-u" && "$prev_arg" != "-u" ]])  ; then
        param="$param$arg "
    fi
    prev_arg=$arg
    let "count+=1"
done

cd "$project_root"

echo "$param"

echo masterdata -c "$db_username/$db_password@$db_sqlplusurl" $param

masterdata -c "$db_username/$db_password@$db_sqlplusurl" $param
cd "$cur_dir"