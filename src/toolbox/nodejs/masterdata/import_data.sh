#!/bin/bash
SCHEMA=$1
PASSWORD=$2
URL=$3
SETTINGS_FILE=$4

cd $(git rev-parse --show-toplevel)

npm run masterdata -- -c "$SCHEMA/$PASSWORD@$URL" -i -f "$SETTINGS_FILE" "$@"