#!/bin/bash
cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "ref"
. "$project_root/conf/env/initiate_vars.sh" "local"

cd "$project_root"
echo Exporting masterdata from reference environment
echo ==================================================
masterdata -c "$ref_db_username/$ref_db_password@$ref_db_sqlplusurl" -e -f "src/main/database/cc/install/data/cc_ref.js" "$@"

echo Importing masterdata into development environment
echo ==================================================
masterdata -c "$local_db_username/$local_db_password@$local_db_sqlplusurl" -i -f "src/main/database/cc/install/data/cc_ref.js" "$@"

echo Post-data script to solve offset problem
echo ==================================================
echo exit | sqlplus "$local_db_username/$local_db_password@$local_db_sqlplusurl" @src/main/database/post-data/update_appnr.sql

cd "$cur_dir"