#!/bin/bash

cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "dev" "cc"

cd "$project_root"
masterdata -c "$db_username/$db_password@$db_sqlplusurl" -e -f "src/main/database/cc/install/data/init.js" "$@"

cd "$cur_dir"