#!/bin/bash

cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "ref"
. "$project_root/conf/env/initiate_vars.sh" "dev"

cd "$project_root"
echo Exporting masterdata from dev environment
echo ==================================================
masterdata -c  "$dev_db_username/$dev_db_password@$dev_db_sqlplusurl" -e -f "src/main/database/cc/install/data/cc_constraint_messa" -o "$@"

echo Importing masterdata into ref environment
echo ==================================================
masterdata -c "$ref_db_username/$ref_db_password@$ref_db_sqlplusurl" -i -f "src/main/database/cc/install/data/cc_constraint_messa.js" -o "$@"

cd "$cur_dir"