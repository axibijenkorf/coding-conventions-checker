#!/bin/bash
project_root=$(git rev-parse --show-toplevel)
cur_dir=$(pwd)

RED=$'\e[31m'
GREEN=$'\e[32m'
RESET=$'\e[0m'
YELLOW=$'\e[33m'

usr_dir=$HOME

echo -e  "${GREEN}Homedir = $usr_dir${RESET}"

echo -e  "${GREEN}Copy .npmrc file to home dir${RESET}"

cp "../../../.npmrc" "$usr_dir" -f


