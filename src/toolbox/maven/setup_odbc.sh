#!/bin/bash
project_root=$(git rev-parse --show-toplevel)
cur_dir=$(pwd)

RED=$'\e[31m'
GREEN=$'\e[32m'
RESET=$'\e[0m'
YELLOW=$'\e[33m'

usr_dir=$HOME/.m2

cp settings-security.xml "$usr_dir" -f
cp settings.xml "$usr_dir" -f

echo -e  "${GREEN}Homedir = $usr_dir${RESET}"

echo -e  "${GREEN}Please registrate and accept @ Oracle${RESET}"

start "http://www.oracle.com/webapps/maven/register/license.html"

read -p ${YELLOW}$'Please give a master password\n'${RESET} master_pwd

mstr=$(mvn -emp $master_pwd)

echo -e  "${GREEN}masterkey = $mstr${RESET}"

echo -e  "${GREEN}Putting masterkey in settings-security.xml${RESET}"

sed -i "s^#master#^$mstr^" "$usr_dir/settings-security.xml"

read -p ${YELLOW}$'Please give in your Oracle email adress (xxx@axi.be)\n'${RESET} email

sed -i "s/#email#/$email/" "$usr_dir/settings.xml"

read -sp ${YELLOW}$'Please give in your Oracle password\n'${RESET} pass

encr_pass=$(mvn -ep "$pass")

echo -e  "${GREEN}encrypted password = $encr_pass${RESET}"

sed -i "s^#password#^$encr_pass^" "$usr_dir/settings.xml"

echo -e  "${GREEN}===FINISHED =====\n You can start using maven. ${RESET}"