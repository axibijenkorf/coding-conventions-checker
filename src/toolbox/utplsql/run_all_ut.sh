env=$1
user=$2

cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "$env" "$user"
export_file="$project_root/src/toolbox/utplsql/run.sql"



echo "run all unit tests"


    echo -e "\e[0m"
    echo "cls" > "$export_file"
    echo "set feedback on" > "$export_file"
    echo "set serveroutput on size 1000000"  >> "$export_file"
    echo "set echo on" >> "$export_file"
    echo "set feedback off" >> "$export_file"
    echo "set linesize 300" >> "$export_file"
    echo "set pagesize 1000" >> "$export_file"
    echo "SET LONGCHUNKSIZE 500000" >> "$export_file"
    echo "column COLUMN_VALUE heading COLUMN_VALUE    Format a200" >> "$export_file"
    echo "set heading off" >> "$export_file"
    echo "SET LONG 500000" >> "$export_file"
    echo "set trimspool on" >> "$export_file"
    echo "set trimout on" >> "$export_file"
    echo "" >> "$export_file"
    echo " select * from  ut.run(); " >> "$export_file"
    echo " rollback;" >> "$export_file"
    echo "exit;" >> "$export_file"
    sqlplus "$db_username/$db_password@$db_sqlplusurl" @"run.sql"

    rm "$project_root/src/toolbox/utplsql/run.sql"


