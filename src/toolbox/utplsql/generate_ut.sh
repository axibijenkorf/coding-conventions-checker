env=$2
user=$3

cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
echo "env $env"
echo "user $user"
. "$project_root/conf/env/initiate_vars.sh" "$env" "$user"

project_root=$(git rev-parse --show-toplevel)
export_file="$project_root/src/toolbox/utplsql/generate_ut.sql"

clear
if ! [ -n "$1" ]; then
    read -p 'enter package_name: ' package
else
    package=$1
fi

echo "generating ut package for $package"
echo "user $db_username"
echo "pwd $db_password"
echo "db $db_sqlplusurl"

package_name=`sqlplus -s "$db_username/$db_password@$db_sqlplusurl" <<EOF
    set pages 0 lines 120 trimout on trimspool on tab off echo off verify off feed off serverout on
    var mavar varchar2(100);
    select max(replace(lower('$package'),'pck','ut')) into :mavar from dba_objects where object_name = upper('$package') ;
    print mavar;
    exit;
EOF
`

echo "'$package_name'"
if [ -n "$package_name" ]; then


    echo -e "\e[0m"

    echo "set feedback off" > $export_file
    echo "set serveroutput on size 1000000"  >> $export_file
    echo "set echo off" >> $export_file
    echo "set linesize 1000" >> $export_file
    echo "set pagesize 32767" >> $export_file
    echo "SET LONGCHUNKSIZE 500000" >> $export_file
    echo "set heading off" >> $export_file
    echo "SET LONG 500000" >> $export_file
    echo "set trimspool on" >> $export_file
    echo "set trimout on" >> $export_file
    echo "" >> $export_file
    echo "spool $project_root/src/test/database/$user/packages/$package_name.pkb" >> $export_file
    echo "select $db_tools_username.pck_ts_generate_code.generate_ut_package('$db_username', upper('$package'),'BODY','$user') from dual;" >> $export_file
    echo "spool off;" >> $export_file
    echo "" >> $export_file
    echo "spool $project_root/src/test/database/$user/packages/$package_name.pks" >> $export_file
    echo "select $db_tools_username.pck_ts_generate_code.generate_ut_package('$db_username', upper('$package'),'SPEC','$user') from dual;" >> $export_file
    echo "spool off;" >> $export_file
    echo "" >> $export_file
    echo "exit;" >> $export_file

    sqlplus "$db_username/$db_password@$db_sqlplusurl" @"generate_ut.sql"

    rm "$project_root/src/toolbox/utplsql/generate_ut.sql"

    clear
    echo -e "\e[32mfile $project_root/src/test/database/$user/packages/$package_name.pkb is created"
    echo -e "\e[32mfile $project_root/src/test/database/$user/packages/$package_name.pks is created"
else
    echo -e "\e[31mpackage name was not correct"
fi
