env=$2
user=$3

cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "$env" "$user"
export_file="$project_root/src/toolbox/utplsql/generate_coverage.sql"

clear
if ! [ -n "$1" ]; then
    read -p 'enter package_name (PCK_CC_XXXX): ' package
else
    package=$1
fi

package=`sqlplus -s "$db_username/$db_password@$db_sqlplusurl" <<EOF
    set pages 0 lines 120 trimout on trimspool on tab off echo off verify off feed off serverout on
    var mavar varchar2(100);
    select max(replace(lower('$package'),'ut_','pck_')) into :mavar from all_objects where object_name = upper('$package')  and object_type ='PACKAGE';
    print mavar;
    exit;
EOF
`
read -p "stop1"

package_ut=`sqlplus -s "$db_username/$db_password@$db_sqlplusurl" <<EOF
    set pages 0 lines 120 trimout on trimspool on tab off echo off verify off feed off serverout on
    var mavar varchar2(100);
    select max(replace(lower('$package'),'pck_','ut_')) into :mavar from all_objects where object_name = upper('$package')  and object_type ='PACKAGE' ;
    print mavar;
    exit;
EOF
`
echo "'$package_ut'"
if [ -n "$package_ut" ]; then

    echo -e "\e[0m"

    echo "set feedback off" > "$export_file"
    echo "set serveroutput on size 1000000"  >> "$export_file"
    echo "set echo off" >> "$export_file"
    echo "set linesize 1000" >> "$export_file"
    echo "set pagesize 32767" >> "$export_file"
    echo "SET LONGCHUNKSIZE 500000" >> "$export_file"
    echo "set heading off" >> "$export_file"
    echo "SET LONG 500000" >> "$export_file"
    echo "set trimspool on" >> "$export_file"
    echo "set trimout on" >> "$export_file"
    echo "" >> "$export_file"
    echo "spool $project_root/src/toolbox/utplsql/coverage/$package.html" >> "$export_file"
    echo "begin" >> "$export_file"
    echo "ut.run(':all.cc',ut_coverage_html_reporter(), a_include_objects=>ut_varchar2_list('$package'));" >> "$export_file"
    echo "end;" >> "$export_file"
    echo "/" >> "$export_file"
    echo "spool off;" >> "$export_file"
    echo "rollback;" >> "$export_file"
    echo "exit;" >> "$export_file"
    sqlplus "$db_username/$db_password@$db_sqlplusurl" @"generate_coverage.sql"

    rm "$project_root/src/toolbox/utplsql/generate_coverage.sql"

    clear

    result=$(grep -Eo "covered_percent.*(covered_strength)" $project_root/src/toolbox/utplsql/coverage/$package.html | cut -c36-40)
    echo "$package covered for $result%"

    echo -e "\e[32mfile $project_root/src/toolbox/utplsql/coverage/$package.html is created"
    start chrome "$project_root/src/toolbox/utplsql/coverage/$package.html"

else
    echo -e "\e[31mpackage $package was not recognized"
fi
