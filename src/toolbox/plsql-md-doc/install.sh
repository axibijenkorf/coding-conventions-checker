folder=$(git rev-parse --show-toplevel)
folder=$(echo $folder|sed 's/\//\\\\\\\\/g')
echo $folder
cd $(git rev-parse --show-toplevel)
cd ..
git clone https://github.com/OraOpenSource/plsql-md-doc.git
cd plsql-md-doc
npm install
code config.json
cp ../control-center/src/toolbox/plsql-md-doc/config.json config.json -i
sed -i -- "s/{folder}/${folder}/g" config.json
code config.json




