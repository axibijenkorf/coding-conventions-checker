#!/bin/bash

SCHEMA=$1
PASSWORD=$2
URL=$3
FOLDER=$4

cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

if ! [ -n "$1" ]; then
    #initiate global variables from properties files
    . "$project_root/conf/env/initiate_vars.sh" "dev"
    SCHEMA= $dev_db_prj_username
    PASSWORD= $dev_db_prj_password
    URL= $dev_database_sqlplusUrl
else
    SCHEMA=$1
    PASSWORD=$2
    URL=$3
fi

if ! [[ $(pwd) == "*src/test/database" ]]; then
    cd src/test/database
fi

sqlplus "$SCHEMA/$PASSWORD@$URL"<< EOF

            @"run.sql" $FOLDER
            /
            commit;

            exit
EOF