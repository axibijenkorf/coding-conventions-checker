set echo off
set verify off
set trimspool on
set feedback off
set linesize 32767
set pagesize 0
set long 200000000
set longchunksize 1000000
set serveroutput on size unlimited format truncated
set arraysize 50


var sonar_test_reporter_id  varchar2(32);
var sonar_coverage_reporter_id varchar2(32);
var xunit_test_reporter_id		varchar2(32);
var html_coverage_reporter_id  varchar2(32);
var cobertura_coverage_reporter_id  varchar2(32);

declare
  l_reporter  ut_reporter_base;
  l_reporters ut_reporters := ut_reporters();
    v_path ut_varchar2_list := ut_varchar2_list();
    v_sources ut_varchar2_list := ut_varchar2_list();
    v_tests ut_varchar2_list := ut_varchar2_list();
    v_folder varchar2(200) := '&&1';
    v_src_dir varchar2(100) ;
    v_tst_dir varchar2(100) ;
begin
    v_src_dir := 'src/main/database/'||v_folder||'/latest/';
    v_tst_dir  := 'src/test/database/'||v_folder||'/';
    v_path.extend;
        if v_folder is null then
      v_path(v_path.last) := ':all';
    else
      v_path(v_path.last) := ':all.'||v_folder;
    end if;


    for rec in (  select      object_name
                  from        user_objects
                  where       object_type = 'PACKAGE'
                  and         object_name like 'PCK_%')
        loop
        v_sources.extend;
        if v_sources(v_sources.last) is not null then
            v_sources(v_sources.last) := v_sources(v_sources.last) || ',';
        end if;
        v_sources(v_sources.last) := v_src_dir || 'packages/' || lower(rec.object_name) || '.pkb';
    end loop;

   -- v_sources.extend;
   -- v_sources(v_sources.last) := '>C:/Repos/control-center/'|| v_src_dir||'/packages';

    for rec in (  select      object_name
                  from        user_objects
                  where       object_type = 'PACKAGE'
                  and         object_name like 'UT_%' ) loop
        v_tests.extend;
        if v_tests(v_tests.last) is not null then
            v_tests(v_tests.last) := v_tests(v_tests.last) || ',';
        end if;
        v_tests(v_tests.last) := v_tst_dir || 'packages/' || lower(rec.object_name) || '.pkb';
    end loop;

  l_reporter := ut_coverage_html_reporter();
  l_reporters.extend;
  l_reporters(l_reporters.last) := l_reporter;
  :html_coverage_reporter_id := l_reporter.get_reporter_id;

  l_reporter := ut_coverage_sonar_reporter( );
  l_reporters.extend;
  l_reporters(l_reporters.last) := l_reporter;
  :sonar_coverage_reporter_id := l_reporter.get_reporter_id;

  l_reporter := ut_sonar_test_reporter( );
  l_reporters.extend;
  l_reporters(l_reporters.last) := l_reporter;
  :sonar_test_reporter_id := l_reporter.get_reporter_id;

  l_reporter := ut_xunit_reporter();
  l_reporters.extend;
  l_reporters(l_reporters.last) := l_reporter;
  :xunit_test_reporter_id := l_reporter.get_reporter_id;

  l_reporter := ut_coverage_cobertura_reporter();
  l_reporters.extend;
  l_reporters(l_reporters.last) := l_reporter;
  :cobertura_coverage_reporter_id := l_reporter.get_reporter_id;

  ut_runner.run(  a_paths => v_path,
                  a_reporters => l_reporters,
                 -- a_source_files => ut_varchar2_list( 'db/main/latest/PACKAGE_BODIES/pck_employees.pkb' )
			            a_source_file_mappings => ut_file_mapper.build_file_mappings(
											  a_file_paths  => v_sources,
											  a_regex_pattern => '.*(\\|\/)(\w+)(\\|\/)(\w+)\.(\w{3})',
											  a_object_owner_subexpression => 6,
											  a_object_type_subexpression => 2,
											  a_object_name_subexpression => 4,
											  a_file_to_object_type_mapping => ut_key_value_pairs(
													ut_key_value_pair('packages', 'PACKAGE BODY')
											  )
											),
                  --, a_test_files => ut_varchar2_list( 'test/pck_interface_in/test_pck_interface_in.pkb' )
			            a_test_file_mappings => ut_file_mapper.build_file_mappings(
											  a_file_paths  => v_tests,
											  a_regex_pattern => '.*(\\|\/)(\w+)(\\|\/)(\w+)\.(\w{3})',
											  a_object_owner_subexpression => 6,
											  a_object_type_subexpression => 2,
											  a_object_name_subexpression => 4,
											  a_file_to_object_type_mapping => ut_key_value_pairs(
													ut_key_value_pair('packages', 'PACKAGE BODY')
											  )
											)
			   );


  end;
/

set timing off
set termout off
set feedback off
set arraysize 50


spool sonar-coverage.xml
declare
   l_reporter ut_output_reporter_base := ut_coverage_sonar_reporter( );
 begin
    l_reporter.set_reporter_id(:sonar_coverage_reporter_id);
    l_reporter.lines_to_dbms_output(a_initial_timeout=>1, a_timeout_sec=>1);
    dbms_output.put_line(' <coverage version="1">');
    --dbms_output.put_line('<file path="axiautdev.add_flex_column">');
    --dbms_output.put_line('<lineToCover lineNumber="1" covered="false"/>');
    --dbms_output.put_line('<lineToCover lineNumber="2" covered="false"/>');
    --dbms_output.put_line('</file>');
    dbms_output.put_line('</coverage>');

 end;
 /
spool off

spool coverage.html
declare
   l_reporter ut_output_reporter_base := ut_coverage_html_reporter();
 begin
    l_reporter.set_reporter_id(:html_coverage_reporter_id);
    l_reporter.lines_to_dbms_output(a_initial_timeout=>1, a_timeout_sec=>1);
 end;
 /
spool off


spool sonar-test-results.xml
declare
   l_reporter ut_output_reporter_base := ut_sonar_test_reporter( );
 begin
   l_reporter.set_reporter_id(:sonar_test_reporter_id);
   l_reporter.lines_to_dbms_output(a_initial_timeout=>1, a_timeout_sec=>1);
 end;
 /
spool off

spool xunit-test-results.xml
 declare
   l_reporter ut_output_reporter_base := ut_xunit_reporter();
 begin
   l_reporter.set_reporter_id(:xunit_test_reporter_id );
   l_reporter.lines_to_dbms_output(a_initial_timeout=>1, a_timeout_sec=>1);
 end;
 /
spool off

spool cobertura_coverage_report.xml
 declare
   l_reporter ut_output_reporter_base := ut_coverage_cobertura_reporter();
 begin
   l_reporter.set_reporter_id(:cobertura_coverage_reporter_id );
   l_reporter.lines_to_dbms_output(a_initial_timeout=>1, a_timeout_sec=>1);
 end;
 /
spool off

exit
