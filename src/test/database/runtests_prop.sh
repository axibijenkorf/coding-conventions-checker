#!/bin/bash

env=$1
user=$2
cur_dir=$(pwd)
project_root=$(git rev-parse --show-toplevel)

#initiate global variables from properties files
. "$project_root/conf/env/initiate_vars.sh" "$env" "$user"

. "$project_root/src/test/database/runtests.sh" $db_username $db_password $db_sqlplusurl
