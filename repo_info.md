# Template Repository

This is the default repository that should be used for all new projects.
For more information you can contact KMRT, KAER, JSGR

# Prerequisites
## Tools
https://axidev.atlassian.net/wiki/spaces/DIS/pages/1658257540/Chocolatey

1. git (https://git-scm.com/downloads)
1. VS-code (https://code.visualstudio.com/download)
1. maven (npm install maven)
1. sourcetree (https://www.sourcetreeapp.com/)
1. Oracle client 12.2 or higher
1. bitbucket account
1. windows versie 1803 of hoger https://www.microsoft.com/en-us/software-download/windows10
1. Go to microsoft VS Code/code.exe > properties > Compatibility > check "Run this program as an administrator"

## Environoment variables
1. add C:\Program Files\Git\bin to path variable
1. C:\Program Files\Git\cmd to path variable
1. C:\sqlcl\bin to path variable
1. make sure JAVA_HOME variable is referenced with a ~ ex: C:\PROGRA~1\Java\jre1.8.0_181 (necessairy for sqlcl)

# setup vs-code (needs to be done once by each developer)
1. set vscode shortcut for "run task" to ctrl+shift+r. (open shortcuts with ctrl+k ctrl+s)
1. install plugins with task "run VS-code plugin's"

# setup project/repository (if not yet available)
1. ask bitbucket admins to create project for you in bitbucket (KMRT,JVHE,KBLN,TMSN)
1. go to https://bitbucket.org/dashboard/overview > template
1. Fork this repository to your local C:/repo folder.
    1. in bitbucket, click on + icon in left menu and select "Fork this repository"
    1. Select correct project
    1. Enter name of your project as repository
    1. hit fork repository button

# setup local project (needs to be executed by each developer)
1. Open sourcetree and open new tab
    1. click on Remote
    1. search for your repository and hit clone
    1. make sure your local path is set to c:\Repos\[your repository]
    1. hit clone
    1. Run toolbox/shell/implementation/init_repo.sh, this script includes:
            - adding upstream
            - adding master to develop
            - removes unnecessary branches
            - adding stable branche
            - adding integration branch
            - setup GIT flow

# setup local project (needs to be executed by the developer that created the repository)
1. start task initiate_schema using ctr+shift+r
1. ask ITS to execute following scripts (in all your environments (database instances))
    1. src/main/database/dba/create_user_dba.sql
    1. scr/main/database/dba/grant_user_dba_dev.sql
    1. scr/main/database/dba/grant_user_dba.sql

# install maven
https://axidev.atlassian.net/wiki/spaces/DIS/pages/1223065715/Maven
complete all steps
creates files in C:\Users\JSGR\.m2

# apex nitro
1. nodejs (https://nodejs.org/en/download/)
1. apex-nitro (https://github.com/OraOpenSource/apex-nitro)

# Standard repo tasks (ctrl-shift-r)
1. Install VS-code plugin's
    this task will install all VS-code extentions that are recommended.
1. export apex apps
    This task will export the apex application(s) for the given application(s) ID. when no application ID is given, the applications defined in global.properties(apex.apps) will be exported.
1. run oracle
    This task compiles the current file to the given schema / database.
1. (re)initiate schema
    This task will (re)initiate your repo folder for the given folder and database schema.
    1. creates all needed folders for your project/schema
    1. modifies the pom.xml to include your project objects
    1. modifies the code coverage report type so your unit testen will run correctly
1. clone data
    under construction
1. UT generate UT package
    creates a template unit-test package for the given package.
1. UT generate coverage reports
    This task will create some code coverage reports that will be used by the "coverage cutters"-plugin
    after you ran this reports you can visualize your code coverage by going to a certain package and hit "Watch" on the bottom of your screen.
    Note: make sure you ran the task "install VS-code plugin's"
1. UT run
    This task will runs the unit test for the current package
1. UT run all
    this task will run all unit tests
1. export masterdata
    this will export all masterdata as defined in init.js

# references
https://axidev.atlassian.net/wiki/spaces/CC/pages/1246298187/Developer+tools+installation+guide
https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf

## Fork from this repository after your project was setup
git remote add upstream "https://bitbucket.org/Axibssmobile/template.git"
git pull upstream develop --allow-unrelated-histories

# jenkins
## jenkins server
1. change value for environmentPrefix variable in global.properties
1. go to jenkins: http://ab00s019.axi.intra:8080/
1. Click on "New Item"
1. enter an item name (for example: VES-GENESIS)
1. select Multibranch Pipeline -> hit OK
1. enter display name
1. Add source -> Git
1. enter project repository (for example: git@bitucket.org:Axibssmobile/genesis.git)
1. credentials: Jenkins (SSH key jenkins user)
1. discover branches -> select filter by name (with wildcards)
    1. include: develop master release/* integration
    1. exclude: Feature* stable
1. add "Advanced clone behaviour" and enable "Fetch tags"
1. hit save
## bitucket settings for jenkins
1. go to repo in bitbucket
1. Repository settings
    1. Access keys -> add key
        1. label: Jenkins
        1. Key:

        ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCwM+Vw3Xb0c78m1QRlNQaeC7mqi4VYIpp3BSuWYnTHhxL1lEaBWfiT3GLnwQo0yfTyJW+Aa5NUBSgcPxTHC5RhIcuQOFdy0eaSnZgKwgIPzfIVrpUGm4fqF1gS4LtCGYvG552rHotRkMMhiIEv0uDnrdSrVyDCCIQhITNcSmF868KFAi2tsClLVg2fIUTgvTObzpdsI4HavDej8h4NFUwbFlII8Q0hq6n3DbGqjVT1EcpzyosJ/nOWwbaO/tJpR99/Ef6VbgUqcCTCIK5PXnErurOiYeJid5PGpkVeSb7mfFRWx7+LzPRNLr3UFR9niMu6Xkb96UEifSO/ZmVV8Cj5 jenkins@ab00s019

    1. Webhooks
        1. in jenkins go to project, right click on "Scan Multibranche Pipeline Now"
                1.  click on "link openen in nieuw tabblad"
                1. copy "/job..." in clipboard
        1. in bitbucket -> Add webhook
        1. Title: Jenkins
        1. URL: https://admin:111e781d19562c8331db1d9a4711512788@jenkinsdsc.axi.be/[copied_part]


## bitucket settings for jenkins
1. repository settings
    1. Webhooks -> Add webhook
        1. Title: Jenkins
        1. URL: https://admin:111e781d19562c8331db1d9a4711512788@jenkinsdsc.axi.be/job/[environmentPrefix]/build

## teams settings for jenkins
1. channel aanmaken in teams
1. right click on connectors
1. Jenkins -> configured
1. enter project specific name for jenkins connection.
1. copy generated string
1. go to jenkins project / credentials -> Folder -> Global credentials -> Add Credentials
1. kind: Secret text
1. Secret: [generated_url]
1. id: office365Connector.webhookUrl
1. Description: [generated_url]

references:
https://axidev.atlassian.net/wiki/spaces/AEPD/pages/1471283209/Customer+implementation

## git LFS (Large file storage)
1. download LFS from https://git-lfs.github.com/
1. run git lfs install    (in your repo cmd)
1. run git lfs track *.fmb
1. run git add .gitattributes

## Vagrant
1. Vagrant installeren (S:\Tools\Software\Vagrant\Software\vagrant_2.2.5_x86_64.msi)
1. VirtualBox installeren (S:\Tools\Software\Vagrant\Software\VirtualBox-6.0.6-130049-Win.exe)
[Setup clean vagrant machine](src/toolbox/vagrant/clean/readme.md)
[Setup latest vagrant machine](src/toolbox/vagrant/latest/readme.md)

## Repo releasing
all the scripts/info can be found in the [shell/release/](src/toolbox/shell/release/readme.md) folder

## sonarlint (code analyses)
https://axidev.atlassian.net/wiki/spaces/DIS/pages/965968031/SonarCloud#Sonarlint-in-VS-Code