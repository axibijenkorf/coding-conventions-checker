#env=$1
project_root=$(git rev-parse --show-toplevel)
#declare -a environments=("dev" "test" "prd" "global" "build" "local" "acc" "ref")
cur_dir=$(pwd)

#check if environment is ok
if [[ ! -z "$env" ]] &&  [[ ! -f "${project_root}/conf/env/${env,,}.properties" ]]; then
    echo "invalid environment: $env"
    return 1
fi

if [[ ! -z "$env" ]] ; then #&& [[ " ${environments[@]} " =~ " ${env,,} " ]];
    file="$project_root/conf/env/generated/${env,,}sh.properties"
    
    if [ -f "$file" ]
    then
        while IFS='=' read -r key value
        do
            if [ ${#key} -gt 1 ]
            then
                unset ${key,,}
            fi
        done < "$file"
    else
    echo "$file not found."
    fi
else
    cd "${project_root}/conf/env/"
    for f in *.properties; do
        gen_env=$f
        gen_env=${f/".properties"/}
        gen_env=${gen_env/"${project_root}/conf/env/"/}
        file="$project_root/conf/env/generated/${gen_env,,}sh.properties"

        if [ -f "$file" ]
        then
                while IFS='=' read -r key value
                do
                    if [ ${#key} -gt 1 ]
                    then
                        unset ${key,,}
                    fi
                done < "$file"
        else
        echo "$file not found."
        fi
    done
fi

cd "$cur_dir"