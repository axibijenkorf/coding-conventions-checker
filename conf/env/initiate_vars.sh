
env=$1
user=""
user=$2
project_root=$(git rev-parse --show-toplevel)
#declare -a environments=("dev" "test" "prd" "global" "build" "local" "acc" "ref")
unset f
cur_dir=$(pwd)

#check if environment is ok
if [[ ! -z "$env" ]] &&  [[ ! -f "${project_root}/conf/env/${env,,}.properties" ]]; then
    echo "invalid environment: $env"
    return 1
fi

#generate files
"${project_root}/conf/env/generate_files.sh"

source "${project_root}/conf/env/generated/globalsh.properties"
if [ -z "$user" ] ;
then
    user=$scripts_default_db_username
fi

# use it as source
if [[ ! -z "$env" ]] ; then #&& [[ " ${environments[@]} " =~ " ${env,,} " ]];
    #if env is given load only env properties file
    source "${project_root}/conf/env/generated/${env,,}sh.properties"
    db_username=${env,,}'_db_'$user'_username'
    db_password=${env,,}'_db_'$user'_password'
    declare ${env,,}'_db_username'=${!db_username}
    declare ${env,,}'_db_password'=${!db_username}
else
    #if no env is given load properties for all environments
    #for gen_env in ${environments[@]}; do
    cd "${project_root}/conf/env/"
    for f in *.properties; do
        gen_env=$f
        gen_env=${f/".properties"/}
        gen_env=${gen_env/"${project_root}/conf/env/"/}
        file="${project_root}/conf/env/generated/${gen_env,,}sh.properties"
        if [ -f "$file" ]
        then
            source "${project_root}/conf/env/generated/${gen_env,,}sh.properties"
            db_username=${gen_env,,}'_db_'$user'_username'
            db_password=${gen_env,,}'_db_'$user'_password'
            declare ${gen_env,,}'_db_username'=${!db_username}
            declare ${gen_env,,}'_db_password'=${!db_username}
        else
            echo "${gen_env,,}.properties file does not exists"
        fi
    done
fi


#initiate db_username
if [ ! -z "$user" ]
then
    db_username='db_'$user'_username'
    db_password='db_'$user'_password'

    db_username=${!db_username}
    db_password=${!db_password}
fi

#initiate colors
RED=$'\e[31m'
GREEN=$'\e[32m'
RESET=$'\e[0m'
YELLOW=$'\e[33m'

cd "$cur_dir"