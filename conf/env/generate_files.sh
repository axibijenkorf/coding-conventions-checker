# generate properties files
project_root=$(git rev-parse --show-toplevel)
declare -a environments=("dev" "test" "prd" "global" "build" "local" "acc" "ref")
force=''
unset f
cur_dir=$(pwd)

show_help()
{
echo " Generate properties files that can be used in shell scripts as source

        Usage: generate_files.sh [-f] [-h]
        -f              Force recreate of files
        -h              Help

        For Example: ./generate_files.sh -f
"
}

#get arguments
OPTIND=1
while getopts "f?h" arg; do
  case $arg in
    f)
      force='J'
      ;;
    h) # help
            show_help
            return
            ;;
    \?)
            echo "Invalid option: -$OPTARG. Use -h flag for help."
            return
            ;;
  esac
done

mkdir -p "${project_root}/conf/env/generated/"

# Iterate the string array using for loop
cd "${project_root}/conf/env/"
for f in *.properties; do
    env=${f/".properties"/}
    env=${env/"${project_root}/conf/env/"/}
    file="${project_root}/conf/env/${env,,}.properties"
    conv_file="${project_root}/conf/env/generated/${env,,}sh.properties"
    gen_content=""

    if [ ! -z $force ]; then
        rm -f "$conv_file"
    fi

    if [ -f "$file" ]
    then
        #only generate when file is older or does not exist
        if [ "$conv_file" -ot "$file" ]; then
             rm -f "$conv_file"
            while IFS='=' read -r key value
            do

                if [ ${#key} -gt 1 ] && [[ ! $key =~ ^#.* ]]
                then
                    key=${key,,}
                    key=${key//./_}
                    key=${key//-/_}

                    echo "$key=$value" >> "$conv_file"
                    echo ${env,,}'_'$key'='$value >> "$conv_file"
                fi
            done < "$file"

            echo "$file regenerated."
        fi
    else
    echo "$file not found."
    fi
done

cd "$cur_dir"
