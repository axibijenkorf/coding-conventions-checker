# Template Repository
General repo info can be found in [repo_info](repo_info.md)

# Coding conventions checker
De applicatie is op te starten via de alias : AXI_CCC

Voorbeeld:
http://172.30.0.241:8080/ords/f?p=AXI_CCC

Wanneer je de builder hebt openstaan komt de taalkeuze region tevoorschijn.
Beschikbare talen:
- Afrikaans -> develop taal
- Engels -> eindgebruiker taal

Wanneer je als eindgebruiker inlogt, zonder de builder te hebben openstaan, kom je standaard in de Engelse versie.

Opgelet:
Wanneer je de taalkeuze region ziet zullen beide opties als "Engels" weergegeven zijn, dit is een issue van APEX. Na het selecteren van één van de opties zal je achteraan de URL de p_lang parameter zien met de correcte taalcode.

## Installatie
Installeren kan door een fork te nemen van de standaard CCC repository in BitBucket

https://bitbucket.org/Axibssmobile/coding-conventions-checker/src

Vervolgens dien je de gegevens voor je gewenste omgeving aan te passen in de conf/env/ map.

De devadm gebruiker wordt gebruikt om de installatie uit te voeren.
Deze kan aangemaakt worden met de scripts "create_user_dba.sql" en "grant_user_dba.sql" in de folder
> src\main\database\dba

Van zodra de properties zijn aangepast, kan je de build starten door het commando:

> maven install -P "environment"

In het installatieproces gaan we er van uit dat er reeds een bestaande APEX workspace ingegeven wordt met de correcte workspace ID.
We maken geen nieuwe workspace aan.
We voegen het CCC schema wel toe tot de schema's van deze workspace.

## Aan de slag
De menu is opgebouwd zodat je de componenten in de correcte volgorde (van boven naar beneden) kan aanmaken.

We leveren ook al enkele zaken aan zoals APEX versies, gradaties en types.

In de eerste versie zullen er nog geen default rules aangeleverd worden.

## Gebruikers
De gebruikers waarmee je kan inloggen in de APEX applicatie zijn dezelfde als deze van de APEX builder.

Hier zit verder geen autorisatie op.